include <BrLib.scad>
include <utils.scad>

type="top";
rotate(180,[1,0,0])023_6x4_power_supply_12_to_24V(type);

remove_z = 16;
remove_y = 4 * Br_Unit;
remove_x = 68+7;
//rotate(180,[1,0,0])005R2_4x4_4xAA_power_supply(topBot="top");


module 023_6x4_power_supply_12_to_24V(typeFnc=type)
{
    if(typeFnc=="top")     023_6x4_power_supply_12_to_24V_topBot(topBot="top");
    if(typeFnc=="bot")     023_6x4_power_supply_12_to_24V_topBot(topBot="bot");
}

005_lidX = 64-1.5+5+1;
005_lidy = 58-1.5;

module removeContacts()
{
    // Contact
    translate([0,12,-7])cube([100, 1.8, 5], center=true);
    translate([0,-12,-7])cube([100, 1.8, 5], center=true);
    
    translate([17,12,-52])cube([13, 1.2, 100], center=true);
    translate([17,-12,-52])cube([13, 1.2, 100], center=true);
}

module lidlBatteryPAP_20_A1()
{
    x = remove_x;
    y = remove_y;
    z = remove_z;
    P = 0.5+0.2;
    
    difference()
    {
        union()
        {
            translate([5+1-3.5,0,-z/2])cube([x, y, z], center=true);
        }
        
        union()
        {
            translate([-55-7,-55,0])rotate(40, [0,0,1])cube([100, 100, 100], center=true);
            translate([-55-7,55,0])rotate(-40, [0,0,1])cube([100, 100, 100], center=true);
            
            translate([0, -y/2, 0])rotate(90, [0,1,0])edgeRounder(x+10+Br_Magic, Br_RoundRadius);
            translate([0, y/2, 0])rotate(-90,[1,0,0])rotate(90, [0,1,0])edgeRounder(x+10+Br_Magic, Br_RoundRadius);
            
            translate([-35, 0, -remove_z/2-2])rotate(-90,[1,0,0])edgeRounder(y+10+Br_Magic, Br_RoundRadius);
            
            difference()
            {
                z1 = 4.5+P;
                x1 = 70;
                y1 = 46;
                z2 = 10 - z1;
                x2 = 47;
                Bz = 5;
                Bx = 7;
                
                union()
                {
                    removeContacts();
                    
                    translate([0,0,-z1/2-z2])union()
                    {
                        
                        cube([x1, y1+P, z1], center=true);
                        translate([(x1-x2)/2,0,z1/2+z2/2])cube([x2, 38+P, z2+Br_Magic], center=true);
                        translate([-2*(x1-x2)/2,0,z1/2+z2/2])cube([x1-x2, y1+P, z2+Br_Magic], center=true);
                        translate([-2*(x1-x2)/2-P,0,z1/2+z2/2])cube([x1-x2, y1+P, z2], center=true);
                        translate([(x1-Bx)/2-P/2-(58-Bx)-7,0,-z1/2-Bz/2])cube([Bx+P, 24.5+P, Bz], center=true);
                        
                        // Remove a bit more
                        translate([-2*(x1-x2)/2-P,0,z1/2+z2/2-z1/2])cube([x1-x2, y1+P+100, z2+z1+Br_Magic], center=true);
                    }
                }
            }
        }
    }
    
    // contacts
    difference()
    {
        union()
        {
            translate([28,0,-z/2])cube([15-P, 31-P, z], center=true);
        }
        union()
        {
            removeContacts();
        }
    }
}

// Make batteries
module 005R2_batteryBox()
{
    z = 16;
    translate([-8,0,z/2-(Br_BottomHeight-Br_WallThickenss)+4]) rotate(0, [0,0,1])
        cube([64, 58, z], center=true);
}

module batterySymbol(offset)
{
    x = 25;
    y = 10;
    z = 0.5;
    W = 1;
    KNOB_X = 2;
    KNOB_Y = 3;
    translate([-5,0,offset-z/2+Br_Magic])
    {
        
        translate([x/2+KNOB_X/2,0,0])
        {
            cube([KNOB_X,KNOB_Y,z], center=true);
        }
        
        difference()
        {
            cube([x,y,z], center=true);
            cube([x-W,y-W,z+Br_Magic], center=true);
        }
    }
}

module 023_top(unitsX=4, unitsY=4, unitsZ=3, con, mountingHoles, pcbSize)
{
    x = unitsX*Br_Unit - 1*Br_SFRed;
    y = unitsY*Br_Unit;
    z = Br_HeightUnit*unitsZ - Br_BottomTotalHeight;
    W_T = Br_WallThickenss*1.8;
    
    difference()
    {
        union()
        {
            translate([-Br_SFRed/2,0,0]) 
            {   
                makeEmptyBox(x, y, z, wallThickness=W_T);
            }
            // Make stronger walls around connectors
            Br_topMakeConnectorAdding(con);
            Br_topMountingHolesAdding(mountingHoles, x,y,z);
            
            // Make box top alignment
            translate([-Br_SFRed/2,0,0]) Br_topBoxAlignment(x, y, z);
            
            // Pcb alignment
            xWidth = 6;
            yWidth = 3;
            translate([32.5+xWidth/2,0,z/2-0.1]) 
            {   
                translate([1,23+1+5,0])  cube([xWidth, yWidth, z], center=true);
                translate([1,-24.5-5,0])  cube([xWidth, yWidth, z], center=true);
            }
            
            // Print support..
            translate([-x/2+1,0,z-3.2]) 
            {
                //cube([6.5, y, 0.3], center=true);
            }
            
            difference()
            {
                union()
                {
                    translate([38.5-1-7.5,10-30+3,-1+Br_HeightUnit])lightGuideAdd();
                    translate([38.5-1-7.5,17-30+3,-1+Br_HeightUnit])lightGuideAdd();
                }
                translate([31,-30,5-1])cube([100,100,10]);
            }
            
            translate([-(x/2-remove_x/2)-3.5,0,z-remove_z/2-3])cube([remove_x-1,remove_y-1,remove_z+Br_Magic], center=true);
            
            // Add some material around switch
            translate([-30-7,-32,0]) cube([17.8+15,5,6.5+3]);
        }
        union()
        {
            // Remove place for connectors
            Br_topMakeConnectorRemoving(con); 
            //005R2_topLid();         
            translate([-0.5,0,0])023_PCB();
            
            //translate([-27,0,0])cube([100,100,100], center=true);
            
            // Remove space for cables
            YY = 1.5;
            ZZ = 5;
            XX = 24;
            //translate([14,y/2-W_T+YY/2,ZZ/2])  cube([XX+Br_Magic, YY+Br_Magic, ZZ+Br_Magic], center=true);
            
            // Space for plug
            translate([-(x/2-remove_x/2)-3.5,0,z-remove_z/2])cube([remove_x,remove_y,remove_z+Br_Magic], center=true);
            translate([-(x/2-remove_x/2)-5.5,0,-80])rotate(180,[1,0,0])removeContacts();
            
            // Remove hole for switch
            translate([-30,-40,0]) cube([17.8,25,6.5]);
        }
    }
    
    translate([-(x/2-remove_x/2)-5.5,0,z])lidlBatteryPAP_20_A1();
}

//==============================
// PCB module
module 023_PCB(thickerPcb = false)
{
    pcbPlay = (thickerPcb)? 1.0 : 0.7;
    x = 1.6+pcbPlay;
    y = 47+12 + pcbPlay;
    
    extraZOnBottom = 0;
    z = 19+1.5+extraZOnBottom;
    PCB_offset = 9;
    translate([25.9,0,0])
    {
        connectors = [ [PCB_offset+Br_con_c_thickness/2, 0, "left"]];
        
        // pcb close to batteries
        //translate([0,0,z/2-(Br_BottomHeight-Br_WallThickenss)]) rotate(0, [0,0,1])
        //    cube([x, y, z], center=true);
        
        //pcb closer to connector
        translate([PCB_offset,0,z/2-(Br_BottomHeight-Br_WallThickenss)-extraZOnBottom-0.5]) rotate(0, [0,0,1])
            cube([x, y, z], center=true);
        
        // switch
        sw_x = 7;
        sw_y = 13;
        sw_z = 10;
        translate([4.9-1.2,-14.5+30,10])
        {
            cube([sw_x,sw_y,sw_z], center=true);
            // Make bump
            x_play = 1.0;
            y_play = 2.0;
            bx = 3.7 + x_play;
            by = 3.8 + 2.2 + y_play;
            bz = 4.65;
            translate([0,0,sw_z/2+bz/2])
            {
                cube([bx,by,bz], center=true);
            }
        }
        
        // leds
        diameter = 3.7;
        translate([11.5-7,10.0-30+3,13.1+Br_HeightUnit])
        {
            //cylinder($fn=Br_outerFn, r = diameter/2, h = 5, center = true);
            lightGuideRemove();
        }
        translate([11.5-7,17.0-30+3,13.1+Br_HeightUnit])
        {
            //cylinder($fn=Br_outerFn, r = diameter/2, h = 5, center = true);
            lightGuideRemove();
        }
        
        Br_topMakeConnectorRemoving(connectors, removeBoxAlignment=false);
        
        // USB connector
        translate([6.5,-26-3,4-1])
        {
            //cube([3.5,5+2+5,8+1], center=true);
        }
        
        // Some components on PCB
        translate([6.5,0,0])
        {
            cube([3,y,7], center=true);
        }
    }
}

//==============================
// Remove some material to make holes
// Mounting holes - in the format [x, y, top/bot, right/left]
module 023_Br_botMountingHolesRemove(mountingHoles)
{
    deeper = 0.7 * 2;
    
    for (i = [0:len(mountingHoles)-1])
    {   // Make stronger walls around connectors
       translate([mountingHoles[i][0],mountingHoles[i][1],-Br_BottomHeight/2])
       {
           h = Br_BottomHeight*2 + 2*Br_BotHeightOfBottomPcbSupport + Br_Magic; // Make it through all -
           cylinder($fn=Br_outerFn,h=h,d=Br_BotMountingHoleMinDia,center=true);
            
           offset = -(Br_BottomHeight/2-Br_BotMountingHoleChampferHeight/2-deeper/2); // -Br_BotMountingHoleChampferHeight/2
           // Make champfer for a screw head
           translate([0,0, offset]) // -Br_BotMountingHoleChampferHeight/2
           // Make ])
           { 
                cylinder($fn=Br_outerFn,h=Br_BotMountingHoleChampferHeight+Br_Magic,d1=Br_BotMountingHoleMaxDia, d2=Br_BotMountingHoleMinDia,center=true); 
           }
           
           offset2 = -(Br_BottomHeight/2); // -Br_BotMountingHoleChampferHeight/2
           translate([0,0, offset2]) // -Br_BotMountingHoleChampferHeight/2
           // Make ])
           { 
               cylinder($fn=Br_outerFn,h=deeper,d=Br_BotMountingHoleMaxDia,center=true); 
           }
       } 
    }
}

//==============================
// Br_bot - make bot box - on the left side - there is always male connector
// unitsX,unitsY - units in lego units
// Mounting holes - in the format [x, y, top/bot, right/left]
module 023_bot(unitsX=4, unitsY=4, mountingHoles)
{
    x = unitsX*Br_Unit - 1*Br_SFRed;
    y = unitsY*Br_Unit;
    z = Br_BottomHeight;
    
    
    difference()
    {
        union()
        {
            rotate(180, [1,0,0])
            {
                translate([-Br_SFRed/2,0,0])
                    makeEmptyBox(x, y, z, wallThickness=Br_WallThickenss, bottomThickness=Br_WallThickenss);
            }
            
            // Make bottom stubs
            Br_botMakeBottomStubs(unitsX, unitsY);
            
            // Add material around holes
            Br_botMountingHolesAdding(mountingHoles);
            
            // Add ribs
            zz = 4;
            T = 1.5;
            
            xReduction = 7+5;
            translate([-Br_SFRed/2-xReduction/2,0,-zz/2])
            {
                translate([0,10,0])cube([x-xReduction, T, zz], center=true);
                translate([0,-10,0])cube([x-xReduction, T, zz], center=true);
            }
            translate([0,0,-zz/2])
            {
                translate([-30,0,0])cube([T, y, zz], center=true);
                translate([-10,0,0])cube([T, y, zz], center=true);
                translate([10,0,0])cube([T, y, zz], center=true);
            }
            
            // Add material around PCB
            translate([34.7-0.5,0,-4])cube([5, y-2, 1], center=true);
            translate([35.5,-24.25,-z/2])cube([10,1,z],center=true);
            translate([35.5,24.25,-z/2])cube([10,1,z],center=true);
        }
        
        // Remove these parts
        union()
        {
            023_Br_botMountingHolesRemove(mountingHoles);
            
            // Remove some space for box alignment..
            translate([-Br_SFRed/2,0,0]) Br_topBoxAlignment(x,y,z, true);
            
            translate([-0.5,0,0.0])023_PCB(thickerPcb=true);
            005R2_batteryBox();
        }
    }
    
    //Br_botMountingHolesAdding_postprocessing_easier_3dPrint(mountingHoles);
}

//==============================
// Create parts as required
module 023_6x4_power_supply_12_to_24V_topBot(topBot="top")
{   
    unitsX = 6;
    unitsY = 4;
    unitsZ = 4;
    
    connectors =[[unitsX*Br_Unit/2-Br_SFRed, 0,  "right"]];   // Female connector - right
    
    mh = [  [Br_Unit*1.9-0.5-2, Br_Unit*1.6, "top", "--"],
            [-Br_Unit*2.7+0.7, Br_Unit*1.3, "top", "right"],
            [Br_Unit*1.9-0.5-2, -Br_Unit*1.6, "bot", "--"],
            [-Br_Unit*2.7+0.7, -Br_Unit*1.3, "bot", "right"]]; 
    
    if(topBot == "top")
    {
        023_top(unitsX=unitsX, unitsY=unitsY, unitsZ=unitsZ, con=connectors, mountingHoles=mh, pcbSize=Generic_pcbSize);
    }
    else
    {
        023_bot(unitsX=unitsX, unitsY=unitsY, mountingHoles=mh);
    }
}




