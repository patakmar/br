include <BrLib.scad>

type="topWithExtarnalCable";
rotate(180,[1,0,0])005_create(type);

module 005_create(type=type)
{
    if(type=="top")     005_4x4_4xAA_power_supply(topBot="top");
    if(type=="bot")     005_4x4_4xAA_power_supply(topBot="bot");
    if(type=="topLid")  005_topLid();
    if(type=="AssmeblyJig")005_PcbAssembpyJig();
    if(type=="topWithExtarnalCable")
    {    
     difference()
     {   
        005_4x4_4xAA_power_supply(topBot="top");
        translate([-45,15,12.7-0.3])rotate(90, [0,1,0])cylinder($fn=Br_outerFn, r = 2, h = 10, center = true);
     }
    }
    if(type=="washerForExtarnalCable")
    {
        cube([17.5,17.5,19]);
    }
}

005_lidX = 64-1.5+5+1;
005_lidy = 58-1.5;

// Make batteries
module 005_batteryBox()
{
    z = 16;
    translate([-8,0,z/2-(Br_BottomHeight-Br_WallThickenss)+4]) rotate(0, [0,0,1])
        cube([64, 58, z], center=true);
}

module 005_top(unitsX=4, unitsY=4, unitsZ=3, con, mountingHoles, pcbSize)
{
    x = unitsX*Br_Unit - 1*Br_SFRed;
    y = unitsY*Br_Unit;
    z = Br_HeightUnit*unitsZ - Br_BottomTotalHeight;
    
    difference()
    {
        union()
        {
            translate([-Br_SFRed/2,0,0]) 
            {   
                makeEmptyBox(x, y, z, wallThickness=Br_WallThickenss*1.5);
            }
            // Make stronger walls around connectors
            Br_topMakeConnectorAdding(con);
            Br_topMountingHolesAdding(mountingHoles, x,y,z);
            
            // Make box top alignment
            translate([-Br_SFRed/2,0,0]) Br_topBoxAlignment(x, y, z);
            
            // Pcb alignment
            xWidth = 5;
            yWidth = 4;
            translate([32.5+xWidth/2,0,z/2]) 
            {   
                translate([0,23,0])  cube([xWidth, yWidth, z], center=true);
                translate([0,-24.5,0])  cube([xWidth, yWidth, z], center=true);
            }
            
            // Print support..
            translate([-x/2+1,0,z-3.2]) 
            {
                //cube([6.5, y, 0.3], center=true);
            }
        }
        union()
        {
            // Remove place for connectors
            Br_topMakeConnectorRemoving(con); 
            005_topLid();         
            005_PCB();
            
            // Open the space from where we could remove batteries - from tom
            translate([-13.5,0,17.6-Br_WallThickenss/2])
            {
                difference()
                {
                    cube([005_lidX+0.5, 005_lidy, Br_WallThickenss*2+0.1+Br_Magic], center=true);
                
                    union()
                    {
                        xx = Br_WallThickenss+1.0;
                        hh = 10;
                        translate([-34.5+xx/2,0, 0])
                        { 
                            // Make a hole in the middle ..
                            cube([xx,20+0.5,hh], center=true);
                        }
                        
                        // tooth
                        translate([35,0,0]) 
                        {
                           cube([4, 3, 100], center=true);
                        }
                    }
                }
            } 
        }
    }
}

//==============================
// PCB module
module 005_PCB()
{
    pcbPlay = 1.2;
    x = 1.6+pcbPlay;
    y = 47 + pcbPlay;
    z = 19+1.5;
    PCB_offset = 9;
    translate([25.9,0,0])
    {
        connectors = [ [PCB_offset+Br_con_c_thickness/2, 0, "left"]];
        
        // pcb close to batteries
        translate([0,0,z/2-(Br_BottomHeight-Br_WallThickenss)]) rotate(0, [0,0,1])
            cube([x, y, z], center=true);
        
        //pcb closer to connector
        translate([PCB_offset,0,z/2-(Br_BottomHeight-Br_WallThickenss)]) rotate(0, [0,0,1])
            cube([x, y, z], center=true);
        
        // switch
        sw_x = 7;
        sw_y = 13;
        sw_z = 10;
        translate([4.9,-14.5,10])
        {
            cube([sw_x,sw_y,sw_z], center=true);
            // Make bump
            x_play = 1.0;
            y_play = 2.0;
            bx = 3.7 + x_play;
            by = 3.8 + 2.2 + y_play;
            bz = 4.65;
            translate([0,0,sw_z/2+bz/2])
            {
                cube([bx,by,bz], center=true);
            }
        }
        
        // leds
        diameter = 3.7;
        translate([9,10.0,10+5+2.5])
        {
            cylinder($fn=Br_outerFn, r = diameter/2, h = 5, center = true);
        }
        translate([9,17.0,10+5+2.5])
        {
            cylinder($fn=Br_outerFn, r = diameter/2, h = 5, center = true);
        }
        
        Br_topMakeConnectorRemoving(connectors);
        
    }
}

//==============================
// Remove some material to make holes
// Mounting holes - in the format [x, y, top/bot, right/left]
module 005_Br_botMountingHolesRemove(mountingHoles)
{
    deeper = 0.7 * 2;
    
    for (i = [0:len(mountingHoles)-1])
    {   // Make stronger walls around connectors
       translate([mountingHoles[i][0],mountingHoles[i][1],-Br_BottomHeight/2])
       {
           h = Br_BottomHeight*2 + 2*Br_BotHeightOfBottomPcbSupport + Br_Magic; // Make it through all -
           cylinder($fn=Br_outerFn,h=h,d=Br_BotMountingHoleMinDia,center=true);
            
           offset = -(Br_BottomHeight/2-Br_BotMountingHoleChampferHeight/2-deeper/2); // -Br_BotMountingHoleChampferHeight/2
           // Make champfer for a screw head
           translate([0,0, offset]) // -Br_BotMountingHoleChampferHeight/2
           // Make ])
           { 
                cylinder($fn=Br_outerFn,h=Br_BotMountingHoleChampferHeight+Br_Magic,d1=Br_BotMountingHoleMaxDia, d2=Br_BotMountingHoleMinDia,center=true); 
           }
           
           offset2 = -(Br_BottomHeight/2); // -Br_BotMountingHoleChampferHeight/2
           translate([0,0, offset2]) // -Br_BotMountingHoleChampferHeight/2
           // Make ])
           { 
               cylinder($fn=Br_outerFn,h=deeper,d=Br_BotMountingHoleMaxDia,center=true); 
           }
       } 
    }
}

//==============================
// Br_bot - make bot box - on the left side - there is always male connector
// unitsX,unitsY - units in lego units
// Mounting holes - in the format [x, y, top/bot, right/left]
module 005_bot(unitsX=4, unitsY=4, mountingHoles)
{
    x = unitsX*Br_Unit - 1*Br_SFRed;
    y = unitsY*Br_Unit;
    z = Br_BottomHeight;
    
    
    difference()
    {
        union()
        {
            rotate(180, [1,0,0])
            {
                translate([-Br_SFRed/2,0,0])
                    makeEmptyBox(x, y, z, wallThickness=Br_WallThickenss, bottomThickness=Br_WallThickenss);
            }
            
            // Make bottom stubs
            Br_botMakeBottomStubs(unitsX, unitsY);
            
            // Add material around holes
            Br_botMountingHolesAdding(mountingHoles);
            
            // Add ribs
            zz = 4;
            T = 1.5;
            
            xReduction = 20;
            translate([-Br_SFRed/2-xReduction/2,0,-zz/2])
            {
                translate([0,10,0])cube([x-xReduction, T, zz], center=true);
                translate([0,-10,0])cube([x-xReduction, T, zz], center=true);
            }
            translate([0,0,-zz/2])
            {
                translate([-30,0,0])cube([T, y, zz], center=true);
                translate([-10,0,0])cube([T, y, zz], center=true);
                translate([10,0,0])cube([T, y, zz], center=true);
            }
            
        }
        
        // Remove these parts
        union()
        {
            005_Br_botMountingHolesRemove(mountingHoles);
            
            // Remove some space for box alignment..
            translate([-Br_SFRed/2,0,0]) Br_topBoxAlignment(x,y,z, true);
            
            005_PCB();
            005_batteryBox();
            
            // Remove some space for a cable 
            H = 1.5;
            translate([-Br_SFRed/2,-25,-H/2])
            {
                cube([x-25, 3, H], center=true);
            }
        }
    }
    
    //Br_botMountingHolesAdding_postprocessing_easier_3dPrint(mountingHoles);
}

//==============================
// Create parts as required
module 005_4x4_4xAA_power_supply(topBot="top")
{   
    unitsX = 6;
    unitsY = 4;
    unitsZ = 3;
    
    connectors =[[unitsX*Br_Unit/2-Br_SFRed, 0,  "right"]];   // Female connector - right
    
    mh = [  [Br_Unit*1.9, Br_Unit*1.6, "top", "left"],
            [-Br_Unit*2.7, Br_Unit*1.3, "top", "right"],
            [Br_Unit*1.9, -Br_Unit*1.6, "bot", "left"],
            [-Br_Unit*2.7, -Br_Unit*1.3, "bot", "right"]]; 
    
    if(topBot == "top")
    {
        difference()
        {
            union()
            {
                005_top(unitsX=unitsX, unitsY=unitsY, unitsZ=unitsZ, con=connectors, mountingHoles=mh, pcbSize=Generic_pcbSize);
            }
            
            union()
            {
               
            }
        }
    }
    else
    {
        005_bot(unitsX=unitsX, unitsY=unitsY, mountingHoles=mh);
    }
}

//==============================
// Make top lid to cover batteries
module 005_topLid()
{
    lidPlay = 0.5;
    // Open the space from where we could remove batteries - from tom
    translate([-8-5-0.8,0,17.5-Br_WallThickenss/2])
    {
        difference()
        {
            xx = 005_lidX-lidPlay+0.3;
            union()
            {
                cube([xx, 005_lidy-lidPlay, Br_WallThickenss*2], center=true);
        
                xPlugWidth = 2.2*Br_WallThickenss;
                translate([(005_lidX-lidPlay)/2,0,-Br_WallThickenss/2])
                {
                    cube([xPlugWidth, 005_lidy-lidPlay, Br_WallThickenss], center=true);
                }
            }
            
            union()
            {
                // make mounting holes
                translate([-29.4,0, 0]) // -Br_BotMountingHoleChampferHeight/2
                // Make ])
                { 
                    translate([0,Br_Unit*1.3,0])
                    {
                        cylinder($fn=Br_outerFn,h=100,d=Br_BotMountingHoleMinDia,center=true);
                        translate([0,0,0.5])cylinder($fn=Br_outerFn, h=Br_BotMountingHoleChampferHeight+Br_Magic, d2=Br_BotMountingHoleMaxDia, d1=Br_BotMountingHoleMinDia, center=true); 
                    } 
                    translate([0,-Br_Unit*1.3,0])
                    {
                        cylinder($fn=Br_outerFn,h=100,d=Br_BotMountingHoleMinDia,center=true); 
                        translate([0,0,0.5])cylinder($fn=Br_outerFn, h=Br_BotMountingHoleChampferHeight+Br_Magic, d2=Br_BotMountingHoleMaxDia, d1=Br_BotMountingHoleMinDia, center=true); 
                    }
                }
                
                translate([-34-0.2,0,Br_WallThickenss])
                rotate(90,[ 0,1,0])
                rotate(90, [1,0,0])
                edgeRounder(80, Br_RoundRadius);
                
                xx = Br_WallThickenss+1.2;
                hh = 10;
                translate([-34.2+xx/2,0, 0])
                { 
                    // Make a hole in the middle ..
                    cube([xx,20+1,hh], center=true);
                }
                
                 // tooth
                translate([35,0,0]) 
                {
                   cube([4, 4, 100], center=true);
                }
                
                // Remove some space so that batteries fits in better
                xL = 11;
                yL = 54;
                zL = 1;
                battX = 16;
                translate([0,0,-Br_WallThickenss+zL/2])
                {
                    translate([27-0*battX,0,0]) cube([xL, yL, zL+Br_Magic], center=true);
                    translate([27-1*battX,0,0]) cube([xL, yL, zL+Br_Magic], center=true);
                    translate([27-2*battX,0,0]) cube([xL, yL, zL+Br_Magic], center=true);
                    translate([27-3*battX,0,0]) cube([xL, yL, zL+Br_Magic], center=true);
                }
            }
        }
    }
    
    // lock on one side
}

//==============================
// Make top lid to cover batteries
module 005_PcbAssembpyJig()
{
    x = 30;
    y = 10;
    z = 30;
    
    difference()
    {
        union()
        {
            translate([30,26,0]) cube([x, y, z], center=true);  
            translate([30,-26,0]) cube([x, y, z], center=true);  
            translate([30,0,-z/2+2]) cube([x, 52, 4], center=true);  
        }
        // Remove the PCB
        005_PCB();
    }
}

