include <BrLib.scad>

MICRO_SW_HOLE = 8;
LIGHT_GUIDE_HOLE = 4-0.5;

type = "lightGuide3Z";
rotate(0,[1,0,0])microSwitchCreate(type);

module microSwitchCreate(type)
{
    if(type == "button3Z")
    {
        microSwithchButton(3);
    }
    else if (type == "lightGuide3Z")
    {
        rotate(-90,[1,0,0])lightGuide(3);
    }
    else if(type == "potDistance")
    {
        potentiometerDistance();
    }
    else if(type == "pcbDistance")
    {
        rotate(-180,[0,1,0])Br_pcbSupportTube();
    }
    else if(type == "pcbDistance_no_pcb")
    {
        rotate(-180,[0,1,0])Br_pcbSupportTube(Br_PcbThickness);
    }
}

module microSwitchHoleRemove()
{
    cylinder($fn=Br_outerFn,h=100,d=MICRO_SW_HOLE+0.3,center=true);
}


module microSwithchButton(unitsZ=3)
{
    Z_top = unitsZ*Br_HeightUnit - Br_BottomTotalHeight - Br_PcbThickness/2 - Br_con_m_height/2;
    ButtonSwSize = 7-2;
    HolePlay = 1.0;
    ButtonOverhead = 1;
    H1 = Z_top - ButtonSwSize + ButtonOverhead;
    
    difference()
    {
        union()
        {
            // Button itself
            cylinder($fn=Br_outerFn,h=H1,d=MICRO_SW_HOLE - HolePlay,center=true);
            // This avoids that button falls through
            WT = 1.5;
            translate([0,0,-H1/2+WT/2+ButtonOverhead+Br_WallThickenss+0.8])cylinder($fn=Br_outerFn,h=WT,d=MICRO_SW_HOLE+1,center=true);
        }
        union()
        {
            // Make hole so that it fits to a button
           translate([0,0,H1/2-0.99])cylinder($fn=Br_outerFn,h=2,d=3.3 + 0.3,center=true);
            
           // make round edges on top
            CS = 8;
            R = MICRO_SW_HOLE;
            translate([0,0,-5])
            difference()
            {
                cube([CS,CS,CS],center=true);
                translate([0,0,CS/2])sphere($fn = 24, r=R/2);
            }
        }
    }
}

module lightGuideRemove(unitsZ = 3)
{ 
    translate([0,0,0])rotate(180,[0,1,0])lightGuide(remove = true);
}

module lightGuideAdd(unitsZ = 3)
{
    difference()
    {
        union()
        {
            height = unitsZ * Br_HeightUnit - Br_BottomTotalHeight - Br_con_m_height/2 - Br_PcbThickness/2 - 2;
            offsetZ = unitsZ * Br_HeightUnit - Br_BottomTotalHeight - height / 2;
            translate([0,0,offsetZ])cylinder($fn=Br_outerFn,h=height,d=2.6+3.5,center=true);
        }
        union()
        {
            translate([0,0,15])lightGuideRemove();
        }
    }
}

module lightGuide(unitsZ = 3, remove = false)
{
    HolePlay = 0.2;
    Z_top = unitsZ*Br_HeightUnit - Br_BottomTotalHeight - Br_PcbThickness/2 - Br_con_m_height/2 - 0.7;
    
    translate([0,0.70,Br_WallThickenss/2])
    {
        cube([3.7,LIGHT_GUIDE_HOLE/2+0.4, Z_top-Br_WallThickenss], center=true);
    }
    
    if(remove)
    {
        cylinder($fn=Br_outerFn,h=Z_top-Br_WallThickenss-1,d=LIGHT_GUIDE_HOLE - HolePlay+0.65,center=true);
        cylinder($fn=Br_outerFn,h=Z_top,d=LIGHT_GUIDE_HOLE - HolePlay + 0.45,center=true);
    }
    else
    {
        cylinder($fn=Br_outerFn,h=Z_top,d=LIGHT_GUIDE_HOLE - HolePlay,center=true);
    }
}

POT_H = 3;
POT_DIA = 16+2.2;
POT_SHAFT_DIA = 6+1.5;
POT_EXTRA_WALL_BELOW = 2;
module potentiometerRemove(unitHeight=3)
{
    difference()
    {
        OFFSET1= unitHeight*Br_HeightUnit-Br_BottomTotalHeight+Br_Magic-POT_H/2+Br_Magic;
        translate([0,0,OFFSET1])cylinder($fn=64,h=POT_H,d=POT_DIA,center=true);
        
        cylinder($fn=64,h=100,d=POT_SHAFT_DIA+2,center=true);
    }
    
    OFFSET2 = unitHeight*Br_HeightUnit-Br_BottomTotalHeight+Br_Magic-POT_EXTRA_WALL_BELOW/2+Br_Magic-POT_H-Br_oneLayerHeight;
    translate([0,0,OFFSET2])cylinder($fn=64,h=100,d=POT_SHAFT_DIA,center=true);
}

module potentiometerAdd(unitHeight=3)
{
    H = POT_H + POT_EXTRA_WALL_BELOW-Br_WallThickenss;
    OFFSET = unitHeight*Br_HeightUnit-Br_BottomTotalHeight-H/2+Br_Magic-Br_WallThickenss/2;
    translate([0,0,OFFSET])cylinder($fn=64,h=H,d=POT_DIA+3,center=true);
}

module potentiometerDistance()
{
    difference()
    {
        cylinder($fn=32,h=2,d=5,center=true);
        cylinder($fn=32,h=2,d=5-3,center=true);
    }
}