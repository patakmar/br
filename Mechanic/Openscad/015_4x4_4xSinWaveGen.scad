include <BrLib.scad>
include <utils.scad>

type = "top";
rotate(180,[1,0,0])015_4x4_4xSinWaveGen(type);

//==============================
// Create parts as required
module 015_4x4_4xSinWaveGen(topBot="top")
{   
    unitsX = 4;
    unitsY = 4;
    unitsZ = 3;
    
    // Holes where to put connectors
    play   = 0.5;
    r_x    = 13;
    r_y    = 10 + play;
    r_z_through_all = 100;
    W               = Br_WallThickenss;
    Z_top = unitsZ*Br_HeightUnit - Br_BottomTotalHeight - // top height
            Br_con_m_height / 2 - Br_PcbThickness/2 - Br_PcbPlayZ;

    SW_X = -Br_SFRed/2 - 3;
    SW_Y1 = -14.5;
    SW_Y2 = -5;
    SW_Y3 = 4.5;
    SW_Y4 = 14.5;
    SW_X_OFFSET = 12;
    
    if(topBot == "top")
    {
        difference()
        {
            union()
            {
                Br_top(unitsX=unitsX, unitsY=unitsY, unitsZ=unitsZ, con=Generic_con_4x4_left_right, mountingHoles=Generic_mountingHoles_4x4, pcbSize=Generic_pcbSize, inputCount=8, outputCount=0, modifiersCount = 0);
                
                translate([SW_X+SW_X_OFFSET, SW_Y1,0])lightGuideAdd();
                translate([SW_X+SW_X_OFFSET, SW_Y2,0])lightGuideAdd();
                translate([SW_X+SW_X_OFFSET, SW_Y3,0])lightGuideAdd();
                translate([SW_X+SW_X_OFFSET, SW_Y4,0])lightGuideAdd();
            }
            
            union()
            {   
                // Holes for LED out ..
                translate([SW_X+SW_X_OFFSET, SW_Y1,10])lightGuideRemove();
                translate([SW_X+SW_X_OFFSET, SW_Y2,10])lightGuideRemove();
                translate([SW_X+SW_X_OFFSET, SW_Y3,10])lightGuideRemove();
                translate([SW_X+SW_X_OFFSET, SW_Y4,10])lightGuideRemove();
                
                
                // JACK connector
                translate([-16,16-7,0])
                {
                    cylinder(h = 100, r=8.5/2, center = false);
                }
                
                // letters
                z = Br_HeightUnit*unitsZ - Br_BottomTotalHeight;
                translate([2.5,0,z-Br_fontDepth])rotate(270,[0,0,1])
                {
                    linear_extrude(Br_fontDepth+Br_Magic)
                    {
                        size = 6;
                        translate([-16.5-3,13,0]) text("4 x", size = size, font=Br_Font, valign = "center");
                    }
                }
                
                // Sine wave
                translate([1,0,z-Br_fontDepth])
                {
                    RAD = 6;
                    T = 2;
                    translate([15,0,0])difference()
                    {
                        cylinder(h = Br_fontDepth+Br_Magic, r=RAD, center = false);
                        union()
                        {
                            cylinder(h = Br_fontDepth, r=RAD-T, center = false);
                            translate([-10,-10,0])cube([10,20,10]);
                        }
                    }
                    translate([15,-2*RAD+T,0])difference()
                    {
                        cylinder(h = Br_fontDepth+Br_Magic, r=RAD, center = false);
                        union()
                        {
                            cylinder(h = Br_fontDepth, r=RAD-T, center = false);
                            translate([0,-10,0])cube([10,20,10]);
                        }
                    }
                }
                
            }
        }
    }
    else if(topBot == "bot")
    {
        Br_bot(unitsX=unitsX, unitsY=unitsY, mountingHoles=Generic_mountingHoles_4x4);
    }
}