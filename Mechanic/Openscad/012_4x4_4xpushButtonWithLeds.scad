include <BrLib.scad>
include <utils.scad>

type = "top";
rotate(180,[1,0,0])012_4x4_4xpushButtonWithLeds(type);

//==============================
// Create parts as required
module 012_4x4_4xpushButtonWithLeds(topBot="top")
{   
    unitsX = 4;
    unitsY = 4;
    unitsZ = 3;
    
    // Holes where to put connectors
    play   = 0.5;
    r_x    = 13;
    r_y    = 10 + play;
    r_z_through_all = 100;
    W               = Br_WallThickenss;
    Z_top = unitsZ*Br_HeightUnit - Br_BottomTotalHeight - // top height
            Br_con_m_height / 2 - Br_PcbThickness/2 - Br_PcbPlayZ;

    SW_X = -Br_SFRed/2 - 3;
    SW_Y1 = -14.5;
    SW_Y2 = -5;
    SW_Y3 = 4.5;
    SW_Y4 = 14.5;
    SW_X_OFFSET = 12;
    
    if(topBot == "top")
    {
        difference()
        {
            union()
            {
                Br_top(unitsX=unitsX, unitsY=unitsY, unitsZ=unitsZ, con=Generic_con_4x4_left_right, mountingHoles=Generic_mountingHoles_4x4, pcbSize=Generic_pcbSize, inputCount=0, outputCount=4, modifiersCount = 0);
                
                translate([SW_X+SW_X_OFFSET, SW_Y1,0])lightGuideAdd();
                translate([SW_X+SW_X_OFFSET, SW_Y2,0])lightGuideAdd();
                translate([SW_X+SW_X_OFFSET, SW_Y3,0])lightGuideAdd();
                translate([SW_X+SW_X_OFFSET, SW_Y4,0])lightGuideAdd();
            }
            
            union()
            {
                // holes for buttons
                translate([SW_X, SW_Y1, 0])microSwitchHoleRemove();
                translate([SW_X, SW_Y2, 0])microSwitchHoleRemove();
                translate([SW_X, SW_Y3, 0])microSwitchHoleRemove();
                translate([SW_X, SW_Y4, 0])microSwitchHoleRemove();
                
                // Holes for LED out ..
                translate([SW_X+SW_X_OFFSET, SW_Y1,0])lightGuideRemove();
                translate([SW_X+SW_X_OFFSET, SW_Y2,0])lightGuideRemove();
                translate([SW_X+SW_X_OFFSET, SW_Y3,0])lightGuideRemove();
                translate([SW_X+SW_X_OFFSET, SW_Y4,0])lightGuideRemove();
                
                // letters
                z = Br_HeightUnit*unitsZ - Br_BottomTotalHeight;
                translate([0,0,z-Br_fontDepth])rotate(270,[0,0,1])
                {
                    linear_extrude(Br_fontDepth+Br_Magic)
                    {
                        size = 6;
                        translate([-16.5,13,0]) text("1", size = size, font=Br_Font, valign = "center");
                        translate([-16.5+9.5*1,13,0]) text("2", size = size, font=Br_Font, valign = "center");
                        translate([-16.5+9.5*2,13,0]) text("3", size = size, font=Br_Font, valign = "center");
                        translate([-16.5+9.5*3,13,0]) text("4", size = size, font=Br_Font, valign = "center");
                    }
                }
            }
        }
    }
    else if(topBot == "bot")
    {
        Br_bot(unitsX=unitsX, unitsY=unitsY, mountingHoles=Generic_mountingHoles_4x4);
    }
}