if not exist "exportedImg" mkdir "exportedImg

openscad --imgsize 600,600 -Dtype=\"lightGuide3Z\"  --camera  -5,-5,2,70,0,315,30           -o exportedImg/lightGuide3Z.png                             utils.scad
openscad --imgsize 600,600 -Dtype=\"pcbDistance\"   --camera  -5,-5,-1,70,0,315,30          -o exportedImg/000_pcb_distance_washer.png                  utils.scad
openscad --imgsize 600,600 -Dtype=\"pcbDistance_no_pcb\" --camera  -5,-5,-1,70,0,315,30     -o exportedImg/000_pcb_distance_washer_no_pcb.png           utils.scad
openscad --imgsize 600,600 -Dtype=\"bot\"           --camera  -40,-37,25,70,0,315,180       -o exportedImg/000_4x4_BottomStandard.png                   001_4x4_interconnect.scad
openscad --imgsize 600,600 -Dtype=\"top\"           --camera  -40,-37,-25,70,-180,315,180       -o exportedImg/007_4x4_3xLed_RGB_singleLed_top.png          007_4x4_3xLed_RGB_singleLed.scad
openscad --imgsize 600,600 -Dtype=\"top\"           --camera  -40,-37,-25,70,-180,315,180       -o exportedImg/002_4x4_3xLed_semaphore_R2.png               002_4x4_3xLed_semaphore_R2.scad
openscad --imgsize 600,600 -Dtype=\"top\"          --camera  -40,-37,-25,70,-180,315,180    -o exportedImg/014_4x4_tacho_top.png                        014_4x4_tacho.scad
openscad --imgsize 600,600 -Dtype=\"top\"          --camera  -40,-37,-25,70,-180,315,180    -o exportedImg/001_4x4_interconnect_top.png                 001_4x4_interconnect.scad
openscad --imgsize 600,600 -Dtype=\"top\"          --camera  -30,-37,-25,70,-180,315,80     -o exportedImg/cableConnector_top.png                       cableConnector.scad
openscad --imgsize 600,600 -Dtype=\"bot\"          --camera  -30,-37,-25,70,-180,315,80     -o exportedImg/cableConnector_bot.png                       cableConnector.scad


openscad --imgsize 600,600 -Dtype=\"top\"           --camera  -40,-37,25,70,0,315,270       -o exportedImg/009_5x6_2x_DC_motor_top.png                  009_5x6_2x_DC_motor.scad
openscad --imgsize 600,600 -Dtype=\"bot\"           --camera  -40,-37,25,70,0,315,270       -o exportedImg/009_5x6_2x_DC_motor_bot.png                  009_5x6_2x_DC_motor.scad

openscad --imgsize 600,600 -Dtype=\"wheel_D50_H8_G_TOP\"    --camera  -40,-37,20,70,0,315,100       -o exportedImg/wheel_D50_H8_G_TOP.png                  wheels.scad
openscad --imgsize 600,600 -Dtype=\"wheel_D50_H8_G_BOT\"    --camera  -40,-37,20,70,0,315,100       -o exportedImg/wheel_D50_H8_G_BOT.png                  wheels.scad
openscad --imgsize 600,600 -Dtype=\"wheel_D35_H8_GD_TOP\"   --camera  -40,-37,20,70,0,315,100       -o exportedImg/wheel_D35_H8_GD_TOP.png                 wheels.scad
openscad --imgsize 600,600 -Dtype=\"wheel_D35_H8_GD_BOT\"   --camera  -40,-37,20,70,0,315,100       -o exportedImg/wheel_D35_H8_GD_BOT.png                 wheels.scad


openscad --imgsize 600,600 -Dtype=\"wheel\"   --camera  -40,-37,20,70,0,315,100       -o exportedImg/wheelsExtra_wheel.png                 wheelsExtra.scad



openscad --imgsize 600,600 -Dtype=\"top\"           --camera  -40,-37,20,70,0,315,270      -o exportedImg/005_6x4_4xAA_power_supply_R4_top.png                  005_6x4_4xAA_power_supply_R4.scad
openscad --imgsize 600,600 -Dtype=\"bot\"           --camera  -40,-37,25,70,0,315,270       -o exportedImg/005_6x4_4xAA_power_supply_R4_bot.png                  005_6x4_4xAA_power_supply_R4.scad
