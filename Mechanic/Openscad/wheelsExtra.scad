
include <BrLib.scad>
use <wheels.scad>

H1 = 6;
H2 = 2;
SHAFT_LENGTH = 10+1;
FN = 64;

type = "none";
rotate(0,[1,0,0])wheelsExtra(type);

module wheelsExtra(type)
{
    if(type == "wheel")exentricWheel();
    if(type == "screw_d10_h14")screw(14, "hex");
    if(type == "screw_d10_h26")screw(26, "hex");
    if(type == "screw_d10_h26_flat")screw(26, "flat");
    if(type == "holder")holder();
    if(type == "wheelToDuplo")rotate(180,[1,0,0])wheelToDuplo();
    if(type == "shaftAdapter")wheelToMotorAdapter();
}

module screwAddRemove(SCREW_HEIGHT, remove=true, extraSize=0)
{
    if(remove)
    {
        hex_screw(SCR_DIA+0.65+extraSize,2,55,SCREW_HEIGHT+1,1.5,2,24,0,0,0);
    }
    else
    {
        hex_screw(SCR_DIA,2,55,SCREW_HEIGHT,1.5,2,24,0,0,0);
    }
}

module holder()
{
    outerDia = SCR_DIA+5;
    RingThick = 1.5;
    RingDia = 3.7;
    SH_LEN = SHAFT_LENGTH-1;
    difference()
    {
        union()
        {
            cylinder($fn=FN,h=SH_LEN, d = outerDia,center=false);
            
            H = 5;
            D = 2.5;
            X_OFFSET = outerDia/2+H/2-1;
            translate([X_OFFSET,0,D/2])cube([H,D,D],center=true);
            translate([X_OFFSET,0,SH_LEN-D/2])cube([H,D,D],center=true);
            
            translate([X_OFFSET+H/2,0,0])cylinder($fn=FN,h=SH_LEN, d = D,center=false);
            
        }
        union()
        {
            cylinder($fn=FN,h=SHAFT_LENGTH-1, d = SCR_DIA+1,center=false);
        }
    }
}

module screw(h, head)
{
    HEX_H = 4;
    HEX_D = 14.38; // spanner size 13
    FLAT_D = 13;
    SCREW_HEIGHT = h;
    
    screwAddRemove(H1, false);
    translate([0,0,-(h-H1)])cylinder($fn=FN,h=h-H1, d = SCR_DIA,center=false); 
    if(head == "hex")
    {
        translate([0,0,-(h-H1)-HEX_H+0.5])cylinder($fn=6,h=HEX_H-0.5, d = HEX_D,center=false); 
        translate([0,0,-(h-H1)-HEX_H])cylinder($fn=6,h=0.5, d2=HEX_D, d1=HEX_D-1,center=false); 
    }
    else if(head == "flat")
    {
        difference()
        {
            union()
            {
                translate([0,0,-(h-H1)-HEX_H+0.5])cylinder($fn=32,h=HEX_H-0.5, d = FLAT_D,center=false); 
                translate([0,0,-(h-H1)-HEX_H])cylinder($fn=32,h=0.5, d2=FLAT_D, d1=FLAT_D-1,center=false); 
            }
            union()
            {
                translate([0,0,-(h-H1)-HEX_H+0.99])cube([2,100,2],center=true);
            }
        }
    }
}


module exentricWheel()
{
    difference()
    {
        D0 = 64;
        H0 = 8;
        
        
        union()
        {
            translate([0,0,-H0/2])shaft(add=true, height = H0);
            translate([0,0,-H1])cylinder($fn=FN,h=H1, d = D0,center=false); 
            translate([0,0,-H1-H2])cylinder($fn=FN,h=H2, d2 = D0, d1 = D0-H2,center=false);
        }
        union()
        {
            translate([0,0,-H0/2])shaft(add=false, height = H0); 
            
            // Make holes
            SCREW_HEIGHT = H1+H2+2;
            H_OFFSET = -H1-H2-2;
            
            translate([0,10,H_OFFSET])screwAddRemove(SCREW_HEIGHT); 
            translate([16,0,H_OFFSET])screwAddRemove(SCREW_HEIGHT);
            translate([0,-13,H_OFFSET])screwAddRemove(SCREW_HEIGHT);
            translate([-19,0,H_OFFSET])screwAddRemove(SCREW_HEIGHT);
        }
    }
}

module bmp(Z, extraTopBumpSize = 0)
{
    rotate(180,[1,0,0])translate([0,0,Br_KnobL/2+Z])topBump(extraTopBumpSize);
}


module wheelToDuplo(extraTopBumpSize = 0)
{
    H0 = 8;
    Z = 8;
    
    difference()
    {
        union()
        {
            rotate(180,[1,0,0])makeEmptyBox(4*Br_Unit, 2*Br_Unit, Z, wallThickness=Br_WallThickenss+100, bottomThickness=Br_WallThickenss, edgesRadius=Br_RoundRadius, roundBottom=0.5);
            
            rotate(180,[1,0,0])makeEmptyBox(2*Br_Unit, 4*Br_Unit, Z, wallThickness=Br_WallThickenss+100, bottomThickness=Br_WallThickenss, edgesRadius=Br_RoundRadius, roundBottom=0);

            translate([0,0,-H0/2])shaft(add=true, height = H0);
            
            for(i=[-2:1])
            {
                translate([i*Br_Unit+Br_Unit/2,+Br_Unit/2,0])bmp(Z, extraTopBumpSize);
                translate([i*Br_Unit+Br_Unit/2,-Br_Unit/2,0])bmp(Z, extraTopBumpSize);
            }
            translate([+Br_Unit/2, -1.5*Br_Unit,0])bmp(Z, extraTopBumpSize);
            translate([-Br_Unit/2, -1.5*Br_Unit,0])bmp(Z, extraTopBumpSize);
            translate([+Br_Unit/2, 1.5*Br_Unit,0])bmp(Z, extraTopBumpSize);
            translate([-Br_Unit/2, 1.5*Br_Unit,0])bmp(Z, extraTopBumpSize);
            
                
            
        }
        union()
        {
            translate([0,0,-H0/2])shaft(add=false, height = H0); 
            wheelToMotorRemove(remove=true);
        }
    }
}