include <BrLib.scad>
include <utils.scad>

type = "top";
rotate(180,[1,0,0])016_4x4_Programmer(type);

//==============================
// Create parts as required
module 016_4x4_Programmer(topBot="top")
{   
    unitsX = 6;
    unitsY = 4;
    unitsZ = 3;
    LedCnt = 16;
    Y1 = 0.2;
    Y2 = -4.9;
    Y3 = -10;
    Y4 = -15.1;
    
    // Definition of positions with LEDS
    X = [-13.5,-13.5,-13.5,-13.5,-3,-3,-3,-3,11,11,11,11,25.5,25.5,25.5, 25.5];
    Y = [14.5,     5,   -5,-14.5,Y1,Y2,Y3,Y4,Y1,Y2,Y3,Y4,  Y1,  Y2,   Y3,  Y4];
    // Definition of positions of buttons
    ButtonCount = 7;
    X_B = [-21.5,-21.5,-21.5,-21.5,  -3.2, 11.2, 25.5];
    Y_B = [ 14.5,4.8  , -4.8,-14.5, 14.5, 14.5, 14.5];
    
    // Holes where to put connectors
    play   = 0.5;
    r_x    = 13;
    r_y    = 10 + play;
    r_z_through_all = 100;
    W               = Br_WallThickenss;
    Z_top = unitsZ*Br_HeightUnit - Br_BottomTotalHeight - // top height
            Br_con_m_height / 2 - Br_PcbThickness/2 - Br_PcbPlayZ;

    
    if(topBot == "top")
    {
        difference()
        {
            union()
            {
                Br_top(unitsX=unitsX, unitsY=unitsY, unitsZ=unitsZ, con=Generic_con_4x6_left_right, mountingHoles=Generic_mountingHoles_4x6, pcbSize=Generic_pcbSize4x6, inputCount=1, outputCount=0, modifiersCount = 0);
                
                for(i=[0:LedCnt-1])
                {
                    translate([X[i]-Br_SFRed/2, Y[i],0])lightGuideAdd();
                }
            }
            
            union()
            {
                // Holes for LED out ..
                for(i=[0:LedCnt-1])
                {
                    translate([X[i]-Br_SFRed/2, Y[i],10])lightGuideRemove();
                }
                
                for(i=[0:ButtonCount-1])
                {
                    translate([X_B[i]-Br_SFRed/2, Y_B[i], 0])microSwitchHoleRemove();
                }
            }
        }
    }
    else if(topBot == "bot")
    {
        Br_bot(unitsX=unitsX, unitsY=unitsY, mountingHoles=Generic_mountingHoles_4x6);
    }
}