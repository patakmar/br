include <BrLib.scad>
include <utils.scad>

type = "top";
rotate(180,[1,0,0])018_4x4_NoiseDetector(type);

//==============================
// Create parts as required
module 018_4x4_NoiseDetector(topBot="top")
{   
    unitsX = 4;
    unitsY = 4;
    unitsZ = 3; 
    
    if(topBot == "top")
    {
        difference()
        {
            MIC_OFFSET_X = 3.7-0.15;
            MIC_OFFSET_Y = 10.3;
            MIC_DIA = 10.5;
            
            union()
            {
                Br_top(unitsX=unitsX, unitsY=unitsY, unitsZ=unitsZ, con=Generic_con_4x4_left_right, mountingHoles=Generic_mountingHoles_4x4, pcbSize=Generic_pcbSize, inputCount=0, outputCount=1, modifiersCount = 0);
                
                // Add some wall around microphone
                aroundHeight = 9;
                zOffset = unitsZ*Br_HeightUnit-Br_BottomTotalHeight-aroundHeight;
                translate([MIC_OFFSET_X,MIC_OFFSET_Y,zOffset])cylinder($fn=32, d = MIC_DIA+3, h = aroundHeight, center = false);
            }
            
            union()
            {   
                // Microphone
                Z = 0.5; 
                zOffset = Br_HeightUnit*unitsZ - Br_BottomTotalHeight - Z/2+0.1;
                translate([-19,10,zOffset])union()
                {
                    
                    cube([2, 7, Z], center=true);
                    translate([3,0,0])cube([4, 2, Z], center=true);
                    translate([8,0,0])difference()
                    {
                        cylinder($fn=32, r = 4, h = Z, center = true);
                        union()
                        {
                            cylinder($fn=32, r = 2.5, h = Z+1, center = true);
                            translate([5,0,0])cube([10, 20, Z+1], center=true);
                        }
                    }
                    
                    translate([9,0,0])cylinder($fn=32, r = 2, h = Z, center = true);
                    translate([13,0,0])cylinder($fn=32, r = 2, h = Z, center = true);
                    translate([11,0,0])cube([3, 4, Z], center=true);
                }
                
                
                // Holes potentiometer .. 
                translate([-Br_SFRed/2-5,-6,0])ponentiometerRemove();
                
                // holes for mic 
                translate([MIC_OFFSET_X,MIC_OFFSET_Y,0])cylinder($fn=32, d = MIC_DIA, h = 100, center = true);
                //translate([-Br_SFRed/2+6.5,12,0])cylinder($fn=6, r = 1, h = 100, center = true);
                //translate([-Br_SFRed/2+6.5+3,12,0])cylinder($fn=6, r = 1, h = 100, center = true);
                //translate([-Br_SFRed/2+6.5,12+3,0])cylinder($fn=6, r = 1, h = 100, center = true);
                //translate([-Br_SFRed/2+6.5+3,12+3,0])cylinder($fn=6, r = 1, h = 100, center = true);
            }
        }
    }
    else if(topBot == "bot")
    {
        Br_bot(unitsX=unitsX, unitsY=unitsY, mountingHoles=Generic_mountingHoles_4x4);
    }
}