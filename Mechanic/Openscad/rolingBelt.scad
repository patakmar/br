include <BrLib.scad>
use<gearbox.scad>
//include<extraWheels.scad>

type = "side16x15_high";

rolingBelt(type);

module rolingBelt(type)
{ 
    if(type=="side16x15_high")side(length=Br_Unit*15, height = 30);
    if(type=="beltAdapter")beltAdapter();
}

module side(length, height)
{
    THICK = 6;
    W = 2;
    
    difference()
    {
        union()
        {
            cube([length, height, THICK]);
        }
        union()
        {
            for (i = [1:length/Br_Unit-1])
            {
                translate([Br_Unit*i,10,0])cylinder($fn=32,h=100, d = SCR_DIA+1,center=true); 
            }
        }
    }
}

module beltAdapter()
{
    D1 = 20;
    D2 = 17;
    H1 = 2;
    H2 = 20;
    
    difference()
    {
        union()
        {   
            translate([0,0,0])cylinder($fn=32,h=H1, d = D1, center=false); 
            translate([0,0,H1])cylinder($fn=32,h=H2, d = D2, center=false);
        }
        union()
        {
            translate([0,0,(H1+H2)/2])screwRemoveFastScrew(H1+H2);
            translate([0,0,4])ringGearBox(D2/2+1.5);
            translate([0,0,10.5])ringGearBox(D2/2+1.5);
            translate([0,0,17])ringGearBox(D2/2+1.5);
        }
    }
    
    // add extra 
    for (i = [0:20:360])
    {
        rotate(i, [0,0,1])translate([D2/2-0.2,0,14])rotate(90, [0,1,0])cylinder($fn=32,h=1, d1 = 1.5, d2 = 0, center=false);
        rotate(i, [0,0,1])translate([D2/2-0.2,0,7.5])rotate(90, [0,1,0])cylinder($fn=32,h=1, d1 = 1.5, d2 = 0, center=false);
        rotate(i, [0,0,1])translate([D2/2-0.2,0,20])rotate(90, [0,1,0])cylinder($fn=32,h=1, d1 = 1.5, d2 = 0, center=false);
    }
}