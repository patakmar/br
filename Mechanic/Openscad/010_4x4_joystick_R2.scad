include <BrLib.scad>

type = "top";
rotate(180, [1,0,0])010_4x4_joystick(type);

//==============================
// Create parts as required
module 010_4x4_joystick(topBot=type)
{   
    unitsX = 4;
    unitsY = 4;
    unitsZ = 3.3+0.50 + 3/Br_HeightUnit;
    
    // Compensate - the joystick is not aligned correctly on PCB..
    xOffset = 0.2-0.5+1+1;
    yOffset = 0.019;
    
    if(topBot == "top")
    {
        difference()
        {
            union()
            {
                Br_top(unitsX=unitsX, unitsY=unitsY, unitsZ=unitsZ, con=Generic_con_4x4_left_right, mountingHoles=Generic_mountingHoles_4x4, pcbSize=Generic_pcbSize, outputCount=3);
                
                // Make stronger walls around a hole
                h = 2.8 - 0.5 - 0.3; // height - through all
                dia = 20 + 4 + 1; // diameter + play
                z2 = unitsZ * Br_HeightUnit - Br_BottomTotalHeight;
                translate([-3.5-xOffset,yOffset,z2-h/2]) cylinder($fn=Br_outerFn,h=h,d=dia,center=true);
            }
            
            // Remove material - make holes for connectors
            union()
            {
                h = 100; // height - through all
                dia = 20 - 1 + 1 + 1; // diameter + play
                translate([-3.5-xOffset,yOffset,0]) cylinder($fn=Br_outerFn,h=h,d=dia,center=true);
                
                zOffset = unitsZ * Br_HeightUnit - Br_BottomTotalHeight - 0.499;
                outDia = dia + 0.5 + 1.6;
                translate([-3.5-xOffset,yOffset,zOffset])cylinder($fn=Br_outerFn,h=1.5, d1 = dia, d2=outDia,center=true); 
            }
        }
    }
    else if(topBot == "knob1")
    {
        knob1();
    }
    else
    {
        Br_bot(unitsX=unitsX, unitsY=unitsY, mountingHoles=Generic_mountingHoles_4x4);
    }
}

module knob1()
{
    RADIUS = 15.5;
    WALL = 1;
    CYL_HEIGHT = 8;
    
    difference()
    {
        
        union()
        {
            sphere($fn=100, r=RADIUS);
        }
        union()
        {
            sphere($fn=100, r=RADIUS-WALL);
            translate([-50,-50,-RADIUS+CYL_HEIGHT])cube([100,100,100]);
        }
    }
    
    //shaft below
    difference()
    {
        translate([0,0,-RADIUS])cylinder($fn=32, r1=4, r2=2.5, h = CYL_HEIGHT);
        HOLE_DEPTH = 5;
        translate([0,0,-RADIUS+CYL_HEIGHT-HOLE_DEPTH/2])cube([1.2,2.2,HOLE_DEPTH+0.1], center=true);
    }
    
    
    NECK_H = 4;
    translate([0,0,-RADIUS-NECK_H-0.5])cylinder($fn=32, r=4, h = NECK_H+1);
    
    // Top
    difference()
    {
        H_TOP = 5;
        translate([0,0,-RADIUS-NECK_H-H_TOP])cylinder($fn=32, r=9, h = H_TOP);
        
        union()
        {
            translate([0,0,-RADIUS-(NECK_H+H_TOP)])cylinder($fn=32, r=9-2, h = 1);
            // rounded edges
            difference()
            {
                translate([0,0,-RADIUS-(NECK_H+H_TOP)])cylinder($fn=32, r=9, h = 1);
                translate([0,0,-RADIUS-(NECK_H+H_TOP)])cylinder($fn=32, r1=8, r2=9, h = 1+0.001);
            }
            
            H = 2;
            R_DIFF = 5;
            translate([0,0,-H])difference()
            {
                translate([0,0,-RADIUS-1*NECK_H])cylinder($fn=32, r=9+0.001, h = H+0.001);
                translate([0,0,-RADIUS-1*NECK_H])cylinder($fn=32, r1=9, r2=9-R_DIFF, h = H+0.001);
            }
        }
    }
}
