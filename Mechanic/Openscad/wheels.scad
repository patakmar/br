include <extern/screw/polyScrewThread.scad>
include <BrLib.scad>

type = "none";

wheels(type);

module wheels(type)
{ 
    H_MIN = 8; 
    RING_SMALL = 1;
    RING_NORMAL = 1.5;
    
    if(type=="wheel_D53_H8_G_TOP")rotate(180 ,[1,0,0])wheelBase(diameter=53, height=H_MIN, ring = RING_SMALL); 
   
    if(type=="wheel_D35_H8_GD_TOP")rotate(180 ,[1,0,0])wheelBase(bumpsOnTop=true, diameter = 35, height = H_MIN, ring = RING_NORMAL);
}

module wheelToMotorRemove(remove = false)
{
    H = 4.5;
    DIA = 15;
    FN = 64;
    
    if(remove)
    {
       translate([0,0,-H])
       {
            hex_screw(DIA+0.6,2,55,H+1,1.5,2,24,0,0,0);
       }
       cylinder($fn=FN,h=100, d = DIA,center=false); 
    }  
}

module wheelToMotorAdapter(ShaftH = 4)
{
    H = 4.5-0.5;
    DIA = 15;
    H0 = ShaftH;
    difference()
    {
        union()
        {
            translate([0,0,-H])hex_screw(DIA,2,55,H,1.5,2,24,0,0,0);
            translate([0,0,0])shaft(add=true, height = H0);
        }
        union()
        {
            RH = 8;
            translate([0,0,H0-RH])shaft(add=false, height = RH); 
        }
    }
}

module shaft(add=true, height = 8)
{
    DIA = 18.5-0.5;
    
    if(add==true)
    {
        // shaft
        cylinder($fn=32,h=height, d = 9,center=false);
    }
    else
    {
        // hole in shaft
        difference()
        {
            translate([0,0,0.01])cylinder($fn=32,h=height, d = 5.9,center=false);
            union()
            {
                translate([2-0.1,-5,0])cube([10, 10, 20], center=false);
                translate([-10-2+0.1,-5,0])cube([10, 10, 20], center=false);
            }
        }
    }
}

module screwHole(depth = 9)
{
    cylinder($fn=Br_outerFn,h=Br_BotMountingHoleChampferHeight+Br_Magic,d2=Br_BotMountingHoleMaxDia, d1=Br_BotMountingHoleMinDia,center=false); 
    translate([0,0,-depth+Br_BotMountingHoleChampferHeight])cylinder($fn=Br_outerFn,h=depth,d2=Br_BotMountingHoleMinDia,center=false); 
}

module ring(R, r) {
  RING_HOLE = 1.0;
    
  rotate_extrude($fn=64)
    translate([R-0.2,0,0])
        {
            circle($fn=16, r);
        } 
}

// Make cylider with rounded edges
module cylinderEdges(h, d)
{
    cylinder($fn=128,h=1, d1 = d-1, d2 = d,center=false);
    translate([0,0,1])cylinder($fn=128,h=h-2, d = d,center=false);
    translate([0,0,h-1])cylinder($fn=128,h=1, d1 = d, d2 = d-1,center=false);
}

module wheelBase(bumpsOnTop=true, diameter = 40, height = 10, ring = 0)
{
    WHEEL_H = height;
    WHEEL_DIA = diameter;
    SHAFT_OFFSET =  WHEEL_H/2-8+3;
    difference()
    {
        translate([0,0,-height/2])difference()
        {
            union()
            {
                translate([0,0,SHAFT_OFFSET])shaft(true);
                cylinder($fn=32,h=height, d = 20,center=true);
                translate([0,0,-WHEEL_H/2])cylinder($fn=64,h=WHEEL_H, d = 10,center=false);
                // Make knobs on top
                if(bumpsOnTop)
                {
                    H_offset = -Br_KnobL/2 - WHEEL_H/2;
                    U = Br_Unit/2;
                    translate([U,U,H_offset])rotate(180,[1,0,0])topBump();
                    translate([-U,U,H_offset])rotate(180,[1,0,0])topBump();
                    translate([U,-U,H_offset])rotate(180,[1,0,0])topBump();
                    translate([-U,-U,H_offset])rotate(180,[1,0,0])topBump();
                }
                
                rotate(45,[0,0,1])cube([WHEEL_DIA-3, 12, WHEEL_H], center=true);
                rotate(-45,[0,0,1])cube([WHEEL_DIA-3, 12, WHEEL_H], center=true);
                
                translate([0,0,-WHEEL_H/2])
                {   difference()
                    {
                        cylinderEdges(h=WHEEL_H, d = WHEEL_DIA);
                        cylinder($fn=16,h=100, d = WHEEL_DIA-6,center=true);
                    }
                }
            }
            union()
            { // remove parts
                
                
                
                if(ring > 0)
                {
                    ring(diameter/2, ring);
                }
            }
        }
    
        wheelToMotorRemove(remove=true);
    }
    
}