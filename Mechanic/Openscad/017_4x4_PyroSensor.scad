include <BrLib.scad>
include <utils.scad>

type = "top";
rotate(180,[1,0,0])017_4x4_PyroSensor(type);

//==============================
// Create parts as required
module 017_4x4_PyroSensor(topBot="top")
{   
    unitsX = 4;
    unitsY = 4;
    unitsZ = 3.71; // so that the pcb fits well
    
    if(topBot == "top")
    {
        difference()
        {
            union()
            {
                Br_top(unitsX=unitsX, unitsY=unitsY, unitsZ=unitsZ, con=Generic_con_4x4_left_right, mountingHoles=Generic_mountingHoles_4x4, pcbSize=Generic_pcbSize, inputCount=0, outputCount=1, modifiersCount = 0);
            }
            
            union()
            {   
                // Holes for LED out ..
                translate([-Br_SFRed/2,0,0])cylinder($fn=64, r = 23.3/2, h = 100, center = true);
            }
        }
    }
    else if(topBot == "bot")
    {
        Br_bot(unitsX=unitsX, unitsY=unitsY, mountingHoles=Generic_mountingHoles_4x4);
    }
    else if(topBot == "washer")
    {
        difference()
        {
            cube([3, 20, 3], center = true);
            translate([0,0,1])cube([3+0.1, 4, 3/2], center = true);
        }
    }
}