// Define resolution of rounded parts
Br_fine_Fn          = 3;     // Improve the fn resolition than default ..
Br_outerFn          = 16 * Br_fine_Fn;  // Resolution on outer circles, stubs, ...
Br_innerFn          = 8 * Br_fine_Fn;   // Resolution on outer sphes, stubs, ...
Br_oneLayerHeight   = 0.3;   // Define printer resolution

Br_Unit             = 16;   // Duplo standard unit 16mm 
Br_HeightUnit       = 9.6;  // Duplo standard unit 16mm
Br_RoundRadius      = 1.5;    // Edges - radius
Br_BotRoundRadius   = 0.3;    // Edges - radius
Br_WallThickenss    = 1.5;  // General wall thickenss
Br_Magic            = 0.01; // Magic constant so that preview is enough to display shapes
Br_SFRed            = 6.2;  // Br_SideWithFemaleConReduction - Reduce the brick when there is a female connector

// Br bottom
Br_BotBumpStandardHeight    = 5.0;  // Standard bump height
Br_BottomHeight             = 5.5;
Br_BottomTotalHeight        = Br_BotBumpStandardHeight + Br_BottomHeight;
Br_BotTubeD                 = 13.2-0.2; // Diameter of tube underside of brick
Br_BotBumpHeight            = 4.0;  // Height of bottom bumps
Br_BotsmoohtPlugInStepCount = 7;
Br_BotsmoothPlugInStepDiff  = 0.2;
Br_BotAlignmentPlay         = 0.3;
Br_BotPcbSupportDiameter    = 2.8 + 2*Br_WallThickenss;  // PCB is supported with this part
Br_BotPcbSupportDepth       = 1.5;  // how much it is in the bottom part
Br_BotAlignmentGap          = 1.5;

// PCB definitions
Br_PcbThickness     = 1.6;  // PCB thickness, standard one
Br_PcbPlayXY        = 0.3;  // Play for PCB - to easier put in - x,y
Br_PcbPlayZ         = 0.3;  // Play for PCB - to easier put in - z

// Connector definitions
Br_con_m_length        = 31.6;              // Main metal part
Br_con_m_height        = 12.7;             // Main metal part
Br_con_m_thickness     = 1.4 + Br_Magic;   // Main metal part
Br_con_c_length        = 20;               // Connector part
Br_con_c_height        = Br_con_m_height;  // Connector part
Br_con_c_thickness     = 11;               // Connector part

// Mounting holes
Br_BotMountingHoleMinDia        = 2.7;
Br_BotMountingHoleMaxDia        = 5.5;
Br_BotMountingHoleChampferHeight= 2.0;
Br_BotHeightOfBottomPcbSupport  = 4;
Br_TopMountingHoleDia           = 2.3;

// Top part definitions
Br_KnobD=9.6;           // Diameter of the top knob     
Br_KnobL=4.6;           // Height of top knob
Br_topKnobsEasierPlug = 0.19; // Define number so that it is easier to plug in bricks on top of this one
Br_bottomEasierPlug = 0.27+0.00; // How much to remove so that it is easier to plug a brick ...
Br_TubeD            = 13.2-Br_bottomEasierPlug; // Diameter of tube underside of brick (3 for 

Br_Font = "Liberation Sans";
Br_fontDepth = 0.3;

SCR_DIA = 10;

//Note: Brick with height 3mm has space 11.15 mm above PCB

//==============================
// Definition of generic connectors, mounting holes...
Generic_con_4x4_all =[    [-2*Br_Unit, 0, "left"],    // Male connector - left
                    [2*Br_Unit-Br_SFRed, 0,  "right"],   // Female connector - right
                    [0, 2*Br_Unit-Br_SFRed,    "bot"],     // Female connector - bottom
                    [0, -2*Br_Unit+Br_SFRed,   "top"]];     // Female connector - top
                    
Generic_con_4x4_left_right =[    [-2*Br_Unit, 0, "left"],    // Male connector - left
                    [2*Br_Unit-Br_SFRed, 0,  "right"]];   // Female connector - right
                    
Generic_con_4x6_left_right =[    [-3*Br_Unit, 0, "left"],    // Male connector - left
                    [3*Br_Unit-Br_SFRed, 0,  "right"]];   // Female connector - right
 
Generic_mountingHoles_4x4 = [   [Br_Unit, Br_Unit, "top", "left"],
                                [-Br_Unit, Br_Unit, "top", "right"],
                                [Br_Unit, -Br_Unit, "bot", "left"],
                                [-Br_Unit, -Br_Unit, "bot", "right"]]; 
                               
Generic_mountingHoles_4x6 = [   [2*Br_Unit, Br_Unit, "top", "left"],
                                [-2*Br_Unit, Br_Unit, "top", "right"],
                                [2*Br_Unit, -Br_Unit, "bot", "left"],
                                [-2*Br_Unit, -Br_Unit, "bot", "right"]]; 

Generic_pcbSize = [46, 40, -Br_SFRed/2, 0]; // x,y, xOffset, yOffset
Generic_pcbSize4x6 = [46+2*Br_Unit, 40, -Br_SFRed/2, 0]; // x,y, xOffset, yOffset

//==============================
// Create removal for rounded edges
module edgeRounder(height, radius)
{
    translate([radius / 2, radius / 2, 0])
    {
        difference() 
        {
            cube([radius + Br_Magic, radius + Br_Magic, height], center = true);

            translate([radius/2, radius/2, 0])
                cylinder($fn=Br_outerFn, r = radius, h = height + 1, center = true);

        }
    }
}

//==============================
// Create removal for rounded corners
module cornerRounder(radius)
{
    difference() 
    {
        cube([2*radius, 2*radius, 2*radius], center = true);
        translate([-radius, -radius, -radius])
            sphere(r=radius, $fn=Br_outerFn);
    }
}


//==============================
// Make empty box with rounded edges
// x,y,z - units in mm
// WallThickness - how thick the walls shall be
// edgesRadius - for round edges set diameter 
module makeEmptyBox(x, y, z, wallThickness=Br_WallThickenss, bottomThickness=Br_WallThickenss, edgesRadius=Br_RoundRadius, roundBottom=0)
{
    Ws = wallThickness;
    Wb = bottomThickness;
    
    translate([0, 0, z/2])
    {
        difference()
        {
            // Basic shape
            cube([x,y,z], center=true);
            
            union()
            {
                // Remove inner space
                translate([0,0,-Wb/2])
                {
                    cube([x-2*Ws,y-2*Ws,z-Wb+Br_Magic], center=true);
                }
                
                // Make rounded edges
                if(edgesRadius > 0)
                {
                    // upper part 
                    translate([0, y/2, z/2])rotate(270, [1,0,0])rotate(90, [0,1,0])edgeRounder(x+Br_Magic, edgesRadius);
                    translate([0, -y/2, z/2])rotate(90, [0,1,0])edgeRounder(x+Br_Magic, edgesRadius);
                    translate([x/2, 0, z/2])rotate(90, [0,0,1])rotate(90, [0,1,0])edgeRounder(y+Br_Magic, edgesRadius);
                    translate([-x/2, 0, z/2])rotate(90, [0,1,0])rotate(90, [1,0,0])edgeRounder(y+Br_Magic, edgesRadius);
                    
                    // Edges around
                    translate([-x/2, y/2, 0])rotate(270, [0,0,1])edgeRounder(z+Br_Magic, edgesRadius);
                    translate([-x/2, -y/2, 0])rotate(0, [0,0,1])edgeRounder(z+Br_Magic, edgesRadius);
                    translate([x/2, -y/2, 0])rotate(90, [0,0,1])edgeRounder(z+Br_Magic, edgesRadius);
                    translate([x/2, y/2, 0])rotate(180, [0,0,1])edgeRounder(z+Br_Magic, edgesRadius);
                    
                    // Edges
                    R = edgesRadius;
                    translate([x/2, y/2, z/2])rotate(0, [0,0,1]) cornerRounder(R);
                    translate([x/2, -y/2, z/2])rotate(270, [0,0,1]) cornerRounder(R);
                    translate([-x/2, y/2, z/2])rotate(90, [0,0,1]) cornerRounder(R);
                    translate([-x/2, -y/2, z/2])rotate(180, [0,0,1]) cornerRounder(R);
                }
                
                if(roundBottom > 0)
                {
                    rotate(180,[1,0,0])
                    {
                        R2 = roundBottom;
                        translate([x/2, y/2, z/2])rotate(0, [0,0,1]) cornerRounder(R2);
                        translate([x/2, -y/2, z/2])rotate(270, [0,0,1]) cornerRounder(R2);
                        translate([-x/2, y/2, z/2])rotate(90, [0,0,1]) cornerRounder(R2);
                        translate([-x/2, -y/2, z/2])rotate(180, [0,0,1]) cornerRounder(R2);
                        // upper part 
                        translate([0, y/2, z/2])rotate(270, [1,0,0])rotate(90, [0,1,0])edgeRounder(x+Br_Magic, roundBottom);
                        translate([0, -y/2, z/2])rotate(90, [0,1,0])edgeRounder(x+Br_Magic, roundBottom);
                        translate([x/2, 0, z/2])rotate(90, [0,0,1])rotate(90, [0,1,0])edgeRounder(y+Br_Magic, roundBottom);
                        translate([-x/2, 0, z/2])rotate(90, [0,1,0])rotate(90, [1,0,0])edgeRounder(y+Br_Magic, roundBottom);
                    }
                }
            }
        }
    }
}

//==============================
// PCB shape
// x,y - units in mm
module pcbSimulation(x, y)
{
    cube([x,y, Br_PcbThickness],center=true);
}

//==============================
// Make stronger walls around connectors
// con (x,y, orientation)
module Br_topMakeConnectorAdding(con)
{
    for (i = [0:len(con)-1])
    {   
        SrongerWalls = 2*Br_WallThickenss; 
        x = con[i][0];
        y = con[i][1];
        orient = con[i][2];
        rotation = (orient == "top" || orient == "bot")? 90 : 0;
        
        xSign = (orient != "left" && orient != "right")? 0 : (orient == "left")? +1 : -1;
        ySign = (orient != "top" && orient != "bot")? 0 : (orient == "top")? 1 : -1;
        
        dx = x + xSign * Br_WallThickenss;
        dy = y + ySign * Br_WallThickenss; 
        
        height = Br_con_m_height + SrongerWalls;
        widht = Br_con_m_length + SrongerWalls;
        
        translate([dx,dy, height/2])
        {
           rotate(rotation, [0,0,1])
           {
               cube([SrongerWalls,widht,height], center=true);
           }
       }
    }
}

//==============================
// Make holes for connectors
// con (x,y, orientation)
module Br_topMakeConnectorRemoving(con, removeBoxAlignment = true)
{
    for (i = [0:len(con)-1])
    {
        x = con[i][0];
        y = con[i][1];
        orient = con[i][2];
        rotation = (orient == "top" || orient == "bot")? 90 : 0;
        
        xSign = (orient != "left" && orient != "right")? 0 : (orient == "left")? +1 : -1;
        ySign = (orient != "top" && orient != "bot")? 0 : (orient == "top")? +1 : -1;
        
        dx = x + xSign * Br_con_m_thickness / 2;
        dy = y + ySign * Br_con_m_thickness / 2; 
        
        
        translate([dx,dy, Br_con_m_height/2])
        {
           rotate(rotation, [0,0,1])
           {
               // metal plate    
               cube([Br_con_m_thickness+Br_Magic,Br_con_m_length,Br_con_m_height+Br_Magic], center=true);
               // plastics around
               cube([Br_con_c_thickness,Br_con_c_length,Br_con_c_height+Br_Magic], center=true);
               
               //Remove also the box alignment .. if there is a connector
               if(removeBoxAlignment)
               {
                   translate([0,0, -5])cube([Br_con_c_thickness,Br_con_c_length,Br_con_c_height+Br_Magic], center=true);
               }
           }
        }
    }
}


//==============================
// Add some material to make support for holes on top
// Mounting holes - in the format [x, y, top/bot, right/left]
// topHeight - height in mm
module Br_topMountingHolesAdding(mountingHoles, boxX,boxY,boxHeight)
{
    for (i = [0:len(mountingHoles)-1])
    {   // Make stronger walls around connectors
        d_outer = Br_TopMountingHoleDia + 2 * Br_WallThickenss;
        d_inner = Br_TopMountingHoleDia;
        x = mountingHoles[i][0];
        y = mountingHoles[i][1];
        orientTopBot = mountingHoles[i][2];
        orientRightLeft = mountingHoles[i][3];
        
        offset = Br_BotHeightOfBottomPcbSupport/2-(Br_BottomHeight-Br_WallThickenss);
        //Make holes
        translate([x,y,boxHeight/2])
        {
            difference()
            {
                union()
                {
                    cylinder($fn=Br_outerFn,h=boxHeight,d=d_outer,center=true);
                    
                    // Make some adjustments so that thse ribs are not visible on rounded parts of the box
                    ribsOffset = Br_RoundRadius/2;
                    ribsHeight = boxHeight - ribsOffset;
                    
                    // add ribs to edges
                    if(orientRightLeft == "right")
                    {
                        xLen = boxX/2 - abs(x) + Br_WallThickenss + 0.1;
                        translate([-xLen/2,0,-ribsOffset/2])
                        {
                            cube([xLen, Br_WallThickenss, ribsHeight], center=true);
                        }
                    }
                    else if(orientRightLeft == "left")
                    {
                        xLen = boxX/2 - abs(x) - Br_SFRed + Br_WallThickenss + 0.1;
                        translate([xLen/2,0,-ribsOffset/2])
                        {
                            cube([xLen, Br_WallThickenss, ribsHeight], center=true);
                        }
                    }
                    
                    if(orientTopBot == "top")
                    {
                        yLen = boxY/2 - abs(y) - Br_WallThickenss;
                        translate([0,yLen/2,-ribsOffset/2])
                        {
                            cube([Br_WallThickenss, yLen, ribsHeight], center=true);
                        }
                    }
                    else if(orientTopBot == "bot")
                    {
                        yLen = boxY/2 - abs(y) - Br_WallThickenss;
                        translate([0,-yLen/2,-ribsOffset/2])
                        {
                            cube([Br_WallThickenss, yLen, ribsHeight], center=true);
                        }
                    }
                }
                
                union()
                {
                    cylinder($fn=Br_outerFn,h=boxHeight+Br_Magic,d=d_inner,center=true);
                }
            } 
        }
    }
}


//==============================
// Br_top - make top box - on the left side - there is always male connector
// unitsX,unitsY,unitsZ - units in lego units
//con - connector list = [[x,y,angle]]
// mountingHoles - position of mounting holes
// pcbSize [x, y, xOffset, yOffset]
// inputCount - marks for inputs .. count 
// outputCount - count
// bumpsOnTop - generate bumps on top ..
module Br_top(unitsX=4, unitsY=4, unitsZ=3, con, mountingHoles, pcbSize, inputCount=0, outputCount=0, modifiersCount=0, bumpsOnTop=false, enableSiteAlignment=true, enableTopAlignment=true)
{
    x = unitsX*Br_Unit - 1*Br_SFRed;
    y = unitsY*Br_Unit - 2*Br_SFRed;
    z = Br_HeightUnit*unitsZ - Br_BottomTotalHeight;
    W = Br_WallThickenss;
    
    difference()
    {
        union()
        {
            translate([-Br_SFRed/2,0,0]) 
            {   
                makeEmptyBox(x, y, z);
            }
            // Make stronger walls around connectors
            Br_topMakeConnectorAdding(con);
            Br_topMountingHolesAdding(mountingHoles, x,y,z);
            
            // Make box top alignment
            if(enableTopAlignment)
            {
                translate([-Br_SFRed/2,0,0]) Br_topBoxAlignment(x, y, z);
            }
            
            // Make some lines so that the upper part holds better in the lower one
            difference()
            {
                X_LEN = 15;
                Y_LEN = 2;
                T = 6;
                G = Br_BotAlignmentGap; // + Play
                
                
                union()
                {
                    if(enableSiteAlignment)
                    {
                        translate([-Br_SFRed/2,y/2-Y_LEN/2-G,1])cube([X_LEN,Y_LEN,T], center=true);
                        translate([-Br_SFRed/2,-y/2+Y_LEN/2+G,1])cube([X_LEN,Y_LEN,T], center=true);
                    }
                }
                
            
                union()
                {
                    if(enableSiteAlignment)
                    {
                        // make smooth edges
                        translate([-Br_SFRed/2,y/2-Y_LEN/2-G,4.5])rotate(-45,[1,0,0])cube([X_LEN+1,Y_LEN,T], center=true);
                        translate([-Br_SFRed/2,-y/2+Y_LEN/2+G,4.5])rotate(45,[1,0,0])cube([X_LEN+1,Y_LEN,T], center=true);
                        // Make gap for middle line
                        translate([-Br_SFRed/2,0,-T/2]) cube([1+0.5+0.8,y,T], center=true);
                        
                        // Remove a bit of material so that it is easier to plug in
                        EasierPlugIn = 0.2;
                        translate([-Br_SFRed/2,y/2-Y_LEN/2-G+Y_LEN-EasierPlugIn,-T/2])cube([X_LEN,Y_LEN,T], center=true);
                        translate([-Br_SFRed/2,-y/2+Y_LEN/2+G-Y_LEN+EasierPlugIn,-T/2])cube([X_LEN,Y_LEN,T], center=true);
                    }
                }
                
            }
            
            // Make signs for input and output.. for output triangles, for input cylinders
            if(inputCount + outputCount + modifiersCount > 0)
            {
                xShift = -(x/2+Br_SFRed/2);
                zShift = z - (z-Br_con_m_height)/2 - 0.5;//Br_con_m_height - (z-Br_con_m_height)/2;
                cubeYZ = 2;
                cubeDepth = 0.3*2;
                ySpace = 3;
                shift = -((inputCount + outputCount + modifiersCount)-1) * ySpace/2;
                for (i=[0:inputCount+outputCount+modifiersCount-1])
                {
                    translate([xShift,shift+i*ySpace,zShift])
                    {
                        if(i < inputCount)
                        {
                            rotate(90,[0,1,0])cylinder($fn=Br_outerFn,h=cubeDepth,d=cubeYZ,center=true);  
                        }
                        else if (i < inputCount + outputCount)
                        { // The code below makes a triangle :)
                            rotate(270,[0,1,0])cylinder($fn=3,h=cubeDepth,d=cubeYZ,center=true);  
                        }
                        else 
                        { // The code below makes a triangle :)
                            rotate(270,[0,1,0])cube([cubeYZ,cubeYZ,cubeDepth], center=true);  
                        }
                    }
                }
            }
            
            if(bumpsOnTop)
            { // Generate tob bumps if needed ..
                // TODO
            }
        }
        union()
        {
            // Remove place for connectors
            Br_topMakeConnectorRemoving(con);
            
            pcbX = pcbSize[0] + 2*Br_PcbPlayXY;
            pcbY = pcbSize[1] + 2*Br_PcbPlayXY;
            pcbZ = z; // Make sure to remove from middle so that PCB can be plugged in
            zOffset = -z/2+Br_con_m_height/2+Br_PcbThickness/2+Br_PcbPlayZ;
            // Remove PCB
            translate([pcbSize[2],pcbSize[3],zOffset])
            {
                cube([pcbX,pcbY, pcbZ],center=true);
            }
        }
    }
}

//==============================
// Create bottom stub
module Br_botMakeStub()
{
    difference() 
    {
        cylinder($fn=Br_outerFn,h=Br_BotBumpHeight,d=Br_BotTubeD,center=true); 
        union()
        {
            cylinder($fn=Br_innerFn,h=Br_BotBumpHeight+Br_Magic,d=Br_BotTubeD-2*Br_WallThickenss,center=true);
            
            // Make smooth edges ...
            height = Br_BotsmoothPlugInStepDiff*Br_BotsmoohtPlugInStepCount;
            translate([0,0, -Br_BotBumpHeight/2+height/2])
            {
                difference()
                {
                    topDiameter = Br_BotTubeD - Br_BotsmoohtPlugInStepCount * Br_BotsmoothPlugInStepDiff;
                    cylinder($fn=Br_outerFn,h=height+Br_Magic,d=Br_BotTubeD+Br_Magic,center=true);    
                    cylinder($fn=Br_outerFn,h=height, d1 = topDiameter, d2=Br_BotTubeD,center=true); 
                }
            }
        }
    }
}

//==============================
// Remove some material to make holes
// Mounting holes - in the format [x, y, top/bot, right/left]
module Br_botMountingHolesRemove(mountingHoles)
{
    deeper = 0.7 * 2;
    
    for (i = [0:len(mountingHoles)-1])
    {   // Make stronger walls around connectors
       translate([mountingHoles[i][0],mountingHoles[i][1],-Br_BottomHeight/2])
       {
           h = Br_BottomHeight*2 + 2*Br_BotHeightOfBottomPcbSupport + Br_Magic; // Make it through all -
           cylinder($fn=Br_outerFn,h=h,d=Br_BotMountingHoleMinDia,center=true);
            
           offset = -(Br_BottomHeight/2-Br_BotMountingHoleChampferHeight/2-deeper/2); // -Br_BotMountingHoleChampferHeight/2
           // Make champfer for a screw head
           translate([0,0, offset]) // -Br_BotMountingHoleChampferHeight/2
           // Make ])
           { 
                cylinder($fn=Br_outerFn,h=Br_BotMountingHoleChampferHeight+Br_Magic,d1=Br_BotMountingHoleMaxDia, d2=Br_BotMountingHoleMinDia,center=true); 
           }
           
           offset2 = -(Br_BottomHeight/2); // -Br_BotMountingHoleChampferHeight/2
           translate([0,0, offset2]) // -Br_BotMountingHoleChampferHeight/2
           // Make ])
           { 
               cylinder($fn=Br_outerFn,h=deeper,d=Br_BotMountingHoleMaxDia,center=true); 
           }
           
           // Space for axtra parts added below PCB
           depth = Br_BotPcbSupportDepth;
           supportPlay = 0.4;
           translate([0,0,Br_BottomHeight/2-depth/2])
           {
               cylinder($fn=Br_outerFn,h=depth+Br_Magic,d=Br_BotPcbSupportDiameter+supportPlay,center=true); 
           }
       }
       
       
    }
}

//==============================
// Add some material to make support for holes
// Mounting holes - in the format [x, y, top/bot, right/left]
module Br_botMountingHolesAdding(mountingHoles)
{
    for (i = [0:len(mountingHoles)-1])
    {   // Make stronger walls around connectors
       d = Br_BotMountingHoleMinDia + 4 * Br_WallThickenss;       
       h = Br_BottomHeight - Br_WallThickenss;
       offset = h/2 - (Br_BottomHeight-Br_WallThickenss);
       translate([mountingHoles[i][0],mountingHoles[i][1],offset])
       {
           cylinder($fn=Br_outerFn,h=h,d=d,center=true);
       }
    }
}

//==============================
// Add some for easier printout of bottom stubs
module Br_botMountingHolesAdding_postprocessing_easier_3dPrint(mountingHoles)
{
    for (i = [0:len(mountingHoles)-1])
    {  
       d = Br_BotMountingHoleMinDia + 4 * Br_WallThickenss;       
       h = Br_BottomHeight - Br_WallThickenss;
       // Make whole plane so that it is printable
       offset2 = h - (Br_BottomHeight-Br_WallThickenss) - Br_BotPcbSupportDepth - Br_oneLayerHeight/2;
       translate([mountingHoles[i][0],mountingHoles[i][1],offset2])
       {
           cylinder($fn=Br_outerFn,h=Br_oneLayerHeight,d=d,center=true);
       }
    }
}

//==============================
// Make bottom stubs
// unitsX,unitsY - units in lego units
module Br_botMakeBottomStubs(unitsX=4, unitsY=4)
{
    startY = -(unitsY-1.0)/2*Br_Unit + 0.5*Br_Unit;
    startX = -(unitsX-1.0)/2*Br_Unit + 0.5*Br_Unit;
    for(dy=[0:1:unitsY-2]) 
    {
        for(dx=[0:1:unitsX-2]) 
        { 
            if ((dx + dy) % 2 == 0) // Make only every second bump
            {
                // Make only evey second bump
                translate([startX+dx*Br_Unit,startY+dy*Br_Unit,-(Br_BottomHeight+Br_BotBumpHeight/2)]) 
                {
                    Br_botMakeStub();
                }
            }
        }           
    }
}

//==============================
// Make box alignment by 4x circles in edges
// extraPlay = true = use extra play - thicker alignment stub
module Br_topBoxAlignment(x, y, z, extraPlay=false)
{
    playDiameter = 1.5;
    playZ = 3.0;
    h1 = (extraPlay)? z+playZ:z;
    offset = z/2 - Br_BottomHeight + 2*Br_WallThickenss;
    d = (extraPlay)? Br_WallThickenss * 2 + playDiameter : Br_WallThickenss * 2;
    w = Br_WallThickenss + Br_BotAlignmentPlay; // increase this coefficient to have some play
    
    // make basic bumps - exceeding to top part
    translate([x/2-w-d/2,y/2-w-d/2, offset]) cylinder($fn=Br_outerFn,h=h1,d=d,center=true);
    translate([x/2-w-d/2,-y/2+w+d/2, offset]) cylinder($fn=Br_outerFn,h=h1,d=d,center=true);
    translate([-x/2+w+d/2,y/2-w-d/2, offset]) cylinder($fn=Br_outerFn,h=h1,d=d,center=true);
    translate([-x/2+w+d/2,-y/2+w+d/2, offset]) cylinder($fn=Br_outerFn,h=h1,d=d,center=true);
    
    // But below make bigger circles which touches edges
    d2 = Br_WallThickenss * 3.5;
    h2 = z - Br_WallThickenss;
    offset2 = h2/2;
    translate([x/2-w-d/2,y/2-w-d/2, offset2]) cylinder($fn=Br_outerFn,h=h2,d=d2,center=true);
    translate([x/2-w-d/2,-y/2+w+d/2, offset2]) cylinder($fn=Br_outerFn,h=h2,d=d2,center=true);
    translate([-x/2+w+d/2,y/2-w-d/2, offset2]) cylinder($fn=Br_outerFn,h=h2,d=d2,center=true);
    translate([-x/2+w+d/2,-y/2+w+d/2, offset2]) cylinder($fn=Br_outerFn,h=h2,d=d2,center=true);
}

//==============================
// Br_bot - make bot box - on the left side - there is always male connector
// unitsX,unitsY - units in lego units
// Mounting holes - in the format [x, y, top/bot, right/left]
module Br_bot(unitsX=4, unitsY=4, mountingHoles, enableTopAlignment=true)
{
    x = unitsX*Br_Unit - 1*Br_SFRed;
    y = unitsY*Br_Unit - 2*Br_SFRed;
    z = Br_BottomHeight;
    W = Br_WallThickenss;
    
    difference()
    {
        union()
        {
            rotate(180, [1,0,0])
            {
                translate([-Br_SFRed/2,0,0])
                    makeEmptyBox(x, y, z, wallThickness=W, bottomThickness=Br_WallThickenss, roundBottom=Br_BotRoundRadius);
            }
            
            // Make bottom stubs
            Br_botMakeBottomStubs(unitsX, unitsY);
            
            // Add material around holes
            Br_botMountingHolesAdding(mountingHoles);
            
            
            T = z-W;
            translate([-Br_SFRed/2,0,-T/2])
            {
                // Make a cross so that the bottom is more stiff... - the X at the bottom
                // and also the bridging makes shorter paths ..
                cube([x-1,1,T], center=true);
                cube([1,y-1,T], center=true);
            }
            
            // Make some lines so that the upper part holds better in the lower one
            X_LEN = 15;
            Y_LEN = 2;
            G = Br_BotAlignmentGap + 0.7; // + Play
            translate([-Br_SFRed/2,y/2-Y_LEN/2-W-G,-T/2])cube([X_LEN,Y_LEN,T], center=true);
            translate([-Br_SFRed/2,-y/2+Y_LEN/2+W+G,-T/2])cube([X_LEN,Y_LEN,T], center=true);
            
            
            // In corners - prevent them to lift up ... make some larger pads ..
            D = 9;
            H = 3*Br_oneLayerHeight;
            difference()
            {
                union()
                {                    
                    translate([x/2-D/2-Br_SFRed/2-W,y/2-D/2-W,-H/2])cube([D,D,H],center=true);
                    translate([x/2-D/2-Br_SFRed/2-W,-(y/2-D/2)+W,-H/2])cube([D,D,H],center=true);
                    translate([-(x/2-D/2+Br_SFRed/2)+W,y/2-D/2-W,-H/2])cube([D,D,H],center=true);
                    translate([-(x/2-D/2+Br_SFRed/2)+W,-(y/2-D/2)+W,-H/2])cube([D,D,H],center=true);
                }
                union()
                {
                    H = 3*Br_oneLayerHeight + Br_Magic;
                    translate([x/2-D/2-Br_SFRed/2-W+D/2,y/2-D/2-W+D/2,-H/2])cube([D,D,H],center=true);
                    translate([x/2-D/2-Br_SFRed/2-W+D/2,-(y/2-D/2)+W-D/2,-H/2])cube([D,D,H],center=true);
                    translate([-(x/2-D/2+Br_SFRed/2)+W-D/2,y/2-D/2-W+D/2,-H/2])cube([D,D,H],center=true);
                    translate([-(x/2-D/2+Br_SFRed/2)+W-D/2,-(y/2-D/2)+W-D/2,-H/2])cube([D,D,H],center=true);
                }
            }
            
        }
        
        // Remove these parts
        union()
        {
            Br_botMountingHolesRemove(mountingHoles);
            // Remove some space for box alignment..
            if(enableTopAlignment)
            {   
                translate([-Br_SFRed/2,0,0]) Br_topBoxAlignment(x,y,z, true);
            }
        }
    }
    
    Br_botMountingHolesAdding_postprocessing_easier_3dPrint(mountingHoles);
}

//==============================
// Make PCB support tubes
// unitsZ 
module Br_pcbSupportTube(extraHeight)
{
    heightPlay = 0.6;
    h = Br_con_m_height/2 - Br_PcbThickness/2 + Br_BotPcbSupportDepth - heightPlay + extraHeight;
    d1 = Br_BotPcbSupportDiameter - 0.1;
    d2 = Br_BotPcbSupportDiameter - 2*Br_WallThickenss + 0.5;
    
    translate([0,0,0])
    {
        difference()
        {
            union()
            {
                cylinder($fn=Br_outerFn,h=h,d=d1,center=false);
                translate([0,0,Br_BotPcbSupportDepth-heightPlay])cylinder($fn=Br_outerFn,h=Br_BotPcbSupportDepth,d=d1+0.5,center=false);
            }
            
            translate([0,0,-Br_Magic/2])cylinder($fn=Br_outerFn,h=h+Br_Magic,d=d2,center=false);
            
            
            translate([0,0,-4.5])difference()
            {
                cube([10,10,10], center=true);
                translate([0,0,3.5+0.8])cylinder($fn=Br_outerFn,h=2,d1=d1-3,d2=d1+1,center=true);
            }
        }
    }
    
    // make something inside so that it holds on a screw
    //translate([0,0,3])cylinder($fn=Br_outerFn,h=0.3,d=d1,center=false);
}

// Make a top bump ..
module topBump(extraTopBumpSize = 0)
{
    Br_TopsmoohtPlugInStepCount = 4;
    Br_TopsmoothPlugInStepDiff = 0.2;
    Br_Tol              = 0.01; // Tolerance so that some parts are better displayed
    Br_BrickWallT = Br_WallThickenss;
    EXT = extraTopBumpSize;
    
    difference()
    {
        cylinder($fn=Br_outerFn, h=Br_KnobL,d=Br_KnobD - Br_topKnobsEasierPlug + EXT,center=true); 
        
        union()
        {
            cylinder($fn=Br_outerFn, h=Br_KnobL+EXT+Br_Magic,d=Br_KnobD-2*Br_BrickWallT,center=true);
       
            // Make smooth edges ...
            height = Br_TopsmoothPlugInStepDiff*Br_TopsmoohtPlugInStepCount+Br_Tol;
            diaSmooth = Br_KnobD + EXT - Br_topKnobsEasierPlug - (Br_TopsmoothPlugInStepDiff*Br_TopsmoohtPlugInStepCount);
            translate([0,0, Br_KnobL/2-height/2])
            {
                difference()
                {
                    topDiameter = Br_TubeD - Br_TopsmoohtPlugInStepCount * Br_TopsmoothPlugInStepDiff;
                    cylinder($fn=Br_outerFn,h=height+Br_Magic,d=Br_KnobD + EXT - Br_topKnobsEasierPlug+Br_Magic,center=true);    
                    cylinder($fn=Br_outerFn,h=height, d2 = diaSmooth, d1=Br_KnobD + EXT - Br_topKnobsEasierPlug,center=true); 
                }
            } 
        }
    }
}

// Make the leg ..
module socketExtra(h)
{
    x = 3.18;
    y = Br_WallThickenss;
    T = 0.8;
    difference()
    {
        translate([T, -y/2, 0])cube([x-T, y, h]);
        rotate(45,[0,1,0])translate([1.6,-5,0])cube([10,10,10]);
    }
}

// Create 2x2 duplo - bottom
module duplo2x2Socket()
{
    SizeX = 2 * Br_Unit;
    SizeY = 2 * Br_Unit;
    SizeZ = 1 * Br_HeightUnit;
    R_big = 13.17;
    
    //empty box
    makeEmptyBox(SizeX, SizeY, SizeZ, edgesRadius=Br_RoundRadius, roundBottom=0.8);
    // Middle bump
    difference()
    {
        StudChampfer = 1.0;
        union()
        {
            translate([0,0,StudChampfer])cylinder($fn=Br_outerFn, h=SizeZ-StudChampfer,d=R_big,center=false);
            cylinder($fn=Br_outerFn, h=StudChampfer,d1=R_big-StudChampfer, d2=R_big,center=false);
        }
        union()
        {
            translate([0,0,-Br_Magic])cylinder($fn=Br_outerFn, h=SizeZ+Br_Magic,d=R_big-2*Br_WallThickenss,center=false);
        }
    }
    
    W = Br_WallThickenss;
    for (i=[0:3]) 
    {    
        rotate(i*90, [0,0,1])union()
        {
            translate([-SizeX/2,Br_Unit/2,0])socketExtra(SizeZ-W);
            translate([-SizeX/2,-Br_Unit/2,0])socketExtra(SizeZ-W);
        }
    }
}




// Demo
connectors=[ [-2*Br_Unit, 0, "left"],    // Male connector - left
             [2*Br_Unit-Br_SFRed, 0,  "right"],   // Female connector - right
             [0, 2*Br_Unit-Br_SFRed,    "bot"],     // Female connector - bottom
             [0, -2*Br_Unit+Br_SFRed,   "top"],     // Female connector - top
];
mountingHoles = [   [Br_Unit, Br_Unit, "top", "left"],
                    [-Br_Unit, Br_Unit, "top", "right"],
                    [Br_Unit, -Br_Unit, "bot", "left"],
                    [-Br_Unit, -Br_Unit, "bot", "right"]];
pcbSize = [46, 40, -Br_SFRed/2, 0]; // x,y, xOffset, yOffset
//translate([-Br_SFRed/2,0,0])pcbSimulation(46,40);
//Br_top(con=Generic_con_4x4_left_right, mountingHoles=mountingHoles, pcbSize=pcbSize, bumpsOnTop=true);
//Br_bot(mountingHoles=mountingHoles);
//Br_pcbSupportTube();