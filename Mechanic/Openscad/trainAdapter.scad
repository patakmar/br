include <BrLib.scad>
include <wheels.scad>

//motorAdapter();
wheel();
//motor2Wheel();
//desk();
//doublewheel();

module motorAdapter(X=6, Y=2)
{
    H_REMOVE = 8-0.5;
    H = H_REMOVE+3;
    
    difference()
    {
        union()
        {
            cube([X*Br_Unit,Y*Br_Unit, H], center=true);
            
            for(i=[0:X*Y-1])
            {
                X = -2.5*Br_Unit + (i % 6) * Br_Unit ;
                Y = -0.5*Br_Unit + floor(i / 6) * Br_Unit;
                translate([X,Y,Br_KnobL/2+H/2])topBump();
            }
        }
        
        union()
        {
            // Remove space to where to glue motor
            translate([Br_Unit/2+3,0,-H/2+H_REMOVE/2])
            {
                cube([64,19.5, H_REMOVE+Br_Magic], center=true);
                translate([5*Br_Unit/2-20,0,-H/2])rotate(90,[1,0,0])cylinder($fn=128,h=100, d = 10,center=true);
            }
            translate([-2.2*Br_Unit,0,-H/2])rotate(90,[1,0,0])cylinder($fn=128,h=100, d = 10,center=true);
            
            translate([-2.7*Br_Unit,0,-H/2])cylinder($fn=128,h=100, d = 2,center=true);
            translate([-1.7*Br_Unit,0,-H/2])cylinder($fn=128,h=100, d = 2,center=true);
        }
    }
}

module desk()
{
    translate([-35.2,0,-9])
    {
        difference()
        {
            union()
            {
                cube([Br_Unit*1.5,2*Br_Unit, 8], center=true);
            }
            union()
            {
                translate([0,0,4])rotate(90,[1,0,0])cylinder($fn=128,h=100, d = 10,center=true);
                
                translate([0.5*Br_Unit,0,0])cylinder($fn=128,h=100, d = 3,center=true);
                translate([-0.5*Br_Unit,0,0])cylinder($fn=128,h=100, d = 3,center=true);
                
                translate([0.5*Br_Unit,0,-3])cylinder($fn=128,h=3, d = 6,center=true);
                translate([-0.5*Br_Unit,0,-3])cylinder($fn=128,h=3, d = 6,center=true);
            }
        }
    }
}

module wheel()
{
    wheelBase(bumpsOnTop=false, diameter = 35+12, height = 5, ring = 0.85);
}

module doublewheel()
{
    difference()
    { 
        union()
        {
            motor2Wheel(2*Br_Unit);
            translate([0,0,2*Br_Unit+1])rotate(180, [0,1,0])motor2Wheel(2*Br_Unit);
        }
        union()
        {
            translate([0,50+4.2,0])cube([100,100, 100], center=true);
        }
    }
}

module motor2Wheel(h = 8)
{
    wheelToMotorAdapter(h);
}

       