include <BrLib.scad>
include <utils.scad>

type = "top";
rotate(180,[1,0,0])014_4x4_tacho(type);

//==============================
// Create parts as required
module 014_4x4_tacho(topBot="top")
{   
    unitsX = 4;
    unitsY = 4;
    unitsZ = 3;
    X = [-11.31, -4.14, 4.14, 11.31, 15.45, 15.45, 11.31, 4.14, -4.14, -11.31];
    Y = [-11.31, -15.45, -15.45, -11.31, -4.14, 4.14, 11.31, 15.45, 15.45, 11.31];
    
    // Holes where to put connectors
    play   = 0.5;
    r_x    = 13;
    r_y    = 10 + play;
    r_z_through_all = 100;
    W               = Br_WallThickenss;
    Z_top = unitsZ*Br_HeightUnit - Br_BottomTotalHeight - // top height
            Br_con_m_height / 2 - Br_PcbThickness/2 - Br_PcbPlayZ;

    
    if(topBot == "top")
    {
        difference()
        {
            union()
            {
                Br_top(unitsX=unitsX, unitsY=unitsY, unitsZ=unitsZ, con=Generic_con_4x4_left_right, mountingHoles=Generic_mountingHoles_4x4, pcbSize=Generic_pcbSize, inputCount=1, outputCount=0, modifiersCount = 0);
                
                for(i=[0:9])
                {
                    translate([X[i]-Br_SFRed/2, Y[i],0])lightGuideAdd();
                }
                
                // Make a ring so that it is more difficult to destroy the lightguides
                translate([-3,0,14])
                {
                    difference()
                    {
                        D = 33;
                        cylinder(h=7,d=D,center=true);
                        cylinder(h=8,d=D-2,center=true);
                    }
                }
            }
            
            union()
            {
                // Holes for LED out ..
                for(i=[0:9])
                {
                    translate([X[i]-Br_SFRed/2, Y[i],13.1])lightGuideRemove();
                }
          
            }
        }
    }
}