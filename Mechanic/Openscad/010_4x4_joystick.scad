include <BrLib.scad>

type = "top";
rotate(180, [1,0,0])010_4x4_joystick(type);

//==============================
// Create parts as required
module 010_4x4_joystick(topBot=type)
{   
    unitsX = 4;
    unitsY = 4;
    unitsZ = 3.3+0.50;
    
    // Compensate - the joystick is not aligned correctly on PCB..
    xOffset = 0.2;
    yOffset = 0.019;
    
    if(topBot == "top")
    {
        difference()
        {
            union()
            {
                Br_top(unitsX=unitsX, unitsY=unitsY, unitsZ=unitsZ, con=Generic_con_4x4_left_right, mountingHoles=Generic_mountingHoles_4x4, pcbSize=Generic_pcbSize, outputCount=3);
                
                // Make stronger walls around a hole
                h = 2.8 - 0.5 - 0.3; // height - through all
                dia = 20 + 4 + 1; // diameter + play
                z2 = unitsZ * Br_HeightUnit - Br_BottomTotalHeight;
                translate([-3.5-xOffset,yOffset,z2-h/2]) cylinder($fn=Br_outerFn,h=h,d=dia,center=true);
            }
            
            // Remove material - make holes for connectors
            union()
            {
                h = 100; // height - through all
                dia = 20 - 1; // diameter + play
                translate([-3.5-xOffset,yOffset,0]) cylinder($fn=Br_outerFn,h=h,d=dia,center=true);
                
                zOffset = unitsZ * Br_HeightUnit - Br_BottomTotalHeight - 0.499;
                outDia = dia + 0.5 + 1.6;
                translate([-3.5-xOffset,yOffset,zOffset])cylinder($fn=Br_outerFn,h=1.5, d1 = dia, d2=outDia,center=true); 
            }
        }
    }
    else
    {
        Br_bot(unitsX=unitsX, unitsY=unitsY, mountingHoles=Generic_mountingHoles_4x4);
    }
}
