include <BrLib.scad>

type = "top";
rotate(180, [1,0,0])008_4x4_Piezo_buzzer(type);


//==============================
// Create parts as required
module 008_4x4_Piezo_buzzer(topBot="top")
{
    unitsX = 4;
    unitsY = 4;
    unitsZ = 3;
    
    if(topBot == "top")
    {
        difference()
        {
            union()
            {
                Br_top(unitsX=unitsX, unitsY=unitsY, unitsZ=unitsZ, con=Generic_con_4x4_left_right, inputCount=1, mountingHoles=Generic_mountingHoles_4x4, pcbSize=Generic_pcbSize);
            }
            union()
            {
                // Make a symbol for a buzzer
                x1 = 3;
                x2 = 7;
                z = Br_oneLayerHeight;
                zOffset = Br_HeightUnit*unitsZ - Br_BottomTotalHeight - z/2;
                points=[[0,-x1],[0,x1],[x1,x1],[x2,x2],[x2,-x2],[x1,-x1]];
                
                translate([-x1-Br_SFRed/2,0,zOffset])
                linear_extrude(height = Br_oneLayerHeight+Br_Magic, center = true)
                {
                    polygon(points=points);
                }
            }
        }
    }
    else
    {
        Br_bot(unitsX=unitsX, unitsY=unitsY, mountingHoles=Generic_mountingHoles_4x4);
    }
}