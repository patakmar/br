include <BrLib.scad>

type = "top";
rotate(180,[1,0,0])004_4x4_4xServo(type);

//==============================
// Create parts as required
module 004_4x4_4xServo(topBot="top")
{   
    unitsX = 4;
    unitsY = 4;
    unitsZ = 3;
    
    // Holes where to put connectors
    play   = 2;
    r_x    = 8 + play;
    r_y    = 12.5 + play;
    r_z_through_all = 100;
    W               = Br_WallThickenss;
    Z_top = unitsZ*Br_HeightUnit - Br_BottomTotalHeight - // top height
            Br_con_m_height / 2 - Br_PcbThickness/2 - Br_PcbPlayZ;
    
    if(topBot == "top")
    {
        difference()
        {
            union()
            {
                Br_top(unitsX=unitsX, unitsY=unitsY, unitsZ=unitsZ, con=Generic_con_4x4_left_right, mountingHoles=Generic_mountingHoles_4x4, pcbSize=Generic_pcbSize, inputCount=4, outputCount=0);
                translate([0,0,Z_top+W+Br_PcbPlayZ*2])
                {
                    cube([r_x+W, r_y+W, Z_top],center=true);
                }
                
            }
            
            union()
            {
               cube([r_x, r_y, r_z_through_all],center=true);
            }
        }
    }
    else
    {
        Br_bot(unitsX=unitsX, unitsY=unitsY, mountingHoles=Generic_mountingHoles_4x4);
    }
}