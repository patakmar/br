include <BrLib.scad>

type = "top";
rotate(180, [1,0,0])001_4x4_interconnect(type);

//==============================
// Create parts as required
module 001_4x4_interconnect(topBot=type)
{   
    unitsX = 4;
    unitsY = 4;
    unitsZ = 3;
    
    if(topBot == "top")
    {
        Br_top(unitsX=unitsX, unitsY=unitsY, unitsZ=unitsZ, con=Generic_con_4x4_all, mountingHoles=Generic_mountingHoles_4x4, pcbSize=Generic_pcbSize);
    }
    else
    {
        Br_bot(unitsX=unitsX, unitsY=unitsY, mountingHoles=Generic_mountingHoles_4x4);
    }
}
