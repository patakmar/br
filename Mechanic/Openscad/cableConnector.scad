include <BrLib.scad>

type = "bot";
if(type == "top")dsubHousingTop();
else   rotate(180, [1,0,0]) dsubHousingBot();

//rotate(180, [1,0,0])
//dsubHousingBot();
//dsubHousingTop();
//dsubHousing();
//dsubConnector();

module dsubConnector()
{
   LengthMM = 1;
   // metal plate    
   cube([Br_con_m_thickness+Br_Magic,Br_con_m_length+Br_Magic,Br_con_m_height+Br_Magic], center=true);
   // plastics around
   cube([Br_con_c_thickness+15+1.5*2,Br_con_c_length-LengthMM,Br_con_c_height-4], center=true);
   // Plastic in back
   translate([Br_con_c_thickness/2/2,0,0])
   cube([Br_con_c_thickness/2,Br_con_c_length-LengthMM,Br_con_c_height-1], center=true);
}

module dsubHousing()
{
    Height = Br_con_m_height+4;
    CABLE_HOLE = 4;
    LL = 20;
    
    difference()
    {
        union()
        {
            // Main body
            translate([LL/2,0,0])
            {
               cube([LL,Br_con_m_length,Height], center=true);
            }
            
            LLL = 2;
            translate([-LLL/2,0,Br_con_m_height/2+0.5])
            {
                // Upper and bottom part
                cube([2,Br_con_m_length-6,3], center=true);
            }
            translate([-LLL/2,0,-Br_con_m_height/2-0.5])
            {
                // Upper and bottom part
                cube([2,Br_con_m_length-6,3], center=true);
            }
        }
        
        union()
        {
            // Remove space for D sub
            translate([Br_con_m_thickness/2,0,0])
            {
               dsubConnector();
            }
            
            // Make space for cable
            rotate(90,[0,1,0])
            {
                cylinder($fn=Br_outerFn,h=100,d=CABLE_HOLE,center=true);
            }
            
            // Holes in front
            rotate(90,[0,1,0])
            {
                translate([0,12.5,0])cylinder($fn=Br_outerFn,h=4,d=6,center=true);
                translate([0,-12.5,0])cylinder($fn=Br_outerFn,h=4,d=6,center=true);
            }
            
            // Cut edges .. 
            translate([25,16,0]) rotate(45,[0,0,1]) cube([20,20,20],center = true);
            translate([25,-16,0]) rotate(-45,[0,0,1]) cube([20,20,20],center = true);
            
            // Rounded edges
            R = 1.5;
            translate([5,-Br_con_m_length/2,Height/2])rotate(0,[1,0,0])rotate(90, [0,1,0])edgeRounder(20, R);
            translate([5,Br_con_m_length/2,Height/2])rotate(-90,[1,0,0])rotate(90, [0,1,0])edgeRounder(20, R);
            translate([5,-Br_con_m_length/2,-Height/2])rotate(90,[1,0,0])rotate(90, [0,1,0])edgeRounder(20, R);
            translate([5,Br_con_m_length/2,-Height/2])rotate(180,[1,0,0])rotate(90, [0,1,0])edgeRounder(20, R);
            
            translate([11.1,-Br_con_m_length/2,Height/2])rotate(45,[0,0,1])rotate(0,[1,0,0])rotate(90, [0,1,0])edgeRounder(30, R);
            translate([11.1,Br_con_m_length/2,Height/2])rotate(-45,[0,0,1])rotate(-90,[1,0,0])rotate(90, [0,1,0])edgeRounder(30, R);
            translate([11.1,-Br_con_m_length/2,-Height/2])rotate(45,[0,0,1])rotate(90,[1,0,0])rotate(90, [0,1,0])edgeRounder(30, R);
            translate([11.1,Br_con_m_length/2,-Height/2])rotate(-45,[0,0,1])rotate(180,[1,0,0])rotate(90, [0,1,0])edgeRounder(30, R);
            
            translate([LL,Br_con_m_length/2-10,-Height/2])rotate(90,[0,0,1])rotate(90,[1,0,0])rotate(90, [0,1,0])edgeRounder(30, R);
            translate([LL,Br_con_m_length/2-10,Height/2])rotate(90,[0,0,1])rotate(0,[1,0,0])rotate(90, [0,1,0])edgeRounder(30, R);
            
            // Smooth cable out
            LX = 2;
            STEPS = 6;
            T1 = LX / STEPS;
            D = CABLE_HOLE;
            DP = 3;
            for (i = [0:STEPS])
            {
                translate([LL-LX+i*T1,0,0])rotate(90,[0,1,0])cylinder($fn=Br_outerFn,h=T1, d1=D+DP*i*i/STEPS/STEPS, d2=D+DP*(i+1)*(i+1)/STEPS/STEPS,center=true); 
            }
        }
    }
    
    // Cock cable
    translate([LL-4,0,0])cableLocker(CABLE_HOLE);
    //translate([LL-6,0,0])cableLocker(CABLE_HOLE);
}

module cableLocker(CABLE_HOLE)
{
    // Lock cable by couple of rubgs
    T = 0.7;
    T2 = 0.7;
    difference()
    {
        rotate(90,[0,1,0])cylinder($fn=Br_outerFn,h=T, d=CABLE_HOLE,center=true); 
        rotate(90,[0,1,0])cylinder($fn=Br_outerFn,h=T, d2=CABLE_HOLE, d1=CABLE_HOLE-T2,center=true); 
    }
}


module dsubHousingTop()
{
    difference()
    {
        dsubHousing();
        
        union()
        {
            // Remove complete top
            translate([-50,-50,0])cube([100,100,100]);
            
            // Make holes for screws
            translate([8,12,0])cylinder($fn=Br_outerFn,h=10,d=Br_TopMountingHoleDia,center=true);
            translate([8,-12,0])cylinder($fn=Br_outerFn,h=10,d=Br_TopMountingHoleDia,center=true);
        }
    }
}

module dsubHousingBot()
{
    difference()
    {
        dsubHousing();
        
        union()
        {
            // Remove complete top
            translate([-50,-50,-100])cube([100,100,100]);
            
            // Make holes for screws
            translate([8,12,(Br_con_m_height+4-Br_BotMountingHoleChampferHeight) / 2])
            {
                cylinder($fn=Br_outerFn,h=100,d=Br_BotMountingHoleMinDia,center=true);
                cylinder($fn=Br_outerFn,h=Br_BotMountingHoleChampferHeight+Br_Magic,
                         d2=Br_BotMountingHoleMaxDia, d1=Br_BotMountingHoleMinDia,center=true); 
            }
            translate([8,-12,(Br_con_m_height+4-Br_BotMountingHoleChampferHeight)/2])
            {
                cylinder($fn=Br_outerFn,h=100,d=Br_BotMountingHoleMinDia,center=true);
                cylinder($fn=Br_outerFn,h=Br_BotMountingHoleChampferHeight+Br_Magic,
                         d2=Br_BotMountingHoleMaxDia, d1=Br_BotMountingHoleMinDia,center=true); 
            }
        }
    }
}
