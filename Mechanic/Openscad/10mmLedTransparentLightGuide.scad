LED_DIA = 10.5;
WALL = 2;
HEIGHT = LED_DIA/2+WALL+2 + 30;



difference()
{
    translate([0,0,HEIGHT/2])cylinder($fn=16,h=HEIGHT,d=LED_DIA+2*WALL,center=true);
    
    union()
    {
        translate([0,0,2.5])sphere(d=LED_DIA, $fn=40);
        translate([0,0,2.5/2])cylinder($fn=40,h=3,d=LED_DIA,center=true);
    }
}