include <BrLib.scad>

EXTERN_MOTOR_MODEL = "../Extern3dModels/motor.stl";

009_Generic_pcbSize = [46+16, 60, -Br_SFRed/2, 0]; // x,y, xOffset, yOffset

009_Generic_con_5x4_left_right =[    [-2.5*Br_Unit, 0, "left"],    // Male connector - left
                    [2.5*Br_Unit-Br_SFRed, 0,  "right"]];   // Female connector - right


009_mountingHoles = [   [-31, -15, "--", "right"],
                        [-31, 15, "--", "right"],
                        //[18, 0, "--", "left"],
                        // Additional so that it is closer to edges
                        [24, 14, "--", "left"],
                        [24, -14, "--", "left"],
                       ]; 
                       
extraMountingHoles = [ [26+5-1.5, 17+23+4],
                       [26+5-1.5, -(17+23+2+2)],
                       [-31-7+1.5, 17+23+4],
                       [-31-7+1.5, -(17+23+2+2)]];

type = "top";
009_build(type);
//009_top();
//009_bot();  
//rotate(180, [1,0,0])009_support();

module 009_build(type=type)
{
    if(type=="top")009_top();
    if(type=="bot")rotate(180, [1,0,0])009_bot();  
    if(type=="supportTop")rotate(180, [1,0,0])009_support();   
}


module 009_motor()
{
    GEN_PLAY = 0.2;
    
    MAIN_X = 36-2.5;
    MAIN_Y = 22.3;
    MAIN_Z = 18.7+0.5+0.3;
    
    //translate([-MAIN_X/2,0,0])import(EXTERN_MOTOR_MODEL, convexity=3);
    // Create the main part - with gears
    translate([0.5,0,0])cube([MAIN_X, MAIN_Y, MAIN_Z], center=true);
    
    // Create axis
    AXIS_DIA = 5.4+GEN_PLAY+6+2;
    AXIS_LENGTH = 36.8+1;
    AXIS_X_OFFSET = MAIN_X/2-11.2-1.5+0.8;
    translate([-AXIS_X_OFFSET,0,0])
    {
        cylinder($fn=Br_outerFn,h=AXIS_LENGTH,d=AXIS_DIA,center=true);
        
        Z_OFFSET = 36-25.5/2;
        //translate([0,0,Z_OFFSET])cylinder($fn=Br_outerFn,h=1.5+1,d=18+2,center=true);
        //translate([0,0,-Z_OFFSET])cylinder($fn=Br_outerFn,h=1.5+1,d=18+2,center=true);
    }
    
    // Small bump
    BUMP_DIA = 3.5+ GEN_PLAY + 2;
    BUMP_LENGTH = MAIN_Z+2*2.0 + 2*GEN_PLAY - 0.5 - 2;
    BUMP_X_OFFSET = 5.2+1.25;
    translate([BUMP_X_OFFSET,0,0])
    {
        //cylinder($fn=Br_outerFn,h=BUMP_LENGTH,d=BUMP_DIA,center=true);
    }
    
    // Extension - motor - make just a cube
    MOTOR_X = 64.5-MAIN_X-2.5;
    MOTOR_Y = MAIN_Y;
    MOTOR_Z = MAIN_Z;
    translate([MAIN_X/2+MOTOR_X/2,0,0])
    {
        cube([MOTOR_X, MOTOR_Y, MOTOR_Z], center=true);
    }
    
    // Motor axis 
    MOT_DIA = 2;
    MOT_LENGTH = 3;
    MOT_X_OFFSET = MAIN_X/2+MOTOR_X;
    translate([MOT_X_OFFSET+MOT_LENGTH/2,0,0]) rotate(90,[0,1,0])
    {
        cylinder($fn=Br_outerFn,h=MOT_LENGTH,d=MOT_DIA,center=true);
    }
    
    // Belt which is around a motor
    BELT_X = 27-1+1.5;
    BELT_Y = 14+6.8;
    BELT_Z = MAIN_Z+2-4;
    BELT_X_OFFSET = MAIN_X/2+BELT_X/2+2;
    translate([BELT_X_OFFSET,0,0])
    {
        cube([BELT_X, BELT_Y, BELT_Z], center=true);
    }
    
    // Holders for a belt ...
    BELT_HOLDERS_X = 6;
    BELT_HOLDERS_Y = 9;
    BELT_HOLDERS_Z = MAIN_Z + 4+2*GEN_PLAY-0.5-1.5;
    BELT_HOLDERS_X_OFFSET = MAIN_X/2+7+2;
    translate([BELT_HOLDERS_X_OFFSET,0,0])
    {
        cube([BELT_HOLDERS_X, BELT_HOLDERS_Y, BELT_HOLDERS_Z], center=true);
    }
}

// Generate bumps on top ..
module BumpsOnTop(unitsX=4, unitsY=2, unitsZ=3)
{
    Br_TopsmoohtPlugInStepCount = 4;
    Br_TopsmoothPlugInStepDiff = 0.2;
    Br_Tol              = 0.01; // Tolerance so that some parts are better displayed
    Br_BrickWallT = Br_WallThickenss;
    zStart = unitsZ * Br_HeightUnit - Br_BottomTotalHeight;
        
    translate([-Br_Unit/2,0,zStart+Br_KnobL/2])
    {
        for(dy=[-(unitsY-0.5)*Br_Unit/2:Br_Unit:(unitsY-.5)*Br_Unit/2]) 
        {
            for(dx=[-(unitsX+.5)*Br_Unit/2:Br_Unit:(unitsX-.5)*Br_Unit/2]) 
            { 
                translate([dx+0.25*Br_Unit,dy+0.25*Br_Unit,0]) difference()
                {
                    topBump();
                }
            } 
        }
    }
}

module 009_top(unitsX=5, unitsY=6+2*Br_SFRed/Br_Unit, unitsZ=3, con, mountingHoles=009_mountingHoles, pcbSize)
{
    z = unitsZ * Br_HeightUnit - Br_BottomTotalHeight;
    difference()
    {
        union()
        {
            Br_top(unitsX=unitsX, unitsY=unitsY, unitsZ=unitsZ, con=    009_Generic_con_5x4_left_right,   mountingHoles=mountingHoles, pcbSize=009_Generic_pcbSize, inputCount=2, bumpsOnTop = true, enableSiteAlignment = false, enableTopAlignment=false);
        
            h = 4;
            W = Br_WallThickenss;
            // make stronger walls around motor ..
            translate([-21,0,z-h/2-W])cube([2, 90, 5], center=true);
            translate([0,0,z-h/2-W])cube([2, 90, 5], center=true);
            //translate([21,0,z-h/2-W])cube([2, 90, 5], center=true);
            
            x = unitsX * Br_Unit - Br_SFRed;
            translate([-Br_SFRed/2,26,z-h/2-W+2])cube([x-1, 1, 2], center=true);
            translate([-Br_SFRed/2,-26,z-h/2-W+2])cube([x-1, 1, 2], center=true);
            
            BumpsOnTop(3, 6, unitsZ);
            
            // Make stronger walls around motors ..
            y = 6 * Br_Unit - 2.7*W;
            z2 = z - W;
            translate([-Br_SFRed/2,y/2,z2/2])cube([x, 1, z2], center=true);
            translate([-Br_SFRed/2,-y/2,z2/2])cube([x, 1, z2], center=true);
            
            // Extra for mounting holes in edges
            H_KNOB = 18;
            for (i = [0:len(extraMountingHoles)-1])
            {
                translate([extraMountingHoles[i][0], extraMountingHoles[i][1], H_KNOB/2])
                {
                    difference()
                    {
                        cube([6,5,H_KNOB], center=true);
                        cylinder($fn=16, h=100, d = Br_TopMountingHoleDia,center=true);
                    }
                }
            }
        }
    
        // remove some parts so that motors fits in ... 
        union()
        {   
            009_createMotors();
            
            // Remove around female connector .. so that pins fits in
            translate([28,0,8.4])cube([10,10,2], center=true);
        }
    }
}

module 009_createMotors()
{
    // Create motors  
    shiftX = 11.5;
    shiftY = 36;
    shiftZ = 6.4;
    translate([shiftX,shiftY,shiftZ]) rotate(180, [0,1,0])rotate(1800,[1,0,0])rotate(90,[1,0,0])
    {
        009_motor();
    }

    translate([shiftX,-shiftY,shiftZ]) rotate(180, [0,1,0])rotate(90,[1,0,0])
    {
        009_motor();
    }
}

//==============================
// Br_bot - make bot box - on the left side - there is always male connector
// unitsX,unitsY - units in lego units
// Mounting holes - in the format [x, y, top/bot, right/left]
module 009_bot(unitsX=5, unitsY=6, mountingHoles=009_mountingHoles)
{
    x = unitsX*Br_Unit - 1*Br_SFRed;
    y = unitsY*Br_Unit;
    z = Br_BottomHeight;
    
    
    difference()
    {
        union()
        {
            rotate(180, [1,0,0])
            {
                translate([-Br_SFRed/2,0,0])
                    makeEmptyBox(x, y, z, wallThickness=Br_WallThickenss, bottomThickness=Br_WallThickenss, roundBottom=Br_BotRoundRadius);
            }
            
            // Make bottom stubs
            Br_botMakeBottomStubs(unitsX, unitsY);
            
            // Add material around holes
            Br_botMountingHolesAdding(mountingHoles);
            
            h = Br_BottomHeight-Br_WallThickenss;
            L = x-Br_WallThickenss;
            translate([-Br_SFRed/2,26,-h/2])cube([L,3,h],center=true);
            translate([-Br_SFRed/2,-26,-h/2])cube([L,3,h],center=true);
            translate([10,0,-h/2])cube([1,y,h],center=true);
            translate([-10,0,-h/2])cube([1,y,h],center=true); 
           
            // Extra for mounting holes in edges
            H_KNOB = 4.5;
            for (i = [0:len(extraMountingHoles)-1])
            {
                translate([extraMountingHoles[i][0], extraMountingHoles[i][1], -H_KNOB/2])
                {
                    cube([6,5,H_KNOB], center=true);
                }
            } 
        }
        
        // Remove these parts
        union()
        {
            Br_botMountingHolesRemove(mountingHoles);
            
            // Remove some space for box alignment..
            //translate([-Br_SFRed/2,0,0]) Br_topBoxAlignment(x,y,z, true);
            
            009_createMotors();
            for (i = [0:len(extraMountingHoles)-1])
            {
                translate([extraMountingHoles[i][0], extraMountingHoles[i][1], -4.50-0.8])
                {
                    cylinder($fn=16, h=100, d = Br_TopMountingHoleDia,center=true);
                    cylinder($fn=Br_outerFn,h=Br_BotMountingHoleChampferHeight+Br_Magic,d1=Br_BotMountingHoleMaxDia, d2=Br_BotMountingHoleMinDia,center=true); 
                }
            }
        }
    }
    
    
    
    //Br_botMountingHolesAdding_postprocessing_easier_3dPrint(mountingHoles);
}

// This makes a support for easier printing
module 009_support(unitsX=5, unitsY=6)
{
    x = unitsX*Br_Unit - 1*Br_SFRed + 2;
    y = unitsY*Br_Unit + 2;
    z = 0;
    
    
    // Fill in connectors
    z2 = 15.3;
    x2 = 4;
    translate([x/2-Br_SFRed/2-x2/2,0,-z2/2+z])cube([x2, 20, z2], center=true);
    translate([-(x/2+Br_SFRed/2-x2/2),0,-z2/2+z])cube([x2, 20, z2], center=true);
    
    // Around the single hole
    x3 = 15;
    z3 = 10;
    x5 = 11;
    translate([x/2-Br_SFRed/2-x5/2-3,14,-z3/2+z])cube([x5, 6, z3], center=true);
    translate([x/2-Br_SFRed/2-x5/2-3,-14,-z3/2+z])cube([x5, 6, z3], center=true);
    x4 = 11;
    translate([-(x/2-Br_SFRed/2-x4/2+3.5),15+0,-z3/2+z])cube([x4, 6, z3], center=true);
    
    translate([-(x/2-Br_SFRed/2-x4/2+3.5),-15-0,-z3/2+z])cube([x4, 6, z3], center=true);
    
    // Main side..
    h3 = 18;
    translate([-Br_SFRed/2,0, -h3/2+z])cube([x, 1, h3], center=true);
    translate([-Br_SFRed/2,26-1.0, -h3/2+z])cube([x, 3, h3], center=true);
    translate([-Br_SFRed/2,-26+1.0, -h3/2+z])cube([x, 3, h3], center=true);
}

module speedWheel()
{
    DIA = 18.5-0.5;
    difference()
    { 
        union()
        {
            // main part
            cylinder($fn=32,h=1.5, d = DIA,center=false);
            // shaft
            cylinder($fn=32,h=12, d = 8,center=false);
        }
        
        union()
        {
            // hole in shaft
            difference()
            {
                cylinder($fn=32,h=20, d = 5.9,center=false);
                union()
                {
                    translate([2-0.1,-5,0])cube([10, 10, 20], center=false);
                    translate([-10-2+0.1,-5,0])cube([10, 10, 20], center=false);
                }
            }
            
            // holes for light beam
            HOLES_COUNT = 16;
            for (i = [0:HOLES_COUNT-1])
            {
                W = 1.3;
                rotate(360/HOLES_COUNT*i,[0,0,1])translate([6.8-1.0-0.5,-W/2,0])cube([3, W, 10], center=false);
            }
        }
    } 
}


//009_top();
//009_createMotors();
//rotate(180, [1,0,0])009_bot();
//rotate(180, [1,0,0])009_support();