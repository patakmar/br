include <BrLib.scad>
use<wheels.scad>
use<wheelsExtra.scad>

compat = "duplo"; // [lego, duplo]
SHAFT_DIA = 9.7;
SHAFT_PLAY = 1.0;
DISK_H = 8;
HOLE_CONNECT_DIA = 2;
OVERLAP = 10;

type = "gearNut8";

gearbox(type);

module gearbox(type)
{ 
    if(type=="shaft25mm")rotate(90,[1,0,0])shaft(25);
    if(type=="shaft50mm")rotate(90,[1,0,0])shaft(50);
    if(type=="shaft75mm")rotate(90,[1,0,0])shaft(75);
    if(type=="shaft100mm")rotate(90,[1,0,0])shaft(100);
    if(type=="shaft120mm")rotate(90,[1,0,0])shaft(120);
    if(type=="shaft150mm")rotate(90,[1,0,0])shaft(150);
    if(type=="shaft200mm")rotate(90,[1,0,0])shaft(200);
    if(type=="shaft250mm")rotate(90,[1,0,0])shaft(250);
    if(type=="shaft300mm")rotate(90,[1,0,0])shaft(300);
        
    if(type=="gearShaft_H2")shaft(shaftH = 2);
    if(type=="gearShaft_H4")shaft(shaftH = 4);
    if(type=="gearShaft_H10")shaft(shaftH = 10);
    if(type=="gearNut5")shaftNut(5);
    if(type=="gearNut8")shaftNut(8);
    if(type=="gearNut16")shaftNut(16);
        
    if(type=="tribe")tribe(withScew=false);
    if(type=="tribe_with_screw")tribe(withScew=true);
    if(type=="tribe_handle_19.5mm")tribeHandle(19.5);
    
    if(type=="gearWheel_D40_BT_BB")wheel([40], bumpsOnTop=true, holesBelow=true); 
    if(type=="gearWheel_D60_BT_BB")wheel([60], bumpsOnTop=true, holesBelow=true); 
    if(type=="gearWheel_D80_BT_BB")wheel([80], bumpsOnTop=true, holesBelow=true);
    if(type=="gearWheel_D100_BT_BB")wheel([100], bumpsOnTop=true, holesBelow=true);  
        
    if(type=="gearWheel_D20")wheel([20]);
    if(type=="gearWheel_D40_D20_BB")wheel([20, 40], holesBelow=true);
    
    if(type=="gearWheel_D20_SCREW")wheel([20], bumpsOnTop=false, holesBelow=false, screw=true); 
    if(type=="gearWheel_D40_BT_BB_SCREW")wheel([40], bumpsOnTop=true, holesBelow=true, screw=true); 
}


module ringGearBox(R) {
  RING_HOLE = 1.0;
    
  rotate_extrude($fn=64)
    translate([R-0.2,0,0])
        scale([0.8,1])
        {
            translate([0,1,0])rotate(180,[0,0,1])circle($fn=3, 2);
            translate([-0.8,0,0])square([3,2],center=true);
            translate([0,-1,0])rotate(180,[0,0,1])circle($fn=3, 2);
        } 
}



// Height in mm
module shaft(shaftH)
{
    height = 2; // [2,3,6,12]
    round_corners = 0.2; // .1
    xscale = 1; // .125
    yscale = 1; // .125
    no_studs=true;
    full_splines=true;
    
    difference()
    {
        union()
        {
            screwAddRemove(shaftH, remove=false);
        }
        union()
        {
            OFFSET = 3.8-0.5;
            translate([-500,OFFSET,-5])cube([1000,1000,1000]);
            translate([-500,-1000-OFFSET,-5])cube([1000,1000,1000]);
        }
    }
}

module screwRemoveFastScrew(height)
{
    translate([0,0,-height/2-1])screwAddRemove(height+1, remove=true, extraSize=0.15);
    translate([0,0,-height/2])cylinder($fn=32,h=1, d1 = SCR_DIA+2, d2 = SCR_DIA,center=true); 
    translate([0,0,height/2])cylinder($fn=32,h=1, d2 = SCR_DIA+2, d1 = SCR_DIA,center=true); 
    
    difference()
    {
        union()
        {
            cylinder($fn=32,h=1000, d = SCR_DIA+0.5,center=true); 
        }
        union()
        {
            OFFSET = 3.5+0.5;
            translate([-500,OFFSET,-500])cube([1000,1000,1000]);
            translate([-500,-1000-OFFSET,-500])cube([1000,1000,1000]);
        }
    }
}

// Connect two shafts together
module shaftNut(height)
{
    DIA = 17;
    
    difference()
    {
        union()
        {
            cylinder($fn=6,h=height-1, d = DIA,center=true); 
            translate([0,0,height/2-0.25])cylinder($fn=6,h=0.5, d1 = DIA, d2 = DIA-0.5,center=true);
            translate([0,0,-height/2+0.25])cylinder($fn=6,h=0.5, d2 = DIA, d1 = DIA-0.5,center=true);
        } 
        
        union()
        {
            screwRemoveFastScrew(height);
        }
    }
}

// Create one dish with global height
module disk(dia, h)
{
    difference()
    {
        cylinder($fn=Br_outerFn, d = dia, h = h, center = true);
        ringGearBox(R = dia/2);
    }
}

// Dimensions = [dia1, dia2, ...]
module wheel(diameters, bumpsOnTop = false, shaftConnect = false, holesBelow = false, screw = false)
{
    H_TOT = DISK_H*len(diameters);
    
    if(bumpsOnTop)
    {
        H_offset = +Br_KnobL/2;
        U = Br_Unit/2;
        translate([U,U,H_offset])topBump();
        translate([-U,U,H_offset])topBump();
        translate([U,-U,H_offset])topBump();
        translate([-U,-U,H_offset])topBump();
    } 
    
    difference()
    {
        union()
        {
            for (i = [0:len(diameters)-1])
            {
                translate([0,0,-DISK_H/2-DISK_H*i])disk(diameters[i], DISK_H);
            }
            
            if(shaftConnect)
            {
                difference()
                {
                    cylinder($fn=16, d=SHAFT_DIA+5, h = 10, center=true);
                    translate([0,0,1.5])rotate(90, [1,0,0])cylinder($fn=16, d=HOLE_CONNECT_DIA, h = 20, center=true);
                }
            }
        }
        
        union()
        {
           // ShaftHole
           if(screw)
           {
                translate([0,0,-H_TOT/2])screwRemoveFastScrew(H_TOT);
           }
           else
           {
                cylinder($fn=Br_outerFn, d = SHAFT_DIA+SHAFT_PLAY, h = 100, center = true);
           }
           if(holesBelow)
           {
                H_REM = 6;
                translate([0,0,-DISK_H*len(diameters)+H_REM/2])
                {
                    difference()
                    {
                        cylinder($fn=32, d = 34.5, h = H_REM, center = true);
                    }
                }
            }
        }
    }
    
    
    if(holesBelow)translate([0,0,-DISK_H*len(diameters)])
    {
        difference()
        {
            union()
            {
                duplo2x2Socket();
                translate([0,0,Br_HeightUnit/2])
                {   
                    cylinder( d =12, h = Br_HeightUnit, center = true);
                    cube([2,2*Br_Unit,Br_HeightUnit],center=true);
                    cube([2*Br_Unit,2,Br_HeightUnit],center=true);
                }
                
                translate([0,0,Br_HeightUnit/2])difference()
                {
                    cylinder($fn=64, d =35, h = Br_HeightUnit, center = true);
                    cube([2*Br_Unit-2,2*Br_Unit-2,Br_HeightUnit],center=true);
                }
            }
            union()
            {
                translate([0,0,50+6])cube([100,100,100], center=true);
                
                difference()
                {
                    cylinder($fn=64, d =50, h = 100, center = true);
                    cylinder($fn=64, d =34.5, h = 100, center = true);
                } 
                // ShaftHole
                if(screw)
                {
                    translate([0,0,H_TOT/2])screwRemoveFastScrew(H_TOT);
                }
                else
                {
                    cylinder($fn=Br_outerFn, d = SHAFT_DIA+SHAFT_PLAY, h = 100, center = true);
                }
            }
        }
    } 
}

module tribe(withScew)
{
    H_offset = +Br_KnobL/2+Br_HeightUnit;
    U = Br_Unit/2;
    
    difference()
    {
        union()
        {
            duplo2x2Socket();
            translate([Br_Unit*3/2+Br_Unit-2,0,0])makeEmptyBox(Br_Unit*3, Br_Unit, Br_HeightUnit);
            translate([Br_Unit*3+6,0,0])makeEmptyBox(1.2*Br_Unit, 1.2*Br_Unit, Br_HeightUnit,wallThickness=10);
            
            translate([U,U,H_offset])topBump();
            translate([-U,U,H_offset])topBump();
            translate([U,-U,H_offset])topBump();
            translate([-U,-U,H_offset])topBump();
        }
        union()
        {
            translate([Br_Unit*3.4,0,0-0.5])hex_screw(SCR_DIA+0.6,2,55,Br_HeightUnit+1,1.5,2,24,0,0,0);
            cylinder($fn=Br_outerFn, d = SHAFT_DIA+SHAFT_PLAY, h = 100, center = true);
        }
    }
    
    if(withScew)
    {
        difference()
        {
            union()
            {
                cylinder($fn=Br_outerFn, d = SHAFT_DIA+SHAFT_PLAY, h = Br_HeightUnit, center = false);
            }
            union()
            {    
                H_TOT = Br_HeightUnit;
                translate([0,0,-1])screwAddRemove(H_TOT+1, remove=true);
                translate([0,0,0])cylinder($fn=32,h=1, d1 = SCR_DIA+2, d2 = SCR_DIA,center=true); 
                translate([0,0,H_TOT])cylinder($fn=32,h=1, d2 = SCR_DIA+2, d1 = SCR_DIA,center=true);
            }
        }
    }
}

module tribeHandle(height)
{
    OUTER_D = 10+3.7;
    
    difference()
    {
        union()
        {
            cylinder($fn=Br_outerFn, d = OUTER_D, h = height-1, center = true);
            translate([0,0,height/2-0.5/2])cylinder($fn=Br_outerFn, d1 = OUTER_D, d2=OUTER_D-0.5, h = 0.5, center = true);
            translate([0,0,-height/2+0.5/2])cylinder($fn=Br_outerFn, d2 = OUTER_D, d1=OUTER_D-0.5, h = 0.5, center = true);
        }
        union()
        {
            cylinder($fn=Br_outerFn, d = 10.7, h = height, center = true);
        }
    }
}
