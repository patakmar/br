include <BrLib.scad>
use <wheelsExtra.scad>


type = "adapter_top_vertical";
rotate(0,[1,0,0])servomotor(type);

ExtraBumpDia = 0.07;

module servomotor(type)
{
    if(type == "adapter_bot_vertical")adapterBotVertical();
    if(type == "adapter_bot_horizontal")adapterBotHorizontal();
    if(type == "adapter_top_vertical")rotate(180,[1,0,0])adapterTopVertical();
    if(type == "adapter_top_horizontal")adapterTopHorizontal();
}

module servoHolder(cableVertical = true)
{
    H = 29;
    X = 42;
    Y = 21.5;
    CABLE_X = 4;
    CABLE_Y = 7.5;
    CABLE_Z = 15;
    
    difference()
    {
        union()
        {
            translate([0,0,H])rotate(180, [0,1,0])
            {
                WIDTH = (cableVertical)? 2*Br_Unit : 2*Br_Unit - 3.5;
                HEIGHT = (cableVertical)? H : 2*Br_Unit-0.7;
                makeEmptyBox(4*Br_Unit, WIDTH, HEIGHT, wallThickness = 15);
            }
        }
        union()
        {
            translate([0,0,H/2])
            {
                
               cube([X, Y, H+Br_Magic], center=true);
               
                if(cableVertical)
                {
                    translate([CABLE_X/2+X/2,0,0])cube([CABLE_X, CABLE_Y, H-4-8+Br_Magic], center=true);
                    translate([CABLE_X/2+X/2-3,0,+10])cube([CABLE_X, CABLE_Y, H-4-8+Br_Magic], center=true);
                    translate([CABLE_X/2+X/2,0,-H/2+4])cube([CABLE_X+20, CABLE_Y, CABLE_Z], center=true);
                }
                else
                {
                    translate([CABLE_X/2+X/2,0,0])cube([CABLE_X, CABLE_Y, H-4+Br_Magic], center=true);
                    translate([CABLE_X/2+X/2-46,0,0])cube([CABLE_X, CABLE_Y, H-4+Br_Magic], center=true);
                    translate([CABLE_X/2+X/2,0,-7])cube([CABLE_X+200, CABLE_Y, 15+Br_Magic], center=true);
                   
                }
            }
            
           
           Screw_Depth = 15; 
           X_D = 48.5;
           Y_D = 10;
            translate([X_D/2,Y_D/2,H-Screw_Depth/2])cylinder($fn=Br_outerFn,h=Screw_Depth,d=Br_TopMountingHoleDia,center=true);
            translate([-X_D/2,Y_D/2,H-Screw_Depth/2])cylinder($fn=Br_outerFn,h=Screw_Depth,d=Br_TopMountingHoleDia,center=true);
            translate([X_D/2,-Y_D/2,H-Screw_Depth/2])cylinder($fn=Br_outerFn,h=Screw_Depth,d=Br_TopMountingHoleDia,center=true);
            translate([-X_D/2,-Y_D/2,H-Screw_Depth/2])cylinder($fn=Br_outerFn,h=Screw_Depth,d=Br_TopMountingHoleDia,center=true);
        }
    }
}

module adapterBotVertical()
{
    
    
    translate([0,0,-Br_HeightUnit])
    {
        translate([Br_Unit, 0,0])duplo2x2Socket();
        translate([-Br_Unit, 0,0])duplo2x2Socket();
    }
    
    servoHolder();
}

module adapterBotHorizontal()
{
    translate([0,0,-Br_HeightUnit])
    {
        translate([Br_Unit, 0,0])duplo2x2Socket();
        translate([-Br_Unit, 0,0])duplo2x2Socket();
    }
    
    translate([0,13.5,Br_Unit-3.5/2])rotate(90,[1,0,0])servoHolder(cableVertical=false);
    
    for(i=[0:7])
    {
        X = -1.5*Br_Unit + (i % 4) * Br_Unit ;
        Y = -0.5*Br_Unit + floor(i / 4) * Br_Unit;
        translate([X,Y,Br_KnobL/2+3*Br_HeightUnit-0.5])topBump(ExtraBumpDia);
    }
}

module holesForServoHolder()
{
    //Four holes
    DIA = 2.3;
    HOLE_H = 5;
    DIST = 14;
    
    translate([0,DIST/2,-HOLE_H/2])cylinder(d=DIA, h = HOLE_H, center=true);
    translate([DIST/2,0,-HOLE_H/2])cylinder(d=DIA, h = HOLE_H, center=true);
    translate([-DIST/2,0,-HOLE_H/2])cylinder(d=DIA, h = HOLE_H, center=true);
    translate([0,-DIST/2,-HOLE_H/2])cylinder(d=DIA, h = HOLE_H, center=true);
    
    D_MIDDLE = 8;
    cylinder($fn=16, d=D_MIDDLE, h = 100, center=true);
}

module adapterTopVertical()
{
    H = 7;
    ExtraBumpDia = 0.07;
    difference()
    {
        union()
        {
            wheelToDuplo(JoinToScrew=false, ExtraBumpDia);
            translate([0,0,-H])cylinder(d=20, h = H);
        }
        union()
        {
           holesForServoHolder();
        }
    }
}

module adapterTopHorizontal()
{
    
    translate([0,Br_Unit,Br_HeightUnit])
    {
        translate([0,0,-Br_HeightUnit])
        {
            translate([Br_Unit, 0,0])duplo2x2Socket();
            translate([-Br_Unit, 0,0])duplo2x2Socket();
        }
        
        for(i=[0:7])
        {
            X = -1.5*Br_Unit + (i % 4) * Br_Unit ;
            Y = -0.5*Br_Unit + floor(i / 4) * Br_Unit;
            translate([X,Y,Br_KnobL/2])topBump(ExtraBumpDia);
        }
    }
    
    difference()
    {
        H = 7;
        DIA = 3.8*Br_Unit;
        translate([0,-H/2,0])rotate(90,[1,0,0])cylinder($fn=16, d=DIA, h = H, center=true);
        union()
        {
            translate([-50,-50,-100])cube([100,100,100]);
            translate([0,-H,DIA/4])rotate(90,[1,0,0])holesForServoHolder();
        }
    }
    
}

