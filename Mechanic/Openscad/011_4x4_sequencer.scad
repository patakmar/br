include <BrLib.scad>
include <utils.scad>

type = "top";
rotate(180,[1,0,0])011_4x4_sequencer(type);


module arrow(height = 0.25)
{
    difference()
    {
        union()
        {
            cylinder($fn=Br_outerFn,h=height+Br_Magic,d=20,center=true);
        }
        
        union()
        {
            translate([-50,0,0])cube([100,100,10], center = true);
            cylinder($fn=Br_outerFn,h=height+1,d=17,center=true);
        }
    }
    
    translate([0,-9.1,0])rotate(180, [0,0,1])cylinder($fn=3,h=height+Br_Magic,d=5,center=true);
}


//==============================
// Create parts as required
module 011_4x4_sequencer(topBot="top")
{   
    unitsX = 4;
    unitsY = 4;
    unitsZ = 3;
    
    // Holes where to put connectors
    play   = 0.5;
    r_x    = 13 + play;
    r_y    = 10 + play;
    r_z_through_all = 100;
    W               = Br_WallThickenss;
    Z_top = unitsZ*Br_HeightUnit - Br_BottomTotalHeight - // top height
            Br_con_m_height / 2 - Br_PcbThickness/2 - Br_PcbPlayZ;
    
    shiftX = 0;
    shiftY = -2;

    SW_X = -Br_SFRed/2;
    SW_Y = 13;
    
    
    
    if(topBot == "top")
    {
        difference()
        {
            union()
            {
                Br_top(unitsX=unitsX, unitsY=unitsY, unitsZ=unitsZ, con=Generic_con_4x4_left_right, mountingHoles=Generic_mountingHoles_4x4, pcbSize=Generic_pcbSize, inputCount=0, outputCount=0, modifiersCount = 12);
                //translate([shiftX-Br_SFRed/2,shiftY,Z_top+W+Br_PcbPlayZ*2])
                //{
                //   #cube([r_x+W, r_y+W, Z_top],center=true);
                //}
                
            }
            
            union()
            {
               translate([shiftX-Br_SFRed/2, shiftY, 0])cube([r_x, r_y, r_z_through_all],center=true);
               translate([SW_X, SW_Y, 0])microSwitchHoleRemove();
                
               ArrowHeight = 0.25;
               translate([10,0,unitsZ*Br_HeightUnit-Br_BottomTotalHeight-ArrowHeight/2])arrow();
            }
        }
    }
    else if(topBot == "bot")
    {
        Br_bot(unitsX=unitsX, unitsY=unitsY, mountingHoles=Generic_mountingHoles_4x4);
    }
    else if(topBot == "washer")
    {
        difference()
        {
            cube([10, 13, 4],center=true);
            union()
            {
                for (i = [0:4])
                {
                    translate([7.52/2, -2.54*2+i*2.54,0])cylinder($fn=Br_outerFn,h=100,d=0.7+0.7,center=true);
                    translate([-7.52/2,-2.54*2+i*2.54,0])cylinder($fn=Br_outerFn,h=100,d=0.7+0.7,center=true);
                }
            }
        }
    }
}