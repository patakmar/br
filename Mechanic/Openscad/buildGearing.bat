:: Wheels
::if not exist wheels/wheel_D45_H15_TOP.stl                          openscad -Dtype=\"wheel_D45_H15_TOP\"  -o wheels/wheel_D45_H15_TOP.stl                 wheels.scad
::if not exist wheels/wheel_D45_H15_BOT.stl                          openscad -Dtype=\"wheel_D45_H15_BOT\"  -o wheels/wheel_D45_H15_BOT.stl                 wheels.scad
::if not exist wheels/wheel_D55_H15_TOP.stl                          openscad -Dtype=\"wheel_D55_H15_TOP\"  -o wheels/wheel_D55_H15_TOP.stl                 wheels.scad
::if not exist wheels/wheel_D55_H15_BOT.stl                          openscad -Dtype=\"wheel_D55_H15_BOT\"  -o wheels/wheel_D55_H15_BOT.stl                 wheels.scad
::if not exist wheels/wheel_D65_H15_TOP.stl                          openscad -Dtype=\"wheel_D56_H15_TOP\"  -o wheels/wheel_D65_H15_TOP.stl                 wheels.scad
::if not exist wheels/wheel_D65_H15_BOT.stl                          openscad -Dtype=\"wheel_D65_H15_BOT\"  -o wheels/wheel_D65_H15_BOT.stl                 wheels.scad
::if not exist wheels/wheel_D35_H10_G_TOP.stl                        openscad -Dtype=\"wheel_D35_H10_G_TOP\"  -o wheels/wheel_D35_H10_G_TOP.stl             wheels.scad
::if not exist wheels/wheel_D35_H10_G_BOT.stl                        openscad -Dtype=\"wheel_D35_H10_G_BOT\"  -o wheels/wheel_D35_H10_G_BOT.stl             wheels.scad
::if not exist wheels/wheel_D36_H10_G_TOP.stl                        openscad -Dtype=\"wheel_D36_H10_G_TOP\"  -o wheels/wheel_D36_H10_G_TOP.stl             wheels.scad
::if not exist wheels/wheel_D36_H10_G_BOT.stl                        openscad -Dtype=\"wheel_D36_H10_G_BOT\"  -o wheels/wheel_D36_H10_G_BOT.stl             wheels.scad
::if not exist wheels/gearShaftConnector.stl                         openscad -Dtype=\"gearShaftConnector\"   -o wheels/gearShaftConnector.stl              gearbox.scad
::if not exist wheels/gearShaft_H15.stl                              openscad -Dtype=\"gearShaft_H15\"              -o wheels/gearShaft_H15.stl             gearbox.scad
::if not exist wheels/gearWheel_D20.stl                              openscad -Dtype=\"gearWheel_D20\"              -o wheels/gearWheel_D20.stl             gearbox.scad
::if not exist wheels/gearWheel_D40_D20.stl                          openscad -Dtype=\"gearWheel_D40_D20\"          -o wheels/gearWheel_D40_D20.stl         gearbox.scad
::if not exist wheels/gearWheel_D40_D30.stl                          openscad -Dtype=\"gearWheel_D40_D30\"          -o wheels/gearWheel_D40_D30.stl         gearbox.scad
::if not exist wheels/gearWheel_D40_D40_BT.stl                       openscad -Dtype=\"gearWheel_D40_D40_BT\"       -o wheels/gearWheel_D40_D40_BT.stl      gearbox.scad
::if not exist wheels/gearWheel_D60_D40_BT.stl                       openscad -Dtype=\"gearWheel_D60_D40_BT\"       -o wheels/gearWheel_D60_D40_BT.stl      gearbox.scad
::if not exist wheels/gearWheel_D60_D20.stl                          openscad -Dtype=\"gearWheel_D60_D20\"          -o wheels/gearWheel_D60_D20.stl         gearbox.scad
::if not exist wheels/gearWheel_D80_D60_D40_D20.stl                  openscad -Dtype=\"gearWheel_D80_D60_D40_D20\"  -o wheels/gearWheel_D80_D60_D40_D20.stl gearbox.scad
::if not exist wheels/gearWheel_D100_D20.stl                         openscad -Dtype=\"gearWheel_D100_D20\"         -o wheels/gearWheel_D100_D20.stl gearbox.scad
::if not exist wheels/gearWheel_D80_D60_D40_BT.stl                   openscad -Dtype=\"gearWheel_D80_D60_D40_BT\"   -o wheels/gearWheel_D80_D60_D40_BT.stl  gearbox.scad
::if not exist wheels/gearWheel_D100_D40_BT.stl                      openscad -Dtype=\"gearWheel_D100_D40_BT\"      -o wheels/gearWheel_D100_D40_BT.stl     gearbox.scad

::if not exist wheels/gearWheel_D60_D40_P.stl                        openscad -Dtype=\"gearWheel_D60_D40_P\"        -o wheels/gearWheel_D60_D40_P.stl       gearbox.scad
::if not exist wheels/gearWheel_D60_P.stl                            openscad -Dtype=\"gearWheel_D60_P\"            -o wheels/gearWheel_D60_P.stl           gearbox.scad
::if not exist wheels/gearWheel_D40_P.stl                            openscad -Dtype=\"gearWheel_D40_P\"            -o wheels/gearWheel_D40_P.stl           gearbox.scad

if not exist gearing/shaft25mm.stl                                  openscad -Dtype=\"shaft25mm\"                  -o gearing/shaft25mm.stl              gearbox.scad
if not exist gearing/shaft50mm.stl                                  openscad -Dtype=\"shaft50mm\"                  -o gearing/shaft50mm.stl              gearbox.scad
if not exist gearing/shaft75mm.stl                                  openscad -Dtype=\"shaft75mm\"                  -o gearing/shaft75mm.stl              gearbox.scad
if not exist gearing/shaft100mm.stl                                 openscad -Dtype=\"shaft100mm\"                 -o gearing/shaft100mm.stl             gearbox.scad
if not exist gearing/shaft120mm.stl                                 openscad -Dtype=\"shaft120mm\"                 -o gearing/shaft120mm.stl             gearbox.scad
if not exist gearing/shaft150mm.stl                                 openscad -Dtype=\"shaft150mm\"                 -o gearing/shaft150mm.stl             gearbox.scad
if not exist gearing/shaft200mm.stl                                 openscad -Dtype=\"shaft200mm\"                 -o gearing/shaft200mm.stl             gearbox.scad
if not exist gearing/shaft250mm.stl                                 openscad -Dtype=\"shaft250mm\"                 -o gearing/shaft250mm.stl             gearbox.scad
if not exist gearing/shaft300mm.stl                                 openscad -Dtype=\"shaft300mm\"                 -o gearing/shaft300mm.stl             gearbox.scad



if not exist gearing/gearShaft_H4.stl                               openscad -Dtype=\"gearShaft_H4\"               -o gearing/gearShaft_H4.stl              gearbox.scad
if not exist gearing/gearShaft_H10.stl                              openscad -Dtype=\"gearShaft_H10\"              -o gearing/gearShaft_H10.stl             gearbox.scad
if not exist gearing/gearNut5.stl                                   openscad -Dtype=\"gearNut5\"                   -o gearing/gearNut5.stl                  gearbox.scad
if not exist gearing/gearNut8.stl                                   openscad -Dtype=\"gearNut8\"                   -o gearing/gearNut8.stl                  gearbox.scad
if not exist gearing/gearNut16.stl                                  openscad -Dtype=\"gearNut16\"                  -o gearing/gearNut16.stl                 gearbox.scad

if not exist gearing/gearWheel_D40_BT_BB.stl                        openscad -Dtype=\"gearWheel_D40_BT_BB\"         -o gearing/gearWheel_D40_BT_BB.stl        gearbox.scad
if not exist gearing/gearWheel_D60_BT_BB.stl                        openscad -Dtype=\"gearWheel_D60_BT_BB\"         -o gearing/gearWheel_D60_BT_BB.stl        gearbox.scad
if not exist gearing/gearWheel_D80_BT_BB.stl                        openscad -Dtype=\"gearWheel_D80_BT_BB\"         -o gearing/gearWheel_D80_BT_BB.stl        gearbox.scad
if not exist gearing/gearWheel_D100_BT_BB.stl                       openscad -Dtype=\"gearWheel_D100_BT_BB\"        -o gearing/gearWheel_D100_BT_BB.stl       gearbox.scad

if not exist gearing/gearWheel_D20.stl                              openscad -Dtype=\"gearWheel_D20\"               -o gearing/gearWheel_D20.stl          gearbox.scad
if not exist gearing/gearWheel_D40_D20_BB.stl                       openscad -Dtype=\"gearWheel_D40_D20_BB\"        -o gearing/gearWheel_D40_D20_BB.stl   gearbox.scad

if not exist gearing/gearWheel_D20_P.stl                       openscad -Dtype=\"gearWheel_D20_P\"        -o gearing/gearWheel_D20_P.stl   gearbox.scad
if not exist gearing/gearWheel_D40_P.stl                       openscad -Dtype=\"gearWheel_D40_P\"        -o gearing/gearWheel_D40_P.stl   gearbox.scad
if not exist gearing/gearWheel_D60_P.stl                       openscad -Dtype=\"gearWheel_D60_P\"        -o gearing/gearWheel_D60_P.stl   gearbox.scad

if not exist gearing/gearWheel_D20_SCREW.stl                   openscad -Dtype=\"gearWheel_D20_SCREW\"         -o gearing/gearWheel_D20_SCREW.stl   gearbox.scad
if not exist gearing/gearWheel_D40_BT_BB_SCREW.stl             openscad -Dtype=\"gearWheel_D40_BT_BB_SCREW\"   -o gearing/gearWheel_D40_BT_BB_SCREW.stl   gearbox.scad



if not exist gearing/tribe.stl                                    openscad -Dtype=\"tribe\"                      -o gearing/tribe.stl   gearbox.scad
if not exist gearing/tribe_with_screw.stl                         openscad -Dtype=\"tribe_with_screw\"           -o gearing/tribe_with_screw.stl   gearbox.scad
if not exist gearing/tribe_handle_19.5mm.stl                      openscad -Dtype=\"tribe_handle_19.5mm\"        -o gearing/tribe_handle_19.5mm.stl   gearbox.scad


