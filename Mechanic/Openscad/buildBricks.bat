:: Build all bricks by bat script
if not exist exportedStl/000_4x4_BottomStandard.stl                     openscad -Dtype=\"bot\"         -o exportedStl/000_4x4_BottomStandard.stl                   001_4x4_interconnect.scad
if not exist exportedStl/potenciometerKnobWasher.stl                    openscad -Dtype=\"washer\"      -o exportedStl/potenciometerKnobWasher.stl                  003_4x4_4xPotenciometer.scad

if not exist exportedStl/000_pcb_distance_washer.stl                    openscad -Dtype=\"pcbDistance\"  -o exportedStl/000_pcb_distance_washer.stl                 utils.scad
if not exist exportedStl/000_pcb_distance_washer_no_pcb.stl             openscad -Dtype=\"pcbDistance_no_pcb\"  -o exportedStl/000_pcb_distance_washer_no_pcb.stl   utils.scad

if not exist exportedStl/microSwitchButton3Z.stl                        openscad -Dtype=\"button3Z\"     -o exportedStl/microSwitchButton3Z.stl                     utils.scad
if not exist exportedStl/lightGuide3Z.stl                               openscad -Dtype=\"lightGuide3Z\" -o exportedStl/lightGuide3Z.stl                            utils.scad
if not exist exportedStl/cableConnector_top.stl                         openscad -Dtype=\"top\"          -o exportedStl/cableConnector_top.stl                      cableConnector.scad
if not exist exportedStl/cableConnector_bot.stl                         openscad -Dtype=\"bot\"          -o exportedStl/cableConnector_bot.stl                      cableConnector.scad
if not exist exportedStl/potenciometerKnob.stl                          openscad -Dtype=\"knob\"         -o exportedStl/potenciometerKnob.stl                       003_4x4_4xPotenciometer.scad
if not exist exportedStl/001_4x4_interconnect_top.stl                   openscad -Dtype=\"top\"         -o exportedStl/001_4x4_interconnect_top.stl                 001_4x4_interconnect.scad
if not exist exportedStl/002_4x4_3xLed_semaphore_top.stl                openscad -Dtype=\"top\"         -o exportedStl/002_4x4_3xLed_semaphore_top.stl              002_4x4_3xLed_semaphore.scad
if not exist exportedStl/002_4x4_3xLed_semaphore_top_R2.stl             openscad -Dtype=\"top\"         -o exportedStl/002_4x4_3xLed_semaphore_top_R2.stl           002_4x4_3xLed_semaphore_R2.scad
if not exist exportedStl/002_4x4_3xLed_semaphore_washer.stl             openscad -Dtype=\"washer\"      -o exportedStl/002_4x4_3xLed_semaphore_washer.stl           002_4x4_3xLed_semaphore.scad
if not exist exportedStl/003_4x4_4xPotenciometer_top.stl                openscad -Dtype=\"top\"         -o exportedStl/003_4x4_4xPotenciometer_top.stl              003_4x4_4xPotenciometer.scad
if not exist exportedStl/003_4x4_4xPotenciometer_bot.stl                openscad -Dtype=\"bot\"         -o exportedStl/003_4x4_4xPotenciometer_bot.stl              003_4x4_4xPotenciometer.scad
if not exist exportedStl/004_4x4_4xServoController_top.stl              openscad -Dtype=\"top\"         -o exportedStl/004_4x4_4xServoController_top.stl            004_4x4_4xServoController.scad
if not exist exportedStl/005_6x4_4xAA_power_supply_R2_top.stl           openscad -Dtype=\"top\"         -o exportedStl/005_6x4_4xAA_power_supply_R2_top.stl         005_6x4_4xAA_power_supply_R2.scad
if not exist exportedStl/005_6x4_4xAA_power_supply_R2_bot.stl           openscad -Dtype=\"bot\"         -o exportedStl/005_6x4_4xAA_power_supply_R2_bot.stl         005_6x4_4xAA_power_supply_R2.scad
if not exist exportedStl/005_6x4_4xAA_power_supply_R2_topLid.stl        openscad -Dtype=\"topLid\"      -o exportedStl/005_6x4_4xAA_power_supply_R2_topLid.stl      005_6x4_4xAA_power_supply_R2.scad
if not exist exportedStl/005_6x4_4xAA_power_supply_R4_top.stl           openscad -Dtype=\"top\"         -o exportedStl/005_6x4_4xAA_power_supply_R4_top.stl         005_6x4_4xAA_power_supply_R4.scad
if not exist exportedStl/005_6x4_4xAA_power_supply_R4_bot.stl           openscad -Dtype=\"bot\"         -o exportedStl/005_6x4_4xAA_power_supply_R4_bot.stl         005_6x4_4xAA_power_supply_R4.scad
if not exist exportedStl/006_4x4_1xUSB_power_only_PWM_control_top.stl   openscad -Dtype=\"top\"         -o exportedStl/006_4x4_1xUSB_power_only_PWM_control_top.stl 006_4x4_1xUSB_power_only_PWM_control.scad
if not exist exportedStl/007_4x4_3xLed_RGB_singleLed_top.stl            openscad -Dtype=\"top\"         -o exportedStl/007_4x4_3xLed_RGB_singleLed_top.stl          007_4x4_3xLed_RGB_singleLed.scad
if not exist exportedStl/007_4x4_3xLed_RGB_singleLed_washer.stl         openscad -Dtype=\"washer\"      -o exportedStl/007_4x4_3xLed_RGB_singleLed_washer.stl       007_4x4_3xLed_RGB_singleLed.scad
if not exist exportedStl/008_4x4_Piezo_buzzer_top.stl                   openscad -Dtype=\"top\"         -o exportedStl/008_4x4_Piezo_buzzer_top.stl                 008_4x4_Piezo_buzzer.scad
if not exist exportedStl/009_5x6_2x_DC_motor_top.stl                   openscad -Dtype=\"top\"         -o exportedStl/009_5x6_2x_DC_motor_top.stl                  009_5x6_2x_DC_motor.scad
if not exist exportedStl/009_5x6_2x_DC_motor_bot.stl                   openscad -Dtype=\"bot\"         -o exportedStl/009_5x6_2x_DC_motor_bot.stl                  009_5x6_2x_DC_motor.scad
if not exist exportedStl/009_5x6_2x_DC_motor_topSupport.stl            openscad -Dtype=\"supportTop\"  -o exportedStl/009_5x6_2x_DC_motor_topSupport.stl           009_5x6_2x_DC_motor.scad
if not exist exportedStl/009_5x6_2x_DC_motor_top_R2.stl                 openscad -Dtype=\"top\"         -o exportedStl/009_5x6_2x_DC_motor_top_R2.stl               009_5x6_2x_DC_motor_R2.scad
if not exist exportedStl/009_5x6_2x_DC_motor_bot_R2.stl                 openscad -Dtype=\"bot\"         -o exportedStl/009_5x6_2x_DC_motor_bot_R2.stl               009_5x6_2x_DC_motor_R2.scad
if not exist exportedStl/009_5x6_2x_DC_motor_topSupport_R2.stl          openscad -Dtype=\"supportTop\"  -o exportedStl/009_5x6_2x_DC_motor_topSupport_R2.stl        009_5x6_2x_DC_motor_R2.scad
if not exist exportedStl/010_4x4_joystick_top.stl                       openscad -Dtype=\"top\"         -o exportedStl/010_4x4_joystick_top.stl                     010_4x4_joystick.scad
if not exist exportedStl/010_4x4_joystick_R2_top.stl                    openscad -Dtype=\"top\"         -o exportedStl/010_4x4_joystick_R2_top.stl                  010_4x4_joystick_R2.scad
if not exist exportedStl/010_4x4_joystick_R2_knob.stl                   openscad -Dtype=\"knob1\"       -o exportedStl/010_4x4_joystick_R2_knob.stl                 010_4x4_joystick_R2.scad
if not exist exportedStl/011_4x4_sequencer_top.stl                      openscad -Dtype=\"top\"         -o exportedStl/011_4x4_sequencer_top.stl                    011_4x4_sequencer.scad
if not exist exportedStl/011_4x4_sequencer_washer.stl                   openscad -Dtype=\"washer\"      -o exportedStl/011_4x4_sequencer_washer.stl                 011_4x4_sequencer.scad
if not exist exportedStl/012_4x4_4xpushButtonWithLeds_top.stl           openscad -Dtype=\"top\"         -o exportedStl/012_4x4_4xpushButtonWithLeds_top.stl         012_4x4_4xpushButtonWithLeds.scad
if not exist exportedStl/013_4x4_bluetooth_RXTX_topTx_R2.stl            openscad -Dtype=\"topTx\"       -o exportedStl/013_4x4_bluetooth_RXTX_topTx_R2.stl          013_4x4_bluetooth_RXTX_R2.scad
if not exist exportedStl/013_4x4_bluetooth_RXTX_topRx_R2.stl            openscad -Dtype=\"topRx\"       -o exportedStl/013_4x4_bluetooth_RXTX_topRx_R2.stl          013_4x4_bluetooth_RXTX_R2.scad
if not exist exportedStl/013_4x4_bluetooth_RXTX_washer_R2.stl           openscad -Dtype=\"washer\"      -o exportedStl/013_4x4_bluetooth_RXTX_washer_R2.stl         013_4x4_bluetooth_RXTX_R2.scad
if not exist exportedStl/014_4x4_tacho_top.stl                          openscad -Dtype=\"top\"         -o exportedStl/014_4x4_tacho_top.stl                        014_4x4_tacho.scad
if not exist exportedStl/015_4x4_4xSinWaveGen_top.stl                   openscad -Dtype=\"top\"         -o exportedStl/015_4x4_4xSinWaveGen_top.stl                 015_4x4_4xSinWaveGen.scad
if not exist exportedStl/016_4x4_Programmer_bot.stl                     openscad -Dtype=\"bot\"         -o exportedStl/016_4x4_Programmer_bot.stl                   016_4x4_Programmer.scad
if not exist exportedStl/016_4x4_Programmer_top.stl                     openscad -Dtype=\"top\"         -o exportedStl/016_4x4_Programmer_top.stl                   016_4x4_Programmer.scad
if not exist exportedStl/017_4x4_PyroSensor_top.stl                     openscad -Dtype=\"top\"         -o exportedStl/017_4x4_PyroSensor_top.stl                   017_4x4_PyroSensor.scad
if not exist exportedStl/017_4x4_PyroSensor_washer.stl                  openscad -Dtype=\"washer\"      -o exportedStl/017_4x4_PyroSensor_washer.stl                017_4x4_PyroSensor.scad
if not exist exportedStl/018_4x4_NoiseDetector_top.stl                  openscad -Dtype=\"top\"         -o exportedStl/018_4x4_NoiseDetector_top.stl                018_4x4_NoiseDetector.scad

if not exist exportedStl/020_4x4_1x_goose_neck_1x_output_top.stl        openscad -Dtype=\"top\"         -o exportedStl/020_4x4_1x_goose_neck_1x_output_top.stl      020_4x4_1x_goose_neck_1x_output.scad
if not exist exportedStl/020_4x4_1x_goose_neck_1x_output_lampBody.stl   openscad -Dtype=\"lampBody\"    -o exportedStl/020_4x4_1x_goose_neck_1x_output_lampBody.stl 020_4x4_1x_goose_neck_1x_output.scad
if not exist exportedStl/020_4x4_1x_goose_neck_1x_output_lampLens.stl   openscad -Dtype=\"lampLens\"    -o exportedStl/020_4x4_1x_goose_neck_1x_output_lampLens.stl 020_4x4_1x_goose_neck_1x_output.scad




if not exist exportedStl/extra_brick_RIGHT_ANGLE_SOCKET_PLUG.stl        openscad -Dtype=\"RIGHT_ANGLE_SOCKET_PLUG\"   -o exportedStl/extra_brick_RIGHT_ANGLE_SOCKET_PLUG.stl             extraDuploBricks.scad
if not exist exportedStl/extra_brick_RIGHT_ANGLE_PLUG_PLUG.stl          openscad -Dtype=\"RIGHT_ANGLE_PLUG_PLUG\"     -o exportedStl/extra_brick_RIGHT_ANGLE_PLUG_PLUG.stl               extraDuploBricks.scad
if not exist exportedStl/extra_brick_RIGHT_ANGLE_SOCKET_SOCKET.stl      openscad -Dtype=\"RIGHT_ANGLE_SOCKET_SOCKET\" -o exportedStl/extra_brick_RIGHT_ANGLE_SOCKET_SOCKET.stl           extraDuploBricks.scad
if not exist exportedStl/extra_brick_BRICK2x2x3_WITH_HOLE.stl           openscad -Dtype=\"BRICK2x2x3_WITH_HOLE\"      -o exportedStl/extra_brick_BRICK2x2x3_WITH_HOLE.stl                extraDuploBricks.scad
if not exist exportedStl/extra_brick_BRICK2x2x3_WITH_HOLE_SCREW.stl     openscad -Dtype=\"BRICK2x2x3_WITH_HOLE_SCREW\" -o exportedStl/extra_brick_BRICK2x2x3_WITH_HOLE_SCREW.stl          extraDuploBricks.scad

