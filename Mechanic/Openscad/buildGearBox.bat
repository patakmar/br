:: Whels
if not exist wheels/wheel_D53_H8_G_TOP.stl                        openscad -Dtype=\"wheel_D53_H8_G_TOP\"            -o wheels/wheel_D53_H8_G_TOP.stl           wheels.scad
if not exist wheels/wheel_D35_H8_GD_TOP.stl                       openscad -Dtype=\"wheel_D35_H8_GD_TOP\"  -o wheels/wheel_D35_H8_GD_TOP.stl             wheels.scad

:: Extra wheels
if not exist wheels/wheelsExtra_wheel.stl          openscad -Dtype=\"wheel\"            -o wheels/wheelsExtra_wheel.stl wheelsExtra.scad
if not exist wheels/wheelsExtra_screw_d10_h14.stl  openscad -Dtype=\"screw_d10_h14\"    -o wheels/wheelsExtra_screw_d10_h14.stl wheelsExtra.scad
if not exist wheels/wheelsExtra_screw_d10_h26.stl  openscad -Dtype=\"screw_d10_h26\"    -o wheels/wheelsExtra_screw_d10_h26.stl wheelsExtra.scad
if not exist wheels/wheelsExtra_screw_d10_h26_flat.stl  openscad -Dtype=\"screw_d10_h26_flat\" -o wheels/wheelsExtra_screw_d10_h26_flat.stl wheelsExtra.scad
if not exist wheels/wheelsExtra_holder.stl         openscad -Dtype=\"holder\"           -o wheels/wheelsExtra_holder.stl wheelsExtra.scad
if not exist wheels/wheelsExtra_wheelToDuplo.stl   openscad -Dtype=\"wheelToDuplo\"     -o wheels/wheelsExtra_wheelToDuplo.stl wheelsExtra.scad
if not exist wheels/wheelsExtra_shaftAdapter.stl   openscad -Dtype=\"shaftAdapter\"     -o wheels/wheelsExtra_shaftAdapter.stl wheelsExtra.scad


if not exist wheels/servoMotor_box_vertical.stl             openscad -Dtype=\"adapter_bot_vertical\"        -o wheels/servoMotor_box_vertical.stl           servoMotor.scad
if not exist wheels/servoMotor_box_horizontal.stl           openscad -Dtype=\"adapter_bot_horizontal\"      -o wheels/servoMotor_box_horizontal.stl         servoMotor.scad
if not exist wheels/servoMotor_box_adapter_vertical.stl     openscad -Dtype=\"adapter_top_vertical\"        -o wheels/servoMotor_box_adapter_vertical.stl   servoMotor.scad
if not exist wheels/servoMotor_box_adapter_horizontal.stl   openscad -Dtype=\"adapter_top_horizontal\"      -o wheels/servoMotor_box_adapter_horizontal.stl servoMotor.scad