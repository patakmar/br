include <BrLib.scad>
include <utils.scad>

minusX = -16;
plusX  = +10;
plusMinusY = 13;

type = "washer";
rotate(180,[1,0,0])003_4x4_4xPotenciometer(type);

//==============================
// Create parts as required
module 003_4x4_4xPotenciometer(topBot="top")
{   
    unitsX = 4;
    unitsY = 4;
    unitsZ = 3;
    yOffset = 5;
    
    mountingHoles = [   [-3, 16, "top", "--"],
                        [-3, -16, "bot", "--"],
                        [-18, 0, "--", "right"],
                        [12, 0, "--", "left"]]; 
    
    pcbSize = [46, 40, -Br_SFRed/2, 0]; // x,y, xOffset, yOffset
    
    if(topBot == "top")
    {
        difference()
        {
            union()
            {
                Br_top(unitsX=unitsX, unitsY=unitsY, unitsZ=unitsZ, con=Generic_con_4x4_left_right, mountingHoles=mountingHoles, pcbSize=pcbSize, inputCount=0, outputCount=4);
                
                translate([minusX,plusMinusY,0]) potentiometerAdd(unitsZ);
                translate([plusX,plusMinusY,0]) potentiometerAdd(unitsZ);
                translate([minusX,-plusMinusY,0]) potentiometerAdd(unitsZ);
                translate([plusX,-plusMinusY,0]) potentiometerAdd(unitsZ);
            }
            
            union()
            {
                // Remove holes in top
                translate([minusX,plusMinusY,0]) potentiometerRemove(unitsZ);
                translate([plusX,plusMinusY,0]) potentiometerRemove(unitsZ);
                translate([minusX,-plusMinusY,0]) potentiometerRemove(unitsZ);
                translate([plusX,-plusMinusY,0]) potentiometerRemove(unitsZ);
                
                // remove some pat around connector holders so that connectors can be plugged- also its pins..
                translate([17.5,0,8]) cube([5,4,4], center=true);
                translate([-17.5-Br_SFRed,0,8]) cube([5,4,4], center=true);
                
                z = Br_HeightUnit*unitsZ - Br_BottomTotalHeight;
                translate([0,0,z-Br_fontDepth])rotate(270,[0,0,1])
                {
                    linear_extrude(Br_fontDepth+Br_Magic)
                    {
                        size = 5;
                        translate([-5-0.7,1,0]) text("1", size = size, font=Br_Font, valign = "center");
                        translate([3-0.7,1,0]) text("2", size = size, font=Br_Font, valign = "center");
                        translate([-5-0.7,-7,0]) text("3", size = size, font=Br_Font, valign = "center");
                        translate([3-0.7,-7,0]) text("4", size = size, font=Br_Font, valign = "center");
                    }
                }
            }
        }
    }
    else if(topBot == "bot")
    {
        Br_bot(unitsX=unitsX, unitsY=unitsY, mountingHoles=mountingHoles);
    }
    else if(topBot == "knob")
    {
        knob();
    }
    else if(topBot == "washer")
    {
        difference()
        {
            cylinder($fn=32, h = 3.3, d1= 5.7,d2 = 5);
            cylinder($fn=32, h = 3.5, d = 2);
        }
    }
    else if(topBot == "jig")
    {
        difference()
        {
            translate([-Br_SFRed/2,0,0])cube([46,40, 10],center=true);
            union()
            {
                D = 6.6;
                H = 100;
                translate([minusX,plusMinusY,0]) cylinder($fn=64, h = H, d= D,center=true);
                translate([plusX,plusMinusY,0]) cylinder($fn=64, h = H, d= D,center=true);
                translate([minusX,-plusMinusY,0]) cylinder($fn=64, h = H, d= D,center=true);
                translate([plusX,-plusMinusY,0]) cylinder($fn=64, h = H, d= D,center=true);
            }
        }
    }
}

module knob()
{
    RES = 64;
    CHAM = 11;
    D1 = CHAM-0.7;
    D2 = CHAM;
    D3 = CHAM+1;
    D4 = CHAM+5;
    D5 = CHAM+5;
    D6 = 9;
    H1 = 1;
    H2 = 9;
    H3 = 2;
    H4 = 1;
    H5 = 3.5;
    H_HOLE = 11.5;
    D_HOLE = 6.5;
    rotate(180,[1,0,0])
    {
        union()
        {
            
            difference()
            {
                union()
                {
                    translate([0,0,0])cylinder($fn=RES, h = H1, d1= D1, d2 = D2);
                    translate([0,0,H1])cylinder($fn=RES, h = H2, d1= D2, d2 = D3);
                    translate([0,0,H1+H2])cylinder($fn=RES, h = H3, d1= D3, d2 = D4);
                    translate([0,0,H1+H2+H3])cylinder($fn=RES, h = H4, d1= D4, d2 = D5);
                    translate([0,0,H1+H2+H3+H4])cylinder($fn=RES, h = H5, d = D6);
                    
                    difference()
                    {
                        K_X = 2;
                        K_Y = 3;
                        translate([-K_X/2,D1/2-0.5,0]) cube([K_X, K_Y, H1+H2+H3+H4]);
                        translate([-2.5,7.5,-5-1]) rotate(45, [1,0,0])cube([5, 5+1, 5]);
                    }
                }
                
                union()
                {
                    HOLE_RES = 32;
                    
                    translate([0,0,H1+H2+H3+H4+H5-H_HOLE])cylinder($fn=HOLE_RES, h = H_HOLE, d = D_HOLE);
                    
                    CHAMPFER = 0.7;
                    translate([0,0,H1+H2+H3+H4+H5-CHAMPFER+Br_Magic])cylinder($fn=HOLE_RES, h = CHAMPFER, d1 = D_HOLE, d2 = D_HOLE+CHAMPFER);
                }
            }
            
            // In shaft- make some points so that it does not turn around
            POINTS = 8;
            DIM_X = 0.4;
            DIM_Y = 0.6;
            Hx = H1+H2+H3+H4+H5-1;
            for (i = [0:POINTS-1])
            {
               rotate(i*360/POINTS,[0,0,1])translate([0,D_HOLE/2,Hx/2]) cube([DIM_X, DIM_Y, Hx], center=true); 
            }
        }
    }
}