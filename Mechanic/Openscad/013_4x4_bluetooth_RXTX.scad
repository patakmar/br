include <BrLib.scad>
include <utils.scad>

type = "topTx";
rotate(180, [1,0,0])013_4x4_bluetooth_RXTX(type);

Generic_con_4x4_left_only =[    [-2*Br_Unit, 0, "left"] ];   // Male connector - left


module triangle(s){ 
 polygon([[0,0],[0,s],[s,0]]); 
} 

module tooth(){ 
translate([0,-10]) 
 difference(){ 
  square(10); 
  translate([1,1,0]) 
  triangle(6.5); 
  translate([11,9,0]) 
  rotate([0,0,180])triangle(10); 
 } 
} 

module 013_bluetooth(h = 0.3)
{
    translate([0,0,-h])
    linear_extrude(height = h+Br_Magic)
    {
        translate([-3,-3,0])
        {
            
            rotate(45,[0,0,1]) scale([0.6, 0.6, 0.6])
            {
                translate([-1,1])tooth(); 
                rotate([0,0,-90])mirror([1,0,0])tooth(); 
            }
        }
    }
    translate([-3.5,-3,-h/2+Br_Magic])difference()
    {
        D = 20;
        cylinder($fn=64,h=h,d=D+2*Br_Magic,center=true); 
        cylinder($fn=64,h=h+10,d=D-2,center=true); 
    }
}


module 013_Arror(h = 0.3)
{
    translate([0,0,-h*2/2])
    {
        cube([7,1.5, h*2]);
        translate([7,1.5/2,0])cylinder($fn=3,h=h*2,d=4,center=false); 
    } 
}

//==============================
// Create parts as required
module 013_4x4_bluetooth_RXTX(topBot="top")
{
    unitsX = 4;
    unitsY = 4;
    unitsZ = 3;
    zShift = Br_HeightUnit * unitsZ - Br_BottomTotalHeight;
    
    led_x = (13.8*0)+14.5-Br_SFRed/2;
    led_y = (-14*0)-10.5;
    
    if(topBot == "topTx" || topBot == "topRx")
    {
        difference()
        {
            union()
            {
                outputNo = (topBot == "topRx")? 12 : 0;
                con = (topBot == "topTx")? Generic_con_4x4_left_only : Generic_con_4x4_left_right;
                
                Br_top(unitsX=unitsX, unitsY=unitsY, unitsZ=unitsZ, con=con, mountingHoles=Generic_mountingHoles_4x4, pcbSize=Generic_pcbSize, inputCount=0, outputCount=outputNo, modifiersCount = 0);
                translate([led_x, led_y,0])lightGuideAdd();
            }
            union()
            {
                translate([0, 0, zShift])
                {
                    translate([-13,13, 0])013_bluetooth();
                    if(topBot == "topTx")
                    {
                        translate([-5, 1, 0])rotate(-40, [0,0,1])013_Arror();
                    }
                    else
                    {
                        translate([-3+5, 3-7, 0])rotate(-40+180, [0,0,1])013_Arror();
                    }
                }
                // Holes for LED out ..
                translate([led_x, led_y,zShift])lightGuideRemove();
            }
        }
    }
    else
    {
        Br_bot(unitsX=unitsX, unitsY=unitsY, mountingHoles=Generic_mountingHoles_4x4);
    }
}