include <BrLib.scad>

type = "top";
rotate(0,[1,0,0])007_4x4_3xLed_RGB_singleLed(topBot=type);

//==============================
// Create parts as required
module 007_4x4_3xLed_RGB_singleLed(topBot="top")
{
    unitsX = 4;
    unitsY = 4;
    unitsZ = 3;
    
    if(topBot == "top")
    {
        difference()
        {
            Br_top(unitsX=unitsX, unitsY=unitsY, unitsZ=unitsZ, con=Generic_con_4x4_left_right, mountingHoles=Generic_mountingHoles_4x4, pcbSize=Generic_pcbSize, inputCount=3, outputCount=0);
            
            // Remove material - make holes for connectors
            union()
            {
                h = 100; // height - through all
                dia = 10 + 0.3 ; // diameter + play
                translate([-3.5,0,0]) cylinder($fn=Br_outerFn,h=h,d=dia,center=true);
            }
        }
    }
    else if(topBot=="bot")
    {
        Br_bot(unitsX=unitsX, unitsY=unitsY, mountingHoles=Generic_mountingHoles_4x4);
    }
    else
    {
        007_4x4_3xLed_RGB_singleLed_LedDistance();
    }
}

//==============================
// Create parts as required
module 007_4x4_3xLed_RGB_singleLed_LedDistance()
{  
    diaOut        = 12.5;
    diaIn        = 8.0;
    z = 3+1;          // Distance
    
    translate([0,0,-z/2])
    {
        difference()
        {
            cylinder($fn=Br_outerFn,h=z,d=diaOut,center=true);
        
            // make holes
            union()
            {
               cylinder($fn=Br_outerFn,h=z+Br_Magic,d=diaIn,center=true);
               translate([0,0,-z/2+1/2])cylinder($fn=Br_outerFn,h=1,d=11.2,center=true); 
            }
        }
    }
}