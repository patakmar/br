include <extern/screw/polyScrewThread.scad>
include <BrLib.scad>

type = "lampBody";
SCREW_HOLE = 7.5 + 0.05;
020_4x4_1x_goose_neck_1x_output(type);


//==============================
// Create parts as required
module 020_4x4_1x_goose_neck_1x_output(topBot=type)
{   
    unitsX = 4;
    unitsY = 4;
    unitsZ = 3;
    dia = 10; // diameter + play
    
    // Compensate - the joystick is not aligned correctly on PCB..
    
    if(topBot == "top")
    {
        rotate(180, [1,0,0])top();
    }
    else  if(topBot == "lampBody")
    {
        lamp("body");
    }
    else  if(topBot == "lampLens")
    {
        lamp("lens");
    }
}

module top()
{
    unitsX = 4;
    unitsY = 4;
    unitsZ = 3; 
    
    difference()
    {
        union()
        {
            Br_top(unitsX=unitsX, unitsY=unitsY, unitsZ=unitsZ, con=Generic_con_4x4_left_right, mountingHoles=Generic_mountingHoles_4x4, pcbSize=Generic_pcbSize, inputCount=1);
            
            // Make stronger walls around a hole
            h = 4; // height - through all
            
            z2 = unitsZ * Br_HeightUnit - Br_BottomTotalHeight;
            translate([-Br_SFRed/2,0,z2-h/2]) cylinder($fn=Br_outerFn,h=h,d=SCREW_HOLE+10,center=true);
        }
        
        // Remove material - make holes for connectors
        union()
        {
            h = 100; // height - through all
            translate([-Br_SFRed/2,0,0]) cylinder($fn=Br_outerFn,h=h,d=SCREW_HOLE,center=true);
            
            zOffset = unitsZ * Br_HeightUnit - Br_BottomTotalHeight - 0.499;
            outDia = SCREW_HOLE + 0.5 + 1.6;
            translate([-Br_SFRed/2,0,zOffset])cylinder($fn=Br_outerFn,h=1.5, d = SCREW_HOLE, center=true); 
        }
    }
}

//MakeLampBody
module lamp(type)
{
    H1 = 6;
    H2 = 20;
    D2_wall_size = 2;
    D2 = 20;
    SCREW_HEIGHT = 5;
    SCR_DIA = 17.5;
    
    if(type == "body")
    {
        difference()
        {
            union()
            {
                cylinder($fn=64,h=H1, d1 = 10, d2 = D2, center=false); 
                translate([0,0,H1])cylinder($fn=64,h=H2, d = D2, center=false);
            }
            
            union()
            {
                cylinder($fn=64,h=100, d=SCREW_HOLE, center=true); 
                translate([0,0,H1])cylinder($fn=16,h=H2+Br_Magic, d = D2-2*D2_wall_size, center=false); 
                
                // make nut inside
                translate([0,0,H1+H2-SCREW_HEIGHT])hex_screw(SCR_DIA,2,55,SCREW_HEIGHT+1,1.5,2,24,0,0,0);
            }
        }
        
        difference()
        {
            H = 2;
            union()
            {
                translate([0,0,H1])cube([8.5,8.5,H], center=true);//cylinder($fn=16,h=3, d = D2-2*D2_wall_size-0.5, center=false);
                
            }
            union()
            {
                translate([0,0,H1+1.5])cylinder($fn=64,h=2, d = 11.2, center=false);
                translate([0,0,H1])cylinder($fn=64,h=100, d = 9, center=true);
            }
        }
    }
    else if(type == "lens")
    {
        TOP_H = 2;
        CHAMPFER = 1;
        difference()
        {
            union()
            {
                hex_screw(SCR_DIA-0.3,2,55,SCREW_HEIGHT,1.5,2,24,0,0,0);
                translate([0,0,-TOP_H])
                {
                    
                    cylinder($fn=64,h=TOP_H, d = D2, center=false); 
                    translate([0,0,-CHAMPFER/2])cylinder($fn=64,h=CHAMPFER, d1 = D2-CHAMPFER, d2 = D2, center=true); 
                }
            }
            union()
            {
                translate([0,0,-TOP_H+1.5])
                {
                    cylinder($fn=64,h=100, d = 10, center=false); 
                }
            }
        }
    }
}
