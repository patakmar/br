use <extern/brickify-master/brickify.scad>;

module brick()
{
    compat = "duplo"; // [lego, duplo]
    shape = "rectangle"; // [rectangle, oval, triangle, blob, heart, wing, star, text]
    height = 2; // [2,3,6,12]
    round_corners = 0.2; // .1
    xscale = 4; // .125
    yscale = 4; // .125
    text = "C";
    no_studs=true;
    full_splines=true;

    brickify(std=compat,units="studs",no_studs=no_studs,height=height,full_splines=full_splines)
    offset(r=round_corners) offset(delta=-round_corners)
    scale([xscale,yscale])
    {
        if (shape=="rectangle") square(1);
    }
}

module armHolder()
{
    difference()
    {
        union()
        {
            brick();
        }
        union()
        {
            translate([9.5,9.5,0])
            {
                D = 3.2;
                translate([0,0,0])cylinder($fn=16, h=100, d=D);
                translate([0,45,0])cylinder($fn=16, h=100, d=D);
                translate([45,0,0])cylinder($fn=16, h=100, d=D);
                translate([45,45,])cylinder($fn=16, h=100, d=D);
                
                translate([(45-25)/2,(45-14)/2])cube([25, 14, 100], centre=true);
            }
        }
    }
}

armHolder();
