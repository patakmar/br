include <BrLib.scad>

type="top";
rotate(180,[1,0,0])006_4x4_1xUSB_power_only_PWM_control(topBot=type);

//==============================
// Create parts as required
module 006_4x4_1xUSB_power_only_PWM_control(topBot="top")
{
    unitsX = 4;
    unitsY = 4;
    unitsZ = 4;
    
    if(topBot == "top")
    {
        difference()
        {
            magicXShift = -1.5+5 - 1.5;
            xd = 7 + 4;
            yd = 14.5 + 4;
            zx = 15;
            union()
            {
                Br_top(unitsX=unitsX, unitsY=unitsY, unitsZ=unitsZ, con=Generic_con_4x4_left_right, mountingHoles=Generic_mountingHoles_4x4, pcbSize=Generic_pcbSize, inputCount=1, outputCount=0);
                
                WallsThickness = 2*2;
                
                zShift = unitsZ*Br_HeightUnit-Br_BottomTotalHeight-zx/2;
                translate([magicXShift,0,zShift])
                {
                    cube([xd+WallsThickness, yd+WallsThickness, zx], center=true);
                }
            }
            
            // Remove material - make holes for connectors
            union()
            {
                translate([magicXShift,0,0])
                {
                    cube([xd, yd, 100], center=true);
                }
            }
        }
    }
    else
    {
        Br_bot(unitsX=unitsX, unitsY=unitsY, mountingHoles=Generic_mountingHoles_4x4);
    }
}