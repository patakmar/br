include<wheelsExtra.scad>
use<wheels.scad>

type = "BRICK6x2";
compat = "duplo"; // [lego, duplo]

brick6x2();

module brick6x2()
{
    difference()
    {
        union()
        {
            translate([0,0,-Br_HeightUnit])
            {
                translate([0,0,0])duplo2x2Socket();
                translate([0,2*Br_Unit,0])duplo2x2Socket();
                translate([0,-2*Br_Unit,0])duplo2x2Socket();
            }
            
            H = Br_HeightUnit * 2;
            translate([0,0,H/2])cube([2*Br_Unit, 6*Br_Unit, H], center = true);
            
            for(i=[0:11])
            {
                X = -Br_Unit/2 + floor(i / 6) * Br_Unit;
                Y = -Br_Unit*2.5 + Br_Unit * (i % 6);
                translate([X,Y,Br_KnobL/2+H])topBump(0);
            }
            
        }
        union()
        {
            H = 5.5;
            D = 16+0.5;
            Offset = 8;
            translate([0,-3*Br_Unit+H/2,Offset])rotate(90,[1,0,0])cylinder($fn=32, d = D, h = H, center = true);
            translate([0,+3*Br_Unit-H/2,Offset])rotate(90,[1,0,0])cylinder($fn=32, d = D, h = H, center = true);
            translate([0,0,Offset])rotate(90,[1,0,0])cylinder($fn=32, d = 6, h = 100, center = true);
            
        }
    }
}

module wheel()
{
    D = 53;
    H = 10;
    difference()
    {
        union()
        {
            translate([0,0,H/2-0.5])cylinder($fn=64, d1 = D, d2 = D-1, h = 1, center = true);
            cylinder($fn=64, d = D, h = H-2, center = true);
            translate([0,0,-H/2+0.5])cylinder($fn=64, d2 = D, d1 = D-1, h = 1, center = true);
            
            translate([0,0,H/2])cylinder($fn=64, d = 7, h = 1, center = true);
        }
        union()
        {
            H2 = 3;
            
            translate([0,0,H2+0.3])#cylinder($fn=32, d = 4.8, h = H, center = true);
            
            translate([0,0,-H/2+H2/2])cylinder($fn=64, d = 10, h = 3, center = true);
            
            translate([0,0,-1.5])ring(D/2,1);
            translate([0,0,1.5])ring(D/2,1);
        }
    }
}