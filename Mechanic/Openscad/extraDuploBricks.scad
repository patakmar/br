include <BrLib.scad>
include<wheelsExtra.scad>

type = "BRICK2x2x3_WITH_HOLE_SCREW";
compat = "duplo"; // [lego, duplo]

UNIT_HEIGHT = 3.2;
DIA = 11;
            
extraDuploBricks(type);

module extraDuploBricks(type)
{ 
    if(type=="RIGHT_ANGLE_SOCKET_PLUG")rotate(-135, [0,1,0])rightAngleSocketPlug();
    if(type=="RIGHT_ANGLE_SOCKET_SOCKET")rotate(-135, [0,1,0])rightAngleSocketSocket();
    if(type=="RIGHT_ANGLE_PLUG_PLUG")rotate(-135, [0,1,0])rightAnglePlugPlug();
    if(type=="BRICK2x2x3_WITH_HOLE")rotate(0, [0,1,0])brick2x2WithHole();
    if(type=="BRICK2x2x3_WITH_HOLE_SCREW")rotate(0, [0,1,0])brick2x2WithHole(screw=true);
}

module plug2x2(height)
{
    translate([0,0,-Br_HeightUnit])
    {
        duplo2x2Socket();
    }
    
    B = Br_Unit;
    cube([2*B,2*B,height], center=true);
}

module rightAngleSocketPlug()
{
    B = Br_Unit;
    
    difference()
    {
        union()
        {
            plug2x2(3);
            
            translate([-B,-B,0])
            {
                cube([2*B,2*B,2*B]);
            }
            
            // Bumps
            rotate(90,[0,1,0])
            {
                translate([-Br_Unit,0,Br_KnobL/2+Br_Unit])
                {
                    H_offset = 0;
                    U = Br_Unit/2;
                    translate([U,U,H_offset])topBump();
                    translate([-U,U,H_offset])topBump();
                    translate([U,-U,H_offset])topBump();
                    translate([-U,-U,H_offset])topBump();
                }
            }
        }
        union()
        {
            translate([-4.9*B,-2*B,2*B])
            {
                rotate(45, [0,1,0])
                {
                    cube([4*B,4*B,4*B]);
                }
            }
            
             // In one of the socket - make the hole so that the shaft can go through
            translate([0,0,B]) rotate(90,[0,1,0])cylinder($fn=Br_outerFn, d = DIA, h = 100, center = true);
            translate([0,0,B]) rotate(0,[0,1,0])cylinder($fn=Br_outerFn, d = DIA, h = 100, center = true);
        }
    }  
}

module rightAnglePlugPlug()
{
    B = Br_Unit;
    
    difference()
    {
        union()
        {
            translate([-B,-B,0])
            {
                cube([2*B,2*B,2*B]);
            }
            
            // Bumps
            rotate(180,[0,1,0])
            {
                translate([0,0,Br_KnobL/2])
                {
                    H_offset = 0;
                    U = Br_Unit/2;
                    translate([U,U,H_offset])topBump();
                    translate([-U,U,H_offset])topBump();
                    translate([U,-U,H_offset])topBump();
                    translate([-U,-U,H_offset])topBump();
                }
            }
            
            // Bumps
            rotate(90,[0,1,0])
            {
                translate([-Br_Unit,0,Br_KnobL/2+Br_Unit])
                {
                    H_offset = 0;
                    U = Br_Unit/2;
                    translate([U,U,H_offset])topBump();
                    translate([-U,U,H_offset])topBump();
                    translate([U,-U,H_offset])topBump();
                    translate([-U,-U,H_offset])topBump();
                }
            }
        }
        union()
        {
            translate([-4.9*B,-2*B,2*B])
            {
                rotate(45, [0,1,0])
                {
                    cube([4*B,4*B,4*B]);
                }
            }
            
             // In one of the socket - make the hole so that the shaft can go through
            translate([0,0,B]) rotate(90,[0,1,0])cylinder($fn=Br_outerFn, d = DIA, h = 100, center = true);
            translate([0,0,B]) rotate(00,[0,1,0])cylinder($fn=Br_outerFn, d = DIA, h = 100, center = true);
        }
    }  
}

module rightAngleSocketSocket()
{
    B = Br_Unit;
    
    difference()
    {
        union()
        {
            plug2x2(3);
            
            translate([-B,-B,0])
            {
                cube([2*B-2*UNIT_HEIGHT,2*B,2*B]);
            }
            
            // Bumps
            rotate(90,[0,1,0])
            {
                translate([-Br_Unit,0,+Br_Unit-2*UNIT_HEIGHT])
                {
                    rotate(180, [0,1,0])
                    {
                        plug2x2(2);
                    }
                }
            }
        }
        union()
        {
            // remove from brick the half ...
            translate([-4.9*B-2*UNIT_HEIGHT,-2*B,2*B])
            {
                rotate(45, [0,1,0])
                {
                    cube([4*B,4*B,4*B]);
                }
            }
            
            // In one of the socket - make the hole so that the shaft can go through
            translate([0,0,B]) rotate(90,[0,1,0])cylinder($fn=Br_outerFn, d = DIA, h = 100, center = true);
        }
    }  
}

module brick2x2WithHole(screw=false)
{
    z = Br_HeightUnit * 2;
    U = Br_Unit/2;
    
    difference()
    {
        union()
        {
            plug2x2(0);
            translate([0,0,-2])
            {   
                makeEmptyBox(2 * Br_Unit, 2 * Br_Unit, z+2, edgesRadius=Br_RoundRadius-0.3, roundBottom=0.8);
            }
            
            translate([0,0,z/2])cube([2 * Br_Unit-1.5,2 * Br_Unit-1.5, z], center=true);
            
            // Bumps on Top
            translate([U,U,z+Br_KnobL/2])topBump();
            translate([U,-U,z+Br_KnobL/2])topBump();
            translate([-U,U,z+Br_KnobL/2])topBump();
            translate([-U,-U,z+Br_KnobL/2])topBump();
        }
        union()
        {
           if(screw)
           {
                H_TOT = Br_HeightUnit * 3;
                translate([0,0,-Br_HeightUnit])screwAddRemove(H_TOT+1, remove=true);
                translate([0,0,-Br_HeightUnit])cylinder($fn=32,h=1, d1 = SCR_DIA+2, d2 = SCR_DIA,center=true); 
                translate([0,0,2*Br_HeightUnit])cylinder($fn=32,h=1, d2 = SCR_DIA+2, d1 = SCR_DIA,center=true); 
                W_TOT = 2*Br_Unit;
                translate([0,0,8]) rotate(90,[0,1,0])
                {
                    translate([0,0,-Br_Unit-0.5])screwAddRemove(W_TOT+1, remove=true);
                    translate([0,0,-Br_Unit])cylinder($fn=32,h=1, d1 = SCR_DIA+2, d2 = SCR_DIA,center=true); 
                    translate([0,0,Br_Unit])cylinder($fn=32,h=1, d2 = SCR_DIA+2, d1 = SCR_DIA,center=true);
                }
           }
           else
           {
                cylinder($fn=Br_outerFn, d = DIA, h = 100, center = true); 
                translate([0,0,8]) rotate(90,[0,1,0])cylinder($fn=Br_outerFn, d = DIA, h = 100, center = true); 
           }
        }
    }   
}