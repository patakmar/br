include <BrLib.scad>

type = "top";
rotate(180,[1,0,0])002_4x4_3xLed_semaphore(type);

//==============================
// Create parts as required
module 002_4x4_3xLed_semaphore(topBot="top")
{   
    unitsX = 4;
    unitsY = 4;
    unitsZ = 3;
    
    if(topBot == "top")
    {
        difference()
        {
            Br_top(unitsX=unitsX, unitsY=unitsY, unitsZ=unitsZ, con=Generic_con_4x4_left_right, mountingHoles=Generic_mountingHoles_4x4, pcbSize=Generic_pcbSize, inputCount=3, outputCount=0);
            
            // Remove material - make holes for connectors
            union()
            {
                h = 100; // height - through all
                dia = 10 + 0.3 ; // diameter + play
                translate([-13,0,0]) cylinder($fn=Br_outerFn,h=h,d=dia,center=true);
                translate([0,0,0]) cylinder($fn=Br_outerFn,h=h,d=dia,center=true);
                translate([14,0,0]) cylinder($fn=Br_outerFn,h=h,d=dia,center=true);
                
                // Make markings 1/2/3 for LEDS
                align = [40, "center"];
                z = Br_HeightUnit*unitsZ - Br_BottomTotalHeight;
                
                translate([0,-7,z-Br_fontDepth])rotate(270,[0,0,1])
                {
                    linear_extrude(Br_fontDepth+Br_Magic)
                    {
                        translate([0,14,0]) text("1", size = 6, font=Br_Font, valign = "center");
                        translate([0,0,0]) text("2", size = 6, font=Br_Font, valign = "center");
                        translate([0,-13,0]) text("3", size = 6, font=Br_Font, valign = "center");
                    }
                }
            }
        }
    }
    else if(topBot == "bot")
    {
        Br_bot(unitsX=unitsX, unitsY=unitsY, mountingHoles=Generic_mountingHoles_4x4);
    }
    else
    {
        // Else print washer
        002_4x4_3xLed_semaphore_LedDistance();
    }
}

//==============================
// Create parts as required
module 002_4x4_3xLed_semaphore_LedDistance()
{  
    x = 31;
    y = 10;
    z = 3;          // Distance
    
    translate([0,0,-z/2])
    {
        difference()
        {
            cube([x,y,z], center=true);
        
            // make holes
            union()
            {
                dia         = 2.0;
                yDistance   = 2.5;
                h           = 100; // through all 
                
                translate([-14,-yDistance/2,0]) cylinder($fn=Br_outerFn,h=h,d=dia,center=true);
                translate([-14,+yDistance/2,0]) cylinder($fn=Br_outerFn,h=h,d=dia,center=true);
                translate([0,-yDistance/2,0]) cylinder($fn=Br_outerFn,h=h,d=dia,center=true);
                translate([0,+yDistance/2,0]) cylinder($fn=Br_outerFn,h=h,d=dia,center=true);
                translate([13,-yDistance/2,0]) cylinder($fn=Br_outerFn,h=h,d=dia,center=true);
                translate([13,+yDistance/2,0]) cylinder($fn=Br_outerFn,h=h,d=dia,center=true);
            }
        }
    }
}