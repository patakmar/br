/**
 * Briskel kids - main file
 * 
 * Bref: Bluetooth - master module
 *
 * Author: Martin Patak
 */

#include <string.h>
#include <intrinsics.h>
#include <stdlib.h>
#include "CommHeader.h"
#include "boot.h"
#include "watchdog.h"
#include "uart.h"
#include "eeprom.h"

#define FW_VERSION 0x10 // 1.0
#define BOARD_NO 13

#define PWR_GPIO_PORT  GPIOB
#define PWR_GPIO_PINS  GPIO_Pin_5
#define STATE_GPIO_PORT  GPIOB
#define STATE_GPIO_PINS  GPIO_Pin_6
#define LED_GPIO_PORT  GPIOB
#define LED_GPIO_PINS  GPIO_Pin_1
#define KEY_GPIO_PORT  GPIOC
#define KEY_GPIO_PINS  GPIO_Pin_4
#define BTN_GPIO_PORT  GPIOB
#define BTN_GPIO_PINS  GPIO_Pin_0
#define RST_GPIO_PORT  GPIOB
#define RST_GPIO_PINS  GPIO_Pin_3

#define LED_FLASHING_PERIOD 300

#define UART_GPIO_TX_PORT       GPIOC
#define UART_GPIO_TX_PINS       GPIO_Pin_5
#define UART_GPIO_RX_BOARD_PORT       GPIOA
#define UART_GPIO_RX_BOARD_PINS       GPIO_Pin_3

#define SET_AT "AT\r\n"
#define SET_RESET "AT+RESETT\r\n"
#define SET_REMOVE_PAIRED "AT+RMAAD\r\n" // remove all paired devices
#define SET_AS_MASTER "AT+ROLE=1\r\n"
#define SET_BAUDRATE "AT+UART=115200,0,0\r\n"
#define SET_CMODE "AT+CMODE=1\r\n"
#define SET_CMODE_DISABLE "AT+CMODE=0\r\n"

static bool newPacketReceived = FALSE;

typedef struct  
{
  u8 bluetoothConfigured;
} Settings;

static Settings settings;

/**
 * Configure HSI clock - HSI - input 16 MHz
 * divide by x - CPU runs at 16/x MHz
 */
static void ConfigureHSIClock()
{
  CLK_SYSCLKDivConfig(CLK_SYSCLKDiv_8);
}

/**
 * Function called when communication is lost
 */
void communicationLostCallback()
{
}

#define SENT_EVERY_XTH_PACKET 5
static u8 sendXthPacket = 0;

/**
 * Callback for a received package
 * @param package - pointer to received package
 */
void receivedPackageCallback(COMM_HEADER * package)
{
  newPacketReceived = TRUE;
  if(sendXthPacket == SENT_EVERY_XTH_PACKET-1)
  {
    sendXthPacket = 0;
    uartProcessNewPacket(package);
    UART_sendPacketDMA((package));
  }
  else
  {
    sendXthPacket++;
  }
}

/**
 * Set LED on or off
 */
void setLed(u8 on)
{
  if(on)
  {
    GPIO_SetBits(LED_GPIO_PORT, LED_GPIO_PINS);
  }
  else
  {
    GPIO_ResetBits(LED_GPIO_PORT, LED_GPIO_PINS);
  }
}

/**
 * Obtain state from the bluetooth board
 */
u8 getState()
{
  u8 state = GPIO_ReadInputDataBit(STATE_GPIO_PORT, STATE_GPIO_PINS);
  return state;
}

/**
 * Send data to UART 
 */
void uartTx(const char * msg, u8 length)
{
  while(length > 0)
  {
    while(!USART_GetFlagStatus(USART1, USART_FLAG_TXE)); // Wait till buffer Tx is empty
    USART_SendData8(USART1, msg[0]);
    length--;
    msg++;
  }
  
  while(!USART_GetFlagStatus(USART1, USART_FLAG_TXE)); // Wait till buffer Tx is empty
}

/**
 * Read data from uart in pooling way
 * return 1 if not ended by \n
 */
u8 uartRead(char * buffer, u8 maxLength)
{
  u8 retVal = 1;
  // Response is on the other channel
  SYSCFG_REMAPPinConfig(REMAP_Pin_USART1TxRxPortA, ENABLE);
  
  // receive if something is in the buffer
  USART_ReceiveData8(USART1);
  USART_ClearFlag(USART1, USART_FLAG_OR);
  
  u16 timeout;
  
  while(maxLength > 0)
  {
    timeout = 1000;
    while(!USART_GetFlagStatus(USART1, USART_FLAG_RXNE) && timeout-- > 0); // Wait till buffer Tx is empty
    buffer[0] = USART_ReceiveData8(USART1);
    
    if(buffer[0] == '\n')
    {
      retVal = 0;
      break;
    }
    else if(timeout == 0) // Or timeout..
    {
      break;
    }
    else
    {
      buffer++;
      maxLength--;
    }
  }
  
  // Go back to original channel
  SYSCFG_REMAPPinConfig(REMAP_Pin_USART1TxRxPortA, DISABLE);
  
  return retVal;
}

/**
 * Simple delay
 */
void delay(u16 value)
{
  while(value-- > 0);
}

/**
 * Reset bluetooth
 */
void resetBluetooth()
{
  watchdogKick();
  GPIO_Init(RST_GPIO_PORT, RST_GPIO_PINS, GPIO_Mode_Out_PP_Low_Slow);
  delay(1500);
  GPIO_Init(RST_GPIO_PORT, RST_GPIO_PINS, GPIO_Mode_Out_PP_High_Slow);
  delay(15000);
  
  watchdogKick();
}

// Send command - return 1 if string was not connected by \n 
u8 sendCommand(const char * data, u8 length)
{
  char buff[60];
  u8 retVal;
  
  watchdogKick();
  memset(buff, 0, sizeof(buff));
  uartTx(data, length);
  retVal = uartRead(buff, sizeof(buff));
  
  delay(1000);
  return retVal;
}

/**
 * Configure device
 */
void configureBluetooth()
{
  u16 timer;
  u8 errors = 0;
  watchdogKick();
  
  // Initialise UART
  GPIO_Init(UART_GPIO_TX_PORT, UART_GPIO_TX_PINS, GPIO_Mode_Out_PP_High_Slow);
  GPIO_Init(UART_GPIO_RX_BOARD_PORT, UART_GPIO_RX_BOARD_PINS, GPIO_Mode_In_PU_No_IT);
  CLK_PeripheralClockConfig(CLK_Peripheral_USART1, ENABLE);
  USART_Init(USART1, 38400, USART_WordLength_8b, USART_StopBits_1, USART_Parity_No, (USART_Mode_TypeDef)(USART_Mode_Rx | USART_Mode_Tx));
  USART_Cmd(USART1, ENABLE);
  
  // Reset and enter AT mode
  GPIO_Init(KEY_GPIO_PORT, KEY_GPIO_PINS, GPIO_Mode_Out_PP_High_Slow);
  resetBluetooth();

  errors += sendCommand(SET_AT, sizeof(SET_AT)-1);
  errors += sendCommand(SET_AT, sizeof(SET_AT)-1);
  errors += sendCommand(SET_REMOVE_PAIRED, sizeof(SET_REMOVE_PAIRED)-1);
  errors += sendCommand(SET_RESET, sizeof(SET_RESET)-1);
  delay(30000);
  
  errors += sendCommand(SET_AT, sizeof(SET_AT)-1);  
  errors += sendCommand(SET_AS_MASTER, sizeof(SET_AS_MASTER)-1);
  errors += sendCommand(SET_BAUDRATE, sizeof(SET_BAUDRATE)-1);
  
  #define DEVICE_ADDR "AT+BIND=98d3, b1, fd8583\r\n"
  errors += sendCommand(DEVICE_ADDR, sizeof(DEVICE_ADDR)-1);
  
  
  // Reset in normal mode
  GPIO_Init(KEY_GPIO_PORT, KEY_GPIO_PINS, GPIO_Mode_Out_PP_Low_Slow);
  resetBluetooth();
  
  
  // Store settings
  settings.bluetoothConfigured = 1;
  storeSettings((u8*)&settings, sizeof(Settings));
}

/** 
 * Function reads settings from EEPROM
*/
void restoreSettings()
{
  if(readSettings((u8*)&settings, sizeof(Settings)) == FALSE)
  { // No settings found - use default
    settings.bluetoothConfigured = 0;
  }
}

/**
 * Main loop
 */
int main( void )
{ 
  //storeMac(13, 2, 0);
  
  // Check boar version before interrupts are enabled
  configureCommunicationProtocol(0,0, FW_VERSION, BOARD_NO);
  
  ConfigureHSIClock();
  
  if(isAppValidated())
  { // Watchdog only for validated application - not for development, ...  
    watchdogInit(); 
  }
  
  GPIO_Init(LED_GPIO_PORT, LED_GPIO_PINS, GPIO_Mode_Out_PP_High_Slow);
  
  // Enable Bluetooth power
  GPIO_Init(KEY_GPIO_PORT, KEY_GPIO_PINS, GPIO_Mode_Out_PP_Low_Slow);
  GPIO_Init(RST_GPIO_PORT, RST_GPIO_PINS, GPIO_Mode_Out_PP_High_Slow); // That is release
  GPIO_Init(PWR_GPIO_PORT, PWR_GPIO_PINS, GPIO_Mode_Out_PP_Low_Slow);
  
  restoreSettings();
  
  if(settings.bluetoothConfigured == 0)
  { // Button pressed - go to this state - it will never return
    configureBluetooth();
  }
  
  UART_init();
  __enable_interrupt();
  
  while(1)
  {   
    if(newPacketReceived)
    {
      newPacketReceived = FALSE;
      
      if(GPIO_ReadInputDataBit(STATE_GPIO_PORT, STATE_GPIO_PINS))
      {
        setLed(1);
      }
      else
      {
        setLed(0);
      }
      
      watchdogKick();
    }
  }
}
