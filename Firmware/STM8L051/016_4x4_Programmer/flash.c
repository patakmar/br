/**
 * Briskel kids - flash driver
 * 
 * Bref: handle basic flahs operations..
 *
 * Author: Martin Patak
 */

#include "flash.h"

static u32 tmpAddress;

void flash_setCsLow(){GPIO_ResetBits(SPI_FLASH_CSN_GPIO_PORT, SPI_FLASH_CSN_GPIO_PINS);}
void flash_setCsHigh(){GPIO_SetBits(SPI_FLASH_CSN_GPIO_PORT, SPI_FLASH_CSN_GPIO_PINS);}

/**
 * Initialise SPI and SS
 */
void initSpi()
{
  GPIO_Init(SPI_SCK_GPIO_PORT, SPI_SCK_GPIO_PINS, GPIO_Mode_Out_PP_Low_Fast);
  GPIO_Init(SPI_MOSI_GPIO_PORT, SPI_MOSI_GPIO_PINS, GPIO_Mode_Out_PP_Low_Fast);
  GPIO_Init(SPI_FLASH_CSN_GPIO_PORT, SPI_FLASH_CSN_GPIO_PINS, GPIO_Mode_Out_PP_High_Fast);

  /* Initialize SPI in Master mode */
  CLK_PeripheralClockConfig(CLK_Peripheral_SPI1, ENABLE);
  SPI_DeInit(SPI1);
  SPI_Init(SPI1, 
         SPI_FirstBit_MSB, 
         SPI_BaudRatePrescaler_4, 
         SPI_Mode_Master, 
         SPI_CPOL_Low, 
         SPI_CPHA_1Edge, 
         SPI_Direction_2Lines_FullDuplex, 
         SPI_NSS_Soft,
         0x07);
  SPI_Cmd(SPI1, ENABLE);
}



/**
 * Return true if operation is ongoing
 */
bool flash_isBusy()
{
  u8 tmp;
  flash_setCsLow();
  tmp = FLASH_REG_STATUS_REG_RD;
  spi_sendSpiData(&tmp, 1);
  
  spi_readSpiData(&tmp, 1);
  flash_setCsHigh();
  
  return (bool)(tmp & FLASH_REG_STATUS_REG_RD_BUSY);
}

/** 
 * Set write enable bit
 */
void flash_setWriteEnable()
{
  u8 tmp;
  flash_setCsLow();
  tmp = FLASH_REG_WRITE_ENABLE;
  spi_sendSpiData(&tmp, 1);
  flash_setCsHigh();
}

/**
 * Read data from the flash
 */
void flash_read(u32 address, u8 * data, u8 length)
{
  u8 tmp;
  flash_setCsLow();
  tmp = FLASH_REG_READ;
  spi_sendSpiData(&tmp, 1);
  // Send address
  tmp = address >> 16;
  spi_sendSpiData(&tmp, 1);
  tmp = address >> 8;
  spi_sendSpiData(&tmp, 1);
  tmp = address >> 0;
  spi_sendSpiData(&tmp, 1);
  // Read data
  spi_readSpiData(data, length);
  flash_setCsHigh();
}

void flash_writePage(u32 address, u8 * data, u8 length)
{
  u8 tmp;
  
  while(flash_isBusy()); // Wait until flash is ready
  
  flash_setWriteEnable();
  
  flash_setCsLow();
  tmp = FLASH_REG_WRITE_PAGE;
  spi_sendSpiData(&tmp, 1);
  // Send address
  tmp = address >> 16;
  spi_sendSpiData(&tmp, 1);
  tmp = address >> 8;
  spi_sendSpiData(&tmp, 1);
  tmp = address >> 0;
  spi_sendSpiData(&tmp, 1);
  // Read data
  spi_sendSpiData(data, length);
  flash_setCsHigh();
}

/** 
 * Erase whole block: 64 kb
 */
void flash_blockErase(u32 address)
{
   u8 tmp;
  
  while(flash_isBusy()); // Wait until flash is ready
  
  flash_setWriteEnable();
  
  flash_setCsLow();
  tmp = FLASH_REG_BLOCK_ERASE;
  spi_sendSpiData(&tmp, 1);
  // Send address
  tmp = address >> 16;
  spi_sendSpiData(&tmp, 1);
  tmp = address >> 8;
  spi_sendSpiData(&tmp, 1);
  tmp = address >> 0;
  spi_sendSpiData(&tmp, 1);
  flash_setCsHigh();
}

/** 
 * Erase whole memory
 */
void flash_erase_whole_mem()
{
  u8 tmp;
  while(flash_isBusy()); // Wait until flash is ready
  
  flash_setWriteEnable();
  flash_setCsLow();
  tmp = FLASH_ERASE_ALL;
  spi_sendSpiData(&tmp, 1);
  flash_setCsHigh();
}

/** 
 * Send data to SPI
 */
void spi_sendSpiData(u8 * data, u8 length)
{
  while(length-- > 0)
  {
    SPI_SendData(SPI1, data[0]);
    data++;
    while(SPI_GetFlagStatus(SPI1, SPI_FLAG_BSY)); // wait until transmission finished
    SPI_ReceiveData(SPI1); // read data and ignore it..
  }
}

/** 
 * Read data from SPI
 */
void spi_readSpiData(u8 * data, u8 length)
{
  while(length > 0)
  {
    SPI_SendData(SPI1, 0);
    length--;
    while(SPI_GetFlagStatus(SPI1, SPI_FLAG_BSY)); // wait until transmission finished
    data[0] = SPI_ReceiveData(SPI1);
    data++;
  }
}

/** 
 * Erase the whole sector for program 0 to 3, if -1 is given reset flash address counter
 * Principle is following - try to read whether device is BUSY or not ..
 * If yes - just return false
 * if not - read data - from upper secrots to lower one and if the first byte from
 * sector is not equal to 0xFF -> then erase whole sector
 * repeat it until the whole used memory is empty 
 * return true when finished
 */
bool ProgramEraseData(s8 progNo)
{
  u32 addr, tmp;
  if(progNo < 0)
  { // Initialise programming address only
    tmpAddress = (u32)ERASE_START; 
    return FALSE;
  }
  
  if(flash_isBusy()) return FALSE;
  
  addr = tmpAddress + progNo * PROGRAM_SIZE;
  flash_read(addr, (u8*)&tmp, 4);
  
  if(tmp != 0xFFFFFFFF)
  { 
    flash_blockErase(tmpAddress + progNo * PROGRAM_SIZE);
    if(tmpAddress != 0)
    {
      tmpAddress -= BLOCK_SIZE;
    }
  }
  else if(tmpAddress == 0)
  {
    return TRUE; // Finished
  }
  else
  {
    tmpAddress -= BLOCK_SIZE;
  }
  
  return FALSE;
}
