/**
 * Briskel kids - flash driver
 * 
 * Bref: handle basic flahs operations..
 *
 * Author: Martin Patak
 */

#ifndef FLASH_FILE_H
#define FLASH_FILE_H
   
#include "stm8l15x_spi.h"


#define SPI_FLASH_CSN_GPIO_PORT  GPIOB
#define SPI_FLASH_CSN_GPIO_PINS  GPIO_Pin_4
#define SPI_SCK_GPIO_PORT  GPIOB
#define SPI_SCK_GPIO_PINS  GPIO_Pin_5
#define SPI_MOSI_GPIO_PORT  GPIOB
#define SPI_MOSI_GPIO_PINS  GPIO_Pin_6

#define FLASH_REG_WRITE_PAGE            0x02
#define FLASH_REG_READ                  0x03
#define FLASH_REG_STATUS_REG_RD         0x05
#define FLASH_REG_STATUS_REG_RD_BUSY         0x01
#define FLASH_REG_WRITE_ENABLE          0x06
#define FLASH_ERASE_ALL                 0xC7
#define FLASH_REG_BLOCK_ERASE           0xD8

#define BLOCK_SIZE ((u32)64*1024)
#define WHOLE_MEM_SIZE ((u32)16*1024*1024)
#define PROGRAM_SIZE (WHOLE_MEM_SIZE/4)
#define ERASE_START (PROGRAM_SIZE-BLOCK_SIZE)

void spi_sendSpiData(u8 * data, u8 length);
void spi_readSpiData(u8 * data, u8 length);

void initSpi();
bool flash_isBusy();
void flash_setCsLow();
void flash_setCsHigh();
void flash_setWriteEnable();
void flash_read(u32 address, u8 * data, u8 length);
void flash_writePage(u32 address, u8 * data, u8 length);
void flash_blockErase(u32 address);
void flash_erase_whole_mem();

bool ProgramEraseData(s8 progNo);

#endif