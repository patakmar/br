/**
 * Briskel kids - main file
 * 
 * Bref: pyro sensor / 
 * Motor/LED : PB2 - TIM2 - CH2
 *
 * Author: Martin Patak
 */

#include <string.h>
#include <intrinsics.h>
#include <stdlib.h>
#include "CommHeader.h"
#include "stm8l15x_spi.h"
#include "boot.h"
#include "watchdog.h"
#include "uart.h"
#include "flash.h"
#include "eeprom.h"
#include "Definitions.h"

#define FW_VERSION 0x10 // 1.0
#define BOARD_NO 16

#define STCP_GPIO_PORT  GPIOB
#define STCP_GPIO_PINS  GPIO_Pin_3

#define STORE_TO_EEPROM_WHEN_NOT_ACTIVE 5*200 // 5 second
#define RECORD_LENGTH 32 // store the packet in this format ..
#define WR_RECORDS_AT_ONE 2 // how many records to write at once ..

#define FLASH_PERIOD_PAUSE 50
#define JUMP_TO_PAUSE_FLAG 0x58
#define BTN_RECORD_TIME (200 * 2)

// Store in following format:
// Byte 0-1 - special flag ...
// Byte 2-32 - storep packet
// This is repeated 2x - so that the length which is written at once is long enough
static u8 TMP_ARR[RECORD_LENGTH*WR_RECORDS_AT_ONE];
static COMM_HEADER latestPacket;
static COMM_HEADER packet2Transmit;

#define BTN_COUNT 7
const GPIO_TypeDef * BTN_TYPEDEF [] = {GPIOB,      GPIOB,      GPIOA,      GPIOA,      GPIOC,      GPIOC,      GPIOC};
const GPIO_Pin_TypeDef BTN_PIN[] =    {GPIO_Pin_1, GPIO_Pin_2, GPIO_Pin_3, GPIO_Pin_2, GPIO_Pin_4, GPIO_Pin_1, GPIO_Pin_0};

#define SPEED_DIVIDER_SLOW 2
#define SPEED_1 1
#define SPEED_2 2
#define SPEED_MAX 4
// Record where we should latest stop recording - make sure it is not recorded to the very end .. therefore -5
const u32 STOP_PROGRAM[4] = {1*PROGRAM_SIZE-(SPEED_MAX+1)*RECORD_LENGTH,
                             2*PROGRAM_SIZE-(SPEED_MAX+1)*RECORD_LENGTH,
                             3*PROGRAM_SIZE-(SPEED_MAX+1)*RECORD_LENGTH,
                             4*PROGRAM_SIZE-(SPEED_MAX+1)*RECORD_LENGTH};
        
enum TRIGGER {TRG_SINGLE=0, TRG_REPEAT=1, TRG_EXT_SINGLE=2, TRG_EXT_REPEAT=3};
enum STATE {PASS_THROUGH, ERASING, RECORDING, PLAY, STOP, PLAY_PAUSE};
enum BUTTON {BTN_STOP=0x01, BTN_PLAY=0x02, BTN_RECORD=0x04, BTN_CANCEL=0x08, BTN_PROGRAM=0x10, BTN_SPEED=0x20, BTN_TRG=0x40};

u8 getBtnRecordPressed()
{
  return GPIO_ReadInputDataBit(GPIOA, GPIO_Pin_3);
}

typedef struct  
{
  u8 program;
  u8 speed;
  enum TRIGGER trigger;
  enum STATE state;
} Settings;

static u8 lastButtons;
static Settings settings;
static u8 ledBtn = 0;
static u32 addr;
static u8 cnt;
static u16 tmp16;
static u16 storetoEEpromCnt = 0;
static enum STATE lastState;

static bool newPacketReceived = FALSE;

/**
 * Configure HSI clock - HSI - input 16 MHz
 * divide by x - CPU runs at 16/x MHz
 */
static void ConfigureHSIClock()
{
  CLK_SYSCLKDivConfig(CLK_SYSCLKDiv_4);
}

/**
 * Function called when communication is lost
 */
void communicationLostCallback()
{
}

/**
 * Callback for a received package
 * @param package - pointer to received package
 */
void receivedPackageCallback(COMM_HEADER * package)
{
  uartProcessNewPacket(package);
  memcpy(&latestPacket, package, sizeof(COMM_HEADER));
  newPacketReceived = TRUE;
  
  if(settings.state == PLAY || settings.state == STOP || settings.state == PLAY_PAUSE)
  {
    UART_sendPacketDMA(&packet2Transmit);
  }
  else
  {
    UART_sendPacketDMA(package);
  }
}

/**
 * Initialise gpio - enable pyrosensor 
 */
void initGpios()
{
  // Init SPI GPIOS
  GPIO_Init(STCP_GPIO_PORT, STCP_GPIO_PINS, GPIO_Mode_Out_PP_Low_Slow);
  initSpi();
}


/**
 * Function set STCP signal high and then low - to apply new values for LEDs
 */
void applayLeds()
{
  volatile u8 i=10;
  u8 ledStatus[2];
  
  ledStatus[1] = ledBtn;
  ledStatus[1] |= 0x80 >> settings.trigger;
  ledStatus[0] = 0x80 >> settings.program;
  ledStatus[0] |= (2 << settings.speed) -1;
  
  
  spi_sendSpiData((u8*)&ledStatus, 2);
  while(i-- > 0);
  GPIO_SetBits(STCP_GPIO_PORT, STCP_GPIO_PINS);
  while(i-- > 0);
  GPIO_ResetBits(STCP_GPIO_PORT, STCP_GPIO_PINS);
}

/**
 * Scan buttons and report only those ones which changed
 */
u8 scanButtons()
{
  u8 newStatus = 0;
  u8 pushedButtons = 0;
  u8 i;
  u8 val = 1;
  for(i=0; i < BTN_COUNT; i++)
  {
    if(GPIO_ReadInputDataBit((GPIO_TypeDef*)BTN_TYPEDEF[i], BTN_PIN[i]))
    { // Button not pressed
    }
    else
    { // Button pressed
      if((lastButtons & val) == 0)
      {
        pushedButtons |= val;
      }
      newStatus |= val;
    }
    
    val = val << 1;
  }
  lastButtons = newStatus;
  return pushedButtons;
}

/**
 * Function starts the play function
 */
void startPlay()
{
  settings.state = PLAY;
  addr = settings.program * PROGRAM_SIZE;
  flash_read(addr, TMP_ARR, RECORD_LENGTH);
  memcpy(&packet2Transmit, &TMP_ARR[2], sizeof(packet2Transmit));
  addr += RECORD_LENGTH;
}
  
/**
 * Function starts erasing followed by recording 
 */
void startErasing()
{
  ProgramEraseData(-1); // Initialise erasing address
  settings.state = ERASING;
  tmp16 = 0;
  cnt = 0;
}

/**
 * Stop ...
 */
void stop()
{
  if(settings.trigger == TRG_REPEAT)
  {
    // we cannot stop when in repeat mode .. because it would jump immediately into next 
    // therefore change the trigger to single ..
    settings.trigger = TRG_SINGLE;
    storetoEEpromCnt = STORE_TO_EEPROM_WHEN_NOT_ACTIVE;
  }
  
  // Try to read address 0 for given programm ... if not found => go to pass through state
  addr = settings.program * PROGRAM_SIZE;
  flash_read(addr, TMP_ARR, RECORD_LENGTH);
  if(TMP_ARR[2] != INIT_CHAR)
  { // End of record -> go to stop
     settings.state = PASS_THROUGH;
  }
  else
  {
      memcpy(&packet2Transmit, &TMP_ARR[2], sizeof(packet2Transmit));
      settings.state = STOP;
  }
}

/** 
 * Function reads settings from EEPROM
*/
void restoreSettings()
{
  if(readSettings((u8*)&settings, sizeof(Settings)) == FALSE)
  { // No settings found - use default
    settings.program = 0;
    settings.speed = 0;
    settings.trigger = TRG_SINGLE;
  }
  
  if(settings.state != PASS_THROUGH)
  {
    stop();
  }
}

/**
 * Main loop
 */
int main( void )
{ 
  u8 btnPressed;
  u8 trgSignal;
  u8 trgSignalLast;
  trgSignal = 0;
  i16 value;
  SETTINGS config;
  u8 pauseRecord;
  u8 pauseRecordLast;
  u16 btnEraseTimer;
  u8 tmp;
  
  ConfigureHSIClock();
  if(isAppValidated())
  { // Watchdog only for validated application - not for development, ...  
    watchdogInit(); 
  }
  UART_init();
  __enable_interrupt();
  
  initGpios();
  configureCommunicationProtocol(0, 1, FW_VERSION, BOARD_NO); 
  lastButtons = 0;
  restoreSettings();
  lastState = settings.state;
    
  while(1)
  {   
    if(newPacketReceived)
    {
      newPacketReceived = FALSE;
      ledBtn = 0;
      
      // Count how long the timer is on .. just cor erase 
      if(getBtnRecordPressed() == 0)
      {
        btnEraseTimer = (btnEraseTimer < 0xFFFF)? btnEraseTimer+1 : 0xFFFF;
      }
      else
      {
        btnEraseTimer = 0;
      }
      
      __disable_interrupt();
      getNewDataFromProtocol(&value, &config); // Process received frame ..
      __enable_interrupt();
      
      if(value > LIMIT_HIGH) trgSignal = 1;
      else if(value < LIMIT_LOW) trgSignal = 0;
      
      // Scan buttons
      btnPressed = scanButtons();
      if((btnPressed & BTN_PROGRAM) && (settings.state == STOP || settings.state == PASS_THROUGH))
      {
        settings.program = (settings.program + 1) % 4;
      }
      if(btnPressed & BTN_SPEED)
      {
        settings.speed = (settings.speed + 1) % 4;
      }
      if(btnPressed & BTN_TRG)
      {
        settings.trigger = (enum TRIGGER)((settings.trigger + 1) % 4);
      }
      if(btnPressed & (BTN_PROGRAM | BTN_SPEED | BTN_TRG))
      {
        storetoEEpromCnt = STORE_TO_EEPROM_WHEN_NOT_ACTIVE;
      }
      
      // Check when to store changed state...
      // Do it only when changing state between pass_through and other states
      if((settings.state == PASS_THROUGH && lastState != PASS_THROUGH)
         ||(settings.state != PASS_THROUGH && lastState == PASS_THROUGH))
      {
        storetoEEpromCnt = STORE_TO_EEPROM_WHEN_NOT_ACTIVE;
      }
      lastState = settings.state;
      
      
      switch(settings.state)
      {
      case PASS_THROUGH:
        ledBtn = BTN_CANCEL;
        if(btnPressed & BTN_PLAY)
        {
          startPlay();
        }
        else if(btnPressed & BTN_CANCEL)
        {
          // stay in the same state
        }
        else if(btnPressed & BTN_STOP)
        {
          stop();
        }
        else if(btnEraseTimer > BTN_RECORD_TIME)
        {
          startErasing();
        }
        break;
        
      case ERASING:
        #define ERASE_FLASH_PERIOD (200/4)
        #define ERASING_TIME_MIN (200*3)
        if(cnt < ERASE_FLASH_PERIOD/2)
        {
          ledBtn = BTN_RECORD;
        }
        cnt = (cnt + 1) % ERASE_FLASH_PERIOD;
        
        if(ProgramEraseData(settings.program) && tmp16++ > ERASING_TIME_MIN)
        {
          settings.state = RECORDING;
          cnt = 0;
          addr = settings.program * PROGRAM_SIZE;
          pauseRecord = 0;
          pauseRecordLast = 0;
        }
        break;
  
      case RECORDING:
        ledBtn = BTN_RECORD;
        if(btnPressed & (BTN_RECORD | BTN_STOP))
        {  // Stop recording
           stop();
        }
        else if(btnPressed & BTN_CANCEL)
        {
          settings.state = PASS_THROUGH;
        }
        else if(addr >= STOP_PROGRAM[settings.program])
        {
          stop();
        }
        else
        {     // Record data to flash ..
          if(btnPressed & BTN_PLAY)
          {  // this pauses recording
            if(pauseRecord) 
            {
              pauseRecord = 0;
            }
            else
            {
              pauseRecord = 1;
            }
          }
          
          if(pauseRecord == 0)
          { // Enable writing when no pause it done ...
            if(pauseRecord == 0 && pauseRecordLast == 1)
            { // Pause was required
              TMP_ARR[cnt*RECORD_LENGTH] = JUMP_TO_PAUSE_FLAG; // Special flag sd
            }
            else
            {
              TMP_ARR[cnt*RECORD_LENGTH] = 0x00; // Special flag 
            }
            TMP_ARR[cnt*RECORD_LENGTH+1] = 0x00; // Special flag
            memcpy(&TMP_ARR[cnt*RECORD_LENGTH+2], &latestPacket, sizeof(COMM_HEADER));
            if(cnt == WR_RECORDS_AT_ONE-1)
            {
              flash_writePage(addr, TMP_ARR, sizeof(TMP_ARR));
              addr += sizeof(TMP_ARR);
              cnt = 0;
            }
            else
            {
              cnt++;
            }
          }
          else
          {
            // when recording is paused => flash with pause 
            tmp16 = (tmp16 > FLASH_PERIOD_PAUSE)? 0 : tmp16+1;
            if(tmp16 > FLASH_PERIOD_PAUSE/2) ledBtn |= BTN_PLAY;
          }
          pauseRecordLast = pauseRecord;
        }
        break;
          
      case PLAY:
        if(btnPressed & BTN_CANCEL)
        {
          settings.state = PASS_THROUGH;
        }
        else if(btnPressed & BTN_STOP)
        {
          stop();
        }
        else if(btnEraseTimer > BTN_RECORD_TIME)
        {
          startErasing();
        }
        else if(btnPressed & BTN_PLAY)
        {  // Make a pause
           settings.state = PLAY_PAUSE;
           if(settings.trigger == TRG_REPEAT)
           {
            // we cannot stop when in repeat mode .. because it would jump immediately into next 
            // therefore change the trigger to single ..
            settings.trigger = TRG_SINGLE;
            storetoEEpromCnt = STORE_TO_EEPROM_WHEN_NOT_ACTIVE;
           }
        }
        else
        {
          ledBtn = BTN_PLAY;
          flash_read(addr, TMP_ARR, RECORD_LENGTH);
          if(TMP_ARR[2] != INIT_CHAR)
          { // End of record -> go to stop
             settings.state = STOP;
          }
          else  if(TMP_ARR[0] == JUMP_TO_PAUSE_FLAG)
          { // Jump to pause is required
            settings.state = PLAY_PAUSE;
            addr += RECORD_LENGTH;
          }
          else
          {
            memcpy(&packet2Transmit, &TMP_ARR[2], sizeof(packet2Transmit));
            
            // Now compute the speed - how fast to process images
            if(settings.speed == 0)
            { // half speed - slower than normal
              if(cnt >= SPEED_DIVIDER_SLOW-1)
              {
                addr += RECORD_LENGTH;
                cnt = 0;
              }
              else
              {
                cnt++;
              }
            }
            else
            {
              // If speed is faster => the readout might miss a pause.. and therefore we need 
              // to read every first byte and if it is a pause .. just stop ..
              if(settings.speed == 1)tmp = SPEED_1;
              if(settings.speed == 2)tmp = SPEED_2;
              if(settings.speed == 3)tmp = SPEED_MAX;
              while(tmp-- > 0)
              {
                addr += RECORD_LENGTH;
                flash_read(addr, TMP_ARR, 1);
                if(TMP_ARR[0] == JUMP_TO_PAUSE_FLAG)
                { // Pause if found .. next this flag needs to be read ..
                  break;
                }
              }
            }
          }
        }
        break;
        
      case PLAY_PAUSE:
        // Flash with LED ..
        if(cnt > FLASH_PERIOD_PAUSE)
        {
          cnt = 0;
        }
        else
        {
          cnt++;
        }
        
        if(cnt < FLASH_PERIOD_PAUSE/2)
        {
          ledBtn = BTN_PLAY;
        }
        
        if(btnPressed & BTN_CANCEL)
        {
          settings.state = PASS_THROUGH;
        }
        else if(btnPressed & BTN_STOP)
        {
          stop();
        }
        else if(btnEraseTimer > BTN_RECORD_TIME)
        {
          startErasing();
        }
        else if(btnPressed & BTN_PLAY || 
                settings.trigger == TRG_REPEAT || 
                (settings.trigger == TRG_EXT_SINGLE && trgSignalLast == 0 && trgSignal == 1) || 
                (settings.trigger == TRG_EXT_REPEAT && trgSignal == 1))
        {  // Release Play again
            settings.state = PLAY;
        }
        break;
    
      case STOP:
        ledBtn = BTN_STOP;
        if(btnPressed & BTN_PLAY)
        {
          startPlay();
        }
        else if(btnPressed & BTN_CANCEL)
        {
          settings.state = PASS_THROUGH;
        }
        else if(btnPressed & BTN_STOP)
        {
          stop();
        }
        else if(btnEraseTimer > BTN_RECORD_TIME)
        {
          startErasing();
        }
        else if(settings.trigger == TRG_REPEAT)
        { // Continous repeat is selected ... start immediately
          startPlay();
        }
        else if(settings.trigger == TRG_EXT_SINGLE && trgSignalLast == 0 && trgSignal == 1)
        { // Start on rising edge of ext TRG
          startPlay();
        }
        else if(settings.trigger == TRG_EXT_REPEAT && trgSignal == 1)
        { // Start whenever the ext signal is high
          startPlay();
        }
        break;
        
      }
      
      applayLeds();
      
      // Store to eeprom if needed - when timeout elapses ..
      if(storetoEEpromCnt == 1 && (settings.state == STOP || settings.state == PASS_THROUGH))
      {
        storeSettings((u8*)&settings, sizeof(Settings));
        storetoEEpromCnt = 0;
      }
      else if(storetoEEpromCnt > 0)
      {
        storetoEEpromCnt--;
      }
      
      trgSignalLast = trgSignal;
      watchdogKick();
    }
  }
}
