/**
 * Briskel kids - main file
 * 
 * Bref: basic control
 *
 * Author: Martin Patak
 */

#include <string.h>
#include <intrinsics.h>
#include "CommHeader.h"
#include "boot.h"
#include "watchdog.h"
#include "uart.h"

#define FW_VERSION 0x10 // 2.0
#define BOARD_NO 23

static void ConfigureHSEClock();
static void Timer3_5MsSettings();
static short int MeasureAdcChannel(ADC_Channel_TypeDef channel, uint16_t averageOf);
static void configureRTC();
static void lowEnergyControl();
void InitGpios(void);
void setRedLed(u8 on);
void setGreenLed(u8 on);
void setPower5V(u8 enable);
void setPower12V(u8 enable);
float getVoltage(ADC_Channel_TypeDef channel, u8 averageOf);
void determineVoltageIncrease(void);


#define ADC_GROUP_SPEEDCHANNEL     ADC_Group_SlowChannels
#define ADC_AVERAGE_OF 4
#define ADC_V_BAT ADC_Channel_13

#define ADC_5V_CHANNEL ADC_Channel_15
#define ADC_12V_CHANNEL ADC_Channel_14

#define LED_RED_GPIO_PORT  GPIOB
#define LED_RED_GPIO_PINS  GPIO_Pin_1
#define LED_GREEN_GPIO_PORT  GPIOC
#define LED_GREEN_GPIO_PINS  GPIO_Pin_1
#define POWER_5V_ENABLE_GPIO_PORT  GPIOB
#define POWER_5V_ENABLE_GPIO_PINS  GPIO_Pin_6
#define POWER_12V_ENABLE_GPIO_PORT  GPIOB
#define POWER_12V_ENABLE_GPIO_PINS  GPIO_Pin_7

#define POWER_SUPPLY_VOLT 3.3
#define VOLTAGE_DIVIDER_VBAT 11
#define VOLTAGE_DIVIDER_5V 2
#define VOLTAGE_DIVIDER_12V 11

#define MAX_ADC 4096

#define WARNING_LOW_VOLTAGE (15.0)
#define CRITICAL_LOW_VOLTAGE (12.5)

#define LOW_VOLTAGE_5V (4.5)
#define LOW_VOLTAGE_12V (11.0)

#define LOW_VOLTAGE_VBAT_TIMEOUT  250
#define CRITIC_ADC (CRITICAL_LOW_VOLTAGE * MAX_ADC / (POWER_SUPPLY_VOLT * VOLTAGE_DIVIDER_VBAT))
#define WARNING_ADC (WARNING_LOW_VOLTAGE * MAX_ADC / (POWER_SUPPLY_VOLT * VOLTAGE_DIVIDER_VBAT))

#define LOW_OUTPUT_VOLTAGE_TIMEOUT 10
#define ADC_VAL_LOW_VOLTAGE_5V (LOW_VOLTAGE_5V * MAX_ADC / (POWER_SUPPLY_VOLT * VOLTAGE_DIVIDER_5V))
#define ADC_VAL_LOW_VOLTAGE_12V (LOW_VOLTAGE_12V * MAX_ADC / (POWER_SUPPLY_VOLT * VOLTAGE_DIVIDER_12V))

static COMM_HEADER header2transfer; // This one is handled by IRQ routine
static COMM_HEADER identification2transfer; // This one is handled by IRQ routine
static u16 underVoltageBatteryCounter = 0; // Count how long it is low
static u16 underVoltage5VCounter = 0; // Count how long it is low
static u16 underVoltage12VCounter = 0; // Count how long it is low
static u8 underVoltageDetected = FALSE;
static  uint16_t offTimer = 0;
static u8 packetCnt = 0;
static u8 timer5ms;
static u16 secondCounter;

typedef enum {NORMAL_RUN, BATTERY_LOW} STATE;

void resetTimeCounters()
{
  timer5ms = 0;
  secondCounter = 0;
}

/**
 * Function called when communication is lost
 */
void communicationLostCallback()
{
}

/**
 * Callback for a received package
 * @param package - pointer to received package
 */
void receivedPackageCallback(COMM_HEADER * package)
{
  uartProcessNewPacket(package);
}

u8 charingDetected()
{
  return  0;
}


STATE underVoltageControl()
{
  u16 voltage;
  STATE retState = NORMAL_RUN;
  volatile u16 dischargeCnt;
  
  voltage = MeasureAdcChannel(ADC_V_BAT, 2);
  if(voltage < CRITIC_ADC || PWR_GetFlagStatus(PWR_FLAG_PVDOF))
  {  // critical voltage ... shut down immediately
    underVoltageDetected = TRUE;
  }
  else if(voltage < WARNING_ADC)
  {
    if(underVoltageBatteryCounter > LOW_VOLTAGE_VBAT_TIMEOUT)
    { // Low voltage too long ..
      underVoltageDetected = TRUE;
    }
    else
    {
      underVoltageBatteryCounter++;
    }
  }
  else
  {
    if(underVoltageBatteryCounter > 0)
    {
      underVoltageBatteryCounter--;
    }
  }
  
  voltage = MeasureAdcChannel(ADC_12V_CHANNEL, 2);
  if(voltage < ADC_VAL_LOW_VOLTAGE_12V)
  {
    if(underVoltage12VCounter < LOW_OUTPUT_VOLTAGE_TIMEOUT)
    {
      underVoltage12VCounter++;
    }
    else
    {
      underVoltageDetected = TRUE;
    }  
  }
  else if(underVoltage12VCounter > 0)
  {
    underVoltage12VCounter--;
  }
  
  voltage = MeasureAdcChannel(ADC_5V_CHANNEL, 2);
  if(voltage < ADC_VAL_LOW_VOLTAGE_5V)
  {
    if(underVoltage5VCounter < LOW_OUTPUT_VOLTAGE_TIMEOUT)
    {
      underVoltage5VCounter++;
    }
    else
    {
      underVoltageDetected = TRUE;
    }  
  }
  else if(underVoltage5VCounter > 0)
  {
    underVoltage5VCounter--;
  }
    
  
  if(underVoltageDetected)
  {
    // too long low voltage
    setPower5V(0); // Enable power off
    setPower12V(0); // Enable power off
    setRedLed(1);
    // Disable UART
    UART_deinit();
    underVoltageDetected = TRUE;
    configureRTC();
    
    // In case the unit was switched off -> then it is detected as low voltage
    // In this case the CPU shall discharge the capacitor so that it is restarted properly
    dischargeCnt = 50000;
    while(dischargeCnt > 0)
    {
      dischargeCnt--;
    }
    
    retState = BATTERY_LOW;
  }
  
  return retState;
}

/**
 * Init power voltage detector
 */
void initPvd()
{
  volatile u16 i = 0;
  PWR_PVDLevelConfig(PWR_PVDLevel_3V05); // Set limit
  PWR_PVDCmd(ENABLE);
  while(i++ < 1000 && PWR_GetFlagStatus(PWR_FLAG_VREFINTF) == RESET);
}

void initCommunicationStructs()
{
  // Create empty packet
  memset((u8*)&header2transfer, 0, sizeof(header2transfer)); // Set all by default to 0
  header2transfer.initChar = INIT_CHAR;
  header2transfer.packetType = STANDARD_DATA_PACKET;
  header2transfer.settings.switchedOn = 1;
  header2transfer.settings.sequencer = 0;
  header2transfer.checksum = computeHeaderCrc(&header2transfer);

  memset((u8*)&identification2transfer, 0, sizeof(identification2transfer)); // Set all by default to 0
  identification2transfer.initChar = INIT_CHAR;
  identification2transfer.packetType = FW_IDENTIFICATION_SEQUENCE;
  identification2transfer.settings.switchedOn = 1;
  identification2transfer.settings.sequencer = 0;
  // Copy the identification string of the device
  memcpy(identification2transfer.channel, getHwConfiguration(), sizeof(HW_CONFIGURATION));
  identification2transfer.checksum = computeHeaderCrc(&identification2transfer);
}

void chargingIndication()
{
  setGreenLed(0);
  if(secondCounter % 2)
  {
    setRedLed(1);
  }
  else
  {
    setRedLed(0);
  }
}

void chargedIndication()
{
  setGreenLed(0);
  setRedLed(0);
}

/**
 * Main function
 */
int main( void )
{ 
  STATE state = NORMAL_RUN;
  
  initCommunicationStructs();
  InitGpios();
//configureCommunicationProtocol(0,0, FW_VERSION, BOARD_NO); // Add it hede to verify board no
  
  // First determine whether to enable volt increase or not - this is then kept all the time..
  setRedLed(1);
  ConfigureHSEClock();
  setRedLed(0);
  initPvd();
  
  if(isAppValidated())
  { // Watchdog only for validated application - not for development, ...  
    watchdogInit(); 
  }
  
  Timer3_5MsSettings();
  UART_init();
  __enable_interrupt();

  
  while(1)
  {
    switch(state)
    {
    case NORMAL_RUN:
      setPower5V(1); 
      setPower12V(1); 
      setGreenLed(1);
      setRedLed(0);
      state = underVoltageControl();
      break;
      
    case BATTERY_LOW:
      setPower12V(0); // Disable power on
      setPower5V(0); // Disable power on
      setGreenLed(0);
      
      lowEnergyControl();
      break;
    }
   
    watchdogKick();
  }
}

// In low power mode - do not keep the RED LED on permanently - switch it off 
// after some time to save batteries ..
// Keep it on just for first one hour .. then up to 4 hours after low power detected - 
// flash every 4 seconds ... after then keep it just off ..
// Then this function forces the CPU to go to sleep ... is woken up by the interrupt..
static void lowEnergyControl()
{
  #define PERMANENTLY_ON_UNTIL (1*3600)
  #define COMPLETELY_OFF_AFTER (4*3600)
  #define FLASHING_EVERY (4)
  volatile u16 delay;
  
  // Energy saving in case of low battery voltage   
  if (offTimer > COMPLETELY_OFF_AFTER)
  { // After more than x hours - keep it off to save energy
    setRedLed(0);
  }
  else if(offTimer < PERMANENTLY_ON_UNTIL|| offTimer % FLASHING_EVERY == 0)
  { // Time permanently on or time to time flash ..
    setRedLed(1);
  }
  else
  {
    setRedLed(0);
  }

  if(offTimer < 0xFFFF)
  {
    offTimer++;
  }
  
  // During first couple of seconds - if someone tries to switch it off and on again 
  // keep the power consumption a bit higher .. so that the capacitors are faster discharged and 
  // it is possible to switch unit on again .. but do it only first X seconds ...
  if(offTimer < 20)
  {
    delay = 10000;
    while(delay-- > 0);
  }
  
  CLK_HaltConfig(CLK_Halt_SlowWakeup, ENABLE);
  PWR_UltraLowPowerCmd(ENABLE);
  halt();
}

void InitGpios(void)
{
  GPIO_Init(LED_RED_GPIO_PORT, LED_RED_GPIO_PINS, GPIO_Mode_Out_PP_Low_Slow);
  GPIO_Init(LED_GREEN_GPIO_PORT, LED_GREEN_GPIO_PINS, GPIO_Mode_Out_PP_High_Slow);
  GPIO_Init(POWER_12V_ENABLE_GPIO_PORT, POWER_12V_ENABLE_GPIO_PINS, GPIO_Mode_Out_PP_Low_Slow);
  GPIO_Init(POWER_5V_ENABLE_GPIO_PORT, POWER_5V_ENABLE_GPIO_PINS, GPIO_Mode_Out_PP_Low_Slow);
}

void setRedLed(u8 on)
{
  GPIO_WriteBit(LED_RED_GPIO_PORT, LED_RED_GPIO_PINS, (BitAction)on);
}

void setGreenLed(u8 on)
{
  GPIO_WriteBit(LED_GREEN_GPIO_PORT, LED_GREEN_GPIO_PINS, (BitAction)!on);
}

void setPower5V(u8 enable)
{
  GPIO_WriteBit(POWER_5V_ENABLE_GPIO_PORT, POWER_5V_ENABLE_GPIO_PINS, (BitAction)enable);
}

void setPower12V(u8 enable)
{
  GPIO_WriteBit(POWER_12V_ENABLE_GPIO_PORT, POWER_12V_ENABLE_GPIO_PINS, (BitAction)enable);
}


  
/* Defines an interrupt handler for the TIM4 vector. */
#pragma vector = TIM3_TIF_vector
__interrupt __root void Tim3IrqHandler(void)
{
  TIM3_ClearFlag(TIM3_FLAG_Update);
  
  // Transfer
  if(packetCnt < 10)
  {
    // Do nothing during the first couple of packets ... so that all board are properly
    // powered and configured
    packetCnt++;
  }
  else if(packetCnt < 13)
  {
    // Send 3x the identification string ..
    UART_sendPacketDMA(&identification2transfer);
    packetCnt++;
  }
  else
  { // Send normal packet .. 
    UART_sendPacketDMA(&header2transfer);
  }
  
  timer5ms++;
  if(timer5ms == 200)
  {
    timer5ms = 0;
    secondCounter++;
  }
}


/**
 * Configure HSE clock - HSE - input 16 MHz
 * divide by x - CPU runs at 16/x MHz
 */
static void ConfigureHSEClock()
{
  volatile u16 i;
  for(i = 0; i < 50000; i++); // just delay
     
  // Use HSI only

  CLK_SYSCLKDivConfig(CLK_SYSCLKDiv_8);
}

/**
 * Configure timer 4 to generate every 5 ms interrupt
 * TIM4CLK is set to SYSTEM CLOCK = 2 MHz,
 * CLK / PRESCALLER / (PERIOD + 1) is required 200 (5ms)
 * therefore e.g. 2e6 / 64 / (124 + 1) = 200 Hz
 */
static void Timer3_5MsSettings()
{
  CLK_PeripheralClockConfig(CLK_Peripheral_TIM3, ENABLE);

  TIM3_TimeBaseInit(TIM3_Prescaler_1, TIM3_CounterMode_Up, 10000-1); // 5ms
  TIM3_ClearFlag(TIM3_FLAG_Update);
  TIM3_ITConfig(TIM3_IT_Update, ENABLE);

  TIM3_Cmd(ENABLE);
}

// Configure RTC to make IRQ every 1s to wake up CPU ...
// LSI - 38kHz ->  38000 / 2 / 8 / (2374+1) = 1Hz
static void configureRTC()
{
  CLK_SYSCLKSourceSwitchCmd(ENABLE);
  CLK_RTCClockConfig(CLK_RTCCLKSource_LSI, CLK_RTCCLKDiv_2);
  while (CLK_GetFlagStatus(CLK_FLAG_LSIRDY) == RESET);
  CLK_PeripheralClockConfig(CLK_Peripheral_RTC, ENABLE);
  RTC_WakeUpClockConfig(RTC_WakeUpClock_RTCCLK_Div8);
  RTC_ITConfig(RTC_IT_WUT, ENABLE);
  RTC_SetWakeUpCounter(2374);
  RTC_WakeUpCmd(ENABLE);
}

/**
 * Read data from ADC channel
 * @param averageOf - make average of n samples
 */
static short int MeasureAdcChannel(ADC_Channel_TypeDef channel, uint16_t averageOf)
{
	short int ADCData;
	uint16_t i;
	unsigned long int average = 0;
	
	CLK_PeripheralClockConfig(CLK_Peripheral_ADC1, ENABLE);
	for(i = 0; i < averageOf; i++)
	{
		ADC_Init(ADC1, ADC_ConversionMode_Single, ADC_Resolution_12Bit, ADC_Prescaler_2);
		ADC_SamplingTimeConfig(ADC1, ADC_GROUP_SPEEDCHANNEL, ADC_SamplingTime_16Cycles);
		ADC_Cmd(ADC1, ENABLE);
		ADC_SchmittTriggerConfig(ADC1, channel, DISABLE);
		ADC_ChannelCmd(ADC1, channel, ENABLE);
		ADC_SoftwareStartConv(ADC1);
		while (ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == 0);
		ADCData = ADC_GetConversionValue(ADC1);
		ADC_ClearFlag(ADC1, ADC_FLAG_EOC);
		average += ADCData & 0x0FFF;
	}
	ADC_DeInit(ADC1);
	CLK_PeripheralClockConfig(CLK_Peripheral_ADC1, DISABLE);
	
	return (short int)(average / averageOf);
}
