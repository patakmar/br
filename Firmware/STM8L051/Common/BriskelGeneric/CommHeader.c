/**
 * Briskel kids - communication header
 * 
 * Bref: Describe basic structure of communication
 *
 * Basic principle - structure is 24 bytes long -> data transfer at 115.2 kbps (1 byte is transferred as 10 bits) ->
 * takes 2.08 ms...  (1 byte transfer takes 0.09ms)
 * Master (power supply) sends every 4.0 ms a new packet - that means 2.1ms packet, 1.9 ms gap
 *
 * Slave shall synchronise on gap of 0.2ms with no communication, then start data reception
 *
 * In case of CRC error - it shall try to resynchronise
 *
 * In case of lost communication for more than 50 ms - device shall switch off (buzzer, leds, motors, ...)
 * and try to resynchronise
 *
 *
 * Author: Martin Patak
 */

#include <string.h>
#include "boot.h"
#include "CommHeader.h"

static u8 CONSUMER_COUNT = 0;
static u8 PRODUCER_COUNT = 0;
static u8 FW_VERSION = 0;
static i16 producedValues[CHANNEL_MAX];
static COMM_HEADER lastProcessedPacket;
static bool identificationRequest = FALSE;

/**
 * Compute COMM_HEADER checksum
 * @param header - pointer to header structure
 * @return checksum
 */
u16 computeHeaderCrc(COMM_HEADER * header)
{
  u8 i;
  u16 checksum = 0;
  
  for(i = 0; i < sizeof(COMM_HEADER) - sizeof(header->checksum); i++)
  {
    checksum += ((u8*)header)[i];
  }
  
  checksum ^= CHECKSUM_XOR;
  
  return checksum;
}

/**
 * Configure the communication protocol, specify how many inputs/outputs 
 * the device have
 * @param producerCount - how many signals the device can produce (potenciometers..)
 * @param consumerCount - how many signals the device consumes .. (LEDS, ..)
 * @param fwVersion - in format - upper 4bits  -major version, bottom 4 bits - minor version => 0x24 = FW2.4
 * @param boardNo - board No
 */
void configureCommunicationProtocol(u8 producerCount, u8 consumerCount, u8 fwVersion, u16 boardNo)
{
  PRODUCER_COUNT = producerCount;
  CONSUMER_COUNT = consumerCount;
  FW_VERSION = fwVersion;
  
  HW_CONFIGURATION * config = getHwConfiguration();
  
  if(config->boardNo != boardNo)
  { // In case board does not match firmware --> jump to bootloader
    //asm("JPF $6000");
    jumpToBooloaderCallback();
  }
}


/**
 * Add new data to protocol ... disable interrupts before calling this function
 * @param newDataSignals - New data - the length must match the configured value by configureCommunicationProtocol
 */
void addNewDataToProtocol(i16 * newDataSignals)
{
  memcpy(producedValues, newDataSignals, sizeof(i16) * PRODUCER_COUNT);
}

/**
 * Get data from protocol
 * @param dataSignalsFromProtocol - the length must match the configured value by configureCommunicationProtocol
 */
void getNewDataFromProtocol(i16 * dataSignalsFromProtocol, SETTINGS * settings)
{
  memcpy(dataSignalsFromProtocol, lastProcessedPacket.channel, sizeof(i16) * CONSUMER_COUNT);
  memcpy(settings, &lastProcessedPacket.settings, sizeof(SETTINGS));
}

/**
 * Create response to identification sequence - fill datainto header's data
 * @param header - pointer to struct which shall be modified ... 
 */
void createIdentificationSequenceResponse(COMM_HEADER * header)
{
  HW_CONFIGURATION * hwConfig = getHwConfiguration();
  u8 * data = (u8*)header->channel;
  header->packetType = FW_IDENTIFICATION_SEQUENCE;
  memset(header->channel, 0, sizeof(header->channel));
  memcpy(&data[0], hwConfig->uidi, sizeof(hwConfig->uidi));
  memcpy(&data[6], &hwConfig->boardNo, sizeof(hwConfig->boardNo));
  data[8] = hwConfig->hwRevision;
  data[9] = hwConfig->hwConfiguration;
  data[10] = FW_VERSION;
}

/**
 * Process new packets - add new produced values, consume used values ....
 * @param header - pointer to struct which shall be modified ... 
 */
void uartProcessNewPacket(COMM_HEADER * header)
{ 
  switch(header->packetType)
  {
  case STANDARD_DATA_PACKET:
    if(identificationRequest)
    { // Identification request is needed ..  replace packet with given data..
      createIdentificationSequenceResponse(header);
      identificationRequest = FALSE;
    }
    else
    {
      memcpy(&lastProcessedPacket, header, sizeof(COMM_HEADER)); // Make copy of a last command
      uartProcessNormalPackage(header);
    }
    break;
    
  case FW_INFO_REQUEST:
    uartGetFwInfoPackage(header);
    header->packetType = FW_INFO_RESPONSE;
    break;
    
  case FW_IDENTIFICATION_SEQUENCE:
    // Send this packet unmodified to next and remember it .. when next time
    // a normal packet is received - replace it current board information
    identificationRequest = TRUE;
    break;
    
  case FW_JUMP_TO_BOOTLOADER:
    header->packetType = FW_JUMP_TO_BOOTLOADER_ACK;
    jumpToBooloaderCallback();
    break;
    
  case FW_VALIDATE_APPLICATION:
    header->packetType = FW_VALIDATE_APPLICATION_ACK;
    validateApplication();
    break;
    
  case FW_JUMP_TO_BOOTLOADER_ACK:
  case FW_VALIDATE_APPLICATION_ACK:
    // Do nothing in these states just pass this information to the output
    // Normaly this message should not come .. but go to boot SW instead
    break;
  }
    
  // Compute checksum always - so that it takes roughlt the same amount of time
  header->checksum = computeHeaderCrc(header); // compute new checksum
}

/**
 * Process normal package which is stored in lastProcessedPacket
 * @param header - pointer to struct which shall be modified ... 
 */
void uartProcessNormalPackage(COMM_HEADER * header)
{
  u8 i;
  u8 signalsToReduce;
  
  if(PRODUCER_COUNT > 0)
  {
    // Shift all signals by 1 up
    for(i = CHANNEL_MAX - 1; i > PRODUCER_COUNT - 1; i--)
    {
      header->channel[i] = header->channel[i - PRODUCER_COUNT];
    }
    
    memcpy(header->channel, producedValues, sizeof(i16) * PRODUCER_COUNT);
  }
  
  if(CONSUMER_COUNT > 0)
  {
    // Here if sequencer is set (but not set to infinite..).. consume the number of signals which is set
    if(header->settings.sequencer > 0)
    {
      if(header->settings.sequencer >= CONSUMER_COUNT)
      {   
        signalsToReduce = CONSUMER_COUNT;
      }
      else
      {
        signalsToReduce = header->settings.sequencer;
      }
      
      if(header->settings.sequencer < 0xF)
      { // Reduce this only if not set to infinity
        header->settings.sequencer -= signalsToReduce;
      }
      
      for(i = 0; i < CHANNEL_MAX - signalsToReduce; i++)
      {
        header->channel[i] = header->channel[i + signalsToReduce];
      }
      // Fill the rest with zeros
      for(; i < CHANNEL_MAX; i++)
      {
        header->channel[i] = 0x0;
      }
    }
  }
}

/**
 * Process Finfo requestW 
 * @param header - pointer to struct which shall be modified ... 
 * Description - clear all values in channes
 * set channels: 0 - board No
 *               1 - fw version
 */
void uartGetFwInfoPackage(COMM_HEADER * header)
{    
  memset(header->channel, 0, sizeof(i16) * CHANNEL_MAX); // clear all
  header->channel[0] = getHwConfiguration()->boardNo;
  header->channel[1] = FW_VERSION;
}

