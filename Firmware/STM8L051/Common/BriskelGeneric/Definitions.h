/**
 * Briskel kids - common definitions
 *
 *
 * Author: Martin Patak
 */

#ifndef BR_Definitions
#define BR_Definitions  

#define LIMIT_HIGH (0.7*0x7FFF)
#define LIMIT_LOW (0.3*0x7FFF)

#endif // BR_Definitions