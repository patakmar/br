/**
 * Briskel kids - communication header
 * 
 * Bref: Describe basic structure of communication
 *
 * Basic principle - structure is 30 bytes long -> data transfer at 115.2 kbps (1 byte is transferred as 10 bits) ->
 * takes 2.08 ms...  (1 byte transfer takes 0.09ms)
 * Master (power supply) sends every 5.0 ms a new packet - that means 2.1ms packet, 2.9 ms gap
 *
 * Slave shall synchronise on gap of 0.2ms with no communication, then start data reception
 *
 * In case of CRC error - it shall try to resynchronise
 *
 * In case of lost communication for more than 50 ms - device shall switch off (buzzer, leds, motors, ...)
 * and try to resynchronise
 *
 *
 * Author: Martin Patak
 */

#ifndef CommonHeader
#define CommonHeader   
   
#include "TypeDefinitions.h"

#define INIT_CHAR 0xA5
#define CHANNEL_MAX 12

// Xor the checksum becuse not all upper bits would be used, therefore do not keep
// them zero .. 
#define CHECKSUM_XOR 0xB5CA


// Define different packet types if necessary
typedef enum
{
  UNKNOWN               = 0x00,
  STANDARD_DATA_PACKET  = 0x01,
  FW_INFO_REQUEST       = 0x10,    // Get info about current software
  FW_INFO_RESPONSE      = 0x11,    // Response to of current software
  FW_IDENTIFICATION_SEQUENCE = 0x12,    // Identification sequence ..
  FW_JUMP_TO_BOOTLOADER = 0x20,    // Request to jump to a bootloader
  FW_JUMP_TO_BOOTLOADER_ACK = 0x21,    // Response that device is going to boot (optional)
  FW_VALIDATE_APPLICATION = 0x30,  // Request to validate application - application is OK
  FW_VALIDATE_APPLICATION_ACK = 0x31,  // Response to validate application - application is OK
  
  PACKET_MAX_MUMBER     = 0xFF, // Force this to be 8-bit number
} PACKET_TYPE;

// Setting fields
typedef struct
{
  // Device shall be switched on (1), or off (0)
  u8 switchedOn : 1; 
  
  // If this is non zero - device shall decrement this by one
  // and shift all channels by 1 (channel 0 = channel 1, channel 1 = channel 2.. 
  // channel CHANNEL_MAX - 2 = channel CHANNEL_MAX - 1, channel CHANNEL_MAX - 1 = 0x0000, 0xF= do not decrement ...
  u8 sequencer : 4;
  
  // Reserved for future use - just observe the whole struct shall be 16 bits
  // Therefore if added something, decrese reserved size
  // By default - these values must be 0
  u8 reserved1 : 3;
  u8 reserved2 : 8;
  
} SETTINGS;

// Main communication structure
typedef struct  __attribute__ ((packed))
{
  // Each packet starts with this constant char = INIT_CHAR
  u8 initChar; // INIT_CHAR - constant value
  
  // Packet type as definition above
  PACKET_TYPE packetType;
  
  // Store some bit fields in settings
  SETTINGS settings;
  
  // Here define channel values, use i16 value
  i16 channel[CHANNEL_MAX];
  
  // Checksum of the all 8-bit fields before xored with CHECKSUM_XOR
  u16 checksum;
} COMM_HEADER;

u16 computeHeaderCrc(COMM_HEADER * header);

void configureCommunicationProtocol(u8 producerCount, u8 consumerCount, u8 fwVersion, u16 boardNo);
void uartProcessNewPacket(COMM_HEADER * header);
void addNewDataToProtocol(i16 * newDataSignals);
void getNewDataFromProtocol(i16 * dataSignalsFromProtocol, SETTINGS * settings);

// 'private methods'
void uartProcessNormalPackage(COMM_HEADER * header);
void uartGetFwInfoPackage(COMM_HEADER * header);
//void processRxFrameConsumer(COMM_HEADER * header, u8 signalsConsumed);
//void processRxFrameProducer(COMM_HEADER * header, i16 newValue);

// Identifier which is stored in the EEPROM memory to identify the board ..
// On request this information is sent in the normal packet - the data are replaced with:
// Byte 0..5 - uidi
// Byte 6..7 - boardNo
// Byte 8 - hwRevision
// Byte 9 - hwConfiguration
// Byte 11 - FW version
typedef struct __attribute__ ((packed))
{
  u8 uidi[6]; // Unique identifier - something like MAC address
  u16 boardNo; // Board number ... PCB number
  u8 hwRevision; // Revision of the board
  u8 hwConfiguration; // Type of the hardware mounted to board e.g. fan/LED lamp/...
} HW_CONFIGURATION;

HW_CONFIGURATION * getHwConfiguration(); // This must be defined somewhere ..
                                              // But it is platform specific..

#endif // CommonHeader