/**
 * Briskel kids - communication header
 * 
 * Bref: EEPROM driver - allows to store settings
 *
 * This function allows to store settings in following format:
 * 
 * Byte 0 = 0x5A
 * Byte 1.. length - settings
 * Byte 1+length..2+length = checksum u16 from Byte 0 to end - xored with 0x5A43
 *
 * Store this settings to beginning of EEPROM
 *
 * The settings is always stored 2x - in case the device would be switched
 * off during writing - the last known state would be used
 *
 * Author: Martin Patak
 */

#include <string.h>
#include "stm8l15x_conf.h" 
#include "CommHeader.h"
#include "eeprom.h"


#define BEGINNING_CHARACTER 0x5A
#define XOR_CHECKSUM 0x5A43

/**
 * Compute checksum of the packet (including beginning character)
 * @param data - pointer to data which shall be stored
 * @param length - how many bytes to store
 */
u16 computeChecksum(u8 * data, u8 length)
{
  u16 checksum = BEGINNING_CHARACTER;
  u8 i;
  
  for(i = 0; i < length; i++)
  {
    checksum += data[i];
  }
  checksum = checksum ^ XOR_CHECKSUM;
  
  return checksum;
}

/** 
 * Store settings to eeprom
 * @param data - pointer to data which shall be stored
 * @param length - how many bytes to store
 */
void storeSettings(u8 * data, u8 length)
{
  u16 checksum;
  u8 i;
  u16 addr = FLASH_DATA_EEPROM_START_PHYSICAL_ADDRESS;
  
  checksum = computeChecksum(data, length);
    
  FLASH_Unlock(FLASH_MemType_Data);
  
  FLASH_ProgramByte(addr++, BEGINNING_CHARACTER);
  for(i = 0; i < length; i++)
  {
    FLASH_ProgramByte(addr++, data[i]);
  }
  FLASH_ProgramByte(addr++, ((u8*)&checksum)[0]);
  FLASH_ProgramByte(addr++, ((u8*)&checksum)[1]);
  
  // Store second copy
  FLASH_ProgramByte(addr++, BEGINNING_CHARACTER);
  for(i = 0; i < length; i++)
  {
    FLASH_ProgramByte(addr++, data[i]);
  }
  FLASH_ProgramByte(addr++, ((u8*)&checksum)[0]);
  FLASH_ProgramByte(addr++, ((u8*)&checksum)[1]);

  FLASH_Lock(FLASH_MemType_Data);
}

/** 
 * Reload settings from eeprom
 * @param data - pointer to data to where to store it
 * @param length - how many bytes to read
 * @return - true if success
 */
bool readSettings(u8 * data, u8 length)
{
    u16 addr = FLASH_DATA_EEPROM_START_PHYSICAL_ADDRESS;
    if(*(u8*)addr == BEGINNING_CHARACTER 
       && computeChecksum((u8*)(addr+1), length) == *(u16*)(addr+length+1))
    {
      memcpy(data, (u8*)(addr+1), length);
      return TRUE;
    }
    else
    {
      // Try it again, but at different location ..
      addr += length + 3; // +3 because init char + checksum
      if(*(u8*)addr == BEGINNING_CHARACTER 
         && computeChecksum((u8*)(addr+1), length) == *(u16*)(addr+length+1))
      {
        memcpy(data, (u8*)(addr+1), length);
        return TRUE;
      }
    }
    
    return FALSE; 
}


/** 
 * Read platrofm specification - it is located at the end of flash memory..
 * @return - pointer to platform specification ..
 */
HW_CONFIGURATION * getHwConfiguration()
{
  u16 address;
  address = (FLASH_DATA_EEPROM_END_PHYSICAL_ADDRESS + 1) - sizeof(HW_CONFIGURATION);
  return (HW_CONFIGURATION *) address;
}

/**
 * Store board number, revision and configuration
 */
void storeMac(u16 boardNo, u8 hwRevision, u8 hwConfiguration)
{
  u32 startAddress = (FLASH_DATA_EEPROM_END_PHYSICAL_ADDRESS + 1) 
                    - sizeof(HW_CONFIGURATION) 
                    + 6;
  
  FLASH_Unlock(FLASH_MemType_Data);
  
  FLASH_ProgramByte(startAddress++, (boardNo >> 8) & 0xFF);
  FLASH_ProgramByte(startAddress++, (boardNo >> 0) & 0xFF);
  FLASH_ProgramByte(startAddress++, hwRevision);
  FLASH_ProgramByte(startAddress++, hwConfiguration);

  FLASH_Lock(FLASH_MemType_Data);
}
