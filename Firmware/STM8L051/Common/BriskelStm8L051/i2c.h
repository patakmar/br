/**
 * file providing easy access to I2C bus
 */ 

#ifndef __I2C_H
 #define __I2C_H 

#include "stm8l15x_conf.h" 

#define I2C_SPEED 100000
#define I2C_TIMEOUT 500
/**
 * Initialise i2c
 */
void i2cInit(void);

/**
 * Deinitialsie i2c
 */
void i2cDeInit(void);

/**
 * Send data to i2c
 * Blocking operation until the data are transmitted
 */
void i2cSend(unsigned char addr, char const * data, int length, bool appendStop);

/**
 * Write first register number (8bit) and then read specified data
 */
void i2cRead(unsigned char addr, char regNo, char * data, int length);

#endif