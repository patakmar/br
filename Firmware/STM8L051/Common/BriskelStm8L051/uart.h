/**
 * Briskel kids - UART communication
 * 
 * Bref: Handle Uart communication
 *
 * Author: Martin Patak
 */

#ifndef __UART
#define __UART   

#include "TypeDefinitions.h"

void UART_init();
void UART_deinit(void);
bool UART_sendPacketDMA(COMM_HEADER * package);
void UART_synchronize();

#endif // __UART
