/**
 * Briskel kids - Boot manager
 * 
 * Bref: Handle Uart communication
 *
 * Author: Martin Patak
 */

#include "stm8l15x_conf.h" 
#include "watchdog.h"

/**
 * Function initialises watchdog - trigger period - cca 1.7s
 */
void watchdogInit(void)
{
  IWDG_Enable();
  IWDG_WriteAccessCmd(IWDG_WriteAccess_Enable);
  IWDG_SetPrescaler(IWDG_Prescaler_256);
  IWDG_SetReload(0xFF);
  IWDG_WriteAccessCmd(IWDG_WriteAccess_Disable);
}


/**
 * Function kisk the dog ..
 */
void watchdogKick(void)
{
  IWDG_ReloadCounter();
}