/**
 * Briskel kids - communication header
 * 
 * Bref: EEPROM driver - allows to store settings
 *
 * This function allows to store settings in following format:
 * 
 * Byte 0 = 0x5A
 * Byte 1.. length - settings
 * Byte 1+length..2+length = checksum u16 from Byte 0 to end - xored with 0x5A43
 *
 * Store this settings to beginning of EEPROM
 * 
 * The settings is always stored 2x - in case the device would be switched
 * off during writing - the last known state would be used
 *
 * Author: Martin Patak
 */

#ifndef EEPROM_DRIVER
#define EEPROM_DRIVER  

void storeSettings(u8 * data, u8 length);
bool readSettings(u8 * data, u8 length);
void storeMac(u16 boardNo, u8 hwRevision, u8 hwConfiguration);

#endif