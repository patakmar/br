/**
 * Briskel kids - Watchdog manager
 * 
 * Bref: Handle Uart communication
 *
 * Author: Martin Patak
 */

#ifndef __WATCHDOG
#define __WATCHDOG   

void watchdogInit(void);
void watchdogKick(void);

#endif // __WATCHDOG