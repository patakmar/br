/**
 * Briskel kids - Boot manager
 * 
 * Bref: Handle Uart communication
 *
 * Author: Martin Patak
 */

#ifndef __BOOT
#define __BOOT   

void jumpToBooloaderCallback(void);
void validateApplication(void);
char isAppValidated(void);

#endif // __BOOT