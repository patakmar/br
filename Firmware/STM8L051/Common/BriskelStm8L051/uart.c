/**
 * Briskel kids - UART communication
 * 
 * Bref: Handle Uart communication
 *
 * Author: Martin Patak
 */

#include <string.h>
#include "CommHeader.h"
#include "uart.h"


#define UART_GPIO_TX_PORT       GPIOC
#define UART_GPIO_TX_PINS       GPIO_Pin_5

#define UART                    USART1
#define RESYNCHRONISING_TIMEOUT 10 // in *8 ms - see RTC configuration => timeout 80 ms

typedef enum
{
  RX_START_SYNCHRONISING,
  RX_WAIT_FOR_COMMUNICATION,
  RX_WAIT_FOR_COMMUNICATION_GAP,
  RX_SYNCHRONISED,
  NO_COMMUNICATION,
} RX_STATE;

static u8 UART_TX_BUFF[sizeof(COMM_HEADER)];
static u8 UART_RX_BUFF[sizeof(COMM_HEADER)];
static u8 UART_RX__PROCESSING_BUF[sizeof(COMM_HEADER)];
static u8 UART_TX_IN_PROGRESS = FALSE;
static RX_STATE rxState = RX_START_SYNCHRONISING;

static u8 resynchrisingTimeout;
//static bool rxReceivedFlag = TRUE;

// This shall be properlz adjusted
#define RTC_DIVIDER_10MS 2000 // cca 8ms //
#define RTC_DIVIDER_02MS 30  // Transfer 1 byte(10bits) at 115200bps takes 87us, divider 30 => 124 us.. (1.42 byte)

/**
  * @brief  Configure RTC peripheral - to generate IRQ every 10 ms
  * @param  None
  * @retval None
  */
static void RTC_Config(void)
{
  CLK_RTCClockConfig(CLK_RTCCLKSource_HSI, CLK_RTCCLKDiv_64);
  CLK_PeripheralClockConfig(CLK_Peripheral_RTC, ENABLE);
  RTC_WakeUpClockConfig(RTC_WakeUpClock_RTCCLK_Div2);
  RTC_ITConfig(RTC_IT_WUT, ENABLE);
  RTC_SetWakeUpCounter(RTC_DIVIDER_10MS);	
  RTC_WakeUpCmd(ENABLE);
}

/*
 * The principle:
 * 1) first clear UART ... in RX_START_SYNCHRONISING state
 * 2) then wait for communication ... until some byte is received in RX_WAIT_FOR_COMMUNICATION
 * 3) Then wait until communication stops ... this shall be the beginning of 
 *    communication gap - RX_WAIT_FOR_COMMUNICATION_GAP state - enable DMA RX
 * 4) Wait and check whether the device is still synchronised.. if not - disable DMA
 *    and try synchronisation again - RX_SYNCHRONISED state
 *
 */
#pragma vector = RTC_WAKEUP_vector
__interrupt __root void RtcTimeoutIrq(void)
{
  switch(rxState)
  {
  case RX_START_SYNCHRONISING:
    // Clear USART RXNE flag..
    USART_ReceiveData8(UART);
    USART_GetFlagStatus(UART, USART_FLAG_OR);
    USART_ClearFlag(UART, USART_FLAG_RXNE);
    
    // Set timer IRQ to generate proper interrupts ..
    RTC_WakeUpCmd(DISABLE);
    RTC_SetWakeUpCounter(RTC_DIVIDER_02MS);   
    RTC_WakeUpCmd(ENABLE);
    
    // And jump to next state
    rxState = RX_WAIT_FOR_COMMUNICATION;
    break;
  
  case RX_WAIT_FOR_COMMUNICATION:
    if(USART_GetFlagStatus(UART, USART_FLAG_OR) || USART_GetFlagStatus(UART, USART_FLAG_RXNE))
    {
      USART_ClearFlag(UART, USART_FLAG_OR);
      USART_ReceiveData8(UART);
      USART_ClearFlag(UART, USART_FLAG_RXNE);
      rxState = RX_WAIT_FOR_COMMUNICATION_GAP;
    }
    break;
    
  case RX_WAIT_FOR_COMMUNICATION_GAP:
    if(USART_GetFlagStatus(UART, USART_FLAG_OR) || USART_GetFlagStatus(UART, USART_FLAG_RXNE))
    { // Overrun .. just clear it 
      USART_ReceiveData8(UART);
      USART_ClearFlag(UART, USART_FLAG_OR);
      USART_GetFlagStatus(UART, USART_FLAG_RXNE);
    }
    else
    {
      // Gap found - now start DMA to receive messages - clear first all..
      USART_ReceiveData8(UART);
      USART_ClearFlag(UART, USART_FLAG_OR);
      USART_GetFlagStatus(UART, USART_FLAG_RXNE);
      
      // Enable DMA RX
      USART_DMACmd(UART, USART_DMAReq_RX, ENABLE);
      DMA_SetCurrDataCounter((DMA_Channel_TypeDef*)DMA1_Channel2_BASE, sizeof(COMM_HEADER));
      DMA_Cmd((DMA_Channel_TypeDef*)DMA1_Channel2_BASE, ENABLE); 
      
      RTC_WakeUpCmd(DISABLE);
      RTC_SetWakeUpCounter(RTC_DIVIDER_10MS);
      RTC_WakeUpCmd(ENABLE);
      rxState = RX_SYNCHRONISED;
    }
    break;
  case RX_SYNCHRONISED:
    if(resynchrisingTimeout > RESYNCHRONISING_TIMEOUT)
    {
      DMA_Cmd((DMA_Channel_TypeDef*)DMA1_Channel2_BASE, DISABLE);
      USART_DMACmd(UART, USART_DMAReq_RX, DISABLE);
      rxState = RX_START_SYNCHRONISING;
      resynchrisingTimeout = 0;
      extern void communicationLostCallback();
      communicationLostCallback();
    }
    else
    {
      resynchrisingTimeout++;
    }
    break;
    
  case NO_COMMUNICATION:
    // Nothing to do just wake up CPU
    break;
  }
  
  RTC_ClearITPendingBit(RTC_IT_WUT);
}

/**
 * Interrupt when DMA transfer RX is finished
 */
#pragma vector = DMA1_CH2_TC_vector
__interrupt __root void DmaUartRxCompletedIrq(void)
{
  if(UART_RX_BUFF[0] == INIT_CHAR)
  {
    // make a copy first so that circualr DMA does not overwrite our data
    memcpy(&UART_RX__PROCESSING_BUF, UART_RX_BUFF, sizeof(UART_RX__PROCESSING_BUF)); 
    DMA_ClearITPendingBit(DMA1_IT_TC2);
    DMA_ClearFlag(DMA1_FLAG_TC2);
    
    if(computeHeaderCrc((COMM_HEADER*)UART_RX__PROCESSING_BUF) == ((COMM_HEADER*)UART_RX__PROCESSING_BUF)->checksum)
    {
      // Packet received correctly
      resynchrisingTimeout = 0;
      extern void receivedPackageCallback(COMM_HEADER * package);
      receivedPackageCallback((COMM_HEADER*)UART_RX_BUFF);
    }
  }
  else
  {
    DMA_ClearITPendingBit(DMA1_IT_TC2);
    DMA_ClearFlag(DMA1_FLAG_TC2);
  }
}

/** Handle DMA TX interruprt 
 */
void DmaUartTxCompleteCallback()
{
  DMA_ClearITPendingBit(DMA1_IT_TC1);
  DMA_ClearFlag(DMA1_FLAG_TC1);
  UART_TX_IN_PROGRESS = FALSE;
  DMA_Cmd((DMA_Channel_TypeDef*)DMA1_Channel1_BASE, DISABLE); 
}

/**
 * Interrupt when DMA transfer TX is finished
 */
#ifndef DISABLE_DMA1_CH1_TC_vector
#pragma vector = DMA1_CH1_TC_vector
__interrupt __root void DmaUartTxCompletedIrq(void)
{
  DmaUartTxCompleteCallback();
}
#endif

/**
 * Initialise UART and DMA
 */
void UART_init(void)
{
  GPIO_Init(UART_GPIO_TX_PORT, UART_GPIO_TX_PINS, GPIO_Mode_Out_PP_High_Slow);
  CLK_PeripheralClockConfig(CLK_Peripheral_USART1, ENABLE);
  USART_Init(UART, 115200, USART_WordLength_8b, USART_StopBits_1, USART_Parity_No, (USART_Mode_TypeDef)(USART_Mode_Rx | USART_Mode_Tx));
 
  USART_DMACmd(UART, USART_DMAReq_TX, ENABLE);
  USART_Cmd(UART, ENABLE);
  RTC_Config();
  
  // Configure DMA
  CLK_PeripheralClockConfig(CLK_Peripheral_DMA1, ENABLE);
  // Configure RX
  DMA_Init( (DMA_Channel_TypeDef*)DMA1_Channel2_BASE,
            (uint32_t)UART_RX_BUFF,
            (uint16_t)&UART->DR,
            sizeof(COMM_HEADER),
            DMA_DIR_PeripheralToMemory,
            DMA_Mode_Circular,
            DMA_MemoryIncMode_Inc,
            DMA_Priority_VeryHigh,
            DMA_MemoryDataSize_Byte
            );
  
   // ConfigureTX
  DMA_Init( (DMA_Channel_TypeDef*)DMA1_Channel1_BASE,
            (uint32_t)UART_TX_BUFF,
            (uint16_t)&UART->DR,
            sizeof(COMM_HEADER),
            DMA_DIR_MemoryToPeripheral,
            DMA_Mode_Normal,
            DMA_MemoryIncMode_Inc,
            DMA_Priority_Low,
            DMA_MemoryDataSize_Byte
            );
  
  DMA_GlobalCmd(ENABLE); 
  
  DMA_ITConfig((DMA_Channel_TypeDef*)DMA1_Channel2_BASE, DMA_ITx_TC, ENABLE);
  DMA_ITConfig((DMA_Channel_TypeDef*)DMA1_Channel1_BASE, DMA_ITx_TC, ENABLE);
}

/**
 * Deinitialise UART and DMA
 */
void UART_deinit(void)
{
  DMA_ITConfig((DMA_Channel_TypeDef*)DMA1_Channel2_BASE, DMA_ITx_TC, DISABLE);
  DMA_ITConfig((DMA_Channel_TypeDef*)DMA1_Channel1_BASE, DMA_ITx_TC, DISABLE);
  DMA_GlobalCmd(DISABLE); 
  USART_Cmd(UART, DISABLE);
  GPIO_Init(UART_GPIO_TX_PORT, UART_GPIO_TX_PINS, GPIO_Mode_In_FL_No_IT);
  RTC_WakeUpCmd(DISABLE);
  rxState = NO_COMMUNICATION;
}

/**
 * Transmit data by UART - by DMA - data must be stored already in
 * UART_TX_BUFF - here specify only how many bytes to transfer
 * @param length - how many bytes from DMA_CHANNEL_UART_TX to transfer - actually now it is fixed to 
 * @return true if success
 */
bool UART_sendPacketDMA(COMM_HEADER * package)
{
  if(UART_TX_IN_PROGRESS == FALSE)
  {
    memcpy(UART_TX_BUFF, package, sizeof(COMM_HEADER));
    UART_TX_IN_PROGRESS = TRUE;
    DMA_SetCurrDataCounter((DMA_Channel_TypeDef*)DMA1_Channel1_BASE, sizeof(COMM_HEADER));
    DMA_Cmd((DMA_Channel_TypeDef*)DMA1_Channel1_BASE, ENABLE); 
    return TRUE;
  }
  else
  {
    return FALSE;
  }
}
