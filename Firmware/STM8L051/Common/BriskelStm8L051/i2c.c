
#include "i2c.h"

/**
 * Initialise i2c
 */
void i2cInit(void)
{
	/* I2C  clock Enable*/
  CLK_PeripheralClockConfig(CLK_Peripheral_I2C1, ENABLE);
	
	 /* Initialize I2C peripheral */
  I2C_Init(I2C1, I2C_SPEED, 0xA0,
           I2C_Mode_I2C, I2C_DutyCycle_2,
           I2C_Ack_Enable, I2C_AcknowledgedAddress_7bit);
}

/**
 * Deinitialsie i2c
 */
void i2cDeInit(void)
{
	CLK_PeripheralClockConfig(CLK_Peripheral_I2C1, DISABLE);
}

/**
 * Send data to i2c
 * Blocking operation until the data are transmitted
 */
void i2cSend(unsigned char addr, char const * data, int length, bool appendStop)
{
  volatile u16 timeout = 0;
  
  I2C_GenerateSTART(I2C1, ENABLE);
  while (!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT) && timeout++ < I2C_TIMEOUT);
  I2C_Send7bitAddress(I2C1, (uint8_t)addr & 0xFE, I2C_Direction_Transmitter);
  timeout = 0;
  while (!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED) && timeout++ < I2C_TIMEOUT);
  
  while(length > 0)
  {
    I2C_SendData(I2C1, data[0]);
    data++;
    length--;
    timeout = 0;
    while (!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_TRANSMITTED) && timeout++ < I2C_TIMEOUT);
  }
  
  if(appendStop)
  {
    I2C_GenerateSTOP(I2C1, ENABLE);
  }
  
  if(timeout >= I2C_TIMEOUT)
  {
      // I2C error, try to init whole i2c
    I2C_DeInit(I2C1);
    i2cInit();
  }
}

/**
 * Write first register number (8bit) and then read specified data
 */
void i2cRead(unsigned char addr, char regNo, char * data, int length)
{
  volatile u16 timeout = 0;
  
  I2C_GenerateSTART(I2C1, ENABLE);
  // Send reg NO
  i2cSend(addr & 0xFE, &regNo, 1, FALSE);
  
  // Read dara from reg
  I2C_AcknowledgeConfig(I2C1, ENABLE);
  I2C_GenerateSTART(I2C1, ENABLE);
  while (!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_MODE_SELECT) && timeout++ < I2C_TIMEOUT);
  I2C_Send7bitAddress(I2C1, (uint8_t)addr | 0x01, I2C_Direction_Receiver);
  timeout = 0;
  while (!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED) && timeout++ < I2C_TIMEOUT);
  
  while(length > 0)
  {
    if(length == 1)
    {
      I2C_AcknowledgeConfig(I2C1, DISABLE);
    }
    timeout = 0;
    while (!I2C_CheckEvent(I2C1, I2C_EVENT_MASTER_BYTE_RECEIVED) && timeout++ < I2C_TIMEOUT);
    data[0] = I2C_ReceiveData(I2C1);
    data++;
    length--;
  }
  
  I2C_GenerateSTOP(I2C1, ENABLE);
  
  if(timeout >= I2C_TIMEOUT)
  {
      // I2C error, try to init whole i2c
    I2C_DeInit(I2C1);
    i2cInit();
  }
}
