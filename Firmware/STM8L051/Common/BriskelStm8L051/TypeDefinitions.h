/**
 * Briskel kids - Type definitions
 * 
 * Bref: Here define some basic types
 *
 * Author: Martin Patak
 */

#ifndef TypeDefinitions
#define TypeDefinitions   

#include "stm8l15x.h"
   
#define u8 uint8_t
#define u16 uint16_t

#define i16 int16_t

#endif // TypeDefinitions