/**
 * Briskel kids - Boot manager
 * 
 * Bref: Handle Uart communication
 *
 * Author: Martin Patak
 */

#include "stm8l15x_conf.h" 
#include "boot.h"

#define BOOT_OPTION_BYTE_ADDR_LOW 0x480B
#define BOOT_OPTION_BYTE_ADDR_HIGH 0x480C
#define BOOT_WORD_LOW 0x55
#define BOOT_WORD_HIGH 0xAA

/**
 * Function jumps to bootloader
 */
void jumpToBooloaderCallback(void)
{
  FLASH_Unlock(FLASH_MemType_Data);
  FLASH_ProgramOptionByte(BOOT_OPTION_BYTE_ADDR_LOW, BOOT_WORD_LOW);
  FLASH_ProgramOptionByte(BOOT_OPTION_BYTE_ADDR_HIGH, BOOT_WORD_HIGH);
  FLASH_Lock(FLASH_MemType_Data);
  WWDG_SWReset();
}

/**
 * Function validates the application
 * and sets that device shall not jump to bootloader any more ..
 */
void validateApplication(void)
{
  FLASH_Unlock(FLASH_MemType_Data);
  FLASH_EraseOptionByte(BOOT_OPTION_BYTE_ADDR_LOW);
  FLASH_EraseOptionByte(BOOT_OPTION_BYTE_ADDR_HIGH);
  FLASH_Lock(FLASH_MemType_Data);
}

/**
 * Function checks whether bootloader is still enabled 
 * @return 1 if application is validated, 0 if not
 */
char isAppValidated(void)
{
  if(*((PointerAttr uint8_t*)BOOT_OPTION_BYTE_ADDR_LOW) == BOOT_WORD_LOW &&
     *((PointerAttr uint8_t*)BOOT_OPTION_BYTE_ADDR_HIGH) == BOOT_WORD_HIGH)
  {
    return 0;
  }
  else
  {
    return 1;
  }
}
