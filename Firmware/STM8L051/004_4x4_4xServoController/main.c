/**
 * Briskel kids - main file
 * 
 * 4x servo motor
 *
 * Bref: 4x servo motor
 * 
 * Clock is 16MHz -> Generate 20Hz - therefore divide by 800 000
 * Therefore e.g.:
 *        Prescaler:  16
 *        Divider:  50 000 - 1
 * Control: 1 to 2 ms ...   50000 = 20ms, 1ms = 2500, 2ms = 5000
 * Author: Martin Patak
 */

#include <string.h>
#include <intrinsics.h>
#include <stdlib.h>
#include "CommHeader.h"
#include "boot.h"
#include "watchdog.h"
#include "uart.h"

#define FW_VERSION 0x10 // 1.0
#define BOARD_NO 4
   
// Servo 1 - TIM3 CH2
#define SERVO1_GPIO_PORT  GPIOD
#define SERVO1_GPIO_PINS  GPIO_Pin_0
// Servo 2 - TIM2 CH1
#define SERVO2_GPIO_PORT  GPIOB
#define SERVO2_GPIO_PINS  GPIO_Pin_0
// Servo 3 - TIM2 CH2
#define SERVO3_GPIO_PORT  GPIOB
#define SERVO3_GPIO_PINS  GPIO_Pin_2
#define SERVO4_GPIO_PORT  GPIOB
// Servo 4 - TIM3 CH1
#define SERVO4_GPIO_PINS  GPIO_Pin_1

// LED settings
#define CLOCK_FREQUENCY 16000000
#define TIM2_PRESCALLER_SETTINGS TIM2_Prescaler_1
#define TIM3_PRESCALLER_SETTINGS TIM3_Prescaler_1
#define MAXIMAL_TIMER_VALUE (40000 - 1)
#define FULL_MOVEMENT (2500)
#define HALF_MOVEMENT (FULL_MOVEMENT/2)
#define ZERO_POSITION (FULL_MOVEMENT+HALF_MOVEMENT)

#define SERVO_COUNT 4
static bool newPacketReceived = FALSE;

void setServos(i16 value1, i16 value2, i16 value3, i16 value4);

/**
 * Configure HSI clock - HSI - input 16 MHz
 * divide by x - CPU runs at 16/x MHz
 */
static void ConfigureHSIClock()
{
  CLK_SYSCLKDivConfig(CLK_SYSCLKDiv_8);
}

/**
 * Function called when communication is lost
 */
void communicationLostCallback()
{
  // do nothing - just do not move
}

/**
 * Callback for a received package
 * @param package - pointer to received package
 */
void receivedPackageCallback(COMM_HEADER * package)
{
  newPacketReceived = TRUE;
  uartProcessNewPacket(package);
  UART_sendPacketDMA((package));
}

void initTimers(void)
{
  CLK_PeripheralClockConfig(CLK_Peripheral_TIM2, ENABLE);
  CLK_PeripheralClockConfig(CLK_Peripheral_TIM3, ENABLE);
  
  GPIO_Init(SERVO1_GPIO_PORT, SERVO1_GPIO_PINS, GPIO_Mode_Out_PP_Low_Slow);
  GPIO_Init(SERVO2_GPIO_PORT, SERVO2_GPIO_PINS, GPIO_Mode_Out_PP_Low_Slow);
  GPIO_Init(SERVO3_GPIO_PORT, SERVO3_GPIO_PINS, GPIO_Mode_Out_PP_Low_Slow);
  GPIO_Init(SERVO4_GPIO_PORT, SERVO4_GPIO_PINS, GPIO_Mode_Out_PP_Low_Slow);
  
  TIM2_TimeBaseInit(TIM2_PRESCALLER_SETTINGS, TIM2_CounterMode_Up, MAXIMAL_TIMER_VALUE);
  TIM3_TimeBaseInit(TIM3_PRESCALLER_SETTINGS, TIM3_CounterMode_Up, MAXIMAL_TIMER_VALUE);
  
  TIM2_OC1Init(TIM2_OCMode_PWM1, TIM2_OutputState_Enable, 0, TIM2_OCPolarity_High, TIM2_OCIdleState_Set);
  TIM2_OC2Init(TIM2_OCMode_PWM1, TIM2_OutputState_Enable, 0, TIM2_OCPolarity_High, TIM2_OCIdleState_Set);
   
  TIM2_OC1PreloadConfig(ENABLE);
  TIM2_OC2PreloadConfig(ENABLE);
  TIM2_CtrlPWMOutputs(ENABLE);

  TIM3_OC1Init(TIM3_OCMode_PWM1, TIM3_OutputState_Enable, 0, TIM3_OCPolarity_High, TIM3_OCIdleState_Set);
  TIM3_OC2Init(TIM3_OCMode_PWM1, TIM3_OutputState_Enable, 0, TIM3_OCPolarity_High, TIM3_OCIdleState_Set);
  
  TIM3_OC1PreloadConfig(ENABLE);
  TIM3_OC2PreloadConfig(ENABLE);
  TIM3_CtrlPWMOutputs(ENABLE);
}

// values = -32767 to 32768
void setServos(i16 value1, i16 value2, i16 value3, i16 value4)
{ 
  u16 val1, val2, val3, val4;
  
  // Conversion 32768 / 128 = 256 ... to make half step - this value multiplied by 5 is 1280 .. 1250 is half of the movement
  val1 = ZERO_POSITION + value1 / 128 * 5; 
  val2 = ZERO_POSITION + value2 / 128 * 5; 
  val3 = ZERO_POSITION + value3 / 128 * 5; 
  val4 = ZERO_POSITION + value4 / 128 * 5; 
  
  TIM3_SetCompare2(val1);
  TIM2_SetCompare1(val2);
  TIM2_SetCompare2(val3);
  TIM3_SetCompare1(val4);
}

/**
 * Main loop
 */
int main( void )
{
  i16 values[SERVO_COUNT];
  SETTINGS settings;
  ConfigureHSIClock();
  if(isAppValidated())
  { // Watchdog only for validated application - not for development, ...  
    watchdogInit(); 
  }
  UART_init();
  __enable_interrupt();
  
  initTimers();
  
  TIM3_Cmd(ENABLE);
  TIM2_Cmd(ENABLE);
  configureCommunicationProtocol(0, SERVO_COUNT, FW_VERSION, BOARD_NO); // 4x SERVO
  
  
  while(1)
  {
    if(newPacketReceived)
    {
      newPacketReceived = FALSE;
      
      __disable_interrupt();
      getNewDataFromProtocol(values, &settings); // Process received frame ..
      __enable_interrupt();
      
      if(settings.switchedOn)
      {
        setServos(values[3], values[2], values[1], values[0]);
      }
      else
      {
        // Device is off ... just do not change the value and do not move motors ..
      }
      watchdogKick();
    }
  }
}
