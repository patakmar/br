/**
 * Briskel kids - main file
 * 
 * Bref: Bluetooth - slave
 *
 * Author: Martin Patak
 */

#include <string.h>
#include <intrinsics.h>
#include <stdlib.h>
#include "CommHeader.h"
#include "boot.h"
#include "watchdog.h"
#include "uart.h"

#define FW_VERSION 0x20 // 1.0
#define BOARD_NO 13

#define PWR_GPIO_PORT  GPIOB
#define PWR_GPIO_PINS  GPIO_Pin_5
#define STATE_GPIO_PORT  GPIOB
#define STATE_GPIO_PINS  GPIO_Pin_3
#define LED_GPIO_PORT  GPIOB
#define LED_GPIO_PINS  GPIO_Pin_1

#define UART_OLD_GPIO_TX_PORT       GPIOC
#define UART_OLD_GPIO_TX_PINS       GPIO_Pin_5
#define UART_GPIO_TX_PORT       GPIOA
#define UART_GPIO_TX_PINS       GPIO_Pin_2

#define LED_FLASHING_PERIOD 300

static bool newPacketReceived = FALSE;
static bool irqFlag = FALSE;
static COMM_HEADER pckBackup; // backup ..
static COMM_HEADER header2transfer; // This one is handled by IRQ routine
static bool dataTransferEnabled = FALSE;

/**
 * Configure HSI clock - HSI - input 16 MHz
 * divide by x - CPU runs at 16/x MHz
 */
static void ConfigureHSIClock()
{
  CLK_SYSCLKDivConfig(CLK_SYSCLKDiv_8);
}

/* Defines an interrupt handler for the TIM4 vector. */
#pragma vector = TIM3_TIF_vector
__interrupt __root void Tim3IrqHandler(void)
{
  TIM3_ClearFlag(TIM3_FLAG_Update);
  if(dataTransferEnabled)
  {
    // First make a copy from backup
    __disable_interrupt();
    memcpy(&header2transfer, &pckBackup, sizeof(COMM_HEADER));
    __enable_interrupt();
    UART_sendPacketDMA(&header2transfer);
  }
  
  irqFlag = TRUE;
}

/**
 * Configure timer 4 to generate every 5 ms interrupt
 * TIM4CLK is set to SYSTEM CLOCK = 2 MHz,
 * CLK / PRESCALLER / (PERIOD + 1) is required 200 (5ms)
 * therefore e.g. 2e6 / 64 / (124 + 1) = 200 Hz
 */
static void Timer3_5MsSettings()

{
  CLK_PeripheralClockConfig(CLK_Peripheral_TIM3, ENABLE);

  TIM3_TimeBaseInit(TIM3_Prescaler_1, TIM3_CounterMode_Up, 10000-1); // 5ms
  TIM3_ClearFlag(TIM3_FLAG_Update);
  TIM3_ITConfig(TIM3_IT_Update, ENABLE);

  TIM3_Cmd(ENABLE);
}


/**
 * Function called when communication is lost
 */
void communicationLostCallback()
{
  dataTransferEnabled = FALSE;
}

/**
 * Callback for a received package - put it to buffer.. nothing else to do here
 * @param package - pointer to received package
 */
void receivedPackageCallback(COMM_HEADER * package)
{
  newPacketReceived = TRUE;
  memcpy(&pckBackup, package, sizeof(COMM_HEADER));
}

/**
 * Set LED on or off
 */
void setLed(u8 on)
{
  if(on)
  {
    GPIO_SetBits(LED_GPIO_PORT, LED_GPIO_PINS);
  }
  else
  {
    GPIO_ResetBits(LED_GPIO_PORT, LED_GPIO_PINS);
  }
}

/**
 * Obtain state from the bluetooth board
 */
u8 getState()
{
  return GPIO_ReadInputDataBit(STATE_GPIO_PORT, STATE_GPIO_PINS);
}

/**
 * Main loop
 */
int main( void )
{ 
  u16 timer = 0;
  ConfigureHSIClock();
  
  // Enable Bluetooth power
  GPIO_Init(PWR_GPIO_PORT, PWR_GPIO_PINS, GPIO_Mode_Out_OD_Low_Slow);
  // Init LED GPIO
  GPIO_Init(LED_GPIO_PORT, LED_GPIO_PINS, GPIO_Mode_Out_PP_Low_Slow);
  
  if(isAppValidated())
  { // Watchdog only for validated application - not for development, ...  
    watchdogInit(); 
  }
  UART_init();
  
  // Change the configuration - because slave is using different pins
  // First revert the default one 
  GPIO_Init(UART_OLD_GPIO_TX_PORT, UART_OLD_GPIO_TX_PINS, GPIO_Mode_In_FL_No_IT);
  // Define a new one
  GPIO_Init(UART_GPIO_TX_PORT, UART_GPIO_TX_PINS, GPIO_Mode_Out_PP_High_Fast);
  // Remap pins
  SYSCFG_REMAPPinConfig(REMAP_Pin_USART1TxRxPortA, ENABLE);
  
  // Configure timer
  Timer3_5MsSettings();
  
  __enable_interrupt();
  
  configureCommunicationProtocol(0, 0, FW_VERSION, BOARD_NO);
  
  while(1)
  {   
    if(newPacketReceived)
    {
      newPacketReceived = FALSE;
      dataTransferEnabled = TRUE;
    }
    
    if(irqFlag)
    {
      irqFlag = FALSE;
      
      // Flash when not connected, when connected => switch LED on
      if((timer > LED_FLASHING_PERIOD/2 && dataTransferEnabled)
         || (timer > LED_FLASHING_PERIOD * 0.9 && dataTransferEnabled == FALSE))
      {
        setLed(1);
      }
      else
      {
        setLed(0);
      }
        
      if(timer >= LED_FLASHING_PERIOD)
      {
        timer = 0;
      }
      else
      {
        timer++;
      }
      
      watchdogKick();
    }
  }
}
