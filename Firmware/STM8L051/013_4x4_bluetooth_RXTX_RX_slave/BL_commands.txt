Press button and power one device -> BL mode - 38400 bps ..


Master:
AT+RESET
reset ..
AT+ROLE=1        // Set device as master
AT+UART=115200,0,0               // Set baudrate 115.. 1 stop bit, no parity
AT+CMODE=1               // Connect module to any bluetooth address ....

after reset set 

Slave:
AT+RESET
reset ..
AT+ROLE=0        // Set device as slave ..
AT+UART=115200,0,0               // Set baudrate 115.. 1 stop bit, no parity




AT+BIND=1234, 56, abcdef\r\n
AT+UART=115200,0,0               // Set baudrate 115.. 1 stop bit, no parity

The most common commands for HC-03 and HC-05 are: AT+ROLE (set master
�slave), AT+CMODE( set address pairing) , AT+PSWD (set password).
If you want the master module has the function of remembering slave module, the most
simply way is: First, set AT+CMODE=1. Make the master module pair with the slave
module. Second, set AT+CMODE=0. Then the master module just can make pair with
that specified slave module.
