/**
 * Briskel kids - main file
 * 
 * Bref: Bluetooth - slave
 *
 * Author: Martin Patak
 */

#include <string.h>
#include <intrinsics.h>
#include <stdlib.h>
#include "CommHeader.h"
#include "boot.h"
#include "watchdog.h"
#include "eeprom.h"
#include "uart.h"

#define FW_VERSION 0x20 // 1.0
#define BOARD_NO 13

#define PWR_GPIO_PORT  GPIOB
#define PWR_GPIO_PINS  GPIO_Pin_5
#define STATE_GPIO_PORT  GPIOB
#define STATE_GPIO_PINS  GPIO_Pin_6
#define LED_GPIO_PORT  GPIOB
#define LED_GPIO_PINS  GPIO_Pin_1
#define KEY_GPIO_PORT  GPIOC
#define KEY_GPIO_PINS  GPIO_Pin_4
#define BTN_GPIO_PORT  GPIOB
#define BTN_GPIO_PINS  GPIO_Pin_0
#define RST_GPIO_PORT  GPIOB
#define RST_GPIO_PINS  GPIO_Pin_3

#define UART_GPIO_TX_PORT       GPIOC
#define UART_GPIO_TX_PINS       GPIO_Pin_5
#define UART_GPIO_RX_BOARD_PORT       GPIOA
#define UART_GPIO_RX_BOARD_PINS       GPIO_Pin_3
#define UART_OLD_GPIO_TX_PORT       GPIOA
#define UART_OLD_GPIO_TX_PINS       GPIO_Pin_2


#define SET_AT "AT\r\n"
#define SET_BAUDRATE "AT+UART=115200,0,0\r\n"
#define GET_ADDRESS "AT+ADDR?\r\n"

#define LED_FLASHING_PERIOD 300

static bool newPacketReceived = FALSE;
static bool irqFlag = FALSE;
static COMM_HEADER pckBackup; // backup ..
static COMM_HEADER header2transfer; // This one is handled by IRQ routine
static bool dataTransferEnabled = FALSE;

typedef struct  
{
  u8 bluetoothConfigured;
} Settings;

static Settings settings;

/**
 * Configure HSI clock - HSI - input 16 MHz
#define BTN_GPIO_PINS  GPIO_Pin_0
#define RST_GPIO_PORT  GPIOB
#define RST_GPIO_PINS  GPIO_Pin_3

 * divide by x - CPU runs at 16/x MHz
 */
static void ConfigureHSIClock()
{
  CLK_SYSCLKDivConfig(CLK_SYSCLKDiv_8);
}

/* Defines an interrupt handler for the TIM4 vector. */
#pragma vector = TIM3_TIF_vector
__interrupt __root void Tim3IrqHandler(void)
{
  TIM3_ClearFlag(TIM3_FLAG_Update);
  if(dataTransferEnabled)
  {
    // First make a copy from backup
    __disable_interrupt();
    memcpy(&header2transfer, &pckBackup, sizeof(COMM_HEADER));
    __enable_interrupt();
    UART_sendPacketDMA(&header2transfer);
  }
  
  irqFlag = TRUE;
}

/**
 * Configure timer 4 to generate every 5 ms interrupt
 * TIM4CLK is set to SYSTEM CLOCK = 2 MHz,
 * CLK / PRESCALLER / (PERIOD + 1) is required 200 (5ms)
 * therefore e.g. 2e6 / 64 / (124 + 1) = 200 Hz
 */
static void Timer3_5MsSettings()

{
  CLK_PeripheralClockConfig(CLK_Peripheral_TIM3, ENABLE);

  TIM3_TimeBaseInit(TIM3_Prescaler_1, TIM3_CounterMode_Up, 10000-1); // 5ms
  TIM3_ClearFlag(TIM3_FLAG_Update);
  TIM3_ITConfig(TIM3_IT_Update, ENABLE);

  TIM3_Cmd(ENABLE);
}


/**
 * Function called when communication is lost
 */
void communicationLostCallback()
{
  dataTransferEnabled = FALSE;
}

/**
 * Callback for a received package - put it to buffer.. nothing else to do here
 * @param package - pointer to received package
 */
void receivedPackageCallback(COMM_HEADER * package)
{
  newPacketReceived = TRUE;
  memcpy(&pckBackup, package, sizeof(COMM_HEADER));
}

/**
 * Set LED on or off
 */
void setLed(u8 on)
{
  if(on)
  {
    GPIO_SetBits(LED_GPIO_PORT, LED_GPIO_PINS);
  }
  else
  {
    GPIO_ResetBits(LED_GPIO_PORT, LED_GPIO_PINS);
  }
}

/**
 * Obtain state from the bluetooth board
 */
u8 getState()
{
  return GPIO_ReadInputDataBit(STATE_GPIO_PORT, STATE_GPIO_PINS);
}

/**
 * Send data to UART 
 */
void uartTx(const char * msg, u8 length)
{
  while(length > 0)
  {
    while(!USART_GetFlagStatus(USART1, USART_FLAG_TXE)); // Wait till buffer Tx is empty
    USART_SendData8(USART1, msg[0]);
    length--;
    msg++;
  }
  
  while(!USART_GetFlagStatus(USART1, USART_FLAG_TXE)); // Wait till buffer Tx is empty
}

/**
 * Read data from uart in pooling way
 * return 1 if not ended by \n
 */
u8 uartRead(char * buffer, u8 maxLength)
{
  u8 retVal = 1;
  // Response is on the other channel
  SYSCFG_REMAPPinConfig(REMAP_Pin_USART1TxRxPortA, ENABLE);
  
  // receive if something is in the buffer
  USART_ReceiveData8(USART1);
  USART_ClearFlag(USART1, USART_FLAG_OR);
  
  u16 timeout;
  
  while(maxLength > 0)
  {
    timeout = 1000;
    while(!USART_GetFlagStatus(USART1, USART_FLAG_RXNE) && timeout-- > 0); // Wait till buffer Tx is empty
    buffer[0] = USART_ReceiveData8(USART1);
    
    if(buffer[0] == '\n')
    {
      retVal = 0;
      break;
    }
    else if(timeout == 0) // Or timeout..
    {
      break;
    }
    else
    {
      buffer++;
      maxLength--;
    }
  }
  
  // Go back to original channel
  SYSCFG_REMAPPinConfig(REMAP_Pin_USART1TxRxPortA, DISABLE);
  
  return retVal;
}

/**
 * Simple delay
 */
void delay(u16 value)
{
  while(value-- > 0);
}

/**
 * Reset bluetooth
 */
void resetBluetooth()
{
  GPIO_Init(RST_GPIO_PORT, RST_GPIO_PINS, GPIO_Mode_Out_PP_Low_Slow);
  delay(5000);
  GPIO_Init(RST_GPIO_PORT, RST_GPIO_PINS, GPIO_Mode_Out_PP_High_Slow);
  delay(15000);
  
  watchdogKick();
}

// Send command - return 1 if string was not connected by \n 
u8 sendCommand(const char * data, u8 length)
{
  char buff[60];
  u8 retVal;
  
  watchdogKick();
  memset(buff, 0, sizeof(buff));
  uartTx(data, length);
  retVal = uartRead(buff, sizeof(buff));
  
  delay(1000);
  return retVal;
}

/**
 * Configure device
 */
void configureBluetooth()
{
  u8 errors = 0;
  watchdogKick();
  
   // Initialise UART
  GPIO_Init(UART_GPIO_TX_PORT, UART_GPIO_TX_PINS, GPIO_Mode_Out_PP_High_Slow);
  GPIO_Init(UART_GPIO_RX_BOARD_PORT, UART_GPIO_RX_BOARD_PINS, GPIO_Mode_In_PU_No_IT);
  CLK_PeripheralClockConfig(CLK_Peripheral_USART1, ENABLE);
  USART_Init(USART1, 38400, USART_WordLength_8b, USART_StopBits_1, USART_Parity_No, (USART_Mode_TypeDef)(USART_Mode_Rx | USART_Mode_Tx));
  USART_Cmd(USART1, ENABLE);
  
  // Reset and enter AT mode
  GPIO_Init(KEY_GPIO_PORT, KEY_GPIO_PINS, GPIO_Mode_Out_PP_High_Slow);
  resetBluetooth();

  //errors += sendCommand(SET_AT, sizeof(SET_AT)-1);
  errors += sendCommand(SET_AT, sizeof(SET_AT)-1);
  //errors += sendCommand(GET_ADDRESS, sizeof(GET_ADDRESS)-1);
  errors += sendCommand(SET_BAUDRATE, sizeof(SET_BAUDRATE)-1);
  
  // Reset in normal mode
  delay(30000);
  GPIO_Init(KEY_GPIO_PORT, KEY_GPIO_PINS, GPIO_Mode_Out_PP_Low_Slow);
  resetBluetooth();
  
  // Store settings
  settings.bluetoothConfigured = 1;
  storeSettings((u8*)&settings, sizeof(Settings));
}

/** 
 * Function reads settings from EEPROM
*/
void restoreSettings()
{
  if(readSettings((u8*)&settings, sizeof(Settings)) == FALSE)
  { // No settings found - use default
    settings.bluetoothConfigured = 0;
  }
}

/**
 * Main loop
 */
int main( void )
{ 
  //storeMac(13, 2, 1);
  
  //configureCommunicationProtocol(0, 0, FW_VERSION, BOARD_NO);
  
  ConfigureHSIClock();
  
  GPIO_Init(LED_GPIO_PORT, LED_GPIO_PINS, GPIO_Mode_Out_PP_High_Slow);
  
  // Enable Bluetooth power
  GPIO_Init(KEY_GPIO_PORT, KEY_GPIO_PINS, GPIO_Mode_Out_PP_Low_Slow);
  GPIO_Init(RST_GPIO_PORT, RST_GPIO_PINS, GPIO_Mode_Out_PP_High_Slow); // That is release
  GPIO_Init(PWR_GPIO_PORT, PWR_GPIO_PINS, GPIO_Mode_Out_PP_Low_Slow);
  
  if(isAppValidated())
  { // Watchdog only for validated application - not for development, ...  
    watchdogInit(); 
  }
  
  restoreSettings();
  if(settings.bluetoothConfigured == 0)
  { // Button pressed - go to this state - it will never return
    // Needed for configuration
    //GPIO_Init(UART_OLD_GPIO_TX_PORT, UART_OLD_GPIO_TX_PINS, GPIO_Mode_Out_PP_High_Slow);
    configureBluetooth();
  }
  
  
  UART_init();
  
  // Change the configuration - because slave is using different pins
  // First revert the default one 
  GPIO_Init(UART_OLD_GPIO_TX_PORT, UART_OLD_GPIO_TX_PINS, GPIO_Mode_Out_PP_High_Slow);
  // Define a new one
  GPIO_Init(UART_GPIO_TX_PORT, UART_GPIO_TX_PINS, GPIO_Mode_In_FL_No_IT);
  // Remap pins
  SYSCFG_REMAPPinConfig(REMAP_Pin_USART1TxRxPortA, ENABLE);
  
  // Configure timer
  Timer3_5MsSettings();
  
  __enable_interrupt();
  
  while(1)
  {   
    if(newPacketReceived)
    {
      newPacketReceived = FALSE;
      dataTransferEnabled = TRUE;
    }
    
    if(irqFlag)
    {
      irqFlag = FALSE;
      
      // Flash when not connected, when connected => switch LED on
      if(GPIO_ReadInputDataBit(STATE_GPIO_PORT, STATE_GPIO_PINS))
      {
        setLed(1);
      }
      else
      {
        setLed(0);
      }
      
      watchdogKick();
    }
  }
}
