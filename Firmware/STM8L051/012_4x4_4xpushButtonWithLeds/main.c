/**
 * Briskel kids - main file
 * 
 * Bref: basic control - 4x push button with LED
 *
 * Author: Martin Patak
 */

#include <string.h>
#include <intrinsics.h>
#include <stdlib.h>
#include "CommHeader.h"
#include "boot.h"
#include "watchdog.h"
#include "uart.h"

#define FW_VERSION 0x10 // 1.0
#define BOARD_NO 12

#define SW1_GPIO_PORT  GPIOC
#define SW1_GPIO_PINS  GPIO_Pin_4
#define SW2_GPIO_PORT  GPIOB
#define SW2_GPIO_PINS  GPIO_Pin_6
#define SW3_GPIO_PORT  GPIOB
#define SW3_GPIO_PINS  GPIO_Pin_3
#define SW4_GPIO_PORT  GPIOB
#define SW4_GPIO_PINS  GPIO_Pin_1

#define LED1_GPIO_PORT  GPIOB
#define LED1_GPIO_PINS  GPIO_Pin_7
#define LED2_GPIO_PORT  GPIOB
#define LED2_GPIO_PINS  GPIO_Pin_5
#define LED3_GPIO_PORT  GPIOB
#define LED3_GPIO_PINS  GPIO_Pin_4
#define LED4_GPIO_PORT  GPIOB
#define LED4_GPIO_PINS  GPIO_Pin_2

#define BUTTON_COUNT 4

i16 SW[BUTTON_COUNT];

/**
 * Configure HSI clock - HSI - input 16 MHz
 * divide by x - CPU runs at 16/x MHz
 */
static void ConfigureHSIClock()
{
  CLK_SYSCLKDivConfig(CLK_SYSCLKDiv_8);
}

/**
 * Function called when communication is lost
 */
void communicationLostCallback()
{
}

/**
 * Callback for a received package
 * @param package - pointer to received package
 */
void receivedPackageCallback(COMM_HEADER * package)
{
  // In case button is not switched => disable the signal ..
  uartProcessNewPacket(package);
  UART_sendPacketDMA((package));
}

/**
 * Enable outputs of LEDs ...
 */
void configureLeds()
{
  GPIO_Init(LED1_GPIO_PORT, LED1_GPIO_PINS, GPIO_Mode_Out_PP_Low_Slow);
  GPIO_Init(LED2_GPIO_PORT, LED2_GPIO_PINS, GPIO_Mode_Out_PP_Low_Slow);
  GPIO_Init(LED3_GPIO_PORT, LED3_GPIO_PINS, GPIO_Mode_Out_PP_Low_Slow);
  GPIO_Init(LED4_GPIO_PORT, LED4_GPIO_PINS, GPIO_Mode_Out_PP_Low_Slow);
}

/**
 * Configure LEDs ..
 */
void setLeds()
{
  if(SW[0]==0)GPIO_ResetBits(LED1_GPIO_PORT, LED1_GPIO_PINS);
  else     GPIO_SetBits  (LED1_GPIO_PORT, LED1_GPIO_PINS);
  if(SW[1]==0)GPIO_ResetBits(LED2_GPIO_PORT, LED2_GPIO_PINS);
  else     GPIO_SetBits  (LED2_GPIO_PORT, LED2_GPIO_PINS);
  if(SW[2]==0)GPIO_ResetBits(LED3_GPIO_PORT, LED3_GPIO_PINS);
  else     GPIO_SetBits  (LED3_GPIO_PORT, LED3_GPIO_PINS);
  if(SW[3]==0)GPIO_ResetBits(LED4_GPIO_PORT, LED4_GPIO_PINS);
  else     GPIO_SetBits  (LED4_GPIO_PORT, LED4_GPIO_PINS);
}

/**
 * Scan switches and store to global variables
 */
void scanSw()
{
  if(!GPIO_ReadInputDataBit(SW1_GPIO_PORT, SW1_GPIO_PINS))
  {
    SW[0] = 0x7FFF;
  }
  else
  {
    SW[0] = 0;
  }
  
  if(!GPIO_ReadInputDataBit(SW2_GPIO_PORT, SW2_GPIO_PINS))
  {
    SW[1] = 0x7FFF;
  }
  else
  {
    SW[1] = 0;
  }
  
  if(!GPIO_ReadInputDataBit(SW3_GPIO_PORT, SW3_GPIO_PINS))
  {
    SW[2] = 0x7FFF;
  }
  else
  {
    SW[2] = 0;
  }
  
  if(!GPIO_ReadInputDataBit(SW4_GPIO_PORT, SW4_GPIO_PINS))
  {
    SW[3] = 0x7FFF;
  }
  else
  {
    SW[3] = 0;
  }
}

/**
 * Main loop
 */
int main( void )
{  
  volatile u16 delay;
  ConfigureHSIClock();
  if(isAppValidated())
  { // Watchdog only for validated application - not for development, ...  
    watchdogInit(); 
  }
  
  // SW 1 and SW2 do not have pull up.. add it here
  GPIO_Init(SW1_GPIO_PORT, SW1_GPIO_PINS, GPIO_Mode_In_PU_No_IT);
  GPIO_Init(SW2_GPIO_PORT, SW2_GPIO_PINS, GPIO_Mode_In_PU_No_IT);
  GPIO_Init(SW3_GPIO_PORT, SW3_GPIO_PINS, GPIO_Mode_In_PU_No_IT);
  GPIO_Init(SW4_GPIO_PORT, SW4_GPIO_PINS, GPIO_Mode_In_PU_No_IT);
  
  // wait a bit so that the pullups makes their job
  for(delay = 256; delay > 0; delay--)
  {
    scanSw();
  }
  
  scanSw();
  configureLeds();
  setLeds();
  configureCommunicationProtocol(BUTTON_COUNT, 0, FW_VERSION, BOARD_NO);
  
  UART_init();
  __enable_interrupt();
  
  while(1)
  {
    scanSw();
    
    __disable_interrupt();
    addNewDataToProtocol(SW);
    __enable_interrupt();
    
    setLeds();
    
    watchdogKick();
  }
}

