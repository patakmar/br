/**
 * Briskel kids - main file
 * 
 * Bref: basic control - PWM ... controlling a buzzer
 * Motor/LED : PB2 - TIM2 - CH2
 *
 * Author: Martin Patak
 */

#include <string.h>
#include <intrinsics.h>
#include <stdlib.h>
#include "CommHeader.h"
#include "boot.h"
#include "watchdog.h"
#include "uart.h"

#define FW_VERSION 0x10 // 1.0
#define BOARD_NO 8

#define BUZZER_GPIO_PORT  GPIOB
#define BUZZER_GPIO_PINS  GPIO_Pin_2

#define CLOCK_FREQUENCY 16000000
#define TIM_PRESCALLER_SETTINGS TIM2_Prescaler_4 // Change the line below as well
#define TIM_PRESCALLER_VALUE 4
#define MIN_VALUE 1000
#define PWM_CHANNEL_COUNT 1

static bool newPacketReceived = FALSE;
static bool BuzzerEnabled = FALSE;
void setPwm(i16 value);
i16 pwmValue;
u16 metronomPeriod;



/**
 * Configure HSI clock - HSI - input 16 MHz
 * divide by x - CPU runs at 16/x MHz
 */
static void ConfigureHSIClock()
{
  CLK_SYSCLKDivConfig(CLK_SYSCLKDiv_4);
}

/**
 * Function called when communication is lost
 */
void communicationLostCallback()
{
  pwmValue = 0;
}

/**
 * Callback for a received package
 * @param package - pointer to received package
 */
void receivedPackageCallback(COMM_HEADER * package)
{
  newPacketReceived = TRUE;
  uartProcessNewPacket(package);
  UART_sendPacketDMA((package));
}

void initPwm(void)
{
  #define DEFAULT_PERIOD 6000 // default value it is overwritten later on
  // Configure Buzzer
  CLK_PeripheralClockConfig(CLK_Peripheral_TIM2, ENABLE);
  // The code below is initialised later on
  //GPIO_Init(BUZZER_GPIO_PORT, BUZZER_GPIO_PINS, GPIO_Mode_Out_PP_Low_Slow); 
  TIM2_TimeBaseInit(TIM_PRESCALLER_SETTINGS, TIM2_CounterMode_Down, DEFAULT_PERIOD);
  TIM2_OC2Init(TIM2_OCMode_PWM2, TIM2_OutputState_Enable, DEFAULT_PERIOD / 4, TIM2_OCPolarity_High, TIM2_OCIdleState_Set);
  TIM2_OC2PreloadConfig(ENABLE);
  TIM2_CtrlPWMOutputs(ENABLE);
  
  // Tim 1 - IRQ with high priority every 1 ms
  // 4000 000 -> 
  CLK_PeripheralClockConfig(CLK_Peripheral_TIM3, ENABLE);
  TIM3_TimeBaseInit(TIM3_Prescaler_1, TIM3_CounterMode_Up, 4000-1); // 1ms
  TIM3_ClearFlag(TIM3_FLAG_Update);
  TIM3_ITConfig(TIM3_IT_Update, ENABLE);
  TIM3_Cmd(ENABLE);
  
  // Set highest priority
  ITC_SetSoftwarePriority(TIM3_UPD_OVF_TRG_BRK_IRQn, ITC_PriorityLevel_2);
}

/* Defines an interrupt handler for the TIM4 vector. */
#pragma vector = TIM3_TIF_vector
__interrupt __root void Tim3IrqHandler(void)
{
  TIM3_ClearFlag(TIM3_FLAG_Update);
  setPwm(pwmValue);
}

// values = -32767 to 32768
void setPwm(i16 value)
{
  #define MIN_FREQUENCY 100.0
  #define MAX_FREQUENCY 2000.0 
  #define BEEP_AUTORELOAD 1500 
  
  u16 autoReloadValue = 0;
  float tmp;
  u16 compareValue = 0;
  
  #define METRONOM_ON_TIME_MS 70
  #define METRONOM_PWM_PERIOD 20000
  static u16 msCounter = 0;
  
  if(value < 0)
  { // Make metronom
    if(msCounter < METRONOM_ON_TIME_MS)
    {
      value = METRONOM_PWM_PERIOD;
    }
    else
    {
      value = 0;
    }
    
    if(msCounter > metronomPeriod)
    {
      msCounter = 0;
    }
    else
    {
      msCounter++;
    }
  }
  else
  {
    msCounter = 0;
  }
  
  if(value < MIN_VALUE)
  {
    if(BuzzerEnabled)
    {
      GPIO_Init(BUZZER_GPIO_PORT, BUZZER_GPIO_PINS, GPIO_Mode_In_FL_No_IT);
      TIM2_Cmd(DISABLE);
      BuzzerEnabled = FALSE;
    }
  }
  else
  {
    tmp = (float)(value - MIN_VALUE);
    tmp = tmp / (0x8000 - MIN_VALUE); // how it shall be value between 0 and 1
    tmp = MIN_FREQUENCY + tmp * (MAX_FREQUENCY - MIN_FREQUENCY); // now it shall be desired frequncy
    // Frequncy = 16E6 / (TIM_PRESCALLER_VALUE * X)  ==>
    // X = CLOCK_FREQUENCY / desired_frequency;...X can be max 2^16-1
    tmp = (CLOCK_FREQUENCY / TIM_PRESCALLER_VALUE) / tmp;

    autoReloadValue = (u16)tmp;
    compareValue = autoReloadValue / 2;
    TIM2_SetAutoreload(autoReloadValue);
    TIM2_SetCompare2(compareValue);
    
    if(BuzzerEnabled == FALSE)
    {
      GPIO_Init(BUZZER_GPIO_PORT, BUZZER_GPIO_PINS, GPIO_Mode_Out_PP_Low_Slow);
      TIM2_Cmd(ENABLE);
      BuzzerEnabled = TRUE;
    }
  } 
}

// Funcion computes metronom total period and sets it to global variable
// This function makes from -32768 -> 4000 ms  cca ..
//                           0     -> 0ms
void computeMetronomPeriod(i16 pwmVal)
{
  metronomPeriod = ((i16)0x7FFE + pwmVal) / 8;
}

/**
 * Main loop
 */
int main( void )
{ 
  SETTINGS settings;
  ConfigureHSIClock();
  if(isAppValidated())
  { // Watchdog only for validated application - not for development, ...  
    watchdogInit(); 
  }
  UART_init();
  __enable_interrupt();
    
  configureCommunicationProtocol(0, PWM_CHANNEL_COUNT, FW_VERSION, BOARD_NO); // 1x PWM
  initPwm();
  
  while(1)
  {   
    if(newPacketReceived)
    {
      newPacketReceived = FALSE;
      
      __disable_interrupt();
      getNewDataFromProtocol(&pwmValue, &settings); // Process received frame ..
      if(!settings.switchedOn 
         || (pwmValue > -MIN_VALUE  && pwmValue < MIN_VALUE))
      {
        pwmValue = 0;
      }
      if(pwmValue < 0)
      {
        computeMetronomPeriod(pwmValue);
      }
      __enable_interrupt();
      
      
      watchdogKick();
    }
  }
}
