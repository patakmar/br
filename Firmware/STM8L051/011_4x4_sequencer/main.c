/**
 * Briskel kids - main file
 * 
 * Bref: basic control - Sequencer
 * 
 * Pins : 
 * A: PC0
 * B: PC4
 * C: PB5
 * D: PB7
 * E: PC1
 * F: PB6
 * G: PB4
 * RDP: PB3
 * PWR: PB1
 * SW: PB2
 * Author: Martin Patak
 */

#include <string.h>
#include <intrinsics.h>
#include <stdlib.h>
#include "CommHeader.h"
#include "boot.h"
#include "watchdog.h"
#include "uart.h"
#include "eeprom.h"

#define FW_VERSION 0x10 // 1.0
#define BOARD_NO 11

#define ADC_GROUP_SPEEDCHANNEL     ADC_Group_SlowChannels
#define ADC_POTENCIOMETER_1_CHANNEL ADC_Channel_15
#define ADC_POTENCIOMETER_2_CHANNEL ADC_Channel_16

const uint16_t CHANNELS [] = {ADC_POTENCIOMETER_1_CHANNEL,
                              ADC_POTENCIOMETER_2_CHANNEL};

typedef struct  
{
  u8 sequencerNo;
  u8 eatSignals;
} Settings;

static Settings settings;
static u8 packetReceived = 0;

#define RDP_GPIO_PORT  GPIOC
#define RDP_GPIO_PINS  GPIO_Pin_0
#define C_GPIO_PORT  GPIOC
#define C_GPIO_PINS  GPIO_Pin_4
#define B_GPIO_PORT  GPIOB
#define B_GPIO_PINS  GPIO_Pin_5
#define D_GPIO_PORT  GPIOB
#define D_GPIO_PINS  GPIO_Pin_7
#define E_GPIO_PORT  GPIOC
#define E_GPIO_PINS  GPIO_Pin_1
#define F_GPIO_PORT  GPIOB
#define F_GPIO_PINS  GPIO_Pin_6
#define G_GPIO_PORT  GPIOB
#define G_GPIO_PINS  GPIO_Pin_4
#define A_GPIO_PORT  GPIOB
#define A_GPIO_PINS  GPIO_Pin_3

#define PWR_GPIO_PORT  GPIOB
#define PWR_GPIO_PINS  GPIO_Pin_1
#define SW_GPIO_PORT  GPIOB
#define SW_GPIO_PINS  GPIO_Pin_2

// Define which segments shall be enabled 
typedef enum 
{
  CHAR_0 = 0x3F,
  CHAR_1 = 0x06,
  CHAR_2 = 0x5B,
  CHAR_3 = 0x4F,
  CHAR_4 = 0x66,
  CHAR_5 = 0x6D,
  CHAR_6 = 0x7D,
  CHAR_7 = 0x07,
  CHAR_8 = 0x7F,
  CHAR_9 = 0x67,
} NUMBERS;

NUMBERS NUM_SEQ[] = {CHAR_0, CHAR_1, CHAR_2, CHAR_3, CHAR_4, CHAR_5, CHAR_6, CHAR_7, CHAR_8, CHAR_9}; 

/**
 * Configure HSI clock - HSI - input 16 MHz
 * divide by x - CPU runs at 16/x MHz
 */
static void ConfigureHSIClock()
{
  CLK_SYSCLKDivConfig(CLK_SYSCLKDiv_8);
}

/**
 * Function called when communication is lost
 */
void communicationLostCallback()
{
}

/**
 * Callback for a received package
 * @param package - pointer to received package
 */
void receivedPackageCallback(COMM_HEADER * package)
{
  u8 reduceCnt, i;
  // Set the sequencer .. the CRC is computed later on.. so it is not necessary to be done here
  if(settings.eatSignals)
  { // Set sequencer so that all consecutive components eat the signal...
    package->settings.sequencer = 0xF; 
  }
  else
  { // All consecutive components should not eat signals ...
    package->settings.sequencer = 0;
  }

  if(package->packetType == STANDARD_DATA_PACKET)
  {     // This is unique, process it here
    reduceCnt = settings.sequencerNo; // Make a local copy
    
    for(i = 0; i < CHANNEL_MAX - reduceCnt; i++)
    {
      package->channel[i] = package->channel[i + reduceCnt];
    }
    // Fill the rest with zeros
    for(; i < CHANNEL_MAX; i++)
    {
      package->channel[i] = 0x0;
    }
    
    package->checksum = computeHeaderCrc(package); // compute new checksum
  }
  else
  {
    uartProcessNewPacket(package);
  }
  
  UART_sendPacketDMA((package));
  packetReceived = 1;
}

/**
 * Initialise all outputs
 */
void initGpios()
{
  // Enable segments
  GPIO_Init(A_GPIO_PORT, A_GPIO_PINS, GPIO_Mode_Out_OD_HiZ_Slow);
  GPIO_Init(B_GPIO_PORT, B_GPIO_PINS, GPIO_Mode_Out_OD_HiZ_Slow);
  GPIO_Init(C_GPIO_PORT, C_GPIO_PINS, GPIO_Mode_Out_OD_HiZ_Slow);
  GPIO_Init(D_GPIO_PORT, D_GPIO_PINS, GPIO_Mode_Out_OD_HiZ_Slow);
  GPIO_Init(E_GPIO_PORT, E_GPIO_PINS, GPIO_Mode_Out_OD_HiZ_Slow);
  GPIO_Init(F_GPIO_PORT, F_GPIO_PINS, GPIO_Mode_Out_OD_HiZ_Slow);
  GPIO_Init(G_GPIO_PORT, G_GPIO_PINS, GPIO_Mode_Out_OD_HiZ_Slow);
  GPIO_Init(RDP_GPIO_PORT, RDP_GPIO_PINS, GPIO_Mode_Out_OD_HiZ_Slow);
  // Enable 
  GPIO_Init(RDP_GPIO_PORT, RDP_GPIO_PINS, GPIO_Mode_Out_OD_Low_Slow);
}

/**
 * Get whether swithc was enabled/ disabled ..
 */
u8 getSw()
{
  return !GPIO_ReadInputDataBit(SW_GPIO_PORT, SW_GPIO_PINS);
}

/**
 * Set segment ...
 * @param segment - bit 0 = A, bit 7 = G, 
 */
void setSegment(NUMBERS segment)
{
  if(segment & 0x01)    GPIO_ResetBits(A_GPIO_PORT, A_GPIO_PINS);
  else                  GPIO_SetBits  (A_GPIO_PORT, A_GPIO_PINS);
  
  if(segment & 0x02)    GPIO_ResetBits(B_GPIO_PORT, B_GPIO_PINS);
  else                  GPIO_SetBits  (B_GPIO_PORT, B_GPIO_PINS);
  
  if(segment & 0x04)    GPIO_ResetBits(C_GPIO_PORT, C_GPIO_PINS);
  else                  GPIO_SetBits  (C_GPIO_PORT, C_GPIO_PINS);
  
  if(segment & 0x08)    GPIO_ResetBits(D_GPIO_PORT, D_GPIO_PINS);
  else                  GPIO_SetBits  (D_GPIO_PORT, D_GPIO_PINS);
  
  if(segment & 0x10)    GPIO_ResetBits(E_GPIO_PORT, E_GPIO_PINS);
  else                  GPIO_SetBits  (E_GPIO_PORT, E_GPIO_PINS);
  
  if(segment & 0x20)    GPIO_ResetBits(F_GPIO_PORT, F_GPIO_PINS);
  else                  GPIO_SetBits  (F_GPIO_PORT, F_GPIO_PINS);
  
  if(segment & 0x40)    GPIO_ResetBits(G_GPIO_PORT, G_GPIO_PINS);
  else                  GPIO_SetBits  (G_GPIO_PORT, G_GPIO_PINS);
}

/**
 * Set decimap point
 */
void setRdp(u8 rdp)
{
  if(rdp)GPIO_ResetBits(RDP_GPIO_PORT, RDP_GPIO_PINS);
  else GPIO_SetBits(RDP_GPIO_PORT, RDP_GPIO_PINS);
}

/** 
 * Function reads settings from EEPROM
*/
void restoreSettings()
{
  if(readSettings((u8*)&settings, sizeof(Settings)) == FALSE)
  { // No settings found - use default
    settings.sequencerNo = 0;
    settings.eatSignals = 0;
  }
}


/**
 * Main loop
 */
int main( void )
{ 
  #define EAT_FLASH_FREQ 100
  #define STORE_TO_EEPROM_WHEN_NOT_ACTIVE 5*200 // 5 second
  u16 sw; // Count how long the button was pressed
  u16 timer = 0;
  u16 storetoEEpromCnt = 0;
  ConfigureHSIClock();
  if(isAppValidated())
  { // Watchdog only for validated application - not for development, ...  
    watchdogInit(); 
  }
  UART_init();
  __enable_interrupt();
  
  initGpios();
  
  restoreSettings();
  setSegment(NUM_SEQ[settings.sequencerNo]);
  sw = 0;
  
  while(1)
  {
    if(packetReceived)
    {
      packetReceived = 0;
      timer++;
      
      // Switch .. observe when it was presse ... and do appropriate actions ..
      if(getSw() > 0)
      {
        if(sw == 0xFFFF)
        {
          // do nothing ..
        }
        else if(sw > 200 * 3)
        {
          settings.eatSignals = !settings.eatSignals;
          sw = 0xFFFF;
          setSegment(NUM_SEQ[settings.sequencerNo]);
          storetoEEpromCnt = STORE_TO_EEPROM_WHEN_NOT_ACTIVE;
        }
        else
        {
          sw++;
        }
      }
      else if(sw > 10 && sw != 0xFFFF)
      { // Released
        settings.sequencerNo = (settings.sequencerNo >= sizeof(NUM_SEQ) - 1)? 0 : settings.sequencerNo + 1;
        setSegment(NUM_SEQ[settings.sequencerNo]);
        storetoEEpromCnt = STORE_TO_EEPROM_WHEN_NOT_ACTIVE;
        sw = 0;
      }
      else
      {
        sw = 0;
      }
      
      // Reset flashing counter
      if(timer > EAT_FLASH_FREQ)
      {
        timer = 0;
      }
      else
      {
        timer++;
      }
      
      // Make flashing for eating signals .. otherwise switch on
      if(settings.eatSignals && timer < EAT_FLASH_FREQ/2)
      {
        setRdp(0);
      }
      else
      {
        setRdp(1);
      }
      
      // Store to eeprom if needed - whe timeout elapses ..
      if(storetoEEpromCnt == 1)
      {
        storeSettings((u8*)&settings, sizeof(Settings));
        storetoEEpromCnt = 0;
      }
      else if(storetoEEpromCnt > 0)
      {
        storetoEEpromCnt--;
      }
      
      watchdogKick();
    } 
  }
}

