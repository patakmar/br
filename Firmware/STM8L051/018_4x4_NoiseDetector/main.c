/**
 * Briskel kids - main file
 * 
 * Bref: Noise detector - sample sound with 20 kHz and make low and high pass filters ..
 *
 * Author: Martin Patak
 */

#include <string.h>
#include <math.h>
#include <intrinsics.h>
#include <stdlib.h>
#include "CommHeader.h"
#include "boot.h"
#include "watchdog.h"
#include "uart.h"

#define FW_VERSION 0x10 // 1.0
#define BOARD_NO 18

#define MIC_ENABLE_GPIO_PORT  GPIOB
#define MIC_ENABLE_GPIO_PINS  GPIO_Pin_1
#define POT_ENABLE_GPIO_PORT  GPIOB
#define POT_ENABLE_GPIO_PINS  GPIO_Pin_4


#define ADC_GROUP_SPEEDCHANNEL     ADC_Group_SlowChannels
#define SIGNAL_ADC_CHANNEL ADC_Channel_13
#define POTENTIOMETER_ADC_CH ADC_Channel_15

#define ADC1_DR_ADDRESS        ((uint16_t)0x5344)
#define SAMPLES_MAX 64
#define BUFFER_SIZE (2*SAMPLES_MAX) 
static u16 ADC_Buffer[BUFFER_SIZE];

#define CPU_FREQ_DIVIDER CLK_SYSCLKDiv_4

// The goat is to achieve 2x 20 kHz sampling (because CPU samples 2x channel)
// 4 MHz / prescaller / (value+1)  = 40 kHz
// => 4 MHz / 40kHz / prescaller = (value + 1)
// 4 MHz / 40 kHz / 2 =  (49+1)
#define TIM2_PRESCALLER_SETTINGS TIM2_Prescaler_4
#define MAXIMAL_TIMER_VALUE (50-1)

static bool newPacketReceived = FALSE;

/**
 * Configure HSI clock - HSI - input 16 MHz
 * divide by x - CPU runs at 16/x MHz
 */
static void ConfigureHSIClock()
{
  CLK_SYSCLKDivConfig(CPU_FREQ_DIVIDER);
}

/**
 * Function called when communication is lost
 */
void communicationLostCallback()
{
}

/**
 * Callback for a received package
 * @param package - pointer to received package
 */
void receivedPackageCallback(COMM_HEADER * package)
{
  newPacketReceived = TRUE;
  uartProcessNewPacket(package);
  UART_sendPacketDMA((package));
}

/**
 * Initialise gpio - enable pyrosensor 
 */
void initGpios()
{
  GPIO_Init(MIC_ENABLE_GPIO_PORT, MIC_ENABLE_GPIO_PINS, GPIO_Mode_Out_PP_High_Slow);
  GPIO_Init(POT_ENABLE_GPIO_PORT, POT_ENABLE_GPIO_PINS, GPIO_Mode_Out_PP_High_Slow);
}

/**
  * @brief  Configure ADC peripheral 
  * @param  None
  * @retval None
  */
static void ADC_Config(void)
{
  /* Enable ADC1 clock */
  CLK_PeripheralClockConfig(CLK_Peripheral_ADC1, ENABLE);
  
  /* Initialize and configure ADC1 */
  ADC_Init(ADC1, ADC_ConversionMode_Single, ADC_Resolution_12Bit, ADC_Prescaler_1);
  ADC_SamplingTimeConfig(ADC1, ADC_Group_SlowChannels, ADC_SamplingTime_16Cycles);
  ADC_SamplingTimeConfig(ADC1, ADC_Group_FastChannels, ADC_SamplingTime_16Cycles);

  /* Enable channel ADC and Signal */
  ADC_ChannelCmd(ADC1, SIGNAL_ADC_CHANNEL, ENABLE);
  ADC_ChannelCmd(ADC1, POTENTIOMETER_ADC_CH, ENABLE); 
  
  /* Enable ADC1 */
  ADC_Cmd(ADC1, ENABLE);
  
  /* Enable ADC1 DMA requests*/
  ADC_DMACmd(ADC1, ENABLE);
  
  /* Start ADC1 Conversion using TIM2 TRGO*/
  ADC_ExternalTrigConfig(ADC1, ADC_ExtEventSelection_Trigger3,
                         ADC_ExtTRGSensitivity_Rising);
}

/**
  * @brief  Configure DMA peripheral 
  * @param  None
  * @retval None
  */
static void DMA_Config(void)
{
  /* Enable DMA1 clock */
  CLK_PeripheralClockConfig(CLK_Peripheral_DMA1, ENABLE);
  
  /* Connect ADC to DMA channel 0 */
  SYSCFG_REMAPDMAChannelConfig(REMAP_DMA1Channel_ADC1ToChannel0);

  DMA_Init(DMA1_Channel0, (uint32_t)ADC_Buffer,
           ADC1_DR_ADDRESS,
           BUFFER_SIZE,
           DMA_DIR_PeripheralToMemory,
           DMA_Mode_Circular,
           DMA_MemoryIncMode_Inc,
           DMA_Priority_VeryHigh,
           DMA_MemoryDataSize_HalfWord);
  
  /* DMA enable */
  DMA_GlobalCmd(ENABLE);
  
  DMA_ITConfig((DMA_Channel_TypeDef*)DMA1_Channel0_BASE, DMA_ITx_TC, ENABLE);
  DMA_ITConfig((DMA_Channel_TypeDef*)DMA1_Channel0_BASE, DMA_ITx_HT, ENABLE);
  
  DMA_Cmd((DMA_Channel_TypeDef*)DMA1_Channel0_BASE, ENABLE); 
  
}

typedef struct
{
  u16 scaling;
  s16 noiseMax;
  s16 noiseMin;
} STATUS;
static STATUS status;

/**
  * Process samples which are stored in global buffer
  * The samples are stored in following order: - see which channels are enabled
  * ...
  */
void processSamples(u8 offset)
{
  u8 addr = offset;
  u8 i;
  #define LOW_PASS_LEN 7 
  #define FIXED_POINT_EXTEND 3
  
  // record one value of actual potentiometer settings
  status.scaling =  ADC_Buffer[1];
  
  for(i = 0; i < SAMPLES_MAX; i = i + 2)
  {
    // Process the min max ..
    if(ADC_Buffer[addr] > status.noiseMax)
    {   
      status.noiseMax = ADC_Buffer[addr];
    }
    
    if(ADC_Buffer[addr] < status.noiseMin)
    {   
      status.noiseMin = ADC_Buffer[addr];
    }
    
    addr += 2; // Increment address by 2 to skip to next audio sample
  }
}

/**
 * Interrupt when DMA transfer TX is finished
 */
#pragma vector = DMA1_CH0_TC_vector
__interrupt __root void DmaUartTxCompletedIrq(void)
{
  if(DMA_GetFlagStatus(DMA1_FLAG_IFC1))
  {
    // UART DMA TX interrupt ..
    extern void DmaUartTxCompleteCallback();
    DmaUartTxCompleteCallback();
  }
  
  if(DMA_GetFlagStatus(DMA1_FLAG_IFC0))
  {    
    if(DMA_GetFlagStatus(DMA1_FLAG_TC0))
    {   // Upper memory
      DMA_ClearITPendingBit(DMA1_IT_TC0);
      DMA_ClearFlag(DMA1_FLAG_TC0);
      processSamples(SAMPLES_MAX);
    }
    else
    {
      DMA_ClearITPendingBit(DMA1_IT_TC0);
      DMA_ClearFlag(DMA1_FLAG_HT0);
      processSamples(0);
    }
  }
}

/**
  * @brief  Generate start of conversion for ADC
  * @param  None
  * @retval None
  */
static void TIM2_Config(void)
{ 
  CLK_PeripheralClockConfig(CLK_Peripheral_TIM2, ENABLE);
  TIM2_TimeBaseInit(TIM2_PRESCALLER_SETTINGS, TIM2_CounterMode_Up, MAXIMAL_TIMER_VALUE);
  
  /* Master Mode selection: Update event */
  TIM2_SelectOutputTrigger(TIM2_TRGOSource_Update);
  /* Enable TIM1 */
  TIM2_Cmd(ENABLE);

}

/**
 * Get scaling - exponentiel
 */
float getScaling(float potAdcVal)
{
  return (4096 - potAdcVal) / 64.0;
}

//* release new values to output
void releaseNewValues()
{
  STATUS backup;
  i16 values[1];
  u32 u32Tmp;
  float scaling;
  static float filteredScaling = 0;
  static float filteredOutput = 0;
  
  // Make a local backup when IRQ is disabled
  __disable_interrupt();
  memcpy(&backup, &status, sizeof(STATUS));
  status.noiseMax -= 20;
  status.noiseMin += 20;
  __enable_interrupt();
  
  // first get scaling
  #define SCALING_FILTER 0.1
  filteredScaling = (1-SCALING_FILTER) * filteredScaling + SCALING_FILTER * (float) backup.scaling;
  scaling = getScaling(filteredScaling);
  
  // Process min and max ... the worst case MIN -> 0, Max 4095..
  #define OUTPUT_FILTER 0.25 
  u32Tmp = backup.noiseMax - backup.noiseMin;
  filteredOutput = (1-OUTPUT_FILTER) * filteredOutput + OUTPUT_FILTER * (u32)(u32Tmp * scaling);
  filteredOutput = log(filteredOutput);
  
  if(filteredOutput < 0.0) filteredOutput = 0;
  else if(filteredOutput > 0x7FFF) filteredOutput = 0x7FFF;
  values[0] = (s16)filteredOutput;
    
  __disable_interrupt();
  addNewDataToProtocol(values); // Process received frame ..
  __enable_interrupt();
}

/**
 * Main loop
 */
int main( void )
{ 
  // Check boar version before interrupts are enabled
  configureCommunicationProtocol(1, 0, FW_VERSION, BOARD_NO);
  ConfigureHSIClock();
  if(isAppValidated())
  { // Watchdog only for validated application - not for development, ...  
    watchdogInit(); 
  }
  UART_init();
  
  initGpios();
  ADC_Config();
  DMA_Config();
  TIM2_Config();
  
  status.noiseMax = 2048;
  status.noiseMin = 2048;
  
  __enable_interrupt();
  
  while(1)
  {   
    if(newPacketReceived)
    {
      newPacketReceived = FALSE; 
      releaseNewValues();
      watchdogKick();
    }
  }
}
