/**
 * Briskel kids - main file
 * 
 * Bref: Bluetooth - master module
 *
 * Author: Martin Patak
 */

#include <string.h>
#include <intrinsics.h>
#include <stdlib.h>
#include "CommHeader.h"
#include "boot.h"
#include "watchdog.h"
#include "uart.h"

#define FW_VERSION 0x10 // 1.0
#define BOARD_NO 13

#define PWR_GPIO_PORT  GPIOB
#define PWR_GPIO_PINS  GPIO_Pin_5
#define STATE_GPIO_PORT  GPIOB
#define STATE_GPIO_PINS  GPIO_Pin_3
#define LED_GPIO_PORT  GPIOB
#define LED_GPIO_PINS  GPIO_Pin_1

#define LED_FLASHING_PERIOD 300

static bool newPacketReceived = FALSE;

/**
 * Configure HSI clock - HSI - input 16 MHz
 * divide by x - CPU runs at 16/x MHz
 */
static void ConfigureHSIClock()
{
  CLK_SYSCLKDivConfig(CLK_SYSCLKDiv_8);
}

/**
 * Function called when communication is lost
 */
void communicationLostCallback()
{
}

#define SENT_EVERY_XTH_PACKET 5
static u8 sendXthPacket = 0;

/**
 * Callback for a received package
 * @param package - pointer to received package
 */
void receivedPackageCallback(COMM_HEADER * package)
{
  newPacketReceived = TRUE;
  if(sendXthPacket == SENT_EVERY_XTH_PACKET-1)
  {
    sendXthPacket = 0;
    uartProcessNewPacket(package);
    UART_sendPacketDMA((package));
  }
  else
  {
    sendXthPacket++;
  }
}

/**
 * Set LED on or off
 */
void setLed(u8 on)
{
  if(on)
  {
    GPIO_SetBits(LED_GPIO_PORT, LED_GPIO_PINS);
  }
  else
  {
    GPIO_ResetBits(LED_GPIO_PORT, LED_GPIO_PINS);
  }
}

/**
 * Obtain state from the bluetooth board
 */
u8 getState()
{
  u8 state = GPIO_ReadInputDataBit(STATE_GPIO_PORT, STATE_GPIO_PINS);
  return state;
}

/**
 * Main loop
 */
int main( void )
{ 
  u16 timer = 0;
  ConfigureHSIClock();
  if(isAppValidated())
  { // Watchdog only for validated application - not for development, ...  
    watchdogInit(); 
  }
  UART_init();
  __enable_interrupt();
  
  // Enable Bluetooth power
  GPIO_Init(PWR_GPIO_PORT, PWR_GPIO_PINS, GPIO_Mode_Out_OD_Low_Slow);
  // State pull up
  //GPIO_Init(STATE_GPIO_PORT, STATE_GPIO_PINS, GPIO_Mode_In_PU_No_IT);
  // Init LED GPIO
  GPIO_Init(LED_GPIO_PORT, LED_GPIO_PINS, GPIO_Mode_Out_PP_Low_Slow);
  
  configureCommunicationProtocol(0, 0, FW_VERSION, BOARD_NO);
  
  while(1)
  {   
    if(newPacketReceived)
    {
      newPacketReceived = FALSE;
      
      // Flash when not connected, when connected => switch LED on
      if(!getState() && timer < LED_FLASHING_PERIOD/2)
      {
        setLed(0);
      }
      else
      {
        setLed(1);
      }
      
      if(timer >= LED_FLASHING_PERIOD)
      {
        timer = 0;
      }
      else
      {
        timer++;
      }
      
      watchdogKick();
    }
  }
}
