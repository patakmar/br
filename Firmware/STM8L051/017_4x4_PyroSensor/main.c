/**
 * Briskel kids - main file
 * 
 * Bref: pyro sensor / 
 * Motor/LED : PB2 - TIM2 - CH2
 *
 * Author: Martin Patak
 */

#include <string.h>
#include <intrinsics.h>
#include <stdlib.h>
#include "CommHeader.h"
#include "boot.h"
#include "watchdog.h"
#include "uart.h"

#define FW_VERSION 0x10 // 1.0
#define BOARD_NO 17

#define PYRO_ENABLE_GPIO_PORT  GPIOC
#define PYRO_ENABLE_GPIO_PINS  GPIO_Pin_1
#define PYRO_SIGNAL_GPIO_PORT  GPIOC
#define PYRO_SIGNAL_GPIO_PINS  GPIO_Pin_0

static bool newPacketReceived = FALSE;

/**
 * Configure HSI clock - HSI - input 16 MHz
 * divide by x - CPU runs at 16/x MHz
 */
static void ConfigureHSIClock()
{
  CLK_SYSCLKDivConfig(CLK_SYSCLKDiv_8);
}

/**
 * Function called when communication is lost
 */
void communicationLostCallback()
{
}

/**
 * Callback for a received package
 * @param package - pointer to received package
 */
void receivedPackageCallback(COMM_HEADER * package)
{
  newPacketReceived = TRUE;
  uartProcessNewPacket(package);
  UART_sendPacketDMA((package));
}

/**
 * Initialise gpio - enable pyrosensor 
 */
void initGpios()
{
  GPIO_Init(PYRO_ENABLE_GPIO_PORT, PYRO_ENABLE_GPIO_PINS, GPIO_Mode_Out_OD_Low_Slow);
}

/**
 * Obtain value from the sensor - make a peak detector ... 
 */
i16 getValue()
{
  static i16 filter = 0;
  if(GPIO_ReadInputDataBit(PYRO_SIGNAL_GPIO_PORT, PYRO_SIGNAL_GPIO_PINS))
  {
    filter = 200*3;
  }
  else if(filter > 0)
  {
    filter--;
  }
  
  return (filter > 0)? 0x7FFF : 0;
}

/**
 * Main loop
 */
int main( void )
{ 
  i16 value;
  ConfigureHSIClock();
  if(isAppValidated())
  { // Watchdog only for validated application - not for development, ...  
    watchdogInit(); 
  }
  UART_init();
  __enable_interrupt();
  
  initGpios();
  
  configureCommunicationProtocol(1, 0, FW_VERSION, BOARD_NO); 
  
  value = getValue();
  
  while(1)
  {   
    if(newPacketReceived)
    {
      newPacketReceived = FALSE;
      
      value = getValue();
      
      __disable_interrupt();
      addNewDataToProtocol(&value); // Process received frame ..
      __enable_interrupt();
      
      watchdogKick();
    }
  }
}
