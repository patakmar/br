/**
 * Briskel kids - main file
 * 
 * Bref: PB0 - TIM2_CH1  - PWM output
 *
 * Author: Martin Patak
 */

#include <string.h>
#include <intrinsics.h>
#include <stdlib.h>
#include "CommHeader.h"
#include "boot.h"
#include "watchdog.h"
#include "uart.h"
#include "Definitions.h"

#define FW_VERSION 0x10 
#define BOARD_NO 15

#define PWM_GPIO_PORT  GPIOB
#define PWM_GPIO_PINS  GPIO_Pin_0

#define LED1_GPIO_PORT  GPIOB
#define LED1_GPIO_PINS  GPIO_Pin_4
#define LED2_GPIO_PORT  GPIOB
#define LED2_GPIO_PINS  GPIO_Pin_3
#define LED3_GPIO_PORT  GPIOB
#define LED3_GPIO_PINS  GPIO_Pin_2
#define LED4_GPIO_PORT  GPIOB
#define LED4_GPIO_PINS  GPIO_Pin_1

#define MAXIMAL_TIMER_VALUE (255)

#define DMA_ARR_SIZE 128
static u8 DMA_ARR[DMA_ARR_SIZE];

#define SIN_LENGTH 256
const u8 SIN_WAVE[] = {32,33,34,34,35,36,37,37,38,39,40,40,41,42,42,43,44,45,45,46,47,47,48,49,49,50,50,51,52,52,53,53,54,54,55,55,56,56,57,57,58,58,59,59,59,60,60,60,61,61,61,61,62,62,62,62,62,63,63,63,63,63,63,63,63,63,63,63,63,63,63,63,62,62,62,62,62,61,61,61,61,60,60,60,59,59,59,58,58,57,57,56,56,55,55,54,54,53,53,52,52,51,50,50,49,49,48,47,47,46,45,45,44,43,42,42,41,40,40,39,38,37,37,36,35,34,34,33,32,31,30,30,29,28,27,27,26,25,24,24,23,22,22,21,20,19,19,18,17,17,16,15,15,14,14,13,12,12,11,11,10,10,9,9,8,8,7,7,6,6,5,5,5,4,4,4,3,3,3,3,2,2,2,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,3,3,3,3,4,4,4,5,5,5,6,6,7,7,8,8,9,9,10,10,11,11,12,12,13,14,14,15,15,16,17,17,18,19,19,20,21,22,22,23,24,24,25,26,27,27,28,29,30,30,31};

#define SIGNALS_CONSUMED 8
#define SIN_WAWES_MAX 4
u16 chIncrease[SIN_WAWES_MAX]; // How much to increase
u16 chPointer[SIN_WAWES_MAX]; // Current pointer - use upper 8 bits
u8 * chU8IncP = (u8*)chPointer;
bool newPacketReceived = FALSE;


/**
 * Configure HSI clock - HSI - input 16 MHz
 * divide by x - CPU runs at 16/x MHz
 */
static void ConfigureHSEClock()
{
  volatile u16 i;
  
  CLK_HSEConfig(CLK_HSE_ON);
  for(i = 0; i < 1000; i++); // Just delay
  for(i = 0; i < 50000; i++) // check flag valid
  {
    if(CLK_GetFlagStatus(CLK_FLAG_HSERDY) == SET)
    {
      CLK_SYSCLKSourceConfig(CLK_SYSCLKSource_HSE);
      CLK_SYSCLKSourceSwitchCmd(ENABLE);
      break;
    }
  }
  
  CLK_SYSCLKDivConfig(CLK_SYSCLKDiv_2);
}

/**
 * Function called when communication is lost
 */
void communicationLostCallback()
{
  chIncrease[0] = 0;
  chIncrease[1] = 0;
  chIncrease[2] = 0;
  chIncrease[3] = 0;
}

/**
 * Callback for a received package
 * @param package - pointer to received package
 */
void receivedPackageCallback(COMM_HEADER * package)
{
  newPacketReceived = TRUE;
  uartProcessNewPacket(package);
  UART_sendPacketDMA((package));
}

void initPwm(void)
{
  CLK_PeripheralClockConfig(CLK_Peripheral_TIM2, ENABLE);
  
  GPIO_Init(PWM_GPIO_PORT, PWM_GPIO_PINS, GPIO_Mode_Out_PP_Low_Slow);
  
  TIM2_TimeBaseInit(TIM2_Prescaler_1, TIM2_CounterMode_Up, MAXIMAL_TIMER_VALUE);
  
  TIM2_OC1Init(TIM2_OCMode_PWM1, TIM2_OutputState_Enable, 0, TIM2_OCPolarity_High, TIM2_OCIdleState_Set);
  TIM2_OC1PreloadConfig(ENABLE);
  TIM2_CtrlPWMOutputs(ENABLE);
}

void initLeds()
{
  GPIO_Init(LED1_GPIO_PORT, LED1_GPIO_PINS, GPIO_Mode_Out_PP_High_Slow);
  GPIO_Init(LED2_GPIO_PORT, LED2_GPIO_PINS, GPIO_Mode_Out_PP_High_Slow);
  GPIO_Init(LED3_GPIO_PORT, LED3_GPIO_PINS, GPIO_Mode_Out_PP_High_Slow);
  GPIO_Init(LED4_GPIO_PORT, LED4_GPIO_PINS, GPIO_Mode_Out_PP_High_Slow);
}

/**
  * Set LEDs based on upper bit of current pointer
  */
void setLeds()
{
  if(chIncrease[0] > 0)GPIO_ResetBits(LED1_GPIO_PORT, LED1_GPIO_PINS);
  else                  GPIO_SetBits(LED1_GPIO_PORT, LED1_GPIO_PINS);
  if(chIncrease[1] > 0)GPIO_ResetBits(LED2_GPIO_PORT, LED2_GPIO_PINS);
  else                  GPIO_SetBits(LED2_GPIO_PORT, LED2_GPIO_PINS);
  if(chIncrease[2] > 0)GPIO_ResetBits(LED3_GPIO_PORT, LED3_GPIO_PINS);
  else                  GPIO_SetBits(LED3_GPIO_PORT, LED3_GPIO_PINS);
  if(chIncrease[3] > 0)GPIO_ResetBits(LED4_GPIO_PORT, LED4_GPIO_PINS);
  else                  GPIO_SetBits(LED4_GPIO_PORT, LED4_GPIO_PINS);
}

/** 
 * Initialise DMA CH1 - data providing to timer
 */ 
void initCH1Dma()
{
  TIM2_DMACmd(TIM2_DMASource_CC1, ENABLE);
  TIM2_SelectCCDMA(ENABLE);
  
  // Configure DMA
  CLK_PeripheralClockConfig(CLK_Peripheral_DMA1, ENABLE);
  // Configure RX
  DMA_Init( (DMA_Channel_TypeDef*)DMA1_Channel0_BASE,
            (uint32_t)DMA_ARR, //DMA_ARR,
            (uint16_t)&TIM2->CCR1L,
            DMA_ARR_SIZE,//DMA_ARR_SIZE,
            DMA_DIR_MemoryToPeripheral,
            DMA_Mode_Circular,
            DMA_MemoryIncMode_Inc,
            DMA_Priority_VeryHigh,
            DMA_MemoryDataSize_Byte
            );
  
  DMA_GlobalCmd(ENABLE); 
  
  DMA_ITConfig((DMA_Channel_TypeDef*)DMA1_Channel0_BASE, DMA_ITx_TC, ENABLE);
  DMA_ITConfig((DMA_Channel_TypeDef*)DMA1_Channel0_BASE, DMA_ITx_HT, ENABLE);
  
  DMA_Cmd((DMA_Channel_TypeDef*)DMA1_Channel0_BASE, ENABLE); 
}

/**
 * Interrupt when DMA transfer TX is finished
 */
#pragma vector = DMA1_CH0_TC_vector
__interrupt __root void DmaUartTxCompletedIrq(void)
{
  u8 start, stop, i;
  
  if(DMA_GetFlagStatus(DMA1_FLAG_IFC1))
  {
    // UART DMA TX interrupt ..
    extern void DmaUartTxCompleteCallback();
    DmaUartTxCompleteCallback();
  }
  
  if(DMA_GetFlagStatus(DMA1_FLAG_IFC0))
  {
    DMA_ClearITPendingBit(DMA1_IT_TC0);
    if(DMA_GetCurrDataCounter((DMA_Channel_TypeDef*)DMA1_Channel0_BASE) > DMA_ARR_SIZE*3/4
       || DMA_GetCurrDataCounter((DMA_Channel_TypeDef*)DMA1_Channel0_BASE) < DMA_ARR_SIZE*1/4)
    {   // Upper memory
      DMA_ClearFlag(DMA1_FLAG_TC0);
      start = DMA_ARR_SIZE/2;
      stop = DMA_ARR_SIZE;
    }
    else
    {
      DMA_ClearFlag(DMA1_FLAG_HT0);
      start = 0;
      stop = DMA_ARR_SIZE/2;
    }
    
    for(i = start; i < stop; i++)
    {
      chPointer[0] += chIncrease[0];
      chPointer[1] += chIncrease[1];
      chPointer[2] += chIncrease[2];
      chPointer[3] += chIncrease[3];
      
      DMA_ARR[i] = SIN_WAVE[chU8IncP[0]] + SIN_WAVE[chU8IncP[2]] + SIN_WAVE[chU8IncP[4]] + SIN_WAVE[chU8IncP[6]];
    }
  }
}

/**
 * Compute new values ... and replace with provided values
 */
void computeNewIncreases(i16 * values, SETTINGS * settings)
{
  u8 i;
  u16 cnt = 0;
  // The tables below - see attached excel sheet
  const u16 INCR_POINTER[] = {549,616,691,732,822,923,1036,1097}; 
  static u8 state[SIGNALS_CONSUMED] = {0,0,0,0,0,0,0,0};
  
  for(i = 0; i < SIGNALS_CONSUMED; i++)
  {
    if(values[i] > LIMIT_HIGH) state[i] = 1;
    else if(values[i] < LIMIT_LOW) state[i] = 0;
  }
  
  for(i = 0; i < SIGNALS_CONSUMED && cnt < SIN_WAWES_MAX; i++)
  {
    if(state[i])
    {
      values[cnt] = INCR_POINTER[i];
      cnt++;
    }
  }
  
  // In case not all signal are assinged, assign the rest with zeros
  for(i = cnt; i < SIN_WAWES_MAX; i++)
  {
    values[cnt] = 0;
    cnt++;
  }
}

/**
 * Main loop
 */
int main( void )
{  
  u8 i;
  volatile u16 delay;
  i16 newValues[SIGNALS_CONSUMED];
  SETTINGS settings;
  
  ConfigureHSEClock();
  
  initPwm();
  TIM2_Cmd(ENABLE);
  TIM2_SetCompare1(128);
  
  chIncrease[0] = 0;
  chIncrease[1] = 0;
  chIncrease[2] = 0;
  chIncrease[3] = 0;
  
  __enable_interrupt();
  initCH1Dma();
  initLeds();
  
  // Disable watchdog ... because if this box is connected by bluetooth - and no signal is comming, 
  // the watchdog restarts it .. and it is annoying to listen the noise because the PWM output 
  // pulled high during reset
  //if(isAppValidated())
  //{ // Watchdog only for validated application - not for development, ...  
   //watchdogInit(); 
  //}
  
  configureCommunicationProtocol(0, SIGNALS_CONSUMED, FW_VERSION, BOARD_NO);
  
  UART_init();
  
  while(1)
  {  
    if(newPacketReceived)
    {
      newPacketReceived = FALSE;
      
      __disable_interrupt();
      getNewDataFromProtocol(newValues, &settings); // get new data from protocol..
      __enable_interrupt();
      
      computeNewIncreases(newValues, &settings); // Compute new values 
      
      // Apply new values when IRQ is disabled ..
      __disable_interrupt();
      for(i = 0; i < SIN_WAWES_MAX; i++)
      {
        chIncrease[i] = (u16)newValues[i];
      }
      __enable_interrupt();
      
      setLeds();
      
      watchdogKick();
    }
  }
}

