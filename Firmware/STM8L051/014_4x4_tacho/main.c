/**
 * Briskel kids - main file
 *
 *
 * Author: Martin Patak
 */

#include <string.h>
#include <intrinsics.h>
#include <stdlib.h>
#include "CommHeader.h"
#include "boot.h"
#include "watchdog.h"
#include "uart.h"

#define FW_VERSION 0x10 // 1.0
#define BOARD_NO 14   

#define CONSUMED_SIGNALS 1
#define PRODUCED_SIGNALS 0
#define FLASHING_PERIOD (750/5) // it is in 5 ms ...

// Define LED array - in sequence from LED 1 to LED10
#define LED_COUNT 10
const GPIO_TypeDef * LED_PORT [] = {GPIOC, GPIOB, GPIOB, GPIOB, GPIOB, GPIOB, GPIOB, GPIOD, GPIOA, GPIOA};
const GPIO_Pin_TypeDef LED_PIN[] = {GPIO_Pin_4, GPIO_Pin_7, GPIO_Pin_6, GPIO_Pin_5, GPIO_Pin_3, GPIO_Pin_2, GPIO_Pin_1, GPIO_Pin_0, GPIO_Pin_3, GPIO_Pin_2};
const u8 POLARITY[] = {0,0,0,0,0,1,1,1,1,1};
const u16 LIMITS[] = {2979, 5958 ,8936, 11915, 14894, 17873, 20852, 23831, 26809, 29788};
static u8 LedValue[] = {0,0,0,0,0,0,0,0,0,0};
#define HYSTERESIS 800

static bool newPacketReceived = FALSE;

void setLeds(i16 value, u16 periodCnt);

/**
 * Configure HSI clock - HSI - input 16 MHz
 * divide by x - CPU runs at 16/x MHz
 */
static void ConfigureHSIClock()
{
  CLK_SYSCLKDivConfig(CLK_SYSCLKDiv_4);
}

/**
 * Function called when communication is lost
 */
void communicationLostCallback()
{
  setLeds(0, 0);
}

/**
 * Callback for a received package
 * @param package - pointer to received package
 */
void receivedPackageCallback(COMM_HEADER * package)
{
  newPacketReceived = TRUE;
  uartProcessNewPacket(package);
  UART_sendPacketDMA((package));
}

void initLed(void)
{
  u8 i;
  for(i = 0; i < LED_COUNT; i++)
  {
    if(POLARITY[i])
    {
      GPIO_Init((GPIO_TypeDef*)LED_PORT[i], LED_PIN[i], GPIO_Mode_Out_PP_Low_Slow);
    }
    else
    {
      GPIO_Init((GPIO_TypeDef*)LED_PORT[i], LED_PIN[i], GPIO_Mode_Out_PP_High_Slow);
    }
  }
}

// values = -32767 to 32768
void setLeds(i16 value, u16 periodCnt)
{
  u8 i;
  u8 negative = 0;
  u8 set; 
  
  if(value < 0)
  {
    negative = 1;
    value = -value;
  }
  
  for(i = 0; i < LED_COUNT; i++)
  {
    set = -1;
    if(!negative)
    {
      if(value > LIMITS[i])
      {
        LedValue[i] = 1;
      }
      else if(value < LIMITS[i] - HYSTERESIS)
      {
        LedValue[i] = 0;
      }
      
      set = LedValue[i];
    } 
    else if(negative)   
    {
      if(value > LIMITS[LED_COUNT-i-1])
      {
        LedValue[i] = 1;
      }
      else if(value < LIMITS[LED_COUNT-i-1] - HYSTERESIS)
      {
        LedValue[i] = 0;
      }
      
      set = LedValue[i];
      
      if(i == 0 && periodCnt > FLASHING_PERIOD/2)
      { // Make flashing with last element
        set = 0;
      }
    }
    
    if(set == 1){
      if(POLARITY[i])GPIO_SetBits((GPIO_TypeDef*)LED_PORT[i], LED_PIN[i]);
      else                GPIO_ResetBits((GPIO_TypeDef*)LED_PORT[i], LED_PIN[i]);
    }
    else if(set == 0)
    {
      if(POLARITY[i])GPIO_ResetBits((GPIO_TypeDef*)LED_PORT[i], LED_PIN[i]);
      else                GPIO_SetBits((GPIO_TypeDef*)LED_PORT[i], LED_PIN[i]);
    }
    else
    {
      // Do not change the LED
    }
  }
  
}

/**
 * Main loop
 */
int main( void )
{
  u16 periodCnt = 0;
  i16 value;
  SETTINGS settings;
  
  configureCommunicationProtocol(PRODUCED_SIGNALS,CONSUMED_SIGNALS, FW_VERSION, BOARD_NO);
  
  ConfigureHSIClock();
  if(isAppValidated())
  { // Watchdog only for validated application - not for development, ...  
    watchdogInit(); 
  }
  UART_init();
  __enable_interrupt();
  
  initLed();
  
  while(1)
  {   
    
    if(newPacketReceived)
    {
      newPacketReceived = FALSE;
      
      __disable_interrupt();
      getNewDataFromProtocol(&value, &settings); // Process received frame ..
      __enable_interrupt();
      
      if(settings.switchedOn)
      {
        setLeds(value, periodCnt);
      }
      else
      {
        setLeds(0,0);
      }
      periodCnt++;
      if(periodCnt > FLASHING_PERIOD)
      {
        periodCnt = 0; 
      }
      
      watchdogKick();
    }
  }
}