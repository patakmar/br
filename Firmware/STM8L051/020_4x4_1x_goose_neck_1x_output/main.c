/**
 * Briskel kids - main file
 * 
 * Bref: basic control - PWM ...
 * Motor/LED : PB1 - TIM3 - CH1
 *
 * Author: Martin Patak
 */

#include <string.h>
#include <intrinsics.h>
#include <stdlib.h>
#include "CommHeader.h"
#include "boot.h"
#include "watchdog.h"
#include "uart.h"

#define FW_VERSION 0x10 // 1.0
#define BOARD_NO 20

#define PWM_GPIO_PORT  GPIOB
#define PWM_GPIO_PINS  GPIO_Pin_1

// LED settings
#define CLOCK_FREQUENCY 16000000
#define TIM2_PRESCALLER_SETTINGS TIM2_Prescaler_1
#define TIM3_PRESCALLER_SETTINGS TIM3_Prescaler_1

// PWM
#define MAXIMAL_TIMER_VALUE (256)
#define PWM_CHANNEL_COUNT 1

#define FLASHING_PERIOD (2000/5) // it is in 5 ms ...

static bool newPacketReceived = FALSE;

void setPwm(i16 value1, u16 periodCnt);

/**
 * Configure HSI clock - HSI - input 16 MHz
 * divide by x - CPU runs at 16/x MHz
 */
static void ConfigureHSIClock()
{
  CLK_SYSCLKDivConfig(CLK_SYSCLKDiv_4);
}

/**
 * Function called when communication is lost
 */
void communicationLostCallback()
{
  setPwm(0,0);
}

/**
 * Callback for a received package
 * @param package - pointer to received package
 */
void receivedPackageCallback(COMM_HEADER * package)
{
  newPacketReceived = TRUE;
  uartProcessNewPacket(package);
  UART_sendPacketDMA((package));
}

void initPwm(void)
{
  CLK_PeripheralClockConfig(CLK_Peripheral_TIM3, ENABLE);
  
  GPIO_Init(PWM_GPIO_PORT, PWM_GPIO_PINS, GPIO_Mode_Out_PP_Low_Slow);
  
  TIM3_TimeBaseInit(TIM3_PRESCALLER_SETTINGS, TIM3_CounterMode_Up, MAXIMAL_TIMER_VALUE);
  
  TIM3_OC1Init(TIM3_OCMode_PWM1, TIM3_OutputState_Enable, 0, TIM3_OCPolarity_High, TIM3_OCIdleState_Set);
  TIM3_OC1PreloadConfig(ENABLE);
  TIM3_CtrlPWMOutputs(ENABLE);
}

// Convert value from u16 into 8u value which might be added to PWM ... 
// output in range 0 to 255
u16 convertValue(i16 value)
{
  const u8 table[256] = {0,0,0,0,0,0,0,0,0,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,250,251,252,253,254,255};
  // The value is now between 0 and 16384... we need to cut it to 255 max .. divide it by 64 .. shift by
  u8 bitShift = 7;
  u16 retValue;
  u8 index;
  
  index = value >> bitShift;
  retValue = table[index];
  
  return retValue;
}

// values = -32767 to 32768
void setPwm(i16 value1, u16 periodCnt)
{
  u8 negativeForceZero = (periodCnt < FLASHING_PERIOD/2) ? 1 : 0;
  
  value1 = (value1 > 0)? value1 : (negativeForceZero)? 0 : -value1;
  value1 = convertValue(value1);
  
  TIM3_SetCompare1(value1);
}

/**
 * Main loop
 */
i16 value1, value2, value3;
int main( void )
{
  u16 periodCnt = 0;
  i16 pwmValue;
  SETTINGS settings;
  ConfigureHSIClock();
  if(isAppValidated())
  { // Watchdog only for validated application - not for development, ...  
    watchdogInit(); 
  }
  UART_init();
  __enable_interrupt();
  
  initPwm();
  
  TIM3_Cmd(ENABLE);
  configureCommunicationProtocol(0, PWM_CHANNEL_COUNT, FW_VERSION, BOARD_NO); // 1x PWM
  
  while(1)
  {   
    
    if(newPacketReceived)
    {
      newPacketReceived = FALSE;
      
      __disable_interrupt();
      getNewDataFromProtocol(&pwmValue, &settings); // Process received frame ..
      __enable_interrupt();
      
      if(settings.switchedOn)
      {
        setPwm(pwmValue, periodCnt);
      }
      else
      {
        setPwm(0, 0);
      }
      
      periodCnt++;
      if(periodCnt > FLASHING_PERIOD)
      {
        periodCnt = 0; 
      }
      
      watchdogKick();
    }
    
      
  }
}