/**
 * Briskel kids - main file
 * 
 * Bref: ultra sound distance measurement
 * Pins: PB2 (TIM2_CH2) - Trigger  - generate 10 us pulses
 *       PB1 (TIM3_CH1) - echo - read pulse width
 *       
 *       Distance = echo width / (340*2) (m)
 *        1cm => width 58.8us
 *        1m  => width 5.88 ms
 * Author: Martin Patak
 */

#include <string.h>
#include <intrinsics.h>
#include <stdlib.h>
#include "CommHeader.h"
#include "boot.h"
#include "watchdog.h"
#include "uart.h"

#define FW_VERSION 0x20 // 1.0
#define BOARD_NO 17

#define TRIGGER_GPIO_PORT  GPIOB
#define TRIGGER_GPIO_PINS  GPIO_Pin_2
#define ECHO_GPIO_PORT  GPIOB
#define ECHO_GPIO_PINS  GPIO_Pin_1

enum State {IDLE, WAIT_PULSE_START, WAIT_PULSE_END} state;

static bool newPacketReceived = FALSE;

u16 measuredPulseWidth = 0;

/**
 * Configure HSI clock - HSI - input 16 MHz
 * divide by x - CPU runs at 16/x MHz
 */
static void ConfigureHSIClock()
{
  CLK_SYSCLKDivConfig(CLK_SYSCLKDiv_8);
}

/**
 * Function called when communication is lost
 */
void communicationLostCallback()
{
}

/**
 * Callback for a received package
 * @param package - pointer to received package
 */
void receivedPackageCallback(COMM_HEADER * package)
{
  newPacketReceived = TRUE;
  uartProcessNewPacket(package);
  UART_sendPacketDMA((package));
}


/**
 * Initialise gpio - enable pyrosensor 
 */
void initGpios()
{
  ITC_DeInit();
  // IRQ priority settings
  ITC_SetSoftwarePriority(TIM2_UPD_OVF_TRG_BRK_USART2_TX_IRQn, ITC_PriorityLevel_2); // Use higher priority than usual ..
  
  
  // Configure timer TIM2_CH2 for 10us pulse generation
  // Timer is running at 2MHz, clock divider 2 - generate 10us => counter couts from 0 to (20/2)-1 = 9..
  // Timer overflows in 2^16/(2/2)Mhz = 65 ms ...
#define TRG_PULSE_LENGTH 10
  CLK_PeripheralClockConfig(CLK_Peripheral_TIM2, ENABLE);
  GPIO_Init(TRIGGER_GPIO_PORT, TRIGGER_GPIO_PINS, GPIO_Mode_Out_PP_Low_Slow);
/*  TIM2_TimeBaseInit(TIM2_Prescaler_16, TIM2_CounterMode_Up, 0xFFFF);
  TIM2_OC2Init(TIM2_OCMode_PWM1, TIM2_OutputState_Enable, 0, TIM2_OCPolarity_High, TIM2_OCIdleState_Set);
  TIM2_SetCompare2(TRG_PULSE_LENGTH-1);
  TIM2_OC2PreloadConfig(ENABLE);
  TIM2_ClearFlag(TIM2_FLAG_Update);
  TIM2_ITConfig(TIM2_IT_Update, ENABLE);
  TIM2_CtrlPWMOutputs(ENABLE);
  TIM2_Cmd(ENABLE);*/
  TIM2_TimeBaseInit(TIM2_Prescaler_16, TIM2_CounterMode_Up, 0x1000);
  TIM2_OC2Init(TIM2_OCMode_PWM1, TIM2_OutputState_Enable, 0, TIM2_OCPolarity_High, TIM2_OCIdleState_Set);
  TIM2_OC2PreloadConfig(ENABLE);
  TIM2_CtrlPWMOutputs(ENABLE);
  
  
  // Define IRQ for echo pin
  GPIO_Init(ECHO_GPIO_PORT, ECHO_GPIO_PINS, GPIO_Mode_In_FL_IT);
  EXTI_SetPinSensitivity(EXTI_Pin_1, EXTI_Trigger_Rising_Falling);
  ITC_SetSoftwarePriority(EXTI1_IRQn, ITC_PriorityLevel_2); // Use higher priority than usual ..
}


/**
 * Interrupt  Tim2 irch overflow
 */
#pragma vector = TIM2_OVR_UIF_vector
__interrupt __root void Tim2Irq(void)
{
  u8 echoPin = GPIO_ReadInputDataBit(ECHO_GPIO_PORT, ECHO_GPIO_PINS);
   
  if(echoPin == 0 && state == IDLE)
  {
    state = WAIT_PULSE_START;
  }
  else
  {
    state = IDLE;
  }
  TIM2_ClearFlag(TIM2_FLAG_Update);
}


/**
 * Interrupt Echo pin changes...
 */
#pragma vector = EXTI1_vector
__interrupt __root void EchoPinIrq(void)
{ 
  EXTI_ClearITPendingBit(EXTI_IT_Pin1);
  u16 counter = TIM2_GetCounter();
 // u8 echoPin = GPIO_ReadInputDataBit(ECHO_GPIO_PORT, ECHO_GPIO_PINS);
  static u16 startTimerVal;
  static u16 endTimerVal;
  
  switch(state)
  {
  case IDLE:
    // Nothing to do in this state
    break;
    
  case WAIT_PULSE_START:
    startTimerVal = counter;
    state = WAIT_PULSE_END;
    break;
    
  case WAIT_PULSE_END:
    endTimerVal = counter;
    measuredPulseWidth = endTimerVal - startTimerVal;
    state = IDLE;
    break;
  }
}

/**
 * Convert pulse width to reasonable values .. 
 */
i16 convertPulseWidth(u16 val)
{
}

 u8 echoPinX;

/**
 * Main loop
 */
int main( void )
{ 
  i16 value;
  u16 pulseWidth = 0;
  ConfigureHSIClock();
  if(isAppValidated())
  { // Watchdog only for validated application - not for development, ...  
    watchdogInit(); 
  }
  
  UART_init();
  initGpios(); // Initialise before IRQ is enabled ..
  __enable_interrupt();
  
  configureCommunicationProtocol(1, 0, FW_VERSION, BOARD_NO); 
  
 
  
  while(1)
  {   
    echoPinX = GPIO_ReadInputDataBit(ECHO_GPIO_PORT, ECHO_GPIO_PINS);
    if(newPacketReceived)
    {
      newPacketReceived = FALSE;
      
      __disable_interrupt();
      pulseWidth = measuredPulseWidth;
      __enable_interrupt();
      value = convertPulseWidth(pulseWidth);
      
      __disable_interrupt();
      addNewDataToProtocol(&value); // Process received frame ..
      __enable_interrupt();
      
      watchdogKick();
    }
  }
}
