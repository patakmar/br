/**
 * Briskel kids - main file
 * 
 * Bref: basic motor control - 
 * Motor 1/1 : PB0 - TIM2_CH1
 * Motor 1/2 : PB2 - TIM2_CH2
 * Motor 1 - nSleep : PB3
 *
 * Motor 2/1 : PB1 - TIM3_CH1
 * Motor 2/2 : PD0 - TIM3_CH2
 * Motor 2 - nSleep : PB4
 *
 * Author: Martin Patak
 */

#include <string.h>
#include <intrinsics.h>
#include <stdlib.h>
#include "CommHeader.h"
#include "boot.h"
#include "watchdog.h"
#include "uart.h"

#define FW_VERSION 0x10 // 1.0
#define BOARD_NO 9

#define M1_PWM_PORT  GPIOD
#define M1_PWM_PIN  GPIO_Pin_0
#define M2_PWM_PORT  GPIOB
#define M2_PWM_PIN  GPIO_Pin_1

#define M1_POS_PORT  GPIOC
#define M1_POS_PIN  GPIO_Pin_4
#define M1_NEG_PORT  GPIOB
#define M1_NEG_PIN  GPIO_Pin_7
#define M2_POS_PORT  GPIOB
#define M2_POS_PIN  GPIO_Pin_6
#define M2_NEG_PORT  GPIOB
#define M2_NEG_PIN  GPIO_Pin_5

#define M1_SPEED_PORT  GPIOB
#define M1_SPEED_PIN  GPIO_Pin_2
#define M2_SPEED_PORT  GPIOB
#define M2_SPEED_PIN  GPIO_Pin_0


// PWM settings
#define CLOCK_FREQUENCY 16000000
#define TIM3_PRESCALLER_SETTINGS TIM3_Prescaler_1

// PWMs
#define PWM_CHANNEL_COUNT 2
#define MAXIMAL_TIMER_VALUE (256)

static bool newPacketReceived = FALSE;

void setPwms();

/**
 * Configure HSI clock - HSI - input 16 MHz
 * divide by x - CPU runs at 16/x MHz
 */
static void ConfigureHSIClock()
{
  CLK_SYSCLKDivConfig(CLK_SYSCLKDiv_4);
}

/**
 * Function called when communication is lost
 */
void communicationLostCallback()
{
  setPwms(0,0);
}

/**
 * Callback for a received package
 * @param package - pointer to received package
 */
void receivedPackageCallback(COMM_HEADER * package)
{
  newPacketReceived = TRUE;
  uartProcessNewPacket(package);
  UART_sendPacketDMA((package));
}

void initPwms(void)
{
  CLK_PeripheralClockConfig(CLK_Peripheral_TIM3, ENABLE);
  
  // Configure outputs..
  GPIO_Init(M1_PWM_PORT, M1_PWM_PIN, GPIO_Mode_Out_PP_Low_Slow);
  GPIO_Init(M2_PWM_PORT, M2_PWM_PIN, GPIO_Mode_Out_PP_Low_Slow);
  
  // set direction selection 
  GPIO_Init(M1_POS_PORT, M1_POS_PIN, GPIO_Mode_Out_PP_Low_Slow);
  GPIO_Init(M1_NEG_PORT, M1_NEG_PIN, GPIO_Mode_Out_PP_Low_Slow);
  GPIO_Init(M2_POS_PORT, M2_POS_PIN, GPIO_Mode_Out_PP_Low_Slow);
  GPIO_Init(M2_NEG_PORT, M2_NEG_PIN, GPIO_Mode_Out_PP_Low_Slow);
  
  TIM3_TimeBaseInit(TIM3_PRESCALLER_SETTINGS, TIM3_CounterMode_Up, MAXIMAL_TIMER_VALUE);
  
  TIM3_OC1Init(TIM3_OCMode_PWM1, TIM3_OutputState_Enable, 0, TIM3_OCPolarity_High, TIM3_OCIdleState_Set);
  TIM3_OC1PreloadConfig(ENABLE);
  
  TIM3_OC2Init(TIM3_OCMode_PWM1, TIM3_OutputState_Enable, 0, TIM3_OCPolarity_High, TIM3_OCIdleState_Set);
  TIM3_OC2PreloadConfig(ENABLE);
  
  TIM3_CtrlPWMOutputs(ENABLE);
  
  TIM3_Cmd(ENABLE);
}

// Convert value from u16 into 8u value which might be added to PWM ... 
// output in range 0 to 255
u16 convertValue(i16 value)
{
  const u8 table[256] = {0,0,0,0,0,0,0,0,0,0,16,17,19,20,22,23,25,27,28,30,31,33,34,36,37,39,41,42,44,45,47,48,50,51,53,54,56,57,59,60,62,63,65,67,68,70,71,73,74,76,77,79,80,81,83,84,86,87,89,90,92,93,95,96,98,99,100,102,103,105,106,108,109,110,112,113,115,116,117,119,120,122,123,124,126,127,128,130,131,132,134,135,136,138,139,140,142,143,144,146,147,148,149,151,152,153,154,156,157,158,159,161,162,163,164,165,167,168,169,170,171,172,174,175,176,177,178,179,180,181,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,208,209,210,211,212,213,214,215,215,216,217,218,219,220,220,221,222,223,223,224,225,226,226,227,228,228,229,230,231,231,232,232,233,234,234,235,236,236,237,237,238,238,239,240,240,241,241,242,242,243,243,244,244,244,245,245,246,246,247,247,247,248,248,248,249,249,249,250,250,250,251,251,251,252,252,252,252,252,253,253,253,253,253,254,254,254,254,254,254,254,255,255,255,255,255,255,255,255,255,255};
  // The value is now between 0 and 16384... we need to cut it to 255 max .. divide it by 64 .. shift by
  u8 bitShift = 7;
  u16 retValue;
  u8 index;
  
  index = value >> bitShift;
  retValue = table[index];
  
  return retValue;
}

typedef struct
{
  bool direction;
  u16 pwm;
} SPEED;

SPEED SpeedM1, SpeedM2;

/**
 * Set Pwm output
 */
void setPwms()
{  
  if(SpeedM1.direction)
  {
    GPIO_ResetBits(M1_NEG_PORT, M1_NEG_PIN);
    GPIO_SetBits(M1_POS_PORT, M1_POS_PIN);
  }
  else
  {
    GPIO_ResetBits(M1_POS_PORT, M1_POS_PIN);
    GPIO_SetBits(M1_NEG_PORT, M1_NEG_PIN);
  }
  TIM3_SetCompare2(convertValue(SpeedM1.pwm));
  
  if(SpeedM2.direction)
  {
    GPIO_ResetBits(M2_NEG_PORT, M2_NEG_PIN);
    GPIO_SetBits(M2_POS_PORT, M2_POS_PIN);
  }
  else
  {
    GPIO_ResetBits(M2_POS_PORT, M2_POS_PIN);
    GPIO_SetBits(M2_NEG_PORT, M2_NEG_PIN);
  }
  TIM3_SetCompare1(convertValue(SpeedM2.pwm));
}


/** 
 * Convert input value into time requirement in 15 us
 * 0x7FFF -> max speed 10ms/15us = 666
 * 0x0000 -> minimal speed       -> 0xFFFF
 */
void convertInputToTime(i16 m1, i16 m2)
{
  if(m1 < 0)
  {     
    SpeedM1.direction = FALSE;
    SpeedM1.pwm = -m1;
  }
  else
  {
    SpeedM1.direction = TRUE;
    SpeedM1.pwm = m1;
  }
                                            
  if(m2 < 0)
  {     
    SpeedM2.direction = FALSE;
    SpeedM2.pwm = -m2;
  }
  else
  {
    SpeedM2.direction = TRUE;
    SpeedM2.pwm = m2;
  }                                          
}



/**
 * Main loop
 */
int main( void )
{
  i16 pwmValues[PWM_CHANNEL_COUNT];
  SETTINGS settings;
  
  ConfigureHSIClock();
  if(isAppValidated())
  { // Watchdog only for validated application - not for development, ...  
    watchdogInit(); 
  }
  UART_init();
  __enable_interrupt();
  
  initPwms();

  configureCommunicationProtocol(0,PWM_CHANNEL_COUNT, FW_VERSION, BOARD_NO);
  
  while(1)
  {   
    if(newPacketReceived)
    {
      newPacketReceived = FALSE;
      
      __disable_interrupt();
      getNewDataFromProtocol(pwmValues, &settings); // Process received frame ..
      __enable_interrupt();
      
      convertInputToTime(pwmValues[0], pwmValues[1]);
      setPwms();
      
      watchdogKick();
    }
  }
}