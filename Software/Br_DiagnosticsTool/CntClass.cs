﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Controls;
using System.Windows.Threading;

namespace Br_DiagnosticsTool
{
    class CntClass
    {
        const int PACKET_LENGTH = 30;
        const int CHECKSUM_LENGTH = 2;
        const int INIT_CHAR = 0xA5;
        const int CHECKSUM_XOR = 0xB5CA;
        const int RX_BUFF_LENGTH = 8192;

        const byte BOOT_ACK = 0x79;
        const byte BOOT_SYNCH = 0x7F;


        const byte STANDARD_DATA_PACKET = 0x01;
        const byte FW_INFO_REQUEST = 0x10;    // Get info about current software
        const byte FW_INFO_RESPONSE = 0x11;    // Response to of current software
        const byte FW_JUMP_TO_BOOTLOADER = 0x20;    // Request to jump to a bootloader
        const byte FW_JUMP_TO_BOOTLOADER_ACK = 0x21;    // Response that device is going to boot (optional)
        const byte FW_VALIDATE_APPLICATION = 0x30;  // Request to validate application - application is OK
        const byte FW_VALIDATE_APPLICATION_ACK = 0x31;  // Response to validate application - application is OK

        SerialPort serialPort;
        byte[] rx_buff = new byte[RX_BUFF_LENGTH];
        int rx_buff_end = 0;

        // Constructor
        public CntClass()
        {
            serialPort = new SerialPort();
        }

        // Listen to communication
        public String connect(String comPort)
        {
            if (serialPort.IsOpen)
            {
                return "Device already opened \r";
            }
            else
            {
                serialPort.PortName = comPort;
                serialPort.BaudRate = 115200;
                serialPort.Parity = Parity.Even;
                serialPort.ReadTimeout = 500;
                serialPort.WriteTimeout = 500;
                serialPort.StopBits = StopBits.One;

                serialPort.Open();

                byte[] msgSynch = { BOOT_SYNCH };
                serialPort.Write(msgSynch, 0, 1);

                return "Device connected to " + comPort + "\r";
            }
        }

        // Disconnect from a port
        public String disconnect()
        {
            if (serialPort.IsOpen)
            {
                serialPort.Close();
                return "Device disconnected\r";
            }
            else
            {
                return "Device was not opened\r";
            }
        }

        // Read data from uart to buffer
        public void readToBuffer()
        {
            int count = serialPort.BytesToRead;
            serialPort.Read(rx_buff, rx_buff_end, count);
            rx_buff_end += count;
        }

        // Compute checksum of the array of 31 words ...
        public UInt16 computeChecksum(byte[] arr)
        {

            UInt16 checksum = 0;
            for (int i = 0; i < PACKET_LENGTH - CHECKSUM_LENGTH; i++)
            {
                checksum += (UInt16)arr[i];
            }

            checksum ^= CHECKSUM_XOR;

            return checksum;
        }

        // Function reads data from UART and return first decoded message
        public byte[] receiveMessage()
        {
            byte[] foundPacket = new byte[0];
            readToBuffer();

            while (rx_buff_end >= PACKET_LENGTH)
            {
                if (rx_buff[0] == INIT_CHAR)
                {
                    UInt16 computedChecksum = computeChecksum(rx_buff);
                    UInt16 receivedChecksum = ((UInt16)rx_buff[PACKET_LENGTH - 2]);
                    receivedChecksum = (UInt16)(receivedChecksum << 8);
                    receivedChecksum += ((UInt16)rx_buff[PACKET_LENGTH - 1]);
                    if (computedChecksum == receivedChecksum)
                    {
                        //Console.WriteLine("Received chekcsum OK\r");
                        foundPacket = new byte[PACKET_LENGTH];
                        Array.Copy(rx_buff, 0, foundPacket, 0, PACKET_LENGTH);

                        Array.Copy(rx_buff, PACKET_LENGTH, rx_buff, 0, PACKET_LENGTH);
                        rx_buff_end -= PACKET_LENGTH;
                    }
                    else
                    {
                        // Delete one character - no init char
                        rx_buff = rx_buff.Skip(1).Concat(new byte[] { 0 }).ToArray();
                        rx_buff_end -= 1;
                    }

                }
                else
                { // Delete one character - no init char
                    rx_buff = rx_buff.Skip(1).Concat(new byte[] { 0 }).ToArray();
                    rx_buff_end -= 1;
                }
            }


            return foundPacket;
        }

        public void createAndSendMessage(byte packetType)
        {
            byte[] packet = new byte[PACKET_LENGTH];
            packet[0] = INIT_CHAR;
            packet[1] = packetType;

            UInt16 checksum = (UInt16)computeChecksum(packet);
            packet[PACKET_LENGTH - 2] = (byte)((checksum >> 8) & 0xFF);
            packet[PACKET_LENGTH - 1] = (byte)((checksum >> 0) & 0xFF);


            serialPort.Write(packet, 0, packet.Length);
        }

        public string getFwVersion()
        {
            String result = "Nothing found\r";
            if (serialPort.IsOpen == false)
            {
                return "Device is not connected\r";
            }
            serialPort.Parity = Parity.None;
            flushBuffer();

            for (int i = 0; i < 20; i++)
            {
                createAndSendMessage(FW_INFO_REQUEST);
                Thread.Sleep(10);
            }

            byte[] msg = receiveMessage();
            while (msg.Length > 0)
            {
                if (msg[1] == FW_INFO_RESPONSE)
                {
                    UInt16 board;
                    UInt16 version;
                    board = (UInt16)((((UInt16)msg[4]) << 8) + msg[5]);
                    version = (UInt16)((((UInt16)msg[6]) << 8) + msg[7]);

                    return "Board no:" + board + " Fw version: " + version.ToString("x") + "\r";
                }
            }

            return result;
        }

        public string jumpToBootloader()
        {
            String result = "Nothing found\r";
            if (serialPort.IsOpen == false)
            {
                return "Device is not connected\r";
            }
            serialPort.Parity = Parity.None;
            flushBuffer();

            Thread.Sleep(100);

            for (int i = 0; i < 20; i++)
            {
                createAndSendMessage(FW_INFO_REQUEST);
                Thread.Sleep(10);
            }

            byte[] msg = receiveMessage();
            while (msg.Length > 0)
            {
                if (msg[1] == FW_INFO_RESPONSE)
                {
                    createAndSendMessage(FW_JUMP_TO_BOOTLOADER);

                    Thread.Sleep(100);
                    serialPort.Parity = Parity.Even;
                    Thread.Sleep(100);
                    byte[] msgSynch = { BOOT_SYNCH };
                    serialPort.Write(msgSynch, 0, 1);
                    Thread.Sleep(100);
                    // Call this function - so that device remains in boot... and does not jump back to application
                    String bootInfo = getBootloaderInfo();
                    return "Device jumped to bootloader\rBootloaderInfo:\r" + bootInfo;
                }
            }

            return result;
        }

        public string validateApplication()
        {
            String result = "Validating error\r";
            int responseCnt = 0;
            if (serialPort.IsOpen == false)
            {
                return "Device is not connected\r";
            }
            serialPort.Parity = Parity.None;
            flushBuffer();
            Thread.Sleep(10);

            for (int i = 0; i < 100; i++)
            {
                createAndSendMessage(STANDARD_DATA_PACKET);
                Thread.Sleep(10);
            }

            byte[] msg = receiveMessage();
            while (msg.Length > 0)
            {
                if (msg[1] == STANDARD_DATA_PACKET)
                {
                    responseCnt++;
                }

                if (responseCnt > 50)
                {
                    // We received enough messages - the application seems to work .. let validate it.
                    createAndSendMessage(FW_VALIDATE_APPLICATION);
                    return "Application validated\r";
                }
            }

            return result;
        }

        protected void flushBuffer()
        {
            serialPort.ReadExisting();
        }

        public string getBootloaderInfo()
        {
            String result = "";
            if (serialPort.IsOpen == false)
            {
                return "Device is not connected\r";
            }
            serialPort.Parity = Parity.Even;

            flushBuffer();

            byte[] msgInfo = { 0x00, 0xFF };
            serialPort.Write(msgInfo, 0, 2);
            Thread.Sleep(30);
            while (serialPort.BytesToRead > 0)
            {
                byte c = (byte)serialPort.ReadByte();
                if (c == BOOT_ACK && serialPort.BytesToRead > 0)
                {
                    byte bytesToRead = (byte)serialPort.ReadByte();

                    result += "Bootloader version: 0x" + serialPort.ReadByte().ToString("x") + "\r";
                    for (int i = 0; i < bytesToRead && serialPort.BytesToRead > 0; i++)
                    {
                        result += "Available commands: 0x" + serialPort.ReadByte().ToString("x") + "\r";
                    }

                    // Read Ack
                    serialPort.ReadByte();
                }
                else
                {
                    result = "NACK received \r";
                }
            }

            return result;
        }

        public String bootloaderSynch()
        {
            if (serialPort.IsOpen == false)
            {
                return "Device is not connected\r";
            }
            serialPort.Parity = Parity.Even;
            flushBuffer();

            for (int i = 0; i < 10; i++)
            {
                byte[] msgSynch = { BOOT_SYNCH };
                serialPort.Write(msgSynch, 0, 1);
                Thread.Sleep(100);
            }

            if (serialPort.BytesToRead > 0 && (byte)serialPort.ReadByte() == BOOT_ACK)
            {   // It is OK..
                return "Bootloader synchronised \r";
            }
            else
            {
                return "Bootloader could not be synchronised\r";
            }

        }

        protected bool initCommunication(byte[] msgInfo)
        {
            serialPort.Write(msgInfo, 0, 2);
            if ((byte)serialPort.ReadByte() != BOOT_ACK)
            {
                const int RETRY_MAX = 5;
                int retryCnt = 0;
                while (retryCnt <= RETRY_MAX)
                {
                    Thread.Sleep(10);

                    byte[] msgSynch = { BOOT_SYNCH };
                    serialPort.Write(msgSynch, 0, 1);
                    Thread.Sleep(20);
                    flushBuffer();

                    Thread.Sleep(10);
                    serialPort.Write(msgInfo, 0, 2);
                    Thread.Sleep(50);
                    if (serialPort.BytesToRead > 0 && (byte)serialPort.ReadByte() == BOOT_ACK)
                    {   // It is OK..
                        break;
                    }
                    else if (retryCnt == RETRY_MAX)
                    {
                        return false;
                    }
                    retryCnt++;
                }
            }

            return true;
        }

        protected String memWrite(UInt32 startAddress, byte[] data)
        {
            byte[] msgInfo = { 0x31, 0xCE };
            UInt32 bytesToWrite = (uint)data.Length;
            if (initCommunication(msgInfo) == false)
            {
                return "Bootloader does not answer\r";
            }
            serialPort.Parity = Parity.Even;

            byte[] address = new byte[5];
            address[0] = (byte)((startAddress >> 24) & 0xFF);
            address[1] = (byte)((startAddress >> 16) & 0xFF);
            address[2] = (byte)((startAddress >> 8) & 0xFF);
            address[3] = (byte)((startAddress >> 0) & 0xFF);
            address[4] = (byte)(address[0] ^ address[1] ^ address[2] ^ address[3]);

            Thread.Sleep(10);
            serialPort.Write(address, 0, 5);

            if ((byte)serialPort.ReadByte() != BOOT_ACK)
            {
                return "Writing failed\r";
            }

            Thread.Sleep(1);

            byte[] cmd = new byte[1 + bytesToWrite + 1];
            cmd[0] = (byte)(bytesToWrite - 1);
            Array.Copy(data, 0, cmd, 1, bytesToWrite);
            byte checksum = 0;
            for (int j = 0; j < cmd.Length - 1; j++)
            {
                checksum = (byte)(checksum ^ cmd[j]);
            }
            cmd[cmd.Length - 1] = checksum;
            serialPort.Write(cmd, 0, cmd.Length);
            Thread.Sleep(20);

            byte answer = (byte)serialPort.ReadByte();
            if (answer != BOOT_ACK)
            {
                return "Writing failed\r";
            }
            else
            {
                return "OK\r";
            }
        }

        public String storeToMemory()
        {
            UInt32 startAddress = 0x008000;
            const UInt32 blockLength = 128; // Write 128 bytes max ..

            byte[] fwImage = { };

            if (serialPort.IsOpen == false)
            {
                return "Device is not connected\r";
            }
            serialPort.Parity = Parity.Even;

            Microsoft.Win32.OpenFileDialog openFileDialog1 = new Microsoft.Win32.OpenFileDialog();
            openFileDialog1.Filter = "Binary file|*.bin";
            openFileDialog1.Title = "Open a Binary File";
            openFileDialog1.ShowDialog();

            // If the file name is not an empty string open it for saving.
            if (openFileDialog1.FileName != "")
            {
                using (System.IO.FileStream stream = new System.IO.FileStream(openFileDialog1.FileName, System.IO.FileMode.Open))
                {
                    using (System.IO.BinaryReader reader = new System.IO.BinaryReader(stream))
                    {
                        int bytesAvailable = (int)stream.Length;
                        fwImage = reader.ReadBytes(bytesAvailable);
                        reader.Close();
                    }
                    stream.Close();
                }

            }
            else
            {
                return "Data reading canceled\r";
            }

            if (fwImage.Length == 0)
            {
                return "No bytes available..\r";
            }


            flushBuffer();

            try
            {
                // First - do not write the beginning - otherwise the bootloader stops working..
                for (UInt32 i = blockLength; i < fwImage.Length; i = i + blockLength)
                {
                    uint bytesToWrite = blockLength;
                    if (i + blockLength > fwImage.Length)
                    { // Not that many data .. cut it 
                        bytesToWrite = (uint)(fwImage.Length - i);
                    }
                    byte[] tmp = new byte[blockLength]; // In case it is not full - fill the end with zeros ..
                    Array.Copy(fwImage, i, tmp, 0, bytesToWrite);
                    if (memWrite(startAddress + i, tmp) != "OK\r")
                    {
                        return "Writing error\r";
                    }
                }

                // At the end - write the first page ...
                byte[] tmp2 = new byte[blockLength];
                Array.Copy(fwImage, 0, tmp2, 0, blockLength);
                if (memWrite(startAddress, tmp2) != "OK\r")
                {
                    return "Writing error\r";
                }
            }
            catch
            {
                return "Error during transmission\r";
            }

            return "Data written, byte count: " + fwImage.Length + "\r";
        }


        public String eraseMemory()
        {
            if (serialPort.IsOpen == false)
            {
                return "Device is not connected\r";
            }
            serialPort.Parity = Parity.Even;

            flushBuffer();

            try
            {
                byte[] msgInfo = { 0x43, 0xBC };
                if (initCommunication(msgInfo) == false)
                {
                    return "Bootloader does not answer\r";
                }

                // Complete erase
                byte[] cmd = new byte[10];
                cmd[0] = (byte)0x8 - 1; // 8 sectors
                byte checksum = 0;
                checksum = (byte)(checksum ^ cmd[0]);
                for (UInt32 i = 0; i < 8; i++)
                {
                    cmd[i + 1] = (byte)i;
                    checksum = (byte)(checksum ^ cmd[i + 1]);
                }
                cmd[9] = checksum;

                serialPort.Write(cmd, 0, 10);
                Thread.Sleep(3000);
                byte received = (byte)serialPort.ReadByte();
                if (received != BOOT_ACK)
                {
                    return "Erasing failed\r";
                }
                else
                {
                    return "Memory erased\r";
                }
            }
            catch
            {
                return "Error during transmission\r";
            }

        }

        public String jumpToApplication()
        {
            UInt32 appStartAddress = 0x008000;

            if (serialPort.IsOpen == false)
            {
                return "Device is not connected\r";
            }
            serialPort.Parity = Parity.Even;

            flushBuffer();

            try
            {
                byte[] msgInfo = { 0x21, 0xDE };
                if (initCommunication(msgInfo) == false)
                {
                    return "Bootloader does not answer\r";
                }

                byte[] address = new byte[5];
                address[0] = (byte)((appStartAddress >> 24) & 0xFF);
                address[1] = (byte)((appStartAddress >> 16) & 0xFF);
                address[2] = (byte)((appStartAddress >> 8) & 0xFF);
                address[3] = (byte)((appStartAddress >> 0) & 0xFF);
                address[4] = (byte)(address[0] ^ address[1] ^ address[2] ^ address[3]);

                Thread.Sleep(10);
                serialPort.Write(address, 0, 5);

                if ((byte)serialPort.ReadByte() != BOOT_ACK)
                {
                    return "Jumping to applciation failed\r";
                }
                else
                {
                    // Now the goal is to verify whether the application works OK... and if yes - try to validate the application 
                    // So that it can be normaly used
                    return validateApplication();
                }
            }
            catch
            {
                return "Error during transmission\r";
            }
        }

        public String readMemory()
        {
            if (serialPort.IsOpen == false)
            {
                return "Device is not connected\r";
            }
            serialPort.Parity = Parity.Even;

            flushBuffer();

            UInt32 startAddress = 0x008000;
            const UInt32 length = 8 * 1024;
            const UInt32 blockLength = 256; // range 1 to 256 bytes
            byte[] readData = new byte[length];

            try
            {
                for (UInt32 i = 0; i < length; i = i + blockLength)
                {
                    byte[] msgInfo = { 0x11, 0xEE };
                    if (initCommunication(msgInfo) == false)
                    {
                        return "Bootloader does not answer\r";
                    }

                    byte[] address = new byte[5];
                    address[0] = (byte)((startAddress >> 24) & 0xFF);
                    address[1] = (byte)((startAddress >> 16) & 0xFF);
                    address[2] = (byte)((startAddress >> 8) & 0xFF);
                    address[3] = (byte)((startAddress >> 0) & 0xFF);
                    address[4] = (byte)(address[0] ^ address[1] ^ address[2] ^ address[3]);

                    Thread.Sleep(10);
                    serialPort.Write(address, 0, 5);

                    if ((byte)serialPort.ReadByte() != BOOT_ACK)
                    {
                        return "Reading failed\r";
                    }

                    Thread.Sleep(10);

                    byte[] readLength = new byte[2];
                    readLength[0] = (byte)(blockLength - 1);
                    readLength[1] = (byte)(~readLength[0]);
                    serialPort.Write(readLength, 0, 2);
                    if ((byte)serialPort.ReadByte() != BOOT_ACK)
                    {
                        return "Reading failed\r";
                    }

                    Thread.Sleep(50);
                    serialPort.Read(readData, (int)i, (int)blockLength);
                    startAddress += blockLength;
                    Thread.Sleep(10);
                }
            }
            catch
            {
                return "Error during transmission\r";
            }

            Microsoft.Win32.SaveFileDialog saveFileDialog1 = new Microsoft.Win32.SaveFileDialog();
            saveFileDialog1.Filter = "Binary file|*.bin";
            saveFileDialog1.Title = "Save a Binary File";
            saveFileDialog1.ShowDialog();

            // If the file name is not an empty string open it for saving.
            if (saveFileDialog1.FileName != "")
            {
                using (System.IO.FileStream stream = new System.IO.FileStream(saveFileDialog1.FileName, System.IO.FileMode.Create))
                {
                    using (System.IO.BinaryWriter writer = new System.IO.BinaryWriter(stream))
                    {
                        writer.Write(readData);
                        writer.Close();
                    }
                    stream.Close();
                }
                return "Data stored to " + saveFileDialog1.FileName + "\r";
            }
            else
            {
                return "Data storing canceled\r";
            }
        }


        const UInt32 HWlength = 10;
        const UInt32 EEPROM_ADDR_INFO_START = 0x000010FF - HWlength + 1;

        public String getHwInfo()
        {
            if (serialPort.IsOpen == false)
            {
                return "Device is not connected\r";
            }
            serialPort.Parity = Parity.Even;

            flushBuffer();

            try
            {
                byte[] msgInfo = { 0x11, 0xEE };
                if (initCommunication(msgInfo) == false)
                {
                    return "Bootloader does not answer\r";
                }

                Thread.Sleep(300);

                // send start address
                byte[] addr = new byte[5];


                addr[0] = (byte)(EEPROM_ADDR_INFO_START >> 24);
                addr[1] = (byte)(EEPROM_ADDR_INFO_START >> 16);
                addr[2] = (byte)(EEPROM_ADDR_INFO_START >> 8);
                addr[3] = (byte)((EEPROM_ADDR_INFO_START >> 0) & 0xFF);
                addr[4] = (byte)(addr[0] ^ addr[1] ^ addr[2] ^ addr[3]);

                serialPort.Write(addr, 0, 5);
                Thread.Sleep(300);
                if ((byte)serialPort.ReadByte() != BOOT_ACK)
                {
                    return "Ack failed\r";
                }

                // send number of bytes
                byte[] noByes = new byte[5];
                noByes[0] = (byte)HWlength;
                noByes[1] = (byte)(~noByes[0]);

                serialPort.Write(noByes, 0, 2);
                Thread.Sleep(300);
                if ((byte)serialPort.ReadByte() != BOOT_ACK)
                {
                    return "Ack failed\r";
                }

                byte[] answer = new byte[HWlength];
                int bytesRead = serialPort.Read(answer, 0, (int)HWlength);


                UInt64 MAC = 0;
                MAC |= ((UInt64)answer[0]) << 40;
                MAC |= ((UInt64)answer[1]) << 32;
                MAC |= ((UInt64)answer[2]) << 24;
                MAC |= ((UInt64)answer[3]) << 16;
                MAC |= ((UInt64)answer[4]) << 8;
                MAC |= ((UInt64)answer[5]) << 0;
                UInt16 boardNo = (UInt16)(answer[7] + (((UInt16)answer[6]) << 8));
                UInt16 revision = answer[8];
                UInt16 config = answer[9];

                if (boardNo == 0x0 || boardNo == 0xFFFF)
                {
                    return "Not configured\r";
                }
                else
                {
                    return "BoardNo: " + boardNo + " rev: " + revision + " config: " + config + " Mac: " + MAC.ToString("X") + "\r";
                }
            }
            catch
            {
                return "Error during transmission\r";
            }

        }

        public String storeHwInfoToMem(UInt64 macAddr, UInt16 boardNo, UInt16 hwRevision, UInt16 hwConfiguration)
        {
            UInt32 startAddress = EEPROM_ADDR_INFO_START;
            String text = boardNo + "," + hwRevision + "," + hwConfiguration + "," + macAddr.ToString("X") + "," + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            String HwInfo = getHwInfo();

            if (serialPort.IsOpen == false)
            {
                return "Device is not connected\r";
            }
            serialPort.Parity = Parity.Even;


            if (String.Equals(HwInfo, "Not configured\r") == false)
            {
                return "Device already programmed with ID\r" + "Failed: " + text + "\r";
            }

            byte[] fwImage = { };

            flushBuffer();

            byte[] tmp2 = new byte[HWlength];

            try
            {
                // At the end - write the first page ...

                tmp2[0] = (byte)((macAddr >> 40) & 0xFF);
                tmp2[1] = (byte)((macAddr >> 32) & 0xFF);
                tmp2[2] = (byte)((macAddr >> 24) & 0xFF);
                tmp2[3] = (byte)((macAddr >> 16) & 0xFF);
                tmp2[4] = (byte)((macAddr >> 8) & 0xFF);
                tmp2[5] = (byte)((macAddr >> 0) & 0xFF);
                tmp2[6] = (byte)((boardNo >> 8) & 0xFF);
                tmp2[7] = (byte)((boardNo >> 0) & 0xFF);
                tmp2[8] = (byte)((hwRevision >> 0) & 0xFF);
                tmp2[9] = (byte)((hwConfiguration >> 0) & 0xFF);

                if (memWrite(startAddress, tmp2) != "OK\r")
                {
                    return "EEPROM Writing error\r";
                }
            }
            catch
            {
                return "Error during transmission\r";
            }


            string curFile = @"C:\Br\Electronics\Kicad\Boards\BoardList.csv";
            File.AppendAllText(curFile, text + Environment.NewLine);

            return "EEPROM config written\r";
        }

        public String receiveIdentification()
        {
            String result = "Nothing found\r";
            if (serialPort.IsOpen == false)
            {
                return "Device is not connected\r";
            }
            serialPort.Parity = Parity.None;
            flushBuffer();

            Thread.Sleep(10);

            for (int i = 0; i < 50; i++)
            {
                if (serialPort.BytesToRead > 50 * 50)
                {
                    break;
                }
                else
                {
                    Thread.Sleep(100);
                }
            }

            if (serialPort.BytesToRead < 30)
            {
                return "not too much bytes received\r";
            }

            String foundDevices = "Found devices: \r";

            // Go through all messages
            while(serialPort.BytesToRead > 30)
            {
                int initChar = serialPort.ReadByte();

                if(initChar != 0xA5)
                {
                    continue;
                }

                byte []packet = new byte[29];
                String tmp = "";
                serialPort.Read(packet, 0, packet.Length);
                if(packet[0] == 0x12)
                { // Identification sequence
                    
                    UInt16 boardNo = (UInt16)((((UInt16)packet[9]) << 8) + (UInt16)packet[10]);
                    tmp += " BoNo: " + boardNo;
                    tmp += " Uidi:";
                    for(UInt32 i = 0; i < 6; i++)
                    {
                        tmp = tmp + " 0x" + packet[i + 3].ToString("X2");
                    }

                    tmp += " Rev: " + packet[11];
                    tmp += " Config: " + packet[12];
                    tmp += " FW: " + (packet[13] >> 4) + "." + (packet[13] & 0xF);
                }
                else
                {
                    // no idintification string - skip the rest ...
                    break;
                }

                foundDevices += tmp + "\r";

            }

            return foundDevices;
        }
    }
}
