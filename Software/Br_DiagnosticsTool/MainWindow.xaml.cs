﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO.Ports;
using System.Threading;

namespace Br_DiagnosticsTool
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        CntClass cntClass;

        public MainWindow()
        {
            InitializeComponent();
            RefreshUartList();
            cntClass = new CntClass();
            richTextBox_status.Document.Blocks.Clear();
            richTextBox_status.AppendText("Status:\r");

        }

        private void RefreshUartList()
        {
            String[] ports = SerialPort.GetPortNames();
            comboBox_UartSelect.Items.Clear();
            for (UInt32 i = 0; i < ports.Length; i++)
            {
                comboBox_UartSelect.Items.Add(ports[i]);
            }
        }

        private void comboBox_UartSelect_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            
        }

        private void ButtonConnect_Click(object sender, RoutedEventArgs e)
        {
            String info = cntClass.connect(comboBox_UartSelect.Text);
            richTextBox_status.AppendText(info);
        }

        private void Disconnect_Click(object sender, RoutedEventArgs e)
        {
            String info = cntClass.disconnect();
            richTextBox_status.AppendText(info);
        }

        private void ButtonGetFwInfo_Click(object sender, RoutedEventArgs e)
        {
            String info = cntClass.getFwVersion();
            richTextBox_status.AppendText(info);
        }

        private void ButtonGetBootloaderInfo_Click(object sender, RoutedEventArgs e)
        {
            String info = cntClass.getBootloaderInfo();
            richTextBox_status.AppendText(info);
        }

        private void richTextBox_status_TextChanged(object sender, TextChangedEventArgs e)
        {
            richTextBox_status.ScrollToEnd();
        }

        private void ButtonReadMemory_Click(object sender, RoutedEventArgs e)
        {
            String info = cntClass.readMemory();
            richTextBox_status.AppendText(info);
        }

        private void ButtonStoreToMemory_Click(object sender, RoutedEventArgs e)
        {
            String info = cntClass.storeToMemory();
            richTextBox_status.AppendText(info);
        }

        private void ButtonEraseMemory_Click(object sender, RoutedEventArgs e)
        {
            String info = cntClass.eraseMemory();
            richTextBox_status.AppendText(info);
        }

        private void ButtonJumpToApplication_Click(object sender, RoutedEventArgs e)
        {
            String info = cntClass.jumpToApplication();
            richTextBox_status.AppendText(info);
        }

        private void ButtonJumpToBootloader_Click(object sender, RoutedEventArgs e)
        {
            String info = cntClass.jumpToBootloader();
            richTextBox_status.AppendText(info);
        }

        private void ButtonValidateApplication_Click(object sender, RoutedEventArgs e)
        {
            String info = cntClass.validateApplication();
            richTextBox_status.AppendText(info);
        }

        private void comboBox_UartSelect_KeyDown(object sender, KeyEventArgs e)
        {
            RefreshUartList();
        }

        private void ButtomBootloaderSynchro_Click(object sender, RoutedEventArgs e)
        {
            String info = cntClass.bootloaderSynch();
            richTextBox_status.AppendText(info);
        }

        private void ButtomBootloaderSynchro_MouseDown(object sender, MouseButtonEventArgs e)
        {
           
        }

        private void comboBox_UartSelect_MouseDown(object sender, MouseButtonEventArgs e)
        {
           
        }

        private void comboBox_UartSelect_IsMouseDirectlyOverChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            cntClass.validateApplication();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            String info = cntClass.getHwInfo();
            richTextBox_status.AppendText(info);
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            var rand = new Random();
            UInt64 rnd1 = (UInt64)rand.Next(0, Int32.MaxValue);
            UInt64 rnd2 = (UInt64)rand.Next(0, Int32.MaxValue); // As we have max 48 bytes .. so 16 is rmaining
            UInt64 mac = (rnd1 + (rnd2 << 31)) & 0xFFFFFFFFFFFF;
            UInt16 boardNo = UInt16.Parse(BoardNo.Text);
            UInt16 boardRev = UInt16.Parse(BoardRevision.Text);
            UInt16 boardConf = UInt16.Parse(BoardConfig.Text);

            String info = cntClass.storeHwInfoToMem(mac, boardNo, boardRev, boardConf);
            richTextBox_status.AppendText(info);
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            String info = cntClass.receiveIdentification();
            richTextBox_status.AppendText(info);
            
        }
    }
}
